//
//  ViewController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 11/24/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

import UIKit
import DuomResourceBrowser
import DuomBase
import Kingfisher

extension OutputModel {
    public convenience init(dic: [String: Any]) {
        self.init()
        if let mType = dic["mediaType"] as? Int {
            if mType == 0 {
                mediaType = .image
            } else if mType == 1 {
                mediaType = .video
            } else if mType == 2 {
                mediaType = .livePhoto
            } else if mType == 3 {
                mediaType = .giphyGif
            }
        }
        
        if let videoPathStr = dic["videoPath"] as? String {
            videoPath = videoPathStr
        }
        
        if let imagePathStr = dic["imagePath"] as? String {
            imagePath = imagePathStr
        }
        
        if let imageCompressPathStr = dic["imageCompressPath"] as? String {
            imageCompressPath = imageCompressPathStr
        }
        
        if let wid = dic["width"] as? Int {
            width = wid
        }
        
        if let wid = dic["height"] as? Int {
            height = wid
        }
        
        if let isSnap = dic["isSnapShot"] as? Int {
            isSnapShot =  isSnap == 0 ? false : true
        }
        
        if let idx = dic["index"] as? Int {
            index = idx
        }
        
        
        if let idStr = dic["id"] as? String {
            id = idStr
        }
    }
}

// 相片间距
let aphotoSpace: CGFloat = 3.0

// 单个line上几个照片
let alineCount: Int = 3

// 单个照片宽高
let aphotoItemWH = (UIScreen.screenWidth - (aphotoSpace * 2)) / CGFloat(alineCount)

class ViewController: UIViewController {
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: aphotoItemWH, height: aphotoItemWH)
        layout.minimumLineSpacing = aphotoSpace
        layout.minimumInteritemSpacing = aphotoSpace
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = .black
        view.scrollsToTop = false
        view.dataSource = self
        view.delegate = self
        view.contentInsetAdjustmentBehavior = .never
        view.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.identifier)
        return view
    }()
    
    var datasources = [OutputModel]()
    
    let showImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }

    
    var datasInfo: [[String: Any]] = [[String: Any]]()
    
    let saveBtn: UIButton = UIButton().then {
        $0.setTitle("SAVE", for: .normal)
        $0.backgroundColor = .red
    }
    
    let clearBtn: UIButton = UIButton().then {
        $0.setTitle("Clear", for: .normal)
        $0.backgroundColor = .red
    }
    
    let installBtn: UIButton = UIButton().then {
        $0.setTitle("Install", for: .normal)
        $0.backgroundColor = .red
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.addSubview(showImageView)
        showImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: UIScreen.screenWidth, height: UIScreen.screenWidth))
        }
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: UIScreen.screenWidth, height: UIScreen.screenWidth))
        }
//        collectionView.isHidden = true
        
        view.addSubview(saveBtn)
        saveBtn.snp.makeConstraints { make in
            make.top.equalTo(collectionView.snp.bottom).offset(50)
            make.left.equalTo(30)
            make.size.equalTo(CGSize(width: 100, height: 100))
        }
        saveBtn.addTarget(self, action: #selector(saveAction(_:)), for: .touchUpInside)
        
        view.addSubview(clearBtn)
        clearBtn.snp.makeConstraints { make in
            make.top.equalTo(collectionView.snp.bottom).offset(50)
            make.leading.equalTo(saveBtn.snp.trailing).offset(50)
            make.size.equalTo(CGSize(width: 100, height: 100))
        }
        clearBtn.addTarget(self, action: #selector(clearAction(_:)), for: .touchUpInside)
        
        view.addSubview(installBtn)
        installBtn.snp.makeConstraints { make in
            make.top.equalTo(collectionView.snp.bottom).offset(50)
            make.leading.equalTo(clearBtn.snp.trailing).offset(50)
            make.size.equalTo(CGSize(width: 100, height: 100))
        }
        installBtn.addTarget(self, action: #selector(installAction(_:)), for: .touchUpInside)
        
        
        UserDefaults.standard.setValue(stickers, forKey: "111")
        UserDefaults.standard.synchronize()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func saveAction(_ sender: UIButton)  {
        ImagePickerInterface.saveToAlbum(selectedList: self.datasInfo) {
            print("save 完成了")
        }
    }
    
    @objc func clearAction(_ sender: UIButton)  {
        ImagePickerInterface.clearCache()
    }
    
    @objc func installAction(_ sender: UIButton)  {
        let twitterURL = URL(string: "twitter://")!
        let can1 = UIApplication.shared.canOpenURL(twitterURL)
        
        let weixinURL = URL(string: "weixin://")!
        let can2 = UIApplication.shared.canOpenURL(weixinURL)
        
        let insURL = URL(string: "instagram://")!
        let can3 = UIApplication.shared.canOpenURL(insURL)
        
        let fbURL = URL(string: "fb://")!
        let can4 = UIApplication.shared.canOpenURL(fbURL)
        
        let mesURL = URL(string: "fb-messenger://")!
        let can5 = UIApplication.shared.canOpenURL(mesURL)
        
        let smsURL = URL(string: "sms://")!
        let can6 = UIApplication.shared.canOpenURL(smsURL)
        
        Logger.log(message: "can: \(can1), \(can2)", level: .info)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let dic = ["imp_next": "abcd", "imp_max_images": "Select a maximum of %d images", "imp_album": "XXXXX",]
        let json = try? JSONSerialization.data(withJSONObject: dic)
        let jsonStr = String(data: json!, encoding: .utf8)
        UserDefaults.standard.set(jsonStr!, forKey: LanguageKey)

        ImagePickerInterface.openBrowserPicker(selectedList: nil, imgMaxNum: 15, videoMaxNum: 0, selectType: 0, imageCropRatio: 6, showGif: true, cancelHandler: nil) { datas in
            print("datas:  \(datas)")
            self.datasInfo = datas
            for item in datas {
                let tempModel: OutputModel = OutputModel(dic: item)
                self.datasources.append(tempModel)

//                var url: String?
//                if tempModel.imageCompressPath.isNotEmpty {
//                   url = PhotoTools.getAbsolutePath(tempModel.imageCompressPath)
//                } else if tempModel.imagePath.isNotEmpty {
//                    url = PhotoTools.getAbsolutePath(tempModel.imagePath)
//                }
//
//                if let img = addWaterMark(url!) {
//                    self.showImageView.image = img
//                }
            }
            self.collectionView.reloadData()

        }
        
//        let twitterUrl = URL(string: "twitter://")!
//        var components = URLComponents()
//        components.scheme = "twitter"
//        components.path = "intent/tweet"
//        components.queryItems = [
//          URLQueryItem(name: "text", value: "tweet")
//        ]
//
//        if let url = components.url {
//          if UIApplication.shared.canOpenURL(url) {
//            UIApplication.shared.open(URL(string: "https://twitter.com/intent/tweet?text=Hello%20world")!)
//          }
//        }
    }
}


extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.identifier, for: indexPath) as! ImageCell
        cell.configCell(self.datasources[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        ImagePickerInterface.openBrowserPickerToEdit(selectedList: self.datasInfo, imgMaxNum: 15, showGif: true, cancelHandler: nil) { datas in
            print("datas:  \(datas)")
            self.datasInfo = datas
            for item in datas {
                let tempModel: OutputModel = OutputModel(dic: item)
                self.datasources.append(tempModel)
            }
            self.collectionView.reloadData()
        }
    }
}


class ImageCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(ImageCell.self)
    
    private lazy var imageView: UIImageView = {
        let imgview = UIImageView()
        imgview.contentMode = .scaleAspectFit
        imgview.clipsToBounds = true
        return imgview
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addUIView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func addUIView()  {
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func configCell(_ model: OutputModel) {
        var url: URL?
        if model.mediaType == .giphyGif {
            url = URL(string: model.imagePath)
        } else {
            if model.imageCompressPath.isNotEmpty {
                url = URL(string: PhotoTools.getAbsolutePath(model.imageCompressPath))
            } else if model.imagePath.isNotEmpty {
                url = URL(string: PhotoTools.getAbsolutePath(model.imagePath))
            }
        }
        
        print("\(url)")
        if let url = url {
            if model.mediaType == .giphyGif {
                imageView.kf.setImage(with: url)
            } else {
                if let imgData = try? Data(contentsOf: url) {
                    imageView.image = UIImage(data: imgData)
                } else {
                    imageView.backgroundColor = .red
                }
            }
            
        } else {
            imageView.backgroundColor = .red
        }
        
    }

    
}


func addWaterMark(_ path: String) -> UIImage? {
//     let outURL = PhotoTools.addWaterMark(path, text: "ajksjdkasjkdjaksajksjdkasjkdjaksajksjdkasjkdjaksajksjdkasjkdjaksajksjdkasjkdjaksajksjdkasjkdjaksajksjdkasjkdjaksajksjdkasjkdjaksajksjdkasjkdjaks")
//    if let imgData = PhotoTools.readFileDataFrom(filePath: outURL) {
//        return UIImage(data: imgData)
//    }
    return nil
}




//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
////        let testVC = TestRebuildImage()
////        self.navigationController?.pushViewController(testVC, animated: true)
////        return
//        // 如果传入数据为空 进入的时候清空缓存
////        PhotosConfiguration.default().selectSingleImg = true
////        PhotosConfiguration.default().maxSelectCount = 1
//        let vc = ImagePickerMainController()
//        let nav = PhotoNavgationController.init(rootViewController: vc)
//        nav.modalPresentationStyle = .fullScreen
////        self.navigationController?.showDetailViewController(nav, sender: nil)
//        self.showDetailViewController(nav, sender: nil)
//
//        SourceInstance.shared.selectedSourceBlock = { models in
//            guard let models = models else {
//                print("none models")
//                return
//            }
//            let hud = DProgressHUD.init(style: .darkBlur)
//            // new
//            hud.show()
//            models.getURLs(compression: PhotoModel.Compression(imageCompressionQuality: 2, videoExportPreset: .highQuality)) { result, model, index in
//                var outModel = OutputModel()
//
//                switch result {
//                case .success(let response):
//                    outModel.id = model.ident!
//                    if model.mediaType == .livePhoto {
//                        if let lv = response.livePhoto {
//                            outModel.path = "\(lv.videoURL)"
//                        }
//                    } else {
//                        outModel.path = "\(response.url)"
//                    }
//                    if let compress = response.compressUrl {
//                        outModel.compressPath = "\(compress)"
//                    }
//                    if let editVideoModel = model.editVideoModel, let duration = editVideoModel.duration {
//                        outModel.duration = duration
//                    } else {
//                        outModel.duration = model.duration
//                    }
//                case .failure(let error):
//                    print("地址获取失败", error)
//                    outModel.id = model.ident!
//                }
//
//                switch model.mediaType {
//                case .image:
//                    outModel.mediaType = .image
//                case .video:
//                    outModel.mediaType = .video
//                case .livePhoto:
//                    outModel.mediaType = .livePhoto
//                case .giphyGif:
//                    outModel.mediaType = .giphyGif
//                default:
//                    outModel.mediaType = .image
//                }
//
//                if let encodeData = try? JSONEncoder().encode(outModel),
//                   let json = String(data: encodeData, encoding: .utf8) {
//                    print("\(json)")
//                }
//
//                self.datasources.append(outModel)
//            } completion: { urls in
//                print("...........allURL: \(urls)")
//                hud.hide()
//                self.collectionView.reloadData()
//            }
//
//        }
//
//        SourceInstance.shared.cancelBlock = {
//            SourceInstance.shared.selectModels.removeAll()
//        }
//    }





let stickers = "[{\"id\":\"2023022111363305400077\",\"celebId\":\"2022112603524872519003\",\"name\":\"sticker first frame2\",\"icon\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103028518729__w:640__h:640.jpg\",\"description\":\" sticker\",\"image\":[{\"original\":\"OlDCjtqo6wIraB9yq+v7nvRu4ZmUeiCo6cOu03DzayXXZfwzbDaSf0GMJEURA4Ou8jKGiPtwKoPqyKJFL/9eMZlSH3W7nneUULN1mxYBu3kZkQgF+ztCzgDRPkZx9b9n5/56leBqBR/N1OWJTxF9Awx8PZGihtLLfzrVdsXpnmg=\",\"firstFrame\":\"fCL4Ry6DXxOVeBgXvH5tVtxIimbMlbyEkQzAa+qh13AONK9IcdYT6PUHD2TS21i6+V2HgxjOYn1Kat7+ixeTS8kafPS4KjN5G2loMqb+VjNpzmuFfBBk7rSK2IqdoQbjCKYP0IlawtWNcPjg1SnlIcJEnA+mgPFoe9+KpE+cAv4=\"},{\"original\":\"kcltficUGSETKszI/xwDeaQ9ecvM4OFy/q0HCHwBZ/RqQHo0wQDpDNw8fLFMMDlq78m1CyszQcn8vU0OHAu9DHL1NMAQgpQ213eZdXVwOPulIfy656QTBH8/OZOoCmvKAet8XB/dvpeMjLRh1yoZW6x3uOLK9aP+/ywKlamNPTk=\",\"firstFrame\":\"owE/Mf0lgsn4n877CGk7Al3Msak5o9G8efWh1DEwnm8UyNMEOPCrq1b5LbzYsWaq79hDcFI3mBdnZlHTvYKIObXRyRBjoVtwmsv4uuMXZG80Gzx9Tm0r7f9iwlpyBul1Jg0xIrUqRlGYJfkmMLx62bvQ/uIM/Yri2EFKYo//8Ys=\"},{\"original\":\"i4fDvArYHy9ukoa7YzX2QZhXJ5ttGTvHF2vBvzS8k+pMfjxNvTEJnLFSmq5hqpVOjliOz0edj4SMZuGyyo+n5oQqSHIXgGoHuve2oTl1ETJjb7vBlHhkNPboc9m2o/6DsmpzHH8A0mA3QC9/SW557yQeVIyq4R1ddSH6Ps9i9R0=\",\"firstFrame\":\"lz8GCW6g9kOR/2qOdqacVpewXYWnpTmaXTmtNY7tvk9cXDHQMK4fF+8czpwXyVtV94gaSO9nSeMYyqs+Ece7AMlex3BYq84eJ9MrSXqDzRV9ZnzV9XaEw+UC/Byp1Cgm+mV1vrV/rh5OMof4/g8VzXyvLChIcgY6U9fN5ueV6EU=\"},{\"original\":\"glg6JPUQziE62kS8VihmK2TX7Lf4iLjuHWlfnErwoskGarb8FNEUJ+Ljndbz1PX8+ooXRiyP84BM15xYhXWmQERGnF1U/SgnYz+Y/7bFzChRoCy/WfglTFoedgKjnUzC+59rXx3rxiXYxYzPMfPFqD0AKix86TCN8/sRV8kAb4Q=\",\"firstFrame\":\"Voi1SXcOP06yAFwJ0jwLrRP7IG+R/Q+8RDhUoix4pe4G4WFowb1+wMGUdxMVNJCr4qrO6gCgc3Px4m9dE49MTWSp39W8H0s8But/NfDnBrSfJDtbTee6fwacMyHKOAHIp1SU9Vp/9e7GF06StD2u8ZrUxNZDbJD0t8qD34AZH4E=\"},{\"original\":\"Ucqla9OOQixZ6iu5Ju9XNX9ObL8LOxTSit2zmU0Sm5d3wRR9dAOjK2uUoWzPkdAddyH7HDOa4Oi3c2L5XzvAYThGJOseVW6VIn199GUEWZMCBN9KPkS8tVNN+2/Mba6UxyEPHxzUHSf5jwhK5upbu8h11i7Xqdnh1aSg0+QRW88=\",\"firstFrame\":\"nui3wyH7569W2+PsjBTNxX9wTfmdYedMwnV5bzFysj+cGnX4cvAaWyd7zDBKCpLias8mL075UrMHDWoqSGwPgll3JBSQlJqa8llJpoiwZu3jcs+drpDpRgbNKOuceyDf0q6pyaXLWl2Uk4TK+neqb9FeavDOcH+STw/yn/6O5r4=\"},{\"original\":\"KuDLJ5JBNMCO4naNOlwTlY9xVj8pbbkpgrmhzlck5+W4eJcZIEqEjeMVfnVZeRyAVgqiiCWlO0I9M+9KFFFl32IGdOS4qwCKzEvAOmzVHh9eRV1+nEpZ/TxIsjLVHaTSYwyZ3BkhFNdtmhvMAvPa5N5PfA0aNHFpEYqUCzde9uY=\",\"firstFrame\":\"NEUm+ly6Gh2yIrIcIkv9CKGLGMm5FdEVAst0R3w51c8bcaFdI3uHtEDirxgPNTawS5cwoV4TBSS3af0MLwY0Wlnlvg2D4AE8MP4ldX2Krd+Yn6RO+MeBjLSMbSn1tYu3Cd7WMeJ5QDXhLhrGLvz9sc0ffDwL6ACHCKSWLT0//P8=\"}],\"priceType\":\"gems\",\"currencyType\":\"USD\",\"price\":10,\"gateType\":\"all\",\"gateValue\":\"\",\"hasPermission\":true,\"purchased\":true,\"downloadStatus\":2,\"decryptImages\":[{\"original\":\"OlDCjtqo6wIraB9yq+v7nvRu4ZmUeiCo6cOu03DzayXXZfwzbDaSf0GMJEURA4Ou8jKGiPtwKoPqyKJFL/9eMZlSH3W7nneUULN1mxYBu3kZkQgF+ztCzgDRPkZx9b9n5/56leBqBR/N1OWJTxF9Awx8PZGihtLLfzrVdsXpnmg=\",\"firstFrame\":\"fCL4Ry6DXxOVeBgXvH5tVtxIimbMlbyEkQzAa+qh13AONK9IcdYT6PUHD2TS21i6+V2HgxjOYn1Kat7+ixeTS8kafPS4KjN5G2loMqb+VjNpzmuFfBBk7rSK2IqdoQbjCKYP0IlawtWNcPjg1SnlIcJEnA+mgPFoe9+KpE+cAv4=\",\"decryptOriginal\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103028518729__w:640__h:640.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103028518729__w:640__h:640.jpg\"},{\"original\":\"kcltficUGSETKszI/xwDeaQ9ecvM4OFy/q0HCHwBZ/RqQHo0wQDpDNw8fLFMMDlq78m1CyszQcn8vU0OHAu9DHL1NMAQgpQ213eZdXVwOPulIfy656QTBH8/OZOoCmvKAet8XB/dvpeMjLRh1yoZW6x3uOLK9aP+/ywKlamNPTk=\",\"firstFrame\":\"owE/Mf0lgsn4n877CGk7Al3Msak5o9G8efWh1DEwnm8UyNMEOPCrq1b5LbzYsWaq79hDcFI3mBdnZlHTvYKIObXRyRBjoVtwmsv4uuMXZG80Gzx9Tm0r7f9iwlpyBul1Jg0xIrUqRlGYJfkmMLx62bvQ/uIM/Yri2EFKYo//8Ys=\",\"decryptOriginal\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230214110836739922__w:1060__h:1325.jpeg\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230214110836739922__w:1060__h:1325.jpeg\"},{\"original\":\"i4fDvArYHy9ukoa7YzX2QZhXJ5ttGTvHF2vBvzS8k+pMfjxNvTEJnLFSmq5hqpVOjliOz0edj4SMZuGyyo+n5oQqSHIXgGoHuve2oTl1ETJjb7vBlHhkNPboc9m2o/6DsmpzHH8A0mA3QC9/SW557yQeVIyq4R1ddSH6Ps9i9R0=\",\"firstFrame\":\"lz8GCW6g9kOR/2qOdqacVpewXYWnpTmaXTmtNY7tvk9cXDHQMK4fF+8czpwXyVtV94gaSO9nSeMYyqs+Ece7AMlex3BYq84eJ9MrSXqDzRV9ZnzV9XaEw+UC/Byp1Cgm+mV1vrV/rh5OMof4/g8VzXyvLChIcgY6U9fN5ueV6EU=\",\"decryptOriginal\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217102959475144__w:300__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217102959475144__w:300__h:300.jpg\"},{\"original\":\"glg6JPUQziE62kS8VihmK2TX7Lf4iLjuHWlfnErwoskGarb8FNEUJ+Ljndbz1PX8+ooXRiyP84BM15xYhXWmQERGnF1U/SgnYz+Y/7bFzChRoCy/WfglTFoedgKjnUzC+59rXx3rxiXYxYzPMfPFqD0AKix86TCN8/sRV8kAb4Q=\",\"firstFrame\":\"Voi1SXcOP06yAFwJ0jwLrRP7IG+R/Q+8RDhUoix4pe4G4WFowb1+wMGUdxMVNJCr4qrO6gCgc3Px4m9dE49MTWSp39W8H0s8But/NfDnBrSfJDtbTee6fwacMyHKOAHIp1SU9Vp/9e7GF06StD2u8ZrUxNZDbJD0t8qD34AZH4E=\",\"decryptOriginal\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103006727392__w:366__h:217.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103006727392__w:366__h:217.jpg\"},{\"original\":\"Ucqla9OOQixZ6iu5Ju9XNX9ObL8LOxTSit2zmU0Sm5d3wRR9dAOjK2uUoWzPkdAddyH7HDOa4Oi3c2L5XzvAYThGJOseVW6VIn199GUEWZMCBN9KPkS8tVNN+2/Mba6UxyEPHxzUHSf5jwhK5upbu8h11i7Xqdnh1aSg0+QRW88=\",\"firstFrame\":\"nui3wyH7569W2+PsjBTNxX9wTfmdYedMwnV5bzFysj+cGnX4cvAaWyd7zDBKCpLias8mL075UrMHDWoqSGwPgll3JBSQlJqa8llJpoiwZu3jcs+drpDpRgbNKOuceyDf0q6pyaXLWl2Uk4TK+neqb9FeavDOcH+STw/yn/6O5r4=\",\"decryptOriginal\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103014669633__w:300__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103014669633__w:300__h:300.jpg\"},{\"original\":\"KuDLJ5JBNMCO4naNOlwTlY9xVj8pbbkpgrmhzlck5+W4eJcZIEqEjeMVfnVZeRyAVgqiiCWlO0I9M+9KFFFl32IGdOS4qwCKzEvAOmzVHh9eRV1+nEpZ/TxIsjLVHaTSYwyZ3BkhFNdtmhvMAvPa5N5PfA0aNHFpEYqUCzde9uY=\",\"firstFrame\":\"NEUm+ly6Gh2yIrIcIkv9CKGLGMm5FdEVAst0R3w51c8bcaFdI3uHtEDirxgPNTawS5cwoV4TBSS3af0MLwY0Wlnlvg2D4AE8MP4ldX2Krd+Yn6RO+MeBjLSMbSn1tYu3Cd7WMeJ5QDXhLhrGLvz9sc0ffDwL6ACHCKSWLT0//P8=\",\"decryptOriginal\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103022092917__w:160__h:170.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103022092917__w:160__h:170.jpg\"}]},{\"id\":\"2023022208502114200100\",\"celebId\":\"2022112603524872519031\",\"name\":\"sticker first frame\",\"icon\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103028518729__w:640__h:640.jpg\",\"description\":\" stickerjhhj\",\"image\":[{\"original\":\"WKMFjOtzISxdShvQANrw22iaNssaNRSORU5T20sNlrzdRfPWYe0+zEmRAGTRXtiF4EoFFCJCJABqWjAAkV23G8/RioJQ0UI9PoMtpsNu6R267PwezZbkjw6il1yimePXhm1jQ3gZQdhv8K5Vp0hB/w3dtWS0WPcFbF0Qhmlgajo=\",\"firstFrame\":\"OVWrYKZeYF59hD/ebAcowqnEBWjQ+aIXdMIQ1/whb/AKLqg6Q+xoej3ZAevMG3ZsMebXHwAY95q8yKxP1IpHetRQpdgGs/Ms1s82QPOXx8qIJFibXO6oK7suGjK0dE3ce3znDEzs3nZNRTWFmo1qxEDqJtvopY2RcwWLNTiq7tU=\"},{\"original\":\"oUAbv59/JEXDmI5StNF7Va1Od2SmSUgnpDbZy0K1+BNGoiBktSuTBGZfcea31KagmULhqFnx5kzWY0GPuW110DwllpsownKM0C+0Vm545E8z9lo+WiaDFSmdSJpZLSQL1gj+i3ps01vG/Pe3gUpqUqqqal+fZrDGa/0NMF7LIgs=\",\"firstFrame\":\"hpKSAU3ZYxpoq6b4gALLNEJymfm3waYwNwoLait7BHylmkSSxe1IDrFfuqkTwkb93lKGI6sZBp4o2b5ZXpwtrb67w8+acVlDx2h7yDilZ107j/wjXCANcoSCx+h17lL/xtjtWhv1nuP0QN6NUVMArWPsPe6NxzMoC3wlyRL21Qc=\"},{\"original\":\"ETGoP0owhTWcGyIOFLtsdOi5AnPw9+RWHFM8B3Yi008OFAqyFD8YjtNKFkFYNCEVQr2/47sfDlacQ9NRhy4woyd7FCpFJPeL7jV5EeBNUMTdiaSs+5lEZleqJDwWw16xkGci8DS7k5N+l1abDSmL8mqgXGjpZdRyRUbaPdNnb+Y=\",\"firstFrame\":\"e+7GlIMXI6LO2YQmnDqluLeWd/Is9q/zsxmrF7bX9hGAY3+0MZPi++vh2NDIFSji0MuabCqtol5HskZsqvZyFvqAtH2Vle/1NKImPd6Oiqekp1i6co5kco5Svj6E3VLDAwUwRGmD51M4FSaKw//r74tXEjogWeVRA48coze1pFM=\"},{\"original\":\"OuvYOHq1tMoPAVqT6kXk32ahDhkgyM+QX6SvzG5i9IGpMr5ebHTfN8e7yS1VI9pI9q9NBhi6LVZ78AWmtp4JwuQrvRoeJkPlLqbA1RHG2B4In4beV+mVmQtOBT8nbRfVT7yxnJGvmMiIZBUkxJB9zdy3k0TebWkLpeXD1FHnGIg=\",\"firstFrame\":\"N6B7F/GrVZxmiH3CJhvJuQlNCp7pU7TzJ7d5wgwZTYSGv6tvByo/dZxdIhROjDK9tQbXgDPM/Ka8ji9N9f8MkEN/C6g/LRf1LpSXx0cPufQ1e1jqXeJFF4H+v+2wMFJyUGHgmFbEeKy25SDBHVX/kkC4u4ECPYoyO1r6zTAwTaU=\"},{\"original\":\"TDk9z1kjMhN6lrCaHh1McZaoYZtvg7TlBPuK3lvr/Vn1yWfegkl3WsH+7TRG1o1pl1GWEG8JkE3KgUjmYMYbWZJKZmOSHvXdFQLB7iSqd713/It0kbwFWF0G5F4Ug5b8qXstSRSdDRfVOUL5+pnItpE9s9N5tCAfoLLknwuEG0s=\",\"firstFrame\":\"V5qjoxOkhuhR0ZkLAf/RtXSECu9N3bNfKJzA8NUdFZsQagoJIY6rIrhKkXSJb1UaeB2Y5ohO+A0qkLtso/midbS6CdHbhcdAGd9Nfcm2RHVs8qybjtt0hWuzqhWFcxPKuEetcSXQ1J6yQ3VaeVOCehnzJfGZOMKZ5Ex1PTbYcXE=\"},{\"original\":\"NaQ6GTXG9INjUAQzYk/OdaWEqRh57ojBpn0eeBzo5kysv+nszgOiA+ZbFjmNzU4fKbnFVTO9d4lcUg/ux9Amx2R+I0zyQm/y02HMjqZndZfoLRXuxLfLx9kawD3JygUEeqOJ5fywBNPSLGLdvKMQLNEXXm5H71ljKZul8vr6s8Q=\",\"firstFrame\":\"bV3JYG4g5OC9h+RhOXKvYP8GgZ9B+xj473TpXUeEfKvcpkNaz9kSaG8zDbZYvIzpBmNFVLzGCmQiHLoC0rkN4hNDhaA8ehpqIVrSDBkxd1E71rlOXep3a4w17cLXpINhj6dUVM2FxGG3nYNu4jHQLXZo1Sj4eY8zTgk4D7kkWV0=\"},{\"original\":\"LUVVOlC3eWYC61BSVKXbl9zetV/LYAMmTMVUIfHKNgfjgrghX0fWiPBRwDNF31S7x0LnNAC4UbZ9cHgqYld//RbVsPniSnHXCa7cjn/AehRVHimamhZCxaGtDv/vpYmhkSz6/D1ia1KTMVgwMzToLcMZLxrXWor7h/5lHF1W8TM=\",\"firstFrame\":\"QG4NgV8dnc6sfd736rzCflerwHBGCO7XvT+RfxYK/r+97gV5ZRjpex7OiMT3V3dd+zsNGrhhyEhz08KoG2s+bWaA9okb4wQRABPRtXz2F/NzuRnr6FiHqW0LGL6+Ebgh0C+aFpIXbBGT1DFzOsr/8UxywEp9ReIP+q4SCwHwvv8=\"},{\"original\":\"WyWV+Jkap43idOZB/pn83JDTdzhD3jwaKrsGfdTDf3KUaEg4aNUgCkSTY7iP3VgxDRQZk6ER2gqpz7fHDjeF5/AhIOch8FO4EdoUp6/QsoF8QTIhfz63krt6sy4u1GZaa6HQM0WTW682qh0x8JBjgnOdtV7hFg72NnFQBBA4JcE=\",\"firstFrame\":\"dyjc0rEQfEKJOr/PQPeUqWMQa5EeO65ML3metbT2ufZzM2BOQ8uZQWwl4YLh0bA8LEEUrldED3GFpqfnX00J35Ir3gn3mimgGgmjV2/34mUJDKQrmIcDiYzzvFeq59mZps6WGYDbJeGq94z2zAxN3qmB8wRlcDNsEGbyETEm68k=\"},{\"original\":\"MMNDwOoGOZXgb94YX0zuoA4uM3tECvfuI4mH8DF8TGwFWEXnD6/fY7ADQeJH7ISw0zmZn8weVZTQ1Q8RWudS1Nj54S8opdswDuvW/93Lj63jmiGyrLI0dAQCgJMKVswNznE6nqMP/Gozg2jh8a+640eS9LJHD+34QPPVFRNPrOk=\",\"firstFrame\":\"Q9M8RVGvu+QKwBpMsBcmdhoFB0tXXdPhkTwYwx6z4tW0ddnbRSWIZIer5wBBLN/kizBE9CBuY7tlEnnu1ZtOdrodMIqonLKm8Rc1MfFChihRijQ6hbWWDUy55wr8D8rwZ2jLD9+Lnw6qBSOQxKocDkLcS2Pv3gvq4CyGMXznaUk=\"},{\"original\":\"MXPcTHo8Hp+2ch+NY9D1PndbiPZZgu/cBySu/ktLAQcwEc0IGSk8FoLFdydtQMpdSMphLlACCdIjuJ7O29Ix4uazzB7jQp5C201SGB4JoLUqwUmH3wiAQud65u6TKW93cqhlyOZZ2Qp3FBhA+MUOzRFpdwG/bfK3glBXvXI62io=\",\"firstFrame\":\"kn7x+UX3BGNnuAEyCpItmKKeb4w79WYWbVDRtL3/9FJyU2RPoTycJ1yzXj/srrOrqt5or4PYdVxAQ45ZXKlicpuCXSZmhjxWN+HfsmZEKLcmbEGkKsqX6MREaafmPJcL5fTH6oytPaZGenCvgIBY6bLmPfWZlRS1CZN+b7wjW+8=\"},{\"original\":\"AjKwhbWpeAQkmgPLMSFKbDvvIuxn0Ol60HsYm1iv+TNNzT0UZ/H/5DKa0xAATMoAXPb5bwLDSuWBzhhlusNwsI+yM4zkiamxoR4tZ6bW9iFCJ488bsQ/v24Ay07KVHkrC6cBquiFQReitpG4X8egwZGxejaDgejrvIakjjkIONU=\",\"firstFrame\":\"raaf/nir6tinvlKvzC89zK/liIc4GYdyn2q8BGhEZ0sW3fYSMILYDEhfQHKwW+Q2hmFs9N061ZONxV/roMZH8eUIUiTzFKaZtiu1zv5E873P/Z7+yexIWndthWRkYn69QuDCpKgyUMSnk+Eq5tiD2hGw6COMNiHax+fP53BHpOQ=\"},{\"original\":\"P8Kgvxk+IET5FMd/QXjA4ibaI3aO/Eb3HTWbJyG/iLIwMX3xnKhyDbOPNA6sHeFcYQ3KtydImxZRoFvNVQk8QnjOwR7y/CH8AhiC3TVle2bpKJHgLVqEhhdBfQm4uqUoQPrjKPEtgVLpVZbtzEPaobTkJah2f6UaXCDf7xGN/QA=\",\"firstFrame\":\"sz3lxwos2MnYV/Ve+WEwoBhbpQEHV27cNqwxUj/hCZ5rBVYfFP9VKghlbgyH4F28UYwBTgBdg32MltmRSZvN+B62UletxogcyXrXmhO3oMWholZIeXh4gs2HX0YF2ubCgmpderslj9lPZ32b+LdVMURAsQ8TL5yy6YBZefWQELQ=\"},{\"original\":\"inqBarCsQIOV5Led2o8QfAA1rGV/ufXvgPXZK2Q/WGb016Y9r6M0ex6OWrVe/UkTTrrWBukeF0PKpnoIwHOWuxvJHGr9PComAvctjTD0tWjxp6UahUdS4LAssAp64BC1w+QtFweAMDNFq3J9JuIXOBnDIjuo78goEit8XedbT2Q=\",\"firstFrame\":\"TYYIb985K2LT53dMjON7GzlEZRFwmP4dAQe4G9fFGbfL2+f5FW8VjUoG9qb+T16RrdijgrW07+R3fs3sdsE/4kLTwbWAqzzZfWrIXzrgDNPj1elFuE419CwSV8tuiCryLGY7ER1DOOm4EF3KwdFMR9WZo/qz5XjLdIsinqs5JCw=\"},{\"original\":\"o2m1DujATpCMasfPBk/wX3GbR23e/yEFKpNWnIW7E8EGnSCwyLl3MN1VmIvvrYd4uxtkr9gwVZzI1hakv6giTE+oR7ZiUyvBOrIWinYWtsukPg3bXg0mDNCdxUGSpaHoxGjeYe7xjppJ6cwDQ273qqfkc5SZx6ZyZuI2GpjgQQ0=\",\"firstFrame\":\"UTuOAgTIWRszx8u+0id2lM1GqxV9YkTC+aWRGNz6Yg0PlL5VC7RqGbyGsqrOWCc3HS8+SneDvEcTWBTyt6LNaKeMiwnatLfyJ1mlA0QVn3suuNPCfdc9TEm+cYDtjjKtkflm1mSNT4CL4cCwHleupuTovfKare32chZJQSzv70w=\"},{\"original\":\"HW/HRuYCaLA47X8dGo/rG35jzJhEMPh8RZtgHyjloMFyzeBS647A9w9Ac3MLGJtg97pdch3+7I5qVdFytaSaVVwaPfXTe7yvhWMM/XZpSyCAaAyUVEvEiaEd+7649yObKBdRbjZB8NWwVn8NZspn7eNAAvL/NONnFv0OcQ+xWkM=\",\"firstFrame\":\"PC0oY1fX6lfhxUkHQqEw8VhkE0SsrhnfWIfmERep3g90++CrinqBrmB7ORUoj+kcDGB9pINq8cCODTYnrwfF7DdpsXcdOAzhDoVcYA6bPuWAeQgoElL+AS2GTwJIdzR4qd6HfTA4hpTga6yX0yafPDBpUpH/g9gBNklUHIcbHl8=\"},{\"original\":\"qzpUKraFoPSBId8EvPbdq+h2mvi0R/pl2gP3FwIqpEmYI5uZrWBVVdDaVS73kvYPtx6sHXYgF2FOmSm+BYtyvL0LLviHWgx+RiyjO84OymwHHHAbsQ4FzEXzZiIGfzVjN8pGgjeLGFYhIezbVggdRlVNApI+TJ7UjVYKhJjmKB+S876WinruMTM+w9uxdeFjSnEXbGY41PraHHncxcnzvN86vbKEmY9UAf9OBBbXPshPV0sE6Bb/EmfgpqPWRHtLvQ5HFPHZ3EdSI5+IDjxr7dx0knYnqyDK/qz4uaZtOg/8hGbRnHhscoorfD/XnVZjGtKzfMIpoabmfyZCqx6Ieg==\",\"firstFrame\":\"kTCV/A8IYVwKK4GHeKlwKaw8gbif4UDc2IHoaPjAr28U7Wko5hlHokwcPVRJzZQrQIZDAwMNxVdtEF131GvnPEiLcDsJBRtlp/thSjIVvkD4QN2eGCE8T8A8hF3rBjrUNytnmBNDb4xvc68tqFKIb9pAjvYzeM4lAUdAO1m2Ko1xiuI2rg8TQKb8fbcZd6aP5iw16Bct8cpMrYMjhL5hmGfy3oiFtDfi1Y3MiV2/x0AgvXu9GXmDW1gxEAb4H74cdTbeZJ8j4Vq5V6kjiqNEoLS8YdIZmLsQnOti3OCiS9GjAkh1JEAItbPeJ22fdo1pOBx/Zvf9cZPDmb6Z+01iyA==\"}],\"priceType\":\"gems\",\"currencyType\":\"USD\",\"price\":10,\"gateType\":\"all\",\"gateValue\":\"\",\"hasPermission\":true,\"purchased\":true,\"downloadStatus\":2,\"decryptImages\":[{\"original\":\"WKMFjOtzISxdShvQANrw22iaNssaNRSORU5T20sNlrzdRfPWYe0+zEmRAGTRXtiF4EoFFCJCJABqWjAAkV23G8/RioJQ0UI9PoMtpsNu6R267PwezZbkjw6il1yimePXhm1jQ3gZQdhv8K5Vp0hB/w3dtWS0WPcFbF0Qhmlgajo=\",\"firstFrame\":\"OVWrYKZeYF59hD/ebAcowqnEBWjQ+aIXdMIQ1/whb/AKLqg6Q+xoej3ZAevMG3ZsMebXHwAY95q8yKxP1IpHetRQpdgGs/Ms1s82QPOXx8qIJFibXO6oK7suGjK0dE3ce3znDEzs3nZNRTWFmo1qxEDqJtvopY2RcwWLNTiq7tU=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063504929666__w:800__h:600.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063504929666__w:800__h:600.png\"},{\"original\":\"oUAbv59/JEXDmI5StNF7Va1Od2SmSUgnpDbZy0K1+BNGoiBktSuTBGZfcea31KagmULhqFnx5kzWY0GPuW110DwllpsownKM0C+0Vm545E8z9lo+WiaDFSmdSJpZLSQL1gj+i3ps01vG/Pe3gUpqUqqqal+fZrDGa/0NMF7LIgs=\",\"firstFrame\":\"hpKSAU3ZYxpoq6b4gALLNEJymfm3waYwNwoLait7BHylmkSSxe1IDrFfuqkTwkb93lKGI6sZBp4o2b5ZXpwtrb67w8+acVlDx2h7yDilZ107j/wjXCANcoSCx+h17lL/xtjtWhv1nuP0QN6NUVMArWPsPe6NxzMoC3wlyRL21Qc=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063512506107__w:226__h:223.gif\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063512506107__w:226__h:223.gif\"},{\"original\":\"ETGoP0owhTWcGyIOFLtsdOi5AnPw9+RWHFM8B3Yi008OFAqyFD8YjtNKFkFYNCEVQr2/47sfDlacQ9NRhy4woyd7FCpFJPeL7jV5EeBNUMTdiaSs+5lEZleqJDwWw16xkGci8DS7k5N+l1abDSmL8mqgXGjpZdRyRUbaPdNnb+Y=\",\"firstFrame\":\"e+7GlIMXI6LO2YQmnDqluLeWd/Is9q/zsxmrF7bX9hGAY3+0MZPi++vh2NDIFSji0MuabCqtol5HskZsqvZyFvqAtH2Vle/1NKImPd6Oiqekp1i6co5kco5Svj6E3VLDAwUwRGmD51M4FSaKw//r74tXEjogWeVRA48coze1pFM=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063521487116__w:580__h:580.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063521487116__w:580__h:580.png\"},{\"original\":\"OuvYOHq1tMoPAVqT6kXk32ahDhkgyM+QX6SvzG5i9IGpMr5ebHTfN8e7yS1VI9pI9q9NBhi6LVZ78AWmtp4JwuQrvRoeJkPlLqbA1RHG2B4In4beV+mVmQtOBT8nbRfVT7yxnJGvmMiIZBUkxJB9zdy3k0TebWkLpeXD1FHnGIg=\",\"firstFrame\":\"N6B7F/GrVZxmiH3CJhvJuQlNCp7pU7TzJ7d5wgwZTYSGv6tvByo/dZxdIhROjDK9tQbXgDPM/Ka8ji9N9f8MkEN/C6g/LRf1LpSXx0cPufQ1e1jqXeJFF4H+v+2wMFJyUGHgmFbEeKy25SDBHVX/kkC4u4ECPYoyO1r6zTAwTaU=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063529946415__w:240__h:240.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063529946415__w:240__h:240.png\"},{\"original\":\"TDk9z1kjMhN6lrCaHh1McZaoYZtvg7TlBPuK3lvr/Vn1yWfegkl3WsH+7TRG1o1pl1GWEG8JkE3KgUjmYMYbWZJKZmOSHvXdFQLB7iSqd713/It0kbwFWF0G5F4Ug5b8qXstSRSdDRfVOUL5+pnItpE9s9N5tCAfoLLknwuEG0s=\",\"firstFrame\":\"V5qjoxOkhuhR0ZkLAf/RtXSECu9N3bNfKJzA8NUdFZsQagoJIY6rIrhKkXSJb1UaeB2Y5ohO+A0qkLtso/midbS6CdHbhcdAGd9Nfcm2RHVs8qybjtt0hWuzqhWFcxPKuEetcSXQ1J6yQ3VaeVOCehnzJfGZOMKZ5Ex1PTbYcXE=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063538020362__w:533__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063538020362__w:533__h:300.png\"},{\"original\":\"NaQ6GTXG9INjUAQzYk/OdaWEqRh57ojBpn0eeBzo5kysv+nszgOiA+ZbFjmNzU4fKbnFVTO9d4lcUg/ux9Amx2R+I0zyQm/y02HMjqZndZfoLRXuxLfLx9kawD3JygUEeqOJ5fywBNPSLGLdvKMQLNEXXm5H71ljKZul8vr6s8Q=\",\"firstFrame\":\"bV3JYG4g5OC9h+RhOXKvYP8GgZ9B+xj473TpXUeEfKvcpkNaz9kSaG8zDbZYvIzpBmNFVLzGCmQiHLoC0rkN4hNDhaA8ehpqIVrSDBkxd1E71rlOXep3a4w17cLXpINhj6dUVM2FxGG3nYNu4jHQLXZo1Sj4eY8zTgk4D7kkWV0=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063546870103__w:1084__h:564.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063546870103__w:1084__h:564.png\"},{\"original\":\"LUVVOlC3eWYC61BSVKXbl9zetV/LYAMmTMVUIfHKNgfjgrghX0fWiPBRwDNF31S7x0LnNAC4UbZ9cHgqYld//RbVsPniSnHXCa7cjn/AehRVHimamhZCxaGtDv/vpYmhkSz6/D1ia1KTMVgwMzToLcMZLxrXWor7h/5lHF1W8TM=\",\"firstFrame\":\"QG4NgV8dnc6sfd736rzCflerwHBGCO7XvT+RfxYK/r+97gV5ZRjpex7OiMT3V3dd+zsNGrhhyEhz08KoG2s+bWaA9okb4wQRABPRtXz2F/NzuRnr6FiHqW0LGL6+Ebgh0C+aFpIXbBGT1DFzOsr/8UxywEp9ReIP+q4SCwHwvv8=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063554287012__w:800__h:600.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063554287012__w:800__h:600.png\"},{\"original\":\"WyWV+Jkap43idOZB/pn83JDTdzhD3jwaKrsGfdTDf3KUaEg4aNUgCkSTY7iP3VgxDRQZk6ER2gqpz7fHDjeF5/AhIOch8FO4EdoUp6/QsoF8QTIhfz63krt6sy4u1GZaa6HQM0WTW682qh0x8JBjgnOdtV7hFg72NnFQBBA4JcE=\",\"firstFrame\":\"dyjc0rEQfEKJOr/PQPeUqWMQa5EeO65ML3metbT2ufZzM2BOQ8uZQWwl4YLh0bA8LEEUrldED3GFpqfnX00J35Ir3gn3mimgGgmjV2/34mUJDKQrmIcDiYzzvFeq59mZps6WGYDbJeGq94z2zAxN3qmB8wRlcDNsEGbyETEm68k=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063601629748__w:300__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063601629748__w:300__h:300.png\"},{\"original\":\"MMNDwOoGOZXgb94YX0zuoA4uM3tECvfuI4mH8DF8TGwFWEXnD6/fY7ADQeJH7ISw0zmZn8weVZTQ1Q8RWudS1Nj54S8opdswDuvW/93Lj63jmiGyrLI0dAQCgJMKVswNznE6nqMP/Gozg2jh8a+640eS9LJHD+34QPPVFRNPrOk=\",\"firstFrame\":\"Q9M8RVGvu+QKwBpMsBcmdhoFB0tXXdPhkTwYwx6z4tW0ddnbRSWIZIer5wBBLN/kizBE9CBuY7tlEnnu1ZtOdrodMIqonLKm8Rc1MfFChihRijQ6hbWWDUy55wr8D8rwZ2jLD9+Lnw6qBSOQxKocDkLcS2Pv3gvq4CyGMXznaUk=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063610821352__w:400__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063610821352__w:400__h:300.png\"},{\"original\":\"MXPcTHo8Hp+2ch+NY9D1PndbiPZZgu/cBySu/ktLAQcwEc0IGSk8FoLFdydtQMpdSMphLlACCdIjuJ7O29Ix4uazzB7jQp5C201SGB4JoLUqwUmH3wiAQud65u6TKW93cqhlyOZZ2Qp3FBhA+MUOzRFpdwG/bfK3glBXvXI62io=\",\"firstFrame\":\"kn7x+UX3BGNnuAEyCpItmKKeb4w79WYWbVDRtL3/9FJyU2RPoTycJ1yzXj/srrOrqt5or4PYdVxAQ45ZXKlicpuCXSZmhjxWN+HfsmZEKLcmbEGkKsqX6MREaafmPJcL5fTH6oytPaZGenCvgIBY6bLmPfWZlRS1CZN+b7wjW+8=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063624705816__w:750__h:1039.jpg\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063624705816__w:750__h:1039.jpg\"},{\"original\":\"AjKwhbWpeAQkmgPLMSFKbDvvIuxn0Ol60HsYm1iv+TNNzT0UZ/H/5DKa0xAATMoAXPb5bwLDSuWBzhhlusNwsI+yM4zkiamxoR4tZ6bW9iFCJ488bsQ/v24Ay07KVHkrC6cBquiFQReitpG4X8egwZGxejaDgejrvIakjjkIONU=\",\"firstFrame\":\"raaf/nir6tinvlKvzC89zK/liIc4GYdyn2q8BGhEZ0sW3fYSMILYDEhfQHKwW+Q2hmFs9N061ZONxV/roMZH8eUIUiTzFKaZtiu1zv5E873P/Z7+yexIWndthWRkYn69QuDCpKgyUMSnk+Eq5tiD2hGw6COMNiHax+fP53BHpOQ=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063633242369__w:94__h:40.png\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063633242369__w:94__h:40.png\"},{\"original\":\"P8Kgvxk+IET5FMd/QXjA4ibaI3aO/Eb3HTWbJyG/iLIwMX3xnKhyDbOPNA6sHeFcYQ3KtydImxZRoFvNVQk8QnjOwR7y/CH8AhiC3TVle2bpKJHgLVqEhhdBfQm4uqUoQPrjKPEtgVLpVZbtzEPaobTkJah2f6UaXCDf7xGN/QA=\",\"firstFrame\":\"sz3lxwos2MnYV/Ve+WEwoBhbpQEHV27cNqwxUj/hCZ5rBVYfFP9VKghlbgyH4F28UYwBTgBdg32MltmRSZvN+B62UletxogcyXrXmhO3oMWholZIeXh4gs2HX0YF2ubCgmpderslj9lPZ32b+LdVMURAsQ8TL5yy6YBZefWQELQ=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063640425288__w:1940__h:992.png\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063640425288__w:1940__h:992.png\"},{\"original\":\"inqBarCsQIOV5Led2o8QfAA1rGV/ufXvgPXZK2Q/WGb016Y9r6M0ex6OWrVe/UkTTrrWBukeF0PKpnoIwHOWuxvJHGr9PComAvctjTD0tWjxp6UahUdS4LAssAp64BC1w+QtFweAMDNFq3J9JuIXOBnDIjuo78goEit8XedbT2Q=\",\"firstFrame\":\"TYYIb985K2LT53dMjON7GzlEZRFwmP4dAQe4G9fFGbfL2+f5FW8VjUoG9qb+T16RrdijgrW07+R3fs3sdsE/4kLTwbWAqzzZfWrIXzrgDNPj1elFuE419CwSV8tuiCryLGY7ER1DOOm4EF3KwdFMR9WZo/qz5XjLdIsinqs5JCw=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063649298442__w:800__h:800.jpg\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063649298442__w:800__h:800.jpg\"},{\"original\":\"o2m1DujATpCMasfPBk/wX3GbR23e/yEFKpNWnIW7E8EGnSCwyLl3MN1VmIvvrYd4uxtkr9gwVZzI1hakv6giTE+oR7ZiUyvBOrIWinYWtsukPg3bXg0mDNCdxUGSpaHoxGjeYe7xjppJ6cwDQ273qqfkc5SZx6ZyZuI2GpjgQQ0=\",\"firstFrame\":\"UTuOAgTIWRszx8u+0id2lM1GqxV9YkTC+aWRGNz6Yg0PlL5VC7RqGbyGsqrOWCc3HS8+SneDvEcTWBTyt6LNaKeMiwnatLfyJ1mlA0QVn3suuNPCfdc9TEm+cYDtjjKtkflm1mSNT4CL4cCwHleupuTovfKare32chZJQSzv70w=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063656211877__w:576__h:911.jpeg\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063656211877__w:576__h:911.jpeg\"},{\"original\":\"HW/HRuYCaLA47X8dGo/rG35jzJhEMPh8RZtgHyjloMFyzeBS647A9w9Ac3MLGJtg97pdch3+7I5qVdFytaSaVVwaPfXTe7yvhWMM/XZpSyCAaAyUVEvEiaEd+7649yObKBdRbjZB8NWwVn8NZspn7eNAAvL/NONnFv0OcQ+xWkM=\",\"firstFrame\":\"PC0oY1fX6lfhxUkHQqEw8VhkE0SsrhnfWIfmERep3g90++CrinqBrmB7ORUoj+kcDGB9pINq8cCODTYnrwfF7DdpsXcdOAzhDoVcYA6bPuWAeQgoElL+AS2GTwJIdzR4qd6HfTA4hpTga6yX0yafPDBpUpH/g9gBNklUHIcbHl8=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063703128552__w:647__h:669.png\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063703128552__w:647__h:669.png\"},{\"original\":\"qzpUKraFoPSBId8EvPbdq+h2mvi0R/pl2gP3FwIqpEmYI5uZrWBVVdDaVS73kvYPtx6sHXYgF2FOmSm+BYtyvL0LLviHWgx+RiyjO84OymwHHHAbsQ4FzEXzZiIGfzVjN8pGgjeLGFYhIezbVggdRlVNApI+TJ7UjVYKhJjmKB+S876WinruMTM+w9uxdeFjSnEXbGY41PraHHncxcnzvN86vbKEmY9UAf9OBBbXPshPV0sE6Bb/EmfgpqPWRHtLvQ5HFPHZ3EdSI5+IDjxr7dx0knYnqyDK/qz4uaZtOg/8hGbRnHhscoorfD/XnVZjGtKzfMIpoabmfyZCqx6Ieg==\",\"firstFrame\":\"kTCV/A8IYVwKK4GHeKlwKaw8gbif4UDc2IHoaPjAr28U7Wko5hlHokwcPVRJzZQrQIZDAwMNxVdtEF131GvnPEiLcDsJBRtlp/thSjIVvkD4QN2eGCE8T8A8hF3rBjrUNytnmBNDb4xvc68tqFKIb9pAjvYzeM4lAUdAO1m2Ko1xiuI2rg8TQKb8fbcZd6aP5iw16Bct8cpMrYMjhL5hmGfy3oiFtDfi1Y3MiV2/x0AgvXu9GXmDW1gxEAb4H74cdTbeZJ8j4Vq5V6kjiqNEoLS8YdIZmLsQnOti3OCiS9GjAkh1JEAItbPeJ22fdo1pOBx/Zvf9cZPDmb6Z+01iyA==\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063715591355__w:1170__h:2532.PNG\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063715591355__w:1170__h:2532.PNG\"}]},{\"id\":\"2023022211015368100105\",\"celebId\":\"2022112603524872519031\",\"name\":\"sticker first frame\",\"icon\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230217103028518729__w:640__h:640.jpg\",\"description\":\" stickerjhhj\",\"image\":[{\"original\":\"ZPGslh592NcauLWaJuhYI4o0tzdQbn81QkbW4dNWqyv5sDof1fx61PE92yLn9QjEYZQsEgDYeYiS6OSndgXphVlGRelSt0k+4z70R1oMoXZWHO33LEhb7vRAj44FNxK5DIvw4DyTG9UlbpCAETZH8MWrw+VRJeWZW5XuKjhd38Y=\",\"firstFrame\":\"qKIXQc6y+VH03L869IPrw0kbIW1k/dQXNrqGpy2qkJhf49mFVmzXo34lTBa6Q2JJXmL8qeCJZ0NYsw2Y6EDaOcfkkgPZLJiv6WTtg5sn6B0xVvvOMmxgBVDI5t58znolMA+2C+/Oi4kOT7YOvqOfE1VAjB3ZW3+uMsO0nu5RYPs=\"},{\"original\":\"TrWDpIklXYUlJpaHYcvWknISC6sQU7riLRNewpKmZGLpK0QIAOGxJTnbd5Ka04RgMRIoe2zfahHN8Z8wgwx64TJN8zx4MQLvChYpA1+rp4x+YHZ2/4888Xcf/ag/TCDYSJiE9dSQ0ydB7A279azCnvu6SB5Jn+TqA7mQuYAtAE0=\",\"firstFrame\":\"NS+bQloy69Gb7OteArWjgDvbmgH66/sbEobx3iqQiGUP2Rvy3su0KTHGpxldmHlaDGkwwz74+EFoNBKUKm8IxELkTvQgl/ZDrHLHvulfumKjK4Q/73HoD3hDgyQvXO2/p7BcuplaB4SpbwcngLN9M27Sxv3ylk7dpX9Du8BnnZc=\"},{\"original\":\"Kh93ufvtbNu1SqmW8WoQuiCEmKn/h80jsMQf/72rBECYy8rP0ncALjbfd6ocVPetDEcpJWogJ0RIvbYmiKwGkMGyY7LwO8rUncTbwzNYSQvLcMFYNCv1zrFqJ1wPQuDbXXB/coe43j6vrR4alea3Sbb8qicWXBBaos8O9Mh/Ti4=\",\"firstFrame\":\"IcwJWBe4NnS5D3C4gvYEVmbIwzW+8icQ64iuIxKqZEPN96v1JblnI5Ej/Y5GLTGowpSl8B5+j49+3HaeTo5bHTa11y9wa7+1+UWujuAas5tmem899pim0nckGyy142eHy1GcBhZBzjj2rlTrWlLg40Hndino0ZBqey6doriln7E=\"},{\"original\":\"WGAIdBFHaRnYwPuMOM4sr4h4DEZ51Q6y+HDNkCNgn6uCR2AUeCUxAUnkXcAibVBzkkH32ECABX8oJrlwzGjn8hQMbmYnwByj4QR44W3A/75WQTbITgPvqNX1XizohQL+3aaOkhgavusFYrL/6QhVsebOtb8VHHADxAtmXbh1SEY=\",\"firstFrame\":\"HZtXbwxscpk/FOz72f2UwQzM6/z8lRUV9ONnb3nGfCnioeqnm+Ny6kkwaaGeRNUJlFE7ANzFA1lzbcgg84G3qd6ioRoDSNZdCLPiSBSmt9u4mP/JieMXmffPr96F/jBEYSS9KoGswoZK8BZLb0EAN74p/Hrhjzj7QHC3fFWlVIQ=\"},{\"original\":\"D2zp4OsjXGmZYRdlcUcSYr2FeNA48JLDlW7q0NCf7oo5ImiDps0CBF3YHW1tVi7oDDvZH15csfEDq/qwTkcZzezB1L+APk85ketXHjD/ZNUXyQzS9H78iNxZW5zE+Q7ybGP3gdiPUgwK33BDgwg5UTRpr4mUyk8ASacbdTUUHf8=\",\"firstFrame\":\"ZeBUiqqEomciYB2zz5/dGpC3VYlOnJT8r+Hki6S3kYB/eKTJOlP6Vx88lqmHFOdVNTO/5QhiO+x4/G+71TjXPWAVKJRZt0uZiMSYfePnVegvvALFgnell9U6fEUzg2Ee3J8ZwoMxR/ynbIdZcP7Lq1KKjbTaElDWSQWcwmlMDIk=\"},{\"original\":\"JQgdQShpdPn3rOBJYtNhC+z5CFT2Bb5yf/Tn0wQxLC5DLu/fCmpqrQPmRXqm4K3SUXsI+3ui9VVEzdVO6nfU6AyWwmUvA6T5/hPH7IRxidtAUTEvxIWTXDht2vodtVes1o2D+q8ZNRvtindpKhErwuxL6feYpg+Ui5MEaQY3A5w=\",\"firstFrame\":\"ew20Jo7eOu80M6jSESNkiY+LluptfXFlPUXo66Js4tO+vjsrFjc94q2Y2yg4MfAZPhiAfCFwrCIAjW9mwnYlaSgrCKg2i7/34WqX0aqd9wfcNk52liOyo/VeL0fpnx2TDvwUVZpTuK9jN53s2BF/8xGyJtqoUuLYSSIgCM6Akvo=\"},{\"original\":\"rbCU3GoIw8wJ90BWhPOuflb0Gv895uieVseL3oQplsoxyfqGPAq5Js2ETJ8zDlm//NTOeLp/r+rxbtwdtxalaN5XvrR9ubpQKaAeKcePjAodME0SAS5jFJpYGIE832kNdHmy8OWd5MoGbH0DSWdNVH+5s+zFANeGjz5l26PJN5I=\",\"firstFrame\":\"gMT6D0XmsISC8nBcOz6NWV33SJ3wb1kPI6a7AaNzrM6rCaAgq/bb4HfwvkeYOftOiuZkxEZjYTvfZOGxhoxAL/CnAOoETbR2t/uxKBZ2W6oT+n1PTy2u/+1+cgbJyfeo6Sx9jGJBO5GN6ikh7FbXlrHaSN15Y/Pc1wMClCpDIIM=\"},{\"original\":\"VVljxNgO710AkZhWdqj4bL62sQZgris6JlVfN+/AhAPsB60/jLGjAn3KKREtvPghNQvy2/45FL2TfHAe/dNnCsMTb5hooE2ieValJKQtG698wN5fPOhDSPocCStpOyRAO+ymmCIzcOJGXlaYLRMehmzfQ9XKw9lEHSQV6/JJfaE=\",\"firstFrame\":\"Lqeo5np+rTA3G17yR+5bGy2YgSFiKHphcZA6YJlUR0SWa/blgPyIfVBNgpVJyy2xMcZpenZWzLNREv+x4C2kr6wXtv/DHeaDTjiIx/OrwWX98p23sKk3iRuLPZuW/pT6g7C72LrjonAiaJs0REBpM+QJQ1mqWbuBtb0hwPirCKk=\"},{\"original\":\"KdCH58uk+wTjT1HIYsQihDS0dQmJfGPfoK0ohOPvyERkkGO/sZKPOBeeGbjejS6AufsKgVdY+WqLiAvsmpKG9DFvJ5fleaMv9u8Buim+mNVLQCGSuKCH01XZBetf5zkXO5JqojWy2WGYepbE7vsc1omXG3MuMCgRbwub+SGkX2k=\",\"firstFrame\":\"RyqFOIae4uC5cOySy7wAthmUqLcXqIXScIq/HOFKHs1mtXiEr68psu9Rwj7OkQamg/BbZiAWEKYZWxiUdKb+sVL+NgwkGZ1gI5CPmDczSKjLRPM2Xr9LtjgQpWQsboS3bBNuZGxgdiJyGNbWIt1Vq8sXws73rFC2EP1SoSCdr4o=\"},{\"original\":\"qRWWoSLtrHKShepsnZkMG1o0gKyM1RFpf6s9toRZAV6DEmjcQJuWIeUIEXcCz5VA+Mc3wfCv9IIVyNgLp6LdgdfC9EF7b7g1O8tp5fZdQ0eEI0TLgXks6zZNhQYtnaLRC5IMWU75hdZLHfph6X11A4tXgtDTWA+4NzNWs6iVqRE=\",\"firstFrame\":\"DconpJFHES+1oeuJFmEHvi7oscgPUzOpLQDy4BlKYxvVIvDPcKsSikulpT/9tfZYBJicMd374Se6QAnoCfNARCP6l0Qz5nOudVtwwsfY1Z8qhkZsmz5b0dEaM+8LUABr+3Z+uQJSs5R6F7xiCdg/4IdRL+Cu0Xhn0sApdChilJI=\"},{\"original\":\"SshEWUbzQq+QCjDg+kVFqWf26ZERSVsr4Gmui49AO89oV7ifyf0AOZ56PVR4sKjrsgCRDB0c5tZnuvBqExNpA+HWxnWXmqwcdU/aU8Iq8hOxhU+vmgfubPLLaWUUdCrANzG7V8qKt8JvQohPb2GhzypLvI7arlS2o2mjLPxxaXs=\",\"firstFrame\":\"nw2h75af2C6jvTwtYPiQX07kaV0g6NN+t7PCdS8pNjFwYsEHIqEvSHn++y17kn8J7YCBYTA7pYCFSX7H3H+ka0rtf2NWkfT6Tpqv2dR9Pn0/Tea00tTqdaDYmnlgIDtcs/4eGSlGJZEylxIoGp/RRqzfFVcDPTy6dUHtx6coVlo=\"},{\"original\":\"rj9/RIjwDY/vH5G3oKDyfpJOyVxoYSCEZ8HaaAP7RQcl672YcYL7TNi4f7Q9APWrF5D/M38PhmUVzWLHq0FHTcTVKw1q7+6ZN/IuoUdofQP8/hBL84k6GMd69l0iaYt2+R2oV+5jNjwWMM4/lXRTCQBMRc6oYZ1z7U2bGHUvXlI=\",\"firstFrame\":\"mxB6daSdtgOYUHEAgJi6mvjpTG3c4oLTAAmVxJ7FAIz6/gEy3J2r8gzim9g687Dq6TBHQeS24SkpkAxTQXrvHGne5oZ5dPhejV+dysffVJgNdhB79l2YGA3To+BrEiIQQX0AkoSUbVISZZTtCZiMUZgVLv2sX4MyYoM8qOsNS0A=\"},{\"original\":\"ncHSJmJ6M9QBg4p3h4fXi437+i25W6Ypyy7Sj/coS1qQmqW7O7xienc7/rpCGOsh17RAoLC4JQNAC1fVe6Ata2eYraPHXDGNfkB5KhkC1SOhYNLQG7go2KDv2R5q/6Qb75A6cuUFRRCr9CAOTk8a06i7fBKEHc7c2VZ1A4gWytM=\",\"firstFrame\":\"dGqZ+Q+wmaHhrrNtUvMIQqSczpcou7Ef6M4KpAwqXEPYXcahk9VG+o/sqSQpaGvtB6OP5VyXqh93HASyERzJLsD53TrVnvhXgo6xcUZUKdsIZTNF5ZHtHA1SexssESaQlYVoTN1q4TELxIbkAQ7YOBL0e/1bPBpNzUznVfCCUho=\"},{\"original\":\"cUk5zWLplmroLg2Rk4wKl7OrV/h5lvc/589aYGWdLsZ/9/wVd1aoIOPwzgn9L/gpS+TcTBEMBg1Q2OsKF7F7GrM+doaBvDSDK+DSnuUxtGk2Wwb8V5hrK2CxQ4XzebfS6uxVHVisIi5oCgqs9OLocCjTjhNgMR85eKFAimmDi8E=\",\"firstFrame\":\"lDw7TVArwcBk8w0Lk+ftZJV0MkjOKWvbsSilWPq7FjYRKPalqwWVMn5leqZeuSlkMOidS4YpsjRc/9T/rxN4uany6T59cGoJlIoAbzbSv4dHjlDz63GICmDnqWajBU3MH6i7myXqXkMMk3lCTo8dAMDLu60nEz9rO/wbB0UwPTo=\"},{\"original\":\"S/pfDCfS4N1tpcrljQ3S6FpS+m3Cb7unPlefUmnuNvz4znPBegAIkxeYTKJc3DW0mmVOCFwJi8lq6Ez1aIoNHTDICgdgajzAQtgPj8v0rOCRRdK65x7tgllyD1SXHVRxm3aN8NHtIDO4G2GQOdIAwbHy5+K4hj3DJjTt4m5fMOI=\",\"firstFrame\":\"GosZ5VmptUznFzvpoyiCt/MzZpgBBqbJ3H3aMWQ+Z68Kngn5KC3oa2N7ZzQol7h9Ti8DevKQQXVvRSzLlt6GLS87pU2t/tLZsOiThX8b2Y2RBAfC+KRjHjrs+p+cD559NDFb2RMfy1BWja1QN45goV/xiXGfwM1LrRG2psCSNmo=\"},{\"original\":\"maUh5tUGp5ZLWEYBFl1TCl3+yqEhENxcSlH6cBALKLy01sJjxGhayWAZuQmhOc1CSmJZUqgpBeUbcJFupiufrIYeKFlogINtzyTGoP9SyrfKES37xPv8dhxXvMkwRxdf8lHId5ZSR1o3Eq0MyyKw012rsAS2s/tZmHTXCTZuum+PKf4ole3d4bhYIYspn7wT/Wokxva1s/saxogb0Il2i/dDFHmf+6zH6jysRh+zTKJLZuvXnCMViNDmCVnVgpYYJqyNVIugMkW003tw4pGoreNKrmo0RT93voMQrN1inOfSzMoBxGoFlC5uL7+8wqOBGOhJwWKUrCS5/zUHVAVpmw==\",\"firstFrame\":\"uIjco3/kqCUB+3IL7inUgyMi7lCJNDoy4ENajQuj8CXrwiUfnKL0Y7PlHWRvCqqaJ2s+Y5zs8VQuam1B5j4Pfz+afmr7Svnno1lHi6twcl5XHbf64wdV/uPLuwgqJf+h8i9pZfQaWX5GHRnBvAXyHcHur5bxKVdvpO7FCFRPv+QNxb+mMZgGMNBuWAlKpOQhdzS/c9MQg3k934DUCed12mxu0wm8sM/Jw/CK5AJvkNurtD9QaYq9p14nivBgue2IDm+0wy23TH64g9vpi0EC1+BeFcRmWwx7284a9n5Tqbpc55C0yf0xSO28mRp5ZTj9Wt7xLznCLg4Hpp7izoJ/6w==\"}],\"priceType\":\"free\",\"currencyType\":\"USD\",\"price\":0,\"gateType\":\"all\",\"gateValue\":\"\",\"hasPermission\":true,\"purchased\":true,\"downloadStatus\":2,\"decryptImages\":[{\"original\":\"ZPGslh592NcauLWaJuhYI4o0tzdQbn81QkbW4dNWqyv5sDof1fx61PE92yLn9QjEYZQsEgDYeYiS6OSndgXphVlGRelSt0k+4z70R1oMoXZWHO33LEhb7vRAj44FNxK5DIvw4DyTG9UlbpCAETZH8MWrw+VRJeWZW5XuKjhd38Y=\",\"firstFrame\":\"qKIXQc6y+VH03L869IPrw0kbIW1k/dQXNrqGpy2qkJhf49mFVmzXo34lTBa6Q2JJXmL8qeCJZ0NYsw2Y6EDaOcfkkgPZLJiv6WTtg5sn6B0xVvvOMmxgBVDI5t58znolMA+2C+/Oi4kOT7YOvqOfE1VAjB3ZW3+uMsO0nu5RYPs=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063504929666__w:800__h:600.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063504929666__w:800__h:600.png\"},{\"original\":\"TrWDpIklXYUlJpaHYcvWknISC6sQU7riLRNewpKmZGLpK0QIAOGxJTnbd5Ka04RgMRIoe2zfahHN8Z8wgwx64TJN8zx4MQLvChYpA1+rp4x+YHZ2/4888Xcf/ag/TCDYSJiE9dSQ0ydB7A279azCnvu6SB5Jn+TqA7mQuYAtAE0=\",\"firstFrame\":\"NS+bQloy69Gb7OteArWjgDvbmgH66/sbEobx3iqQiGUP2Rvy3su0KTHGpxldmHlaDGkwwz74+EFoNBKUKm8IxELkTvQgl/ZDrHLHvulfumKjK4Q/73HoD3hDgyQvXO2/p7BcuplaB4SpbwcngLN9M27Sxv3ylk7dpX9Du8BnnZc=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063512506107__w:226__h:223.gif\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063512506107__w:226__h:223.gif\"},{\"original\":\"Kh93ufvtbNu1SqmW8WoQuiCEmKn/h80jsMQf/72rBECYy8rP0ncALjbfd6ocVPetDEcpJWogJ0RIvbYmiKwGkMGyY7LwO8rUncTbwzNYSQvLcMFYNCv1zrFqJ1wPQuDbXXB/coe43j6vrR4alea3Sbb8qicWXBBaos8O9Mh/Ti4=\",\"firstFrame\":\"IcwJWBe4NnS5D3C4gvYEVmbIwzW+8icQ64iuIxKqZEPN96v1JblnI5Ej/Y5GLTGowpSl8B5+j49+3HaeTo5bHTa11y9wa7+1+UWujuAas5tmem899pim0nckGyy142eHy1GcBhZBzjj2rlTrWlLg40Hndino0ZBqey6doriln7E=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063521487116__w:580__h:580.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063521487116__w:580__h:580.png\"},{\"original\":\"WGAIdBFHaRnYwPuMOM4sr4h4DEZ51Q6y+HDNkCNgn6uCR2AUeCUxAUnkXcAibVBzkkH32ECABX8oJrlwzGjn8hQMbmYnwByj4QR44W3A/75WQTbITgPvqNX1XizohQL+3aaOkhgavusFYrL/6QhVsebOtb8VHHADxAtmXbh1SEY=\",\"firstFrame\":\"HZtXbwxscpk/FOz72f2UwQzM6/z8lRUV9ONnb3nGfCnioeqnm+Ny6kkwaaGeRNUJlFE7ANzFA1lzbcgg84G3qd6ioRoDSNZdCLPiSBSmt9u4mP/JieMXmffPr96F/jBEYSS9KoGswoZK8BZLb0EAN74p/Hrhjzj7QHC3fFWlVIQ=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063529946415__w:240__h:240.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063529946415__w:240__h:240.png\"},{\"original\":\"D2zp4OsjXGmZYRdlcUcSYr2FeNA48JLDlW7q0NCf7oo5ImiDps0CBF3YHW1tVi7oDDvZH15csfEDq/qwTkcZzezB1L+APk85ketXHjD/ZNUXyQzS9H78iNxZW5zE+Q7ybGP3gdiPUgwK33BDgwg5UTRpr4mUyk8ASacbdTUUHf8=\",\"firstFrame\":\"ZeBUiqqEomciYB2zz5/dGpC3VYlOnJT8r+Hki6S3kYB/eKTJOlP6Vx88lqmHFOdVNTO/5QhiO+x4/G+71TjXPWAVKJRZt0uZiMSYfePnVegvvALFgnell9U6fEUzg2Ee3J8ZwoMxR/ynbIdZcP7Lq1KKjbTaElDWSQWcwmlMDIk=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063538020362__w:533__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063538020362__w:533__h:300.png\"},{\"original\":\"JQgdQShpdPn3rOBJYtNhC+z5CFT2Bb5yf/Tn0wQxLC5DLu/fCmpqrQPmRXqm4K3SUXsI+3ui9VVEzdVO6nfU6AyWwmUvA6T5/hPH7IRxidtAUTEvxIWTXDht2vodtVes1o2D+q8ZNRvtindpKhErwuxL6feYpg+Ui5MEaQY3A5w=\",\"firstFrame\":\"ew20Jo7eOu80M6jSESNkiY+LluptfXFlPUXo66Js4tO+vjsrFjc94q2Y2yg4MfAZPhiAfCFwrCIAjW9mwnYlaSgrCKg2i7/34WqX0aqd9wfcNk52liOyo/VeL0fpnx2TDvwUVZpTuK9jN53s2BF/8xGyJtqoUuLYSSIgCM6Akvo=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063546870103__w:1084__h:564.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063546870103__w:1084__h:564.png\"},{\"original\":\"rbCU3GoIw8wJ90BWhPOuflb0Gv895uieVseL3oQplsoxyfqGPAq5Js2ETJ8zDlm//NTOeLp/r+rxbtwdtxalaN5XvrR9ubpQKaAeKcePjAodME0SAS5jFJpYGIE832kNdHmy8OWd5MoGbH0DSWdNVH+5s+zFANeGjz5l26PJN5I=\",\"firstFrame\":\"gMT6D0XmsISC8nBcOz6NWV33SJ3wb1kPI6a7AaNzrM6rCaAgq/bb4HfwvkeYOftOiuZkxEZjYTvfZOGxhoxAL/CnAOoETbR2t/uxKBZ2W6oT+n1PTy2u/+1+cgbJyfeo6Sx9jGJBO5GN6ikh7FbXlrHaSN15Y/Pc1wMClCpDIIM=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063554287012__w:800__h:600.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063554287012__w:800__h:600.png\"},{\"original\":\"VVljxNgO710AkZhWdqj4bL62sQZgris6JlVfN+/AhAPsB60/jLGjAn3KKREtvPghNQvy2/45FL2TfHAe/dNnCsMTb5hooE2ieValJKQtG698wN5fPOhDSPocCStpOyRAO+ymmCIzcOJGXlaYLRMehmzfQ9XKw9lEHSQV6/JJfaE=\",\"firstFrame\":\"Lqeo5np+rTA3G17yR+5bGy2YgSFiKHphcZA6YJlUR0SWa/blgPyIfVBNgpVJyy2xMcZpenZWzLNREv+x4C2kr6wXtv/DHeaDTjiIx/OrwWX98p23sKk3iRuLPZuW/pT6g7C72LrjonAiaJs0REBpM+QJQ1mqWbuBtb0hwPirCKk=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063601629748__w:300__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063601629748__w:300__h:300.png\"},{\"original\":\"KdCH58uk+wTjT1HIYsQihDS0dQmJfGPfoK0ohOPvyERkkGO/sZKPOBeeGbjejS6AufsKgVdY+WqLiAvsmpKG9DFvJ5fleaMv9u8Buim+mNVLQCGSuKCH01XZBetf5zkXO5JqojWy2WGYepbE7vsc1omXG3MuMCgRbwub+SGkX2k=\",\"firstFrame\":\"RyqFOIae4uC5cOySy7wAthmUqLcXqIXScIq/HOFKHs1mtXiEr68psu9Rwj7OkQamg/BbZiAWEKYZWxiUdKb+sVL+NgwkGZ1gI5CPmDczSKjLRPM2Xr9LtjgQpWQsboS3bBNuZGxgdiJyGNbWIt1Vq8sXws73rFC2EP1SoSCdr4o=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063610821352__w:400__h:300.gif\",\"decryptFirstFrame\":\"https://uploaddev.duom.link/0000000000000000000000/operation/public/Sticker/20230222063610821352__w:400__h:300.png\"},{\"original\":\"qRWWoSLtrHKShepsnZkMG1o0gKyM1RFpf6s9toRZAV6DEmjcQJuWIeUIEXcCz5VA+Mc3wfCv9IIVyNgLp6LdgdfC9EF7b7g1O8tp5fZdQ0eEI0TLgXks6zZNhQYtnaLRC5IMWU75hdZLHfph6X11A4tXgtDTWA+4NzNWs6iVqRE=\",\"firstFrame\":\"DconpJFHES+1oeuJFmEHvi7oscgPUzOpLQDy4BlKYxvVIvDPcKsSikulpT/9tfZYBJicMd374Se6QAnoCfNARCP6l0Qz5nOudVtwwsfY1Z8qhkZsmz5b0dEaM+8LUABr+3Z+uQJSs5R6F7xiCdg/4IdRL+Cu0Xhn0sApdChilJI=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063624705816__w:750__h:1039.jpg\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063624705816__w:750__h:1039.jpg\"},{\"original\":\"SshEWUbzQq+QCjDg+kVFqWf26ZERSVsr4Gmui49AO89oV7ifyf0AOZ56PVR4sKjrsgCRDB0c5tZnuvBqExNpA+HWxnWXmqwcdU/aU8Iq8hOxhU+vmgfubPLLaWUUdCrANzG7V8qKt8JvQohPb2GhzypLvI7arlS2o2mjLPxxaXs=\",\"firstFrame\":\"nw2h75af2C6jvTwtYPiQX07kaV0g6NN+t7PCdS8pNjFwYsEHIqEvSHn++y17kn8J7YCBYTA7pYCFSX7H3H+ka0rtf2NWkfT6Tpqv2dR9Pn0/Tea00tTqdaDYmnlgIDtcs/4eGSlGJZEylxIoGp/RRqzfFVcDPTy6dUHtx6coVlo=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063633242369__w:94__h:40.png\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063633242369__w:94__h:40.png\"},{\"original\":\"rj9/RIjwDY/vH5G3oKDyfpJOyVxoYSCEZ8HaaAP7RQcl672YcYL7TNi4f7Q9APWrF5D/M38PhmUVzWLHq0FHTcTVKw1q7+6ZN/IuoUdofQP8/hBL84k6GMd69l0iaYt2+R2oV+5jNjwWMM4/lXRTCQBMRc6oYZ1z7U2bGHUvXlI=\",\"firstFrame\":\"mxB6daSdtgOYUHEAgJi6mvjpTG3c4oLTAAmVxJ7FAIz6/gEy3J2r8gzim9g687Dq6TBHQeS24SkpkAxTQXrvHGne5oZ5dPhejV+dysffVJgNdhB79l2YGA3To+BrEiIQQX0AkoSUbVISZZTtCZiMUZgVLv2sX4MyYoM8qOsNS0A=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063640425288__w:1940__h:992.png\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063640425288__w:1940__h:992.png\"},{\"original\":\"ncHSJmJ6M9QBg4p3h4fXi437+i25W6Ypyy7Sj/coS1qQmqW7O7xienc7/rpCGOsh17RAoLC4JQNAC1fVe6Ata2eYraPHXDGNfkB5KhkC1SOhYNLQG7go2KDv2R5q/6Qb75A6cuUFRRCr9CAOTk8a06i7fBKEHc7c2VZ1A4gWytM=\",\"firstFrame\":\"dGqZ+Q+wmaHhrrNtUvMIQqSczpcou7Ef6M4KpAwqXEPYXcahk9VG+o/sqSQpaGvtB6OP5VyXqh93HASyERzJLsD53TrVnvhXgo6xcUZUKdsIZTNF5ZHtHA1SexssESaQlYVoTN1q4TELxIbkAQ7YOBL0e/1bPBpNzUznVfCCUho=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063649298442__w:800__h:800.jpg\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063649298442__w:800__h:800.jpg\"},{\"original\":\"cUk5zWLplmroLg2Rk4wKl7OrV/h5lvc/589aYGWdLsZ/9/wVd1aoIOPwzgn9L/gpS+TcTBEMBg1Q2OsKF7F7GrM+doaBvDSDK+DSnuUxtGk2Wwb8V5hrK2CxQ4XzebfS6uxVHVisIi5oCgqs9OLocCjTjhNgMR85eKFAimmDi8E=\",\"firstFrame\":\"lDw7TVArwcBk8w0Lk+ftZJV0MkjOKWvbsSilWPq7FjYRKPalqwWVMn5leqZeuSlkMOidS4YpsjRc/9T/rxN4uany6T59cGoJlIoAbzbSv4dHjlDz63GICmDnqWajBU3MH6i7myXqXkMMk3lCTo8dAMDLu60nEz9rO/wbB0UwPTo=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063656211877__w:576__h:911.jpeg\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063656211877__w:576__h:911.jpeg\"},{\"original\":\"S/pfDCfS4N1tpcrljQ3S6FpS+m3Cb7unPlefUmnuNvz4znPBegAIkxeYTKJc3DW0mmVOCFwJi8lq6Ez1aIoNHTDICgdgajzAQtgPj8v0rOCRRdK65x7tgllyD1SXHVRxm3aN8NHtIDO4G2GQOdIAwbHy5+K4hj3DJjTt4m5fMOI=\",\"firstFrame\":\"GosZ5VmptUznFzvpoyiCt/MzZpgBBqbJ3H3aMWQ+Z68Kngn5KC3oa2N7ZzQol7h9Ti8DevKQQXVvRSzLlt6GLS87pU2t/tLZsOiThX8b2Y2RBAfC+KRjHjrs+p+cD559NDFb2RMfy1BWja1QN45goV/xiXGfwM1LrRG2psCSNmo=\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063703128552__w:647__h:669.png\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063703128552__w:647__h:669.png\"},{\"original\":\"maUh5tUGp5ZLWEYBFl1TCl3+yqEhENxcSlH6cBALKLy01sJjxGhayWAZuQmhOc1CSmJZUqgpBeUbcJFupiufrIYeKFlogINtzyTGoP9SyrfKES37xPv8dhxXvMkwRxdf8lHId5ZSR1o3Eq0MyyKw012rsAS2s/tZmHTXCTZuum+PKf4ole3d4bhYIYspn7wT/Wokxva1s/saxogb0Il2i/dDFHmf+6zH6jysRh+zTKJLZuvXnCMViNDmCVnVgpYYJqyNVIugMkW003tw4pGoreNKrmo0RT93voMQrN1inOfSzMoBxGoFlC5uL7+8wqOBGOhJwWKUrCS5/zUHVAVpmw==\",\"firstFrame\":\"uIjco3/kqCUB+3IL7inUgyMi7lCJNDoy4ENajQuj8CXrwiUfnKL0Y7PlHWRvCqqaJ2s+Y5zs8VQuam1B5j4Pfz+afmr7Svnno1lHi6twcl5XHbf64wdV/uPLuwgqJf+h8i9pZfQaWX5GHRnBvAXyHcHur5bxKVdvpO7FCFRPv+QNxb+mMZgGMNBuWAlKpOQhdzS/c9MQg3k934DUCed12mxu0wm8sM/Jw/CK5AJvkNurtD9QaYq9p14nivBgue2IDm+0wy23TH64g9vpi0EC1+BeFcRmWwx7284a9n5Tqbpc55C0yf0xSO28mRp5ZTj9Wt7xLznCLg4Hpp7izoJ/6w==\",\"decryptOriginal\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063715591355__w:1170__h:2532.PNG\",\"decryptFirstFrame\":\"https://uploadalpha.duom.link/0000000000000000000000/operation/public/Sticker/20230222063715591355__w:1170__h:2532.PNG\"}]}]"
