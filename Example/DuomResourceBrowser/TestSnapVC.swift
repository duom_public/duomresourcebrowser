//
//  TestSnapVC.swift
//  DuomResourceBrowser_Example
//
//  Created by kuroky on 2023/7/5.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import DuomResourceBrowser

class TestSnapVC: UIViewController {
    var imgView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        view.addSubview(imgView)
        imgView.backgroundColor = .red
        imgView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 300, height: 300))
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let tuple = getScreenSnapShotFile()
        let pathstring = "\(tuple!.0)"
        let index = pathstring.index(pathstring.startIndex, offsetBy: 7)
        let path = pathstring[index...]
        let data = FileManager.default.contents(atPath: String(path))
        let img = UIImage(data: data!)
        imgView.image = img
//        imgView.image = getScreenSnapshotImage()
    }
}
