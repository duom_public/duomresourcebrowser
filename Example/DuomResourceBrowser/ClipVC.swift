//
//  ClipVC.swift
//  DuomResourceBrowser_Example
//
//  Created by kuroky on 2023/8/4.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import DuomBase

class ClipVC: UIViewController {
    let clipView: ClipView = ClipView()
    override func viewDidLoad() {
         super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(clipView)
        clipView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        clipView.layoutIfNeeded()
        clipView.addCircleMask()
    }
    
}


class ClipView: UIView {
    static let lineWidth: CGFloat = 1
    
    static let circleSize: CGSize = CGSize(width: 280, height: 280)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
//    override func draw(_ rect: CGRect) {
//        let context = UIGraphicsGetCurrentContext()
//
//        context?.setStrokeColor(UIColor.white.withAlphaComponent(0.3).cgColor)
//        context?.setLineWidth(ClipView.lineWidth)
//        context?.beginPath()
//
//        let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
//        let radius = ClipView.circleSize.width / 2 - ClipView.lineWidth
//        context?.addArc(center: center, radius: radius, startAngle: 0, endAngle: .pi * 2, clockwise: true)
//        context?.addRect(bounds)
//        context?.setFillColor(UIColor.black.withAlphaComponent(0.3).cgColor)
//        context?.fillPath(using:.evenOdd)
//    }
    
    func addCircleMask()  {
        let viewSize = bounds.size
        let maskSize = CGSize(width: 280, height: 280)
//
//        let circleLayer = CAShapeLayer()
//        circleLayer.path = UIBezierPath(ovalIn: CGRect(origin: .zero, size: maskSize)).cgPath
//        circleLayer.frame = CGRect(x: (viewSize.width - maskSize.width) / 2, y: (viewSize.height - maskSize.height) / 2, width: maskSize.width, height: maskSize.height)
//        self.layer.mask = circleLayer
//        circleLayer.lineWidth = 10
//        circleLayer.strokeColor = UIColor.blue.cgColor
//        circleLayer.fillColor = UIColor.clear.cgColor
        
        let path = UIBezierPath.init(rect: bounds)
        let circlePath = UIBezierPath(ovalIn: CGRect(x: (viewSize.width - maskSize.width) / 2, y: (viewSize.height - maskSize.height) / 2, width: maskSize.width, height: maskSize.height))
        path.append(circlePath)
        path.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = UIColor.black.withAlphaComponent(0.3).cgColor
        
        let lineLayer = CAShapeLayer()
        lineLayer.path = UIBezierPath(ovalIn: CGRect(x: (viewSize.width - maskSize.width) / 2 - 1, y: (viewSize.height - maskSize.height) / 2 - 1, width: maskSize.width + 2, height: maskSize.height + 2)).cgPath
        lineLayer.lineWidth = 10
        lineLayer.strokeColor = UIColor.blue.cgColor
        lineLayer.fillColor = UIColor.clear.cgColor
        
        layer.addSublayer(fillLayer)
        layer.addSublayer(lineLayer)
    }
}

