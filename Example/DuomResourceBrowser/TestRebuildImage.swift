//
//  TestRebuildImage.swift
//  DuomResourceBrowser_Example
//
//  Created by kuroky on 2023/2/13.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import DuomBase

class TestRebuildImage: UIViewController {
    let containerView = UIView()
    let bigImg = UIImageView(image: UIImage(named: "sticker_test"))
    let smallImg = UIImageView(image: UIImage(named: "sticker_test"))
    
    let showimg = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(containerView)
        containerView.addSubview(bigImg)
        containerView.snp.makeConstraints { make in
            make.leading.equalTo(20)
            make.trailing.equalTo(-20)
            make.height.equalTo(200)
            make.centerY.equalToSuperview().offset(-200)
        }
        bigImg.addSubview(smallImg)
        bigImg.backgroundColor = .blue
        smallImg.backgroundColor = .red
        bigImg.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        smallImg.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets.init(hAxis: 20, vAxis: 20))
        }
        
        view.addSubview(showimg)
        showimg.backgroundColor = .yellow
        showimg.snp.makeConstraints { make in
            make.leading.equalTo(20)
            make.trailing.equalTo(-20)
            make.height.equalTo(200)
            make.centerY.equalToSuperview().offset(100)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let newImg = rebuildImg()
        showimg.image = newImg
    }
    
    func rebuildImg() -> UIImage {
//        UIGraphicsBeginImageContextWithOptions(bigImg.frame.size, false, 0)
//        smallImg.draw(CGRect(x: 0, y: 0, width: smallImg.frame.size.width, height: smallImg.frame.size.height))
////        bigImg.draw(CGRect(x: 0, y: 0, width: bigImg.frame.size.width, height: bigImg.frame.size.height))
//        bigImg.draw(CGRect(x: (smallImg.frame.size.width - bigImg.frame.size.width) / 2, y: (smallImg.frame.size.height - bigImg.frame.size.height) / 2, width: bigImg.frame.size.width, height: bigImg.frame.size.height))
//        let newImg = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return newImg
        
        UIGraphicsBeginImageContextWithOptions(containerView.frame.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        containerView.layer.render(in: context!)
        let newImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImg!
    }
    
    
}
