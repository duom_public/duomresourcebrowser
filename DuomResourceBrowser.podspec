#
# Be sure to run `pod lib lint DuomResourceBrowser.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'DuomResourceBrowser'
    s.version          = '1.2.4'
    s.summary          = 'Duom 资源选择器'
    
    # This description is used to generate tags and improve search results.
    #   * Think: What does it do? Why did you write it? What is the focus?
    #   * Try to keep it short, snappy and to the point.
    #   * Write the description between the DESC delimiters below.
    #   * Finally, don't worry about the indent, CocoaPods strips it!
    
    s.description      = <<-DESC
    TODO: Add long description of the pod here.
    DESC
    
    s.homepage         = 'https://gitlab.com/duomiOS/duomresourcebrowser'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'kuroky' => 'kai.he@duom.com' }
    s.source           = { :git => 'https://gitlab.com/duomiOS/duomresourcebrowser.git', :tag => s.version.to_s }
    # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
    
    s.ios.deployment_target = '13.0'
    s.swift_version = '5.0'
    
    s.resources = 'DuomResourceBrowser/Assets/*'
    
    s.subspec 'Modules' do |ss|
        ss.subspec 'CommonViews' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Modules/CommonViews/**/*'
        end
        
        ss.subspec 'Main' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Modules/Main/**/*'
        end
        
        ss.subspec 'Video' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Modules/Video/**/*'
        end
        
        ss.subspec 'Album' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Modules/Album/**/*'
        end
        
        ss.subspec 'Camera' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Modules/Camera/**/*'
        end
        
        ss.subspec 'Editor' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Modules/Editor/**/*'
        end
        
        ss.subspec 'Gif' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Modules/Gif/**/*'
        end
    end
    
    s.subspec 'Shared' do |ss|
        ss.subspec 'Interface' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Shared/Interface/**/*'
        end
        
        ss.subspec 'AssetManager' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Shared/AssetManager/**/*'
        end
        
        ss.subspec 'Configuration' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Shared/Configuration/**/*'
        end
        
        ss.subspec 'Tools' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Shared/Tools/**/*'
        end
        
        ss.subspec 'Wrapper' do |sss|
            sss.source_files = 'DuomResourceBrowser/Classes/Shared/Wrapper/**/*'
        end
    end
    
    s.subspec 'Models' do |ss|
        ss.source_files = 'DuomResourceBrowser/Classes/Models/**/*'
    end
    
    s.subspec 'Extensions' do |ss|
        ss.source_files = 'DuomResourceBrowser/Classes/Extensions/**/*'
    end
    
    s.dependency 'DuomBase'
    s.dependency 'Giphy'
end
