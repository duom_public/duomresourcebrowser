//
//  DuomPhotoModel.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/30.
//

import UIKit
import Photos
import DuomBase
import GiphyUISDK

public class PhotoModel: NSObject {
    public var ident: String?
    
    public var asset: PHAsset?
    
    public var avAseet: AVAsset?
    
    public var mediaType: MediaType = .unknown
    
    public var duration: String = ""
    
    public var isSelected: Bool = false
    
    public var second: Second {
        guard mediaType == .video, let asset = asset else {
            return 0
        }
        return Int(round(asset.duration))
    }
    
    public var width: Int = 0
    
    public var height: Int = 0
    
    // 宽高比
    public var whRatio: CGFloat {
        return CGFloat(width) / CGFloat(height)
    }
    
    // 文件路径
    public var fileURL: URL?
    
    // 如果是视频 视频的首帧缩略图
    public var firstFrameOfVideoFileURL: URL?
    
    // 图片元数据
    public var imageOriginalData: Data?
    
    
    // 图片编辑id
    public var editId: String?
    
    // 图片编辑状态
    public var editImageModel: EditImageModel?
    
    // 是否是手机拍摄
    public var isTakedPhoto: Bool = false
     
    // gif缩略对象
    public var gifThumb: OutputModel?
    
    // 是否是iCloud资源
    public var inICloud: Bool {
        if let asset = asset {
            return !asset.hk.isLocallyAvailable
        }
        return false
    }

    // iCloud资源下载状态
    public var downloadStatus: DownloadStatus = .notDownload
    
    // 标记是否是截屏图片
    public var isSnapShot: Bool = false
 
    public init(asset: PHAsset?) {
        ident = asset?.localIdentifier
        self.asset = asset
        super.init()
        
        if let asset = asset {
            mediaType = transformAssetType(for: asset)
            if mediaType == .video {
                duration = transformDuration(for: asset)
            }
            
            setSizeWithAsset(for: asset)
        }
    }
}

public extension PhotoModel {
    enum MediaType: Int {
        case unknown = 0
        case image
        case video
        case livePhoto
        case gif
        case giphyGif // giphy SDK 提供的gif
    }
    
    enum DownloadStatus: Int {
        case notDownload = 0
        case downloading
        case downloaded
    }
}


public extension PhotoModel {
    static func ==(lhs: PhotoModel, rhs: PhotoModel) -> Bool {
        return lhs.ident == rhs.ident
    }
}

extension PhotoModel {
    /// 根据asset对象 划分mediaType
    public func transformAssetType(for asset: PHAsset) -> PhotoModel.MediaType {
        switch asset.mediaType {
        case .video:
            return .video
        case .image:
            if (asset.value(forKey: "filename") as? String)?.hasSuffix("GIF") == true {
                return .gif
            }
            
            if asset.mediaSubtypes.contains(.photoLive) {
                return .livePhoto
            }
            
            return .image
        default:
            return.unknown
        }
    }
    
    private func setSizeWithAsset(for asset: PHAsset) {
        if mediaType != .giphyGif {
            width = asset.pixelWidth
            height = asset.pixelHeight
        }
    }
    
    /// 时间格式化
    public func transformDuration(for asset: PHAsset) -> String {
        let dur = Int(round(asset.duration))
        
        switch dur {
        case 0..<60:
            return String(format: "00:%02d", dur)
        case 60..<3600:
            let m = dur / 60
            let s = dur % 60
            return String(format: "%02d:%02d", m, s)
        case 3600...:
            let h = dur / 3600
            let m = (dur % 3600) / 60
            let s = dur % 60
            return String(format: "%02d:%02d:%02d", h, m, s)
        default:
            return ""
        }
    }
    
}



// MARK: 视频导出模式
public enum ExportPreset {
    case lowQuality
    case mediumQuality
    case highQuality
    case ratio_640x480
    case ratio_960x540
    case ratio_1280x720
    
    public var name: String {
        switch self {
        case .lowQuality:
            return AVAssetExportPresetLowQuality
        case .mediumQuality:
            return AVAssetExportPresetMediumQuality
        case .highQuality:
            return AVAssetExportPresetHighestQuality
        case .ratio_640x480:
            return AVAssetExportPreset640x480
        case .ratio_960x540:
            return AVAssetExportPreset960x540
        case .ratio_1280x720:
            return AVAssetExportPreset1280x720
        }
    }
}

