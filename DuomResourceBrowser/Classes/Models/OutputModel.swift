//
//  OutputModel.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/13.
//  对外输出的数据结构
//

import Foundation

@objc public class OutputModel: NSObject, Codable {
    public enum MediaType: Int, Codable {
        case image = 0
        case video
        case livePhoto
        case giphyGif
    }
    
    public var id: String = ""
    // 只有编辑过才有
    public var editId: String?
    public var videoPath: String = ""
    public var imagePath: String = ""
    public var imageCompressPath: String = ""
//    public var path: String = ""
//    public var compressPath: String = ""
    public var mediaType: MediaType = .image
    public var duration: Int = 0
    public var height: Int = 0
    public var width: Int = 0
    public var isSnapShot: Bool = false
    public var isTakePhoto: Bool = false
    public var thumb: OutputModel?
    //  排序 下标
    public var index: Int = 0
    
    public override init() {}
    
    public func toDic() -> [String: Any]{
        var redic = [String: Any]()
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(self)
            redic = try (JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any])!
        } catch {
            print(error)
        }
        return redic
    }
}
