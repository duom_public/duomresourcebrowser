//
//  EditImageModel.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/4/12.
//

import Foundation

// 图片编辑状态
public struct EditImageModel: Codable {
    // 原图地址
    public var originImageData: Data?
    
    // 裁剪后图片
    public var clipImageData: Data?
    
    public var editRect: CGRect?
    
    public var editImage: Data?
    
    public var clipRatio: Int?
    
    public var stickerInfo: [EditImageStickerModel] = [EditImageStickerModel]()
    
    public init(originImageData: Data? = nil, clipImageData: Data? = nil, editRect: CGRect? = nil, editImage: Data? = nil, clipRatio: Int? = nil) {
        self.originImageData = originImageData
        self.clipImageData = clipImageData
        self.editRect = editRect
        self.editImage = editImage
        self.clipRatio = clipRatio
    }
    
    enum CodingKeys: CodingKey {
        case originImageData
        case clipImageData
        case editRect
        case editImage
        case clipRatio
        case stickerInfo
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.originImageData = try container.decodeIfPresent(Data.self, forKey: .originImageData)
        self.clipImageData = try container.decodeIfPresent(Data.self, forKey: .clipImageData)
        self.editRect = try container.decodeIfPresent(CGRect.self, forKey: .editRect)
        self.editImage = try container.decodeIfPresent(Data.self, forKey: .editImage)
        self.clipRatio = try container.decodeIfPresent(Int.self, forKey: .clipRatio)
        self.stickerInfo = try container.decode([EditImageStickerModel].self, forKey: .stickerInfo)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(originImageData, forKey: .originImageData)
        try container.encode(clipImageData, forKey: .clipImageData)
        try container.encode(editRect, forKey: .editRect)
        try container.encode(editImage, forKey: .editImage)
        try container.encode(clipRatio, forKey: .clipRatio)
        try container.encode(stickerInfo, forKey: .stickerInfo)
    }
}


public struct EditImageStickerModel: Codable {
    public var stickURL: URL?
    
    public var index: Int?
    
    public var rect: CGRect?
    
    public var center: CGPoint?
    
    public var scale: CGFloat?
    
    public var angle: CGFloat?
    
    
    public init(stickURL: URL? = nil, index: Int? = nil, rect: CGRect? = nil, center: CGPoint? = nil, scale: CGFloat? = nil, angle: CGFloat? = nil) {
        self.stickURL = stickURL
        self.index = index
        self.rect = rect
        self.center = center
        self.scale = scale
        self.angle = angle
    }
}


// 视频编辑状态
public struct EditVideoModel {
    public var fileURL: URL?
    
    public var firstFrameImgURL: URL?
    
    public var duration: String?
    
    init(fileURL: URL? = nil,
         firstFrameImgURL: URL? = nil,
         duration: String? = nil) {
        self.fileURL = fileURL
        self.firstFrameImgURL = firstFrameImgURL
        self.duration = duration
    }
}
