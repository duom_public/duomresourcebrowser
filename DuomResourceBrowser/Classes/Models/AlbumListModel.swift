//
//  AlbumListModel.swift
//  ResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import Foundation
import Photos

public struct AlbumListModel {
    var title: String
    
    var count: Int {
        return result.count
    }
    
    var result: PHFetchResult<PHAsset>
    
    let collection: PHAssetCollection
    
    let option: PHFetchOptions
    
    let isCameraRoll: Bool
    
    var headImageAsset: PHAsset? {
        return result.lastObject
    }
    
    var models: [PhotoModel] = []
    
    init(title: String,
         result: PHFetchResult<PHAsset>,
         collection: PHAssetCollection,
         option: PHFetchOptions,
         isCameraRoll: Bool) {
        self.title = title
        self.result = result
        self.collection = collection
        self.option = option
        self.isCameraRoll = isCameraRoll
    }
    
    public mutating func refetchPhotos()  {
        let models = AssetManager.fetchPhoto(in: result,
                                                ascending: PhotosConfiguration.default().sortAscending,
                                                allowSelectImage: PhotosConfiguration.default().allowSelectImage,
                                                allowSelectVideo: PhotosConfiguration.default().allowSelectVideo)
        self.models.removeAll()
        self.models.append(contentsOf: models)
    }
    
    mutating func refreshResult()  {
        result = PHAsset.fetchAssets(in: collection, options: option)
    }
    
}

extension AlbumListModel {
    static func ==(lhs: AlbumListModel, rhs: AlbumListModel) -> Bool {
        return lhs.title == rhs.title &&
               lhs.count == rhs.count &&
               lhs.headImageAsset?.localIdentifier == rhs.headImageAsset?.localIdentifier
    }
}
