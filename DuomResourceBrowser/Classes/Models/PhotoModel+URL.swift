//
//  PhotoModel+URL.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/3.
//

import Foundation
import Photos
import DuomBase

public enum LivePhotoError {
    case imageError(Error?)
    case videoError(Error?)
    case allError(Error?, Error?)
}

public extension PhotoModel {
    typealias AssetURLCompletion = (Result<AssetURLResult, AssetError>) -> Void
    
    // 压缩参数
    public struct Compression {
         let imageCompressionQuality: CGFloat? // 图片质量最大值：M的倍数 🌰：2: 2M
         let videoExportPreset: ExportPreset?
//         let videoQuality: Int?
         
         /// 压图片
         /// - Parameter imageCompressionQuality 图片质量最大值：M的倍数
         public init(imageCompressionQuality: CGFloat) {
             self.imageCompressionQuality = imageCompressionQuality
             self.videoExportPreset = nil
         }
         
         /// 压缩视频
         /// - Parameters:
         ///   - videoExportPreset: 视频分辨率
         public init(videoExportPreset: ExportPreset) {
             self.imageCompressionQuality = nil
             self.videoExportPreset = videoExportPreset
         }
         
         /// 压缩图片、视频
         /// - Parameters:
         ///   - imageCompressionQuality: 图片质量最大值：M的倍数
         ///   - videoExportPreset: 视频分辨率
         public init(imageCompressionQuality: CGFloat, videoExportPreset: ExportPreset) {
             self.imageCompressionQuality = imageCompressionQuality
             self.videoExportPreset = videoExportPreset
         }
     }
    
    
    /// 获取图片url
    /// - Parameters:
    ///   - compressionQuality: 图片质量最大值：M的倍数。 gif不会压缩
    ///   - complection: 获取完成
    func getImageURL(compressionQuality: CGFloat?,
                     complection: @escaping AssetURLCompletion) {
        requestImageURL(compressionQuality: compressionQuality, resultHandler: complection)
    }
    
    
    /// 获取LivePhoto里的图片和视频URL
    /// - Parameters:
    ///   - compressionQuality: compressionQuality description
    ///   - completion: completion description
    func getLivePhotoURL(compression: Compression?,
                         completion: @escaping AssetURLCompletion)  {
        requestLivePhotoURL(compression: compression, completion: completion)
    }
    
    
    /// 获取视频
    /// - Parameters:
    ///   - exportPreset: 视频导出的分辨率
    ///   - exportSession: exportSession description
    ///   - completion: completion description
    func getVideoURL(exportPreset: ExportPreset? = PhotosConfiguration.default().videoExportPresent,
                     exportSession: ((AVAssetExportSession) -> Void)? = nil,
                     completion: @escaping AssetURLCompletion) {
        requestAssetVideoURL(exportPreset: exportPreset, resultHandler: completion)
    }
    
    
    // 如果是录制的视频 需要取帧视图 并保存
    func getFirstFrameImage(forAsset asset: PHAsset, complection: @escaping (() -> Void))  {
        AssetManager.fetchAvAsset(forVideo: asset) { avAsset, info in
            if let avAsset = avAsset {
                let generator = AVAssetImageGenerator(asset: avAsset)
                // 指定生成的图像的最大尺寸
                generator.maximumSize = CGSize(width: UIScreen.screenWidth, height: UIScreen.screenHeight)
                generator.appliesPreferredTrackTransform = true
                generator.requestedTimeToleranceBefore = .zero
                generator.requestedTimeToleranceAfter = .zero
                generator.apertureMode = .productionAperture
                
                let time = CMTimeMakeWithSeconds(0, preferredTimescale: avAsset.duration.timescale)
                generator.generateCGImagesAsynchronously(forTimes: [NSValue(time: time)]) { _, cgImage, _, result, _ in
                    if result == .succeeded, let cg = cgImage {
                        let image = UIImage(cgImage: cg)
                        // 转为jpeg 写入 
                        if let imgData = PhotoTools.getImageData(for: image),
                           let jpegData = PhotoTools.transToJpegData(imgData),
                           let compressData = PhotoTools.imageCompress(jpegData, compressionQuality: PhotosConfiguration.default().imageQualityCount),
                           let url = PhotoTools.write(imageData: compressData) {
                            self.firstFrameOfVideoFileURL = url
                            DispatchQueue.main.async {
                                complection()
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            complection()
                        }
                    }
                }
            }
        }
    }
    
    // 获取giphy gif 地址
    func getGiphyGifURL(completion: @escaping AssetURLCompletion) {
        if let fileURL = fileURL {
            completion(.success(.init(url: fileURL, mediaType: .giphyGif)))
            return
        }
        completion(.failure(.invalidData))
    }
}
