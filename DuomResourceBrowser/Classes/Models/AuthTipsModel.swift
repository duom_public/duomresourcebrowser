//
//  AuthTipsModel.swift
//  DuomResourceBrowser
//
//  Created by Henry on 2023/10/13.
//

import Foundation

public class AuthTipsModel: NSObject {
    public var title: String
    
    public var youUseOption: AuthTipsOptionModel
    
    public var weUseOption: AuthTipsOptionModel
    
    public var settingsOption: AuthTipsOptionModel
    
    public var buttonTitle: String
    
    public init(title: String,
                youUseOption: AuthTipsOptionModel,
                weUseOption: AuthTipsOptionModel,
                settingsOption: AuthTipsOptionModel,
                buttonTitle: String) {
        self.title = title
        self.youUseOption = youUseOption
        self.weUseOption = weUseOption
        self.settingsOption = settingsOption
        self.buttonTitle = buttonTitle
    }
}

public class AuthTipsOptionModel: NSObject {
    public var iconName: String
    
    public var title: String
    
    public var desc: String
    
    public init(iconName: String,
                title: String,
                desc: String) {
        self.iconName = iconName
        self.title = title
        self.desc = desc
    }
}
