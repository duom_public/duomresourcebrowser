//
//  PhotoModel+Request.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/3.
//

import Foundation
import Photos
import DuomBase

public extension PhotoModel {
    struct ImageDataResult {
        let imageData: Data
        let imageOrientation: UIImage.Orientation
        let info: [AnyHashable: Any]?
    }
    
    func requestImageURL(toFile fileURL: URL? = nil, compressionQuality: CGFloat? = nil, resultHandler: @escaping AssetURLCompletion)  {
        if let fileURL = self.fileURL {
            getLocalImageURL(compressionQuality: compressionQuality, resultHandler: resultHandler)
            return
        }
        
        guard let asset = asset else {
            toast(CopyWitterInstance.default().AssetNil)
            resultHandler(.failure(.invalidPHAsset))
            return
        }
        
        var imageFileURL: URL
        if let fileURL = fileURL {
            imageFileURL = fileURL
        } else {
            var suffix: String
            suffix = mediaType == .gif ? "gif" : "png"
            imageFileURL = PhotoTools.getTmpURL(for: suffix)
        }
        
        var imageCompressURL: URL
        imageCompressURL = PhotoTools.getTmpURL(for: mediaType == .gif ? "gif" : "png")
        
        let isGif = mediaType == .gif
        AssetManager.requestImageURL(for: asset, toFile: imageFileURL) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let imageURL):
                func resultSuccess(_ url: URL, compressUrl: URL? = nil) {
                    if DispatchQueue.isMain {
                        resultHandler(.success(.init(url: url, compressUrl: compressUrl, mediaType: self.mediaType)))
                    } else {
                        DispatchQueue.main.async {
                            resultHandler(.success(.init(url: url, compressUrl: compressUrl, mediaType: self.mediaType)))
                        }
                    }
                }
                
                if isGif, self.mediaType == .gif {
                    // gif不压缩
//                    Logger.log(message: "\(imageURL)", level: .info)
                    resultSuccess(imageURL)
                } else {
                    if let compressionQuality = compressionQuality {
                        guard let imageData = try? Data(contentsOf: imageURL) else {
                            resultHandler(.failure(.imageCompressionFailed))
                            return
                        }
                        DispatchQueue.global().async {
                            if let data = PhotoTools.imageCompress(imageData, compressionQuality: compressionQuality),
                               let compressUrl = PhotoTools.write(toFile: imageCompressURL, imageData: data) {
                                resultSuccess(imageURL, compressUrl: compressUrl)
                            } else{
                                DispatchQueue.main.async {
                                    resultHandler(.failure(.imageCompressionFailed))
                                }
                            }
                        }
                        return
                    } else {
                        resultSuccess(imageURL)
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                   resultHandler(.failure(error))
                }
            }
        }
    }
    
    // 本地image: 1.未保存到相册的(截图)  2.拍照 不可编辑，直接回调的
    private func getLocalImageURL(compressionQuality: CGFloat? = nil, resultHandler: @escaping AssetURLCompletion)  {
        guard let fileURL = self.fileURL else {
            return
        }
        
        if let compressionQuality = compressionQuality {
            guard let imageData = try? Data(contentsOf: fileURL) else {
                resultHandler(.failure(.imageCompressionFailed))
                return
            }
            var imageCompressURL: URL
            imageCompressURL = PhotoTools.getTmpURL(for: mediaType == .gif ? "gif" : "png")
            DispatchQueue.global().async {
                if let data = PhotoTools.imageCompress(imageData, compressionQuality: compressionQuality),
                   let compressUrl = PhotoTools.write(toFile: imageCompressURL, imageData: data) {
                    DispatchQueue.main.async {
                        resultHandler(.success(.init(url: fileURL, compressUrl: compressUrl, mediaType: .image)))
                    }
                } else{
                    DispatchQueue.main.async {
                        resultHandler(.failure(.imageCompressionFailed))
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                resultHandler(.success(.init(url: fileURL, mediaType: .image)))
            }
        }
    }
    
    
    func requestLivePhotoURL(compression: Compression? = nil, completion: @escaping AssetURLCompletion) {
        if let fileURL = self.fileURL {
            getLocalImageURL(compressionQuality: nil, resultHandler: completion)
            return
        }
        
        guard let asset = asset else {
            return
        }
        var imageURL: URL?
        var videoURL: URL?
        
        AssetManager.requestLivePhoto(for: asset) { imgURL in
            imageURL = imgURL
        } videoURLHandler: { videourl in
            videoURL = videourl
        } completion: { error in
            if let error = error {
                switch error {
                case .allError(let imageError, let videoError):
                    completion(.failure(.exportLivePhotoURLFailed(imageError, videoError)))
                case .imageError(let error):
                    completion(.failure(.exportLivePhotoImageURLFailed(error)))
                case .videoError(let error):
                    completion(.failure(.exportLivePhotoVideoURLFailed(error)))
                }
            } else {
                // 回调函数
                func completionFunc(_ image_URL: URL?, _ compress_URL: URL?, _ video_URL: URL?) {
                    if let image_URL = image_URL,
                       let video_URL = video_URL {
                        completion(
                            .success(
                                .init(url: image_URL,
                                      compressUrl: compress_URL,
                                      mediaType: .image,
                                      livePhoto: .init(imageURL: image_URL, videoURL: video_URL))
                            )
                        )
                    } else if image_URL != nil {
                        completion(.failure(.exportLivePhotoVideoURLFailed(nil)))
                    } else if video_URL != nil {
                        completion(.failure(.exportLivePhotoImageURLFailed(nil)))
                    } else {
                        completion(.failure(.exportLivePhotoURLFailed(nil, nil)))
                    }
                }
                
                // 压缩image函数
                func imageCompressor(_ url: URL, _ compressionQuality: CGFloat) -> URL? {
                    guard let imageData = try? Data(contentsOf: url) else {
                        return nil
                    }
                    
                    guard let data = PhotoTools.imageCompress(imageData, compressionQuality: compressionQuality) else {
                        return nil
                    }
                    
                    if asset.hk.assetIsHEIF(), let jpegData = PhotoTools.transToJpegData(imageData)  {
                        return PhotoTools.write(imageData: jpegData)
                    } else {
                        return PhotoTools.write(imageData: data)
                    }
                }
                
                // 压缩video函数
                func videoCompressor(_ url: URL, exportPreset: ExportPreset, completionHandler: ((URL?, Error?) -> Void)?) {
                    let avAsset = AVAsset(url: url)
                    avAsset.loadValuesAsynchronously(forKeys: ["tracks"]) {
                        if avAsset.statusOfValue(forKey: "tracks", error: nil) != .loaded {
                            completionHandler?(nil, nil)
                            return
                        }
                        AssetManager.exportVideoURL(
                            forVideo: avAsset,
                            toFile: PhotoTools.getVideoTmpURL(),
                            exportPreset: exportPreset) { video_URL, error in
                                if FileManager.default.fileExists(atPath: url.path) {
                                    try? FileManager.default.removeItem(at: url)
                                }
                                completionHandler?(video_URL, error)
                            }
                    }
                }
                
                
                if let imageCompression = compression?.imageCompressionQuality,
                   let videoExportPresent = compression?.videoExportPreset {
                    let group = DispatchGroup()
                    let imageQueue = DispatchQueue(label: "hkphoto.requestVideo.imageURL")
                    var image_URL: URL?
                    var compressImg_URL: URL?
                    var video_URL: URL?
                    // 优先压图片
                    imageQueue.async(group: group, execute: DispatchWorkItem(block: {
                        compressImg_URL = imageCompressor(imageURL!, imageCompression)
                    }))

                    let videoQueue = DispatchQueue(label: "hkphoto.requestVideo.videoURL")
                    let semaphore = DispatchSemaphore(value: 0)
                    videoQueue.async(group: group, execute: DispatchWorkItem(block: {
                        videoCompressor(videoURL!, exportPreset: videoExportPresent) { url, error in
                            video_URL = url
                            semaphore.signal()
                        }
                        semaphore.wait()
                    }))
                    group.notify(queue: .main, work: DispatchWorkItem(block: {
                        completionFunc(imageURL, compressImg_URL, video_URL)
                    }))
                } else if let imageCompression = compression?.imageCompressionQuality {
                    DispatchQueue.global().async {
                        let url = imageCompressor(imageURL!, imageCompression)
                        DispatchQueue.main.async {
                            completionFunc(imageURL, url, videoURL)
                        }
                    }
                } else if let videoExportPreset = compression?.videoExportPreset {
                    DispatchQueue.global().async {
                        videoCompressor(videoURL!, exportPreset: videoExportPreset) { url, error in
                            DispatchQueue.main.async {
                                completionFunc(imageURL, nil, url)
                            }
                        }
                    }
                } else {
                    completionFunc(imageURL, nil, videoURL)
                }
            }
        }
    }
    

    /// 请求视频路径
    /// - Parameters:
    ///   - fileURL: 路径
    ///   - exportSession: exportSession description
    ///   - resultHandler: resultHandler description
    func requestAssetVideoURL(toFile fileURL: URL? = nil,
                              exportPreset: ExportPreset? = PhotosConfiguration.default().videoExportPresent,
                              exportSession: ((AVAssetExportSession) -> Void)? = nil,
                              resultHandler: @escaping AssetURLCompletion) {
        // 获取视频编辑后的路 编辑后的作为新视频存入相册 所以这里废弃⚠️
//        if editVideoModel != nil {
//            getEditedVideoURL(resultHandler: resultHandler)
//            return
//        }
//
        
        // 获取本地录制的视频路径
        // 修改： 本地录制的视频是h265 需要转码
//        if self.fileURL != nil, firstFrameOfVideoFileURL != nil {
//            getLocalVideoURL(resultHandler: resultHandler)
//            return
//        }
        
        guard let asset = asset else {
            Logger.log(message: "缺少PHAsset入参", level: .info)
            return
        }
        
        let toFile = fileURL == nil ? PhotoTools.getVideoTmpURL() : fileURL!
        if let exportPreset = exportPreset {
//            var before: Int64 = 0
//            before = CLongLong(Date().timeIntervalSince1970 * 1000)
            AssetManager.exportVideoURL(asset: asset, avAsset: avAseet, toFile: toFile, exportPreset: exportPreset) { result in
//                Logger.log(message: "视频写入后耗时: \(CLongLong(Date().timeIntervalSince1970 * 1000) - before)", level: .info)
                switch result {
                case .success(let _videourl):
                    self.getVideoCoverURL { coverURL in
//                        Logger.log(message: "视频获取封面后耗时: \(CLongLong(Date().timeIntervalSince1970 * 1000) - before)", level: .info)
                        resultHandler(.success(.init(url: _videourl, compressUrl: coverURL, mediaType: .video)))
                    }
                case .failure(let error):
                    resultHandler(.failure(error.error))
                }
            }
            return
        }
    }
    
    // 获取视频封面URL
    func getVideoCoverURL(toFile fileURL: URL? = nil,
                          resultHandler: @escaping (URL?) -> Void) {
        // 如果是录制的 在save的时候已经存储了 直接取
        if let firstUrl = firstFrameOfVideoFileURL {
            DispatchQueue.main.async {
                resultHandler(firstUrl)
            }
        } else {
            guard let asset = asset else {
                return
            }
            // 本地视频的封面不压缩了
            let coverURL = fileURL ?? PhotoTools.getImageTmpURL(.png)
            AssetManager.fetchOriginalImageData(for: asset) { imgData, info, isDegraded in
                let imageData = imgData
                let jpegData = PhotoTools.transToJpegData(imageData)
                DispatchQueue.global().async {
                    if let imageURL = PhotoTools.write(toFile: coverURL, imageData: jpegData ?? imageData) {
                        DispatchQueue.main.async {
                            resultHandler(imageURL)
                        }
                    } else {
                        DispatchQueue.main.async {
                            Logger.log(message: "视频封面URL 写入失败", level: .info)
                            resultHandler(nil)
                        }
                    }
                }
            }
        }
    }
    
    // 获取视频编辑后地址
//    func getEditedVideoURL(resultHandler: @escaping AssetURLCompletion) {
//        guard let videoURL = editVideoModel?.fileURL, let firstFrameURL = editVideoModel?.firstFrameImgURL else {
//            resultHandler(.failure(.localURLIsEmpty))
//            return
//        }
//        resultHandler(.success(.init(url: videoURL, compressUrl: firstFrameURL, mediaType: .video)))
//    }
    
    // 获取本地录制视频 且无编辑操作的地址
    func getLocalVideoURL(resultHandler: @escaping AssetURLCompletion) {
        guard let videoURL = self.fileURL, let firstFrameURL = self.firstFrameOfVideoFileURL else {
            resultHandler(.failure(.localURLIsEmpty))
            return
        }
        resultHandler(.success(.init(url: videoURL, compressUrl: firstFrameURL, mediaType: .video)))
    }
    
    // 设置model对应的imageOriginalData
    func setModelImageOriginalData(completion: @escaping () -> Void)  {
        guard let asset = asset else {
            return
        }
        _ = AssetManager.fetchOriginalImageData(for: asset, completion: { [weak self] data, _, _ in
            self?.imageOriginalData = data
            DispatchQueue.main.async {
                completion()
            }
        })
    }
}

