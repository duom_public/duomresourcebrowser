//
//  Array+PhotoWrapper.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/3.
//

import Foundation
import Photos
import DuomBase

public extension Array where Element: PhotoModel {
    typealias URLHandler = (Result<AssetURLResult, AssetError>, PhotoModel, Int) -> Void
    typealias completionHandler = ([(URL, URL?)]) -> Void
    
    /// 获取已选资源的地址
    /// - Parameters:
    ///   - compression: 压缩参数， nil 则原图
    ///   - urlReceivedHandler: 获取到的url的回调
    ///        -result 获取的结果
    ///            - model
    ///            - 索引
    ///   - complection: 全部获取完成
    ///       -urls: url集合
    ///        $0: 资源路径  $1: 图片压缩路径
    func getURLs(compression: PhotoModel.Compression? = nil,
                 urlReceivedHandler handler: URLHandler? = nil,
                 completion: @escaping completionHandler) {
        
        getURLsBySingleThread(compression: compression, urlReceivedHandler: handler, completion: completion)
//        getURLsByMultiThreads(compression: compression, urlReceivedHandler: handler, completion: completion)
    }
    
    private func getURLsBySingleThread(compression: PhotoModel.Compression? = nil,
                                       urlReceivedHandler handler: URLHandler? = nil,
                                       completion: @escaping completionHandler) {
        Logger.log(message: "单线程处理", level: .warning)
        let group = DispatchGroup()
        let queue = DispatchQueue(label: "hkphoto.request.urls")
        var urls: [(URL, URL?)] = []
        
        var before: Int64 = 0
        before = CLongLong(Date().timeIntervalSince1970 * 1000)
        for (index, hkModel) in enumerated() {
            Logger.log(message: "第\(index)个", level: .warning)
            queue.async(group: group, execute: DispatchWorkItem(block: {
                /// 同步获取
                let semaphore = DispatchSemaphore(value: 0)
                var mediaType = hkModel.mediaType

                let resultHandler: PhotoModel.AssetURLCompletion = { result in
                    switch result {
                    case .success(let response):
                        urls.append((response.url, response.compressUrl))
                    case .failure(_):
                        break
                    }

                    handler?(result, hkModel, index)
                    semaphore.signal()
                }
                
                if mediaType == .image {
                    hkModel.getImageURL(compressionQuality: compression?.imageCompressionQuality) {
                        resultHandler($0)
                    }
                } else if mediaType == .livePhoto {
                    hkModel.getLivePhotoURL(compression: compression) {
                        resultHandler($0)
                    }
                } else if mediaType == .video {
                    hkModel.getVideoURL(exportPreset: compression?.videoExportPreset) {
                        resultHandler($0)
                    }
                } else if mediaType == .gif {
                    // gif不压缩
                    hkModel.getImageURL(compressionQuality: nil) {
                        resultHandler($0)
                    }
                } else if mediaType == .giphyGif {
                    hkModel.getGiphyGifURL {
                        resultHandler($0)
                    }
                }
                
                semaphore.wait()
            }))
        }

        group.notify(queue: .main) {
            Logger.log(message: "单线程耗时: \(CLongLong(Date().timeIntervalSince1970 * 1000) - before)", level: .info)
            completion(urls)
        }
    }
    
    private func getURLsByMultiThreads(compression: PhotoModel.Compression? = nil,
                                       urlReceivedHandler handler: URLHandler? = nil,
                                       completion: @escaping completionHandler)  {
        Logger.log(message: "多线程处理", level: .warning)
        let group = DispatchGroup()
        let queue = DispatchQueue(label: "hkphoto.request.urls")
        var urls: [(URL, URL?)] = []
        
        var before: Int64 = 0
        before = CLongLong(Date().timeIntervalSince1970 * 1000)
        for (index, hkModel) in enumerated() {
            group.enter()
            queue.async(group: group, execute: DispatchWorkItem(block: {
                Logger.log(message: "第\(index)个", level: .warning)
                var mediaType = hkModel.mediaType

                let resultHandler: PhotoModel.AssetURLCompletion = { result in
                    switch result {
                    case .success(let response):
                        urls.append((response.url, response.compressUrl))
                    case .failure(_):
                        break
                    }
                    handler?(result, hkModel, index)
                    group.leave()
                }

                if mediaType == .image {
                    hkModel.getImageURL(compressionQuality: compression?.imageCompressionQuality) {
                        resultHandler($0)
                    }
                } else if mediaType == .livePhoto {
                    hkModel.getLivePhotoURL(compression: compression) {
                        resultHandler($0)
                    }
                } else if mediaType == .video {
                    hkModel.getVideoURL(exportPreset: compression?.videoExportPreset) {
                        resultHandler($0)
                    }
                } else if mediaType == .gif {
                    // gif不压缩
                    hkModel.getImageURL(compressionQuality: nil) {
                        resultHandler($0)
                    }
                } else if mediaType == .giphyGif {
                    hkModel.getGiphyGifURL {
                        resultHandler($0)
                    }
                }
            }))
        }
        
       group.notify(queue: .main) {
           Logger.log(message: "耗时: \(CLongLong(Date().timeIntervalSince1970 * 1000) - before)", level: .info)
           completion(urls)
       }
    }
    
}
