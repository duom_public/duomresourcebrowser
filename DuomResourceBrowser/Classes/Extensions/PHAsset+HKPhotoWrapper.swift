//
//  PHAsset+Extension.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/1.
//

import Foundation
import Photos

extension DPhotoWrapper where Base: PHAsset{
    var resource: PHAssetResource? {
        return PHAssetResource.assetResources(for: base).first
    }
    
    var isLocallyAvailable: Bool {
        guard let resource = resource else {
            return false
        }
        return resource.value(forKey: "locallyAvailable") as? Bool ?? false
    }
    
    
    func assetIsHEIF() -> Bool {
        var isHEIF: Bool = false
        let resourceList = PHAssetResource.assetResources(for: base)
        
        for (_, obj) in resourceList.enumerated() {
            if let objRes = obj as? PHAssetResource {
                let UTI = objRes.uniformTypeIdentifier
                if UTI == "public.heif" || UTI == "public.heic" {
                    return true
                }
            }
        }
        return false
    }
    
    func assetIsRAW() -> Bool {
        var isHEIF: Bool = false
        let resourceList = PHAssetResource.assetResources(for: base)
        
        for (_, obj) in resourceList.enumerated() {
            if #available(iOS 14, *) {
                if let objRes = obj as? PHAssetResource {
                    if let utiType = UTType(objRes.uniformTypeIdentifier), utiType.conforms(to: .rawImage) {
                        return true
                    }
                }
            } else {
                if let objRes = obj as? PHAssetResource {
                    let UTI = objRes.uniformTypeIdentifier
                    if UTI == "com.adobe.raw-image" {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func assetIsJPG() -> Bool {
        let resourceList = PHAssetResource.assetResources(for: base)
        
        for (_, obj) in resourceList.enumerated() {
            if let objRes = obj as? PHAssetResource {
                let UTI = objRes.uniformTypeIdentifier
                if UTI == "public.jpeg" || UTI == "public.jpg" {
                    return true
                }
            }
        }
        return false
    }
    
    func assetIsPNG() -> Bool {
        let resourceList = PHAssetResource.assetResources(for: base)
        
        for (_, obj) in resourceList.enumerated() {
            if let objRes = obj as? PHAssetResource {
                let UTI = objRes.uniformTypeIdentifier
                if UTI == "public.png" {
                    return true
                }
            }
        }
        return false
    }
    
}
