//
//  UIImage+Extension.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/4.
//

import Foundation
import UIKit
import ImageIO
import CoreGraphics
import MobileCoreServices
import Accelerate

extension UIImage {
    var width: CGFloat {
        size.width
    }
    var height: CGFloat {
        size.height
    }
    
    func scaleSuitableSize() -> UIImage? {
        var imageSize = self.size
        while imageSize.width * imageSize.height > 3 * 1000 * 1000 {
            imageSize.width *= 0.5
            imageSize.height *= 0.5
        }
        return self.scaleToFillSize(size: imageSize)
    }
    func scaleToFillSize(size: CGSize, equalRatio: Bool = false, scale: CGFloat = 0) -> UIImage? {
        if __CGSizeEqualToSize(self.size, size) {
            return self
        }
        let scale = scale == 0 ? self.scale : scale
        let rect: CGRect
        if size.width / size.height != width / height && equalRatio {
            let scale = size.width / width
            var scaleHeight = scale * height
            var scaleWidth = size.width
            if scaleHeight < size.height {
                scaleWidth = size.height / scaleHeight * size.width
                scaleHeight = size.height
            }
            rect = CGRect(
                x: -(scaleWidth - size.height) * 0.5,
                y: -(scaleHeight - size.height) * 0.5,
                width: scaleWidth,
                height: scaleHeight
            )
        }else {
            rect = CGRect(origin: .zero, size: size)
        }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        self.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    func scaleImage(toScale: CGFloat) -> UIImage? {
        if toScale == 1 {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: width * toScale, height: height * toScale),
            false,
            self.scale
        )
        self.draw(in: CGRect(x: 0, y: 0, width: width * toScale, height: height * toScale))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    class func image(for color: UIColor?, havingSize: CGSize, radius: CGFloat = 0) -> UIImage? {
        if let color = color {
            let rect: CGRect
            if havingSize.equalTo(CGSize.zero) {
                rect = CGRect(x: 0, y: 0, width: 1, height: 1)
            }else {
                rect = CGRect(x: 0, y: 0, width: havingSize.width, height: havingSize.height)
            }
            UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
            let context = UIGraphicsGetCurrentContext()
            context?.setFillColor(color.cgColor)
            let path = UIBezierPath(roundedRect: rect, cornerRadius: radius).cgPath
            context?.addPath(path)
            context?.fillPath()
        
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }
        return nil
    }
    
    func normalizedImage() -> UIImage? {
        if imageOrientation == .up {
            return self
        }
        return repaintImage()
    }
    func repaintImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    func roundCropping() -> UIImage? {
        UIGraphicsBeginImageContext(size)
        let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        path.addClip()
        draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    func cropImage(toRect cropRect: CGRect, viewWidth: CGFloat, viewHeight: CGFloat) -> UIImage? {
        if cropRect.isEmpty {
            return self
        }
        let imageViewScale = max(size.width / viewWidth,
                                 size.height / viewHeight)

        // Scale cropRect to handle images larger than shown-on-screen size
        let cropZone = CGRect(
            x: cropRect.origin.x * imageViewScale,
            y: cropRect.origin.y * imageViewScale,
            width: cropRect.size.width * imageViewScale,
            height: cropRect.size.height * imageViewScale
        )

        // Perform cropping in Core Graphics
        guard let cutImageRef: CGImage = cgImage?.cropping(to: cropZone)
        else {
            return nil
        }

        // Return image to UIImage
        let croppedImage: UIImage = UIImage(cgImage: cutImageRef)
        return croppedImage
    }
    
    func rotation(to orientation: UIImage.Orientation) -> UIImage? {
        if let cgImage = cgImage {
            func swapWidthAndHeight(_ toRect: CGRect) -> CGRect {
                var rect = toRect
                let swap = rect.width
                rect.size.width = rect.height
                rect.size.height = swap
                return rect
            }
            var rect = CGRect(x: 0, y: 0, width: cgImage.width, height: cgImage.height)
            while rect.width * rect.height > 3 * 1000 * 1000 {
                rect.size.width = rect.width * 0.5
                rect.size.height = rect.height * 0.5
            }
            var trans: CGAffineTransform
            var bnds = rect
            switch orientation {
            case .up:
                return self
            case .upMirrored:
                trans = .init(translationX: rect.width, y: 0)
                trans = trans.scaledBy(x: -1, y: 1)
            case .down:
                trans = .init(translationX: rect.width, y: rect.height)
                trans = trans.rotated(by: CGFloat.pi)
            case .downMirrored:
                trans = .init(translationX: 0, y: rect.height)
                trans = trans.scaledBy(x: 1, y: -1)
            case .left:
                bnds = swapWidthAndHeight(bnds)
                trans = .init(translationX: 0, y: rect.width)
                trans = trans.rotated(by: 3 * CGFloat.pi * 0.5)
            case .leftMirrored:
                bnds = swapWidthAndHeight(bnds)
                trans = .init(translationX: rect.height, y: rect.width)
                trans = trans.scaledBy(x: -1, y: 1)
                trans = trans.rotated(by: 3 * CGFloat.pi * 0.5)
            case .right:
                bnds = swapWidthAndHeight(bnds)
                trans = .init(translationX: rect.height, y: 0)
                trans = trans.rotated(by: CGFloat.pi * 0.5)
            case .rightMirrored:
                bnds = swapWidthAndHeight(bnds)
                trans = .init(scaleX: -1, y: 1)
                trans = trans.rotated(by: CGFloat.pi * 0.5)
            default:
                return self
            }
            UIGraphicsBeginImageContext(bnds.size)
            let context = UIGraphicsGetCurrentContext()
            switch orientation {
            case .left, .leftMirrored, .right, .rightMirrored:
                context?.scaleBy(x: -1, y: 1)
                context?.translateBy(x: -rect.height, y: 0)
            default:
                context?.scaleBy(x: 1, y: -1)
                context?.translateBy(x: 0, y: -rect.height)
            }
            context?.concatenate(trans)
            context?.draw(cgImage, in: rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
    func rotation(angle: Int, isHorizontal: Bool) -> UIImage? {
        switch angle {
        case 0, 360, -360:
            if isHorizontal {
                return rotation(to: .upMirrored)
            }
        case 90, -270:
            if !isHorizontal {
                return rotation(to: .right)
            }else {
                return rotation(to: .rightMirrored)
            }
        case 180, -180:
            if !isHorizontal {
                return rotation(to: .down)
            }else {
                return rotation(to: .downMirrored)
            }
        case 270, -90:
            if !isHorizontal {
                return rotation(to: .left)
            }else {
                return rotation(to: .leftMirrored)
            }
        default:
            break
        }
        return self
    }
    func merge(_ image: UIImage, origin: CGPoint, scale: CGFloat = UIScreen.main.scale) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(in: CGRect(origin: .zero, size: size))
        image.draw(in: CGRect(origin: origin, size: size))
        let mergeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return mergeImage
    }
    func merge(images: [UIImage], scale: CGFloat = UIScreen.main.scale) -> UIImage? {
        if images.isEmpty {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(in: CGRect(origin: .zero, size: size))
        for image in images {
            image.draw(in: CGRect(origin: .zero, size: size))
        }
        let mergeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return mergeImage
    }
    
    class func merge(images: [UIImage], scale: CGFloat = UIScreen.main.scale) -> UIImage? {
        if images.isEmpty {
            return nil
        }
        if images.count == 1 {
            return images.first
        }
        UIGraphicsBeginImageContextWithOptions(images.first!.size, false, scale)
        for image in images {
            image.draw(in: CGRect(origin: .zero, size: image.size))
        }
        let mergeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return mergeImage
    }
    
    
    /// 修复转向
    func fixOrientation() -> UIImage {
        if self.imageOrientation == .up {
            return self
        }
        
        var transform = CGAffineTransform.identity
        
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = CGAffineTransform(translationX: width, y: height)
            transform = transform.rotated(by: .pi)
        case .left, .leftMirrored:
            transform = CGAffineTransform(translationX: width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2)
        case .right, .rightMirrored:
            transform = CGAffineTransform(translationX: 0, y: height)
            transform = transform.rotated(by: -CGFloat.pi / 2)
        default:
            break
        }
        
        switch self.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default:
            break
        }
        
        guard let cgImage = self.cgImage, let colorSpace = cgImage.colorSpace else {
            return self
        }
        let context = CGContext(
            data: nil,
            width: Int(width),
            height: Int(height),
            bitsPerComponent: cgImage.bitsPerComponent,
            bytesPerRow: 0,
            space: colorSpace,
            bitmapInfo: cgImage.bitmapInfo.rawValue
        )
        context?.concatenate(transform)
        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            context?.draw(cgImage, in: CGRect(x: 0, y: 0, width: height, height: width))
        default:
            context?.draw(cgImage, in: CGRect(x: 0, y: 0, width: width, height: height))
        }
        
        guard let newCgImage = context?.makeImage() else {
            return self
        }
        return UIImage(cgImage: newCgImage)
    }
    
    
    /// Processing speed is better than resize(:) method
    func resize_vI(_ size: CGSize) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        var format = vImage_CGImageFormat(
            bitsPerComponent: 8,
            bitsPerPixel: 32,
            colorSpace: nil,
            bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.first.rawValue),
            version: 0,
            decode: nil,
            renderingIntent: .defaultIntent
        )
        
        var sourceBuffer = vImage_Buffer()
        defer {
            if #available(iOS 13.0, *) {
                sourceBuffer.free()
            } else {
                sourceBuffer.data.deallocate()
            }
        }
        
        var error = vImageBuffer_InitWithCGImage(&sourceBuffer, &format, nil, cgImage, numericCast(kvImageNoFlags))
        guard error == kvImageNoError else { return nil }
        
        let destWidth = Int(size.width)
        let destHeight = Int(size.height)
        let bytesPerPixel = cgImage.bitsPerPixel / 8
        let destBytesPerRow = destWidth * bytesPerPixel
        
        let destData = UnsafeMutablePointer<UInt8>.allocate(capacity: destHeight * destBytesPerRow)
        defer {
            destData.deallocate()
        }
        var destBuffer = vImage_Buffer(data: destData, height: vImagePixelCount(destHeight), width: vImagePixelCount(destWidth), rowBytes: destBytesPerRow)
        
        // scale the image
        error = vImageScale_ARGB8888(&sourceBuffer, &destBuffer, nil, numericCast(kvImageHighQualityResampling))
        guard error == kvImageNoError else { return nil }
        
        // create a CGImage from vImage_Buffer
        guard let destCGImage = vImageCreateCGImageFromBuffer(&destBuffer, &format, nil, nil, numericCast(kvImageNoFlags), &error)?.takeRetainedValue() else { return nil }
        guard error == kvImageNoError else { return nil }
        
        // create a UIImage
        return UIImage(cgImage: destCGImage, scale: self.scale, orientation: self.imageOrientation)
    }
    
    func clipImage(angle: CGFloat, editRect: CGRect, isCircle: Bool? = false) -> UIImage? {
        let a = ((Int(angle) % 360) - 360) % 360
        var newImage: UIImage = self
        if a == -90 {
            newImage = rotation(to: .left)!
        } else if a == -180 {
            newImage = rotation(to: .down)!
        } else if a == -270 {
            newImage = rotation(to: .right)!
        }
        guard editRect.size != newImage.size else {
            return newImage
        }
        let origin = CGPoint(x: -editRect.minX, y: -editRect.minY)
        UIGraphicsBeginImageContextWithOptions(editRect.size, false, newImage.scale)
        let context = UIGraphicsGetCurrentContext()
        if let isCircle = isCircle, isCircle {
            context?.addEllipse(in: CGRect(origin: .zero, size: editRect.size))
            context?.clip()
        }
        newImage.draw(at: origin)
        let temp = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgi = temp?.cgImage else {
            return temp
        }
        let clipImage = UIImage(cgImage: cgi, scale: newImage.scale, orientation: .up)
        return clipImage
    }
    
    
    func clipCircleImage(angle: CGFloat, editRect: CGRect) -> UIImage? {
        let a = ((Int(angle) % 360) - 360) % 360
        var newImage: UIImage = self
        if a == -90 {
            newImage = rotation(to: .left)!
        } else if a == -180 {
            newImage = rotation(to: .down)!
        } else if a == -270 {
            newImage = rotation(to: .right)!
        }
        
        let origin = CGPoint(x: -editRect.minX, y: -editRect.minY)
        UIGraphicsBeginImageContextWithOptions(editRect.size, false, newImage.scale)
        let context = UIGraphicsGetCurrentContext()
        context?.addEllipse(in: CGRect(origin: .zero, size: CGSize(width: min(editRect.width, editRect.height), height: min(editRect.width, editRect.height))))
        context?.clip()
        newImage.draw(at: origin)
        let temp = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgi = temp?.cgImage else {
            return temp
        }
        let clipImage = UIImage(cgImage: cgi, scale: newImage.scale, orientation: .up)
        return clipImage
    }
}
