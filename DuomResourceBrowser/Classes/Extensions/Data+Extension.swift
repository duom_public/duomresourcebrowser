//
//  Data+Extension.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/3.
//

import Foundation

extension Data {
    var imageContentType: ImageContentType {
        var values = [UInt8](repeating: 0, count: 1)
        copyBytes(to: &values, count: 1)
        switch values[0] {
        case 0xFF:
            return .jpg
        case 0x89:
            return .png
        case 0x47, 0x49, 0x46:
            return .gif
        default:
            return .unknown
        }
    }
    
    var isGif: Bool {
        imageContentType == .gif
    }
}


public enum ImageContentType: String {
    case jpg, png, gif, unknown
    public var fileExtension: String {
        return self.rawValue
    }
}
