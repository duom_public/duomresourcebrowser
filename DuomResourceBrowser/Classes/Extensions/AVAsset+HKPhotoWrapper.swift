//
//  AVAsset+HKPhotoWrapper.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/7/27.
//

import Foundation
import Photos
import CoreMedia
import AVFoundation
import DuomBase

extension DPhotoWrapper where Base: AVAsset{
    func isHEVC() -> Bool {
        for track in base.tracks {
            for formatDescription in track.formatDescriptions {
                let codecType = CMFormatDescriptionGetMediaSubType(formatDescription as! CMFormatDescription)
                if codecType == kCMVideoCodecType_HEVC {
                    return true
                }
            }
        }
        return false
    }
    
    func audioIsEmpty() -> Bool {
        let audioTrack = base.tracks(withMediaType: .audio)
        if audioTrack.isEmpty {
            return true
        } else {
            return false
        }
    }
    
    func degress() -> Int {
        var degress = 0
        guard let tracks = base.tracks(withMediaType: .video).first else {
            return degress
        }
        
        let t = tracks.preferredTransform
        if (t.a == 0 && t.b == 1.0 && t.c == -1.0 && t.d == 0) {
            degress = 90
        } else if (t.a == 0 && t.b == -1.0 && t.c == 1.0 && t.d == 0) {
            degress = 270
        } else if (t.a == 1.0 && t.b == 0 && t.c == 0 && t.d == 1.0) {
            degress = 0
        } else if (t.a == -1.0 && t.b == 0 && t.c == 0 && t.d == -1.0) {
            degress = 180
        }
        return degress
    }
}
