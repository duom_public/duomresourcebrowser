//
//  BrowserPickerInterface.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/13.
//  对外暴露方法接口
//

import Foundation
import UIKit
import DuomBase
import RxCocoa
import RxSwift
import Photos
import GiphyUISDK

public class ImagePickerInterface {
    static let disposeBag = DisposeBag()
    /// 打开资源选择器
    /// - Parameters:
    ///   - selectedList: 已选资源
    ///   - imgMaxNum: 最大可选几张image
    ///   - videoMaxNum: 最大可选视频数量
    ///   - selectType: 资源选择类型
    ///   - imageQualityCompress: 图片压缩后最大值 单位KB
    ///   - imageCropRatio: 图片裁剪比例 1: (4:5)  2: (5:4)  3: (1:1)  4: (9:16)  5: (16:9)
    ///   - cancelHandler: 取消回调
    ///   - completion: 选择完成回调
    public static func openBrowserPicker(selectedList: [[String: Any]]?,
                                         imgMaxNum: Int, videoMaxNum: Int,
                                         selectType: Int,
                                         imageCompressSize: CGFloat? = nil,
                                         imageCropRatio: Int? = nil,
                                         showGif: Bool,
                                         cancelHandler: (() -> Void)?,
                                         completion: @escaping ([[String: Any]]) -> Void)  {
        
        self.saveConfiguration(imgMaxNum: imgMaxNum, videoMaxNum: videoMaxNum, selectType: selectType, imageCompressSize: imageCompressSize, imageCropRatio: imageCropRatio, showGif: showGif)
        self.saveDefaultSources(selectedList: selectedList)
        self.transToPhotoModel()
        if selectType != 4 { // 非拍照场景
            AuthorizationHelper.checkAuthorizationWithOutAlert(option: [.albumPermission])
                .subscribe(onCompleted: {
                    gotoMainVC(cancelHandler: cancelHandler, completion: completion)
                }, onError: { _ in
                    gotoMainVC(cancelHandler: cancelHandler, completion: completion)
                })
                .disposed(by: disposeBag)
        } else {
            gotoMainVC(cancelHandler: cancelHandler, completion: completion)
        }
    }
    
    /// 打开资源选择器 直接进入编辑页面
    /// - Parameters:
    ///   - selectedList: 已选资源
    ///   - imgMaxNum: 最大可选几张image
    ///   - imageQualityCompress: 图片压缩后最大值 单位KB
    ///   - cancelHandler: 取消回调
    ///   - completion: 选择完成回调
    public static func openBrowserPickerToEdit(selectedList: [[String: Any]]?,
                                               imgMaxNum: Int,
                                               imageCompressSize: CGFloat? = nil,
                                               index: Int = 0,
                                               showGif: Bool,
                                               cancelHandler: (() -> Void)?,
                                               completion: @escaping ([[String: Any]]) -> Void)  {
        
        self.saveConfiguration(imgMaxNum: imgMaxNum, imageCompressSize: imageCompressSize, showGif: showGif)
        self.saveDefaultSources(selectedList: selectedList)
        self.transToPhotoModel()
        AuthorizationHelper.checkAuthorizationWithOutAlert(option: [.albumPermission])
            .subscribe(onCompleted: {
                gotoMainVC(toEdit: true, index: index, cancelHandler: cancelHandler, completion: completion)
            }, onError: { _ in
                gotoMainVC(toEdit: true, index: index, cancelHandler: cancelHandler, completion: completion)
            })
            .disposed(by: disposeBag)
        
    }
    
    // 清除本地数据
    public static func clearCache() {
        Logger.log(message: "clear picker cache", level: .info)
        PhotoTools.removeCache()
    }
    
    // 保存到相册
    public static func saveToAlbum(selectedList: [[String: Any]]?, completion: @escaping () -> Void) {
        guard let selectedList = selectedList, selectedList.isNotEmpty else {
            return
        }
        
        var images = [UIImage]()
        selectedList.forEach{
            if let model = toModel($0) {
                if model.editId != nil || model.isSnapShot || model.isTakePhoto {
                    if model.imagePath.isNotEmpty,
                       let imgData = PhotoTools.readFileDataFrom(filePath: PhotoTools.getAbsolutePath(model.imagePath)),
                       let tempImg = UIImage(data: imgData) {
                        images.append(tempImg)
                    }
                }
            }
        }
        
        guard images.isNotEmpty else { return }
        
        let group = DispatchGroup()
        let queue = DispatchQueue(label: "hkphoto.save.album")

        var before: Int64 = 0
        before = CLongLong(Date().timeIntervalSince1970 * 1000)
        for (idx, item) in images.enumerated() {
            group.enter()
            queue.async(group: group, execute: DispatchWorkItem(block: {
//                Logger.log(message: "第\(idx)个保存中>>>>>>", level: .info)
                AssetManager.saveImageToAlbum(image: item) { suc, phAsset in
                    if !suc {
                        Logger.log(message: "第\(idx)个保存失败了<<<<<<<<<<<", level: .info)
                    }
                    group.leave()
                }
            }))
        }
        
        group.notify(queue: .main) {
//            Logger.log(message: "全部保存完了>>>>>>", level: .info)
//            clearCache()
            completion()
        }
    }
    
    // 关闭资源选择器
    public static func closeImagePicker() {
        let topNav = UIApplication.topViewController()?.navigationController as? PhotoNavgationController
        topNav?.dismiss(animated: true, completion: { [weak topNav] in
            SourceInstance.shared.cancelBlock?()
        })
    }
}


// MARK: private funs
extension ImagePickerInterface {
    private static func gotoMainVC(toEdit: Bool = false,
                                   index: Int = 0,
                                   cancelHandler: (() -> Void)?,
                                   completion: @escaping ([[String: Any]]) -> Void) {
        var vc: UIViewController = UIViewController()
        if toEdit {
            vc = EditorController()
        } else {
            if PhotosConfiguration.default().selectType == .image {
                vc = PresentMainAlbumViewController()
            } else if PhotosConfiguration.default().selectType == .takePhoto {
                vc = CameraController(isMain: true)
            } else {
                vc = ImagePickerMainController()
            }
        }
        
        if let vc = vc as? EditorController, index != 0 {
            vc.setIndex(index)
        }
        
        let nav = PhotoNavgationController.init(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        UIApplication.topViewController()?.showDetailViewController(nav, sender: nil)
        
        var datas = [OutputModel]()
        SourceInstance.shared.cancelBlock = {
            SourceInstance.shared.selectModels.removeAll()
            cancelHandler?()
        }
        SourceInstance.shared.selectedSourceBlock = { models in
            guard let models = models else {
                return
            }
            let hud = DProgressHUD.init(style: .darkBlur)
            hud.show()
            models.getURLs(compression: PhotoModel.Compression(imageCompressionQuality: PhotosConfiguration.default().imageQualityCount,
                                                               videoExportPreset: .highQuality)) { result, model, index in
                var outModel = OutputModel()
                outModel = transToOutModel(model)
                outModel.index = index
                switch result {
                case .success(let response):
                    if model.mediaType == .video {
                        outModel.videoPath = PhotoTools.getRelativePath("\(response.url)")
                        outModel.imagePath = PhotoTools.getRelativePath("\(response.compressUrl)")
                    } else if model.mediaType == .giphyGif {
                        outModel.imagePath = "\(response.url)"
                    } else {
                        if model.mediaType == .image, model.isSnapShot {
                            outModel.imagePath = "\(response.url)"
                        } else {
                            outModel.imagePath = PhotoTools.getRelativePath("\(response.url)")
                        }
                    }
                    
                    if let compress = response.compressUrl {
                        outModel.imageCompressPath = PhotoTools.getRelativePath("\(compress)")
                    } else {
                        if model.mediaType == .gif  { // gif压缩地址和原地址一致(不压缩)
                            outModel.imageCompressPath = PhotoTools.getRelativePath("\(response.url)")
                        } else if model.mediaType == .giphyGif {
                            outModel.imageCompressPath = "\(response.url)"
                        }
                    }
                case .failure(let error):
                    print("getURL error: \(error)")
                }
                datas.append(outModel)
            } completion: { urls in
                hud.hide()
                SourceInstance.shared.clear()
                var sortDatas = [[String: Any]]()
                let sortModels = datas.sorted(by: { $0.index < $1.index })
                sortModels.forEach {
                    sortDatas.append($0.toDic())
                }
                DispatchQueue.main.async {
                    completion(sortDatas)
                }
            }
        }
    }
    
    
    // 设置整体configuration
    private static func saveConfiguration(imgMaxNum: Int = 15, videoMaxNum: Int = 1, selectType: Int = 0, imageCompressSize: CGFloat? = 2, imageCropRatio: Int? = nil, showGif: Bool = false) {
        PhotosConfiguration.default().maxSelectCount = imgMaxNum
        PhotosConfiguration.default().maxVideoSelectCount = videoMaxNum
        if let imageCompressSize = imageCompressSize {
            PhotosConfiguration.default().imageQualityCount = imageCompressSize * 1024
        }
        
        if let imageCropRatio = imageCropRatio {
            var clipRatio: ImageClipRatio = ImageClipRatio.wh1x1
            switch imageCropRatio {
            case 0:  // 不裁剪 不编辑
                PhotosConfiguration.default().allowEdit = false
            case 1:
                clipRatio = ImageClipRatio.wh4x5
                PhotosConfiguration.default().allowEdit = true
            case 2:
                clipRatio = ImageClipRatio.wh5x4
                PhotosConfiguration.default().allowEdit = true
            case 3:
                clipRatio = ImageClipRatio.wh1x1
                PhotosConfiguration.default().allowEdit = true
            case 4:
                clipRatio = ImageClipRatio.wh9x16
                PhotosConfiguration.default().allowEdit = true
            case 5:
                clipRatio = ImageClipRatio.wh16x9
                PhotosConfiguration.default().allowEdit = true
            case 6:
                clipRatio = ImageClipRatio.circle
                PhotosConfiguration.default().allowEdit = true
            default:
                clipRatio = ImageClipRatio.wh1x1
                PhotosConfiguration.default().allowEdit = true
            }
            PhotosConfiguration.default().editImageConfiguration.assignClipRations = clipRatio
        }
        
        // 默认false
        PhotosConfiguration.default().selectSingleImg = false
        // 0全部 1图片和拍照 2视频 3图片 4拍照
        switch selectType {
        case 0:
            PhotosConfiguration.default().selectType = .all
            PhotosConfiguration.default().allowSelectVideo = true
            PhotosConfiguration.default().allowSelectImage = true
        case 1:
            PhotosConfiguration.default().selectType = .imageAndTakePhoto
            PhotosConfiguration.default().allowSelectVideo = false
        case 2:
            PhotosConfiguration.default().selectType = .video
            PhotosConfiguration.default().allowSelectImage = false
        case 3:
            PhotosConfiguration.default().selectType = .image
            PhotosConfiguration.default().allowSelectVideo = false
            //单选照片模式
            if imgMaxNum == 1 {
                PhotosConfiguration.default().selectSingleImg = true
            }
        case 4:
            PhotosConfiguration.default().selectType = .takePhoto
            PhotosConfiguration.default().allowSelectVideo = false
            //单选照片模式
            if imgMaxNum == 1 {
                PhotosConfiguration.default().selectSingleImg = true
            }
        default:
            PhotosConfiguration.default().selectType = .all
            PhotosConfiguration.default().allowSelectVideo = true
            PhotosConfiguration.default().allowSelectImage = true
        }
        
        PhotosConfiguration.default().showGif = showGif
    }
    
    // 保存外部传入数据(已选数据)
    private static func saveDefaultSources(selectedList: [[String: Any]]?) {
        SourceInstance.shared.cancelBlock = nil
        SourceInstance.shared.selectedSourceBlock = nil
        SourceInstance.shared.selectModels.removeAll()
        SourceInstance.shared.outSelectedModels.removeAll()
        
        var selectSources = [OutputModel]()
        selectedList?.forEach{
            if let model = toModel($0) {
                selectSources.append(model)
            }
        }
        SourceInstance.shared.outSelectedModels = selectSources
    }
    
    private static func transToOutModel(_ model: PhotoModel) -> OutputModel {
        var outModel = OutputModel()
        if let ident = model.ident {
            outModel.id = ident
        }
        outModel.duration = model.second
        outModel.width = model.width
        outModel.height = model.height
        outModel.isSnapShot = model.isSnapShot
        outModel.isTakePhoto = model.isTakedPhoto
        outModel.editId = model.editId
        if let gifThumb = model.gifThumb {
            outModel.thumb = gifThumb
        }
        
        switch model.mediaType {
        case .image:
            outModel.mediaType = .image
        case .video:
            outModel.mediaType = .video
        case .livePhoto:
            outModel.mediaType = .livePhoto
        case .giphyGif:
            outModel.mediaType = .giphyGif
        default:
            outModel.mediaType = .image
        }
        return outModel
    }
    
    private static func transToPhotoModel(_ outModel: OutputModel) -> PhotoModel {
        var tempModel = PhotoModel(asset: nil)
        tempModel.ident = outModel.id
        tempModel.editId = outModel.editId
        tempModel.isSelected = true
        tempModel.isSnapShot = outModel.isSnapShot
        
        switch outModel.mediaType.rawValue {
        case 0:
            tempModel.mediaType = .image
        case 1:
            tempModel.mediaType = .video
        case 2:
            tempModel.mediaType = .livePhoto
        case 3:
            tempModel.mediaType = .giphyGif
        default:
            tempModel.mediaType = .image
        }
        
        if tempModel.mediaType == .video {
            tempModel.fileURL = URL(string: PhotoTools.getAbsolutePath(outModel.imagePath))
        } else {
            if tempModel.mediaType == .giphyGif {
                tempModel.fileURL = URL(string: outModel.imagePath)
            } else {
                tempModel.fileURL = URL(string: PhotoTools.getAbsolutePath(outModel.imagePath))
            }
        }
        
        if outModel.mediaType == .giphyGif {
            tempModel.gifThumb = outModel.thumb
        } else if outModel.isSnapShot || outModel.mediaType == .image || outModel.mediaType == .livePhoto {
            tempModel.imageOriginalData = PhotoTools.readFileDataFrom(filePath: PhotoTools.getAbsolutePath(outModel.imagePath))
        } else {
            AssetManager.fetchAssetWith(ids: [outModel.id]).enumerateObjects { asset, idx, _ in
                tempModel.asset = asset
                tempModel.setModelImageOriginalData {
                    Logger.log(message: "\(tempModel.ident)== 获取data", level: .info)
                }
            }
        }
        
        tempModel.width = outModel.width
        tempModel.height = outModel.height
        
        if outModel.mediaType == .image || outModel.mediaType == .livePhoto {
            getEditInfo(tempModel)
        }
        
        return tempModel
    }
    
    private static func getEditInfo(_ model: PhotoModel)  {
        guard let editId = model.editId else { return }
        let editURL = PhotoTools.getImageEditCacheURL("\(editId)")
        if let editData = PhotoTools.readFileDataFrom(filePath: editURL.path),
            let editInfo = try? JSONDecoder().decode(EditImageModel.self, from: editData) {
            model.editImageModel = editInfo
        } else {
            Logger.log(message: "editInfo 读取失败了", level: .warning)
        }
    }
    
    
    // outModel -> PhotoModle 赋值给SourceInstance.shared.selectModels
    private static func transToPhotoModel() {
        guard SourceInstance.shared.outSelectedModels.isNotEmpty else {
            SourceInstance.shared.selectModels = []
            SourceInstance.shared.backupModels = []
            return
        }
        var models = [PhotoModel]()
        
        SourceInstance.shared.outSelectedModels.forEach {
            models.append(transToPhotoModel($0))
        }
        
        SourceInstance.shared.selectModels = models
        SourceInstance.shared.backupModels = models
    }
    
    private static func toModel(_ dic: [String: Any]) -> OutputModel? {
        var safeDic = dic
        // 1.13-->1.14版本duration字段数据类型由string转为Int
        if let value = safeDic["duration"] as? String {
            safeDic["duration"] = value.isEmpty ? 0 : Int(value)
        }
        if let thumbDic = safeDic["thumb"] as? [String: Any], let thumbDuration = thumbDic["duration"] as? String {
            var tempDic = thumbDic
            tempDic["duration"] = thumbDuration.isEmpty ? 0 : Int(thumbDuration)
            safeDic["thumb"] = tempDic
        }
        let data = try? JSONSerialization.data(withJSONObject: safeDic, options: [.fragmentsAllowed])
        if let data = data {
            return try? JSONDecoder().decode(OutputModel.self, from: data)
        }
        return nil
    }
}

// MARK: language
public extension ImagePickerInterface {
    // 清除单例语言
    static func clearLanguageCache() {
        CopyWitterInstance.destory()
    }
}
