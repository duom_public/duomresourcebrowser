//
//  SnapShotInterface.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/7/5.
//

import Foundation

public class SnapShotInterface {
    public static func getSnapShotFile() -> [String: Any]? {
        guard let urlTuple = getScreenSnapShotFile() else {
            return nil
        }
        
        var outModel = OutputModel()
        outModel.id = "\(CLongLong(Date().timeIntervalSince1970 * 1000))"
        outModel.imagePath = PhotoTools.getRelativePath("\(urlTuple.0)")
        outModel.imageCompressPath = PhotoTools.getRelativePath("\(urlTuple.1)")
        outModel.mediaType = .image
        outModel.width = Int(UIScreen.screenWidth)
        outModel.height = Int(UIScreen.screenHeight)
        outModel.isSnapShot = true
        return outModel.toDic()
    }
}

