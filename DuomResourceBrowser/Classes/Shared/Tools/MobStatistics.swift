//
//  MobStatistics.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/7/26.
//

import Foundation
import DuomBase

public class MobStatistics {
    public static func trackAction(_ identifier: String) {
        guard !identifier.isEmpty else {
            Logger.log(message: "埋点identifier为空", level: .info)
            return
        }
        
        NotificationCenter.default.post(name: PhotosConfiguration.default().MobStatisticsNotification, object: identifier)
    }
}
