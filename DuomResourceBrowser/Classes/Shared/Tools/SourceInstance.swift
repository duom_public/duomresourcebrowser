//
//  SourceInstance.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/12.
//

import Foundation


public final class SourceInstance {
    public static let shared = SourceInstance()
    
    /// 外部传入内部的models
    public var outSelectedModels: [OutputModel] = [] 
    
    /// 内部已经选择好的models
    public var selectModels: [PhotoModel] = []
    
    /// 备份数据 用户整体恢复RN传入数据
    public var backupModels: [PhotoModel] = []
    
    public var selectedSourceBlock: (([PhotoModel]?) -> Void)?

    public var cancelBlock: (() -> Void)?
    
    public func clear() {
        outSelectedModels = []
        selectModels = []
        backupModels = []
    }
    
}
