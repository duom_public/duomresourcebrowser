//
//  Image+WaterMark.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/9/5.
//

import Foundation
import DuomBase
import UIKit
import ImageIO
import CoreGraphics

// MARK: 水印&to base64
public extension UIImage {
    public func waterMark(_ waterImage: UIImage,
                          text: String?,
                          location: WaterLocation,
                          hMargin: CGFloat,
                          vMarigin: CGFloat,
                          textSpace: CGFloat = 8) -> UIImage? {
        
        let textHeight:CGFloat = 18
        let imageScale = max(size.width / UIScreen.screenWidth, size.height / UIScreen.screenHeight)
        let whRatio = size.width / size.height
        UIGraphicsBeginImageContext(size)
        draw(in: CGRect(origin: .zero, size: size))
        
        if let _ = text {
            if whRatio > 1 {
                let backImg = UIImage(named: "rebuild_top_img")
                backImg!.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width, height: size.height * 0.3)))
            } else {
                let backImg = UIImage(named: "rebuild_bottom_img")
                backImg!.draw(in: CGRect(origin: CGPoint(x: 0, y: size.height - size.height * 0.4), size: CGSize(width: size.width, height: size.height * 0.4)))
            }
        }
     
        var origin: CGPoint = .zero
        var textAttributes: [NSAttributedString.Key: Any]?
        if let text = text {
            let paraStyle = NSMutableParagraphStyle()
            paraStyle.alignment = .right
            paraStyle.lineBreakMode = .byTruncatingTail
            textAttributes = [
                NSAttributedString.Key.font: UIFont(name: "TTCommons-Regular", size: 16 * imageScale),
                NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.6),
                NSAttributedString.Key.paragraphStyle: paraStyle,
            ]
        }
        
        var textRect: CGRect = CGRect.init(x: hMargin * imageScale, y: size.height - (34 + textHeight) * imageScale, width: size.width - (hMargin * 2) * imageScale, height: textHeight * imageScale)
        
        switch location {
        case .topLeft:
            origin = CGPoint(x: hMargin * imageScale, y: vMarigin * imageScale)
        case .topRight:
            origin = CGPoint(x: size.width - (waterImage.size.width + hMargin) * imageScale, y: vMarigin * imageScale)
        case .bottomLeft:
            origin = CGPoint(x: hMargin * imageScale, y: size.height - (waterImage.size.height + vMarigin) * imageScale)
        case .bottomRight:
            origin = CGPoint(x: size.width - (waterImage.size.width + hMargin) * imageScale, y: size.height - (waterImage.size.height + vMarigin) * imageScale)
            textRect.origin = CGPoint(x: hMargin * imageScale, y: origin.y - (textSpace + textHeight) * imageScale)
        }
        
        if let text = text, text.isNotEmpty, let textAttributes = textAttributes {
            text.draw(in: textRect, withAttributes: textAttributes)
        }
        waterImage.draw(in: CGRect(origin: origin, size: CGSize(width: waterImage.size.width * imageScale, height: waterImage.size.height * imageScale)))
        let mergeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return mergeImage
    }
    
    // Gif 水印
    public func waterMarkOnGif(_ waterImage: UIImage,
                          text: String?,
                          location: WaterLocation,
                          hMargin: CGFloat,
                          vMarigin: CGFloat,
                          textSpace: CGFloat = 8) -> UIImage? {
        guard let imgData = jpegData(compressionQuality: 1),
              let firtImg = PhotoTools.getFirstGifImagesWithData(imgData) else {
            return nil
        }
        return waterMark(waterImage, text: text, location: location, hMargin: hMargin, vMarigin: vMarigin)
    }
    
    func toBase64() -> String? {
        guard let imageData = jpegData(compressionQuality: 1) else {
            return nil
        }
        
        return imageData.base64EncodedString()
    }
}
