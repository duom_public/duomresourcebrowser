//
//  PhotoTools.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/3.
//

import Foundation
import DuomBase
import ImageIO
import CoreGraphics
import Photos

public struct PhotoTools {
    public enum MediaType {
        case image
        case video
        case audio
    }
    
    /// 获取文件大小
    /// - Parameter path: 文件路径
    /// - Returns: 文件大小
    public static func fileSize(atPath path: String) -> Int {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path) {
            let fileSize = try? fileManager.attributesOfItem(atPath: path)[.size]
            if let size = fileSize as? Int {
                return size
            }
        }
        return 0
    }
    
    /// 获取文件夹里的所有文件大小
    /// - Parameter path: 文件夹路径
    /// - Returns: 文件夹大小
    public static func folderSize(atPath path: String) -> Int {
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: path) { return 0 }
        let childFiles = fileManager.subpaths(atPath: path)
        var folderSize = 0
        childFiles?.forEach({ (fileName) in
            let fileAbsolutePath = path + "/" + fileName
            folderSize += fileSize(atPath: fileAbsolutePath)
        })
        return folderSize
    }
    
    public static func getSystemDocumentFolderPath() -> String {
        NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
    }
    
    /// 获取系统缓存文件夹路径
    public static func getSystemCacheFolderPath() -> String {
        NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).last!
    }
    
    /// 获取图片缓存文件夹路径
    public static func getImageCacheFolderPath() -> String {
        //        var cachePath = getSystemCacheFolderPath()
        var cachePath = NSTemporaryDirectory()
        cachePath.append(contentsOf: "hkPhotosBrowser/imageCache")
        folderExists(atPath: cachePath)
        return cachePath
    }
    
    /// 获取视频缓存文件夹路径
    public static func getVideoCacheFolderPath() -> String {
        //        var cachePath = getSystemCacheFolderPath()
        var cachePath = NSTemporaryDirectory()
        cachePath.append(contentsOf: "hkPhotosBrowser/videoCache")
        folderExists(atPath: cachePath)
        return cachePath
    }
    
    public static func getAudioTmpFolderPath() -> String {
        var tmpPath = NSTemporaryDirectory()
        tmpPath.append(contentsOf: "hkPhotosBrowser/audioCache")
        folderExists(atPath: tmpPath)
        return tmpPath
    }
    
    public static func getLivePhotoImageCacheFolderPath() -> String {
        var cachePath = getImageCacheFolderPath()
        cachePath.append(contentsOf: "/LivePhoto")
        folderExists(atPath: cachePath)
        return cachePath
    }
    
    public static func getLivePhotoVideoCacheFolderPath() -> String {
        var cachePath = getVideoCacheFolderPath()
        cachePath.append(contentsOf: "/LivePhoto")
        folderExists(atPath: cachePath)
        return cachePath
    }
    
    /// 删除缓存
    public static func removeCache() {
        removeVideoCache()
        removeImageCache()
        //        removeAudioCache()
        
        removeEditCache()
    }
    
    public static func removeAllImagePickerCache() -> Bool {
        var cachePath = NSTemporaryDirectory()
        cachePath.append(contentsOf: "hkPhotosBrowser")
        folderExists(atPath: cachePath)
        return removeFile(filePath: cachePath)
    }
    
    /// 删除视频缓存
    @discardableResult
    public static func removeVideoCache() -> Bool {
        return removeFile(filePath: getVideoCacheFolderPath())
    }
    
    /// 删除图片缓存
    @discardableResult
    public static func removeImageCache() -> Bool {
        return removeFile(filePath: getImageCacheFolderPath())
    }
    
    /// 删除音频临时缓存
    @discardableResult
    public static func removeAudioCache() -> Bool {
        return removeFile(filePath: getAudioTmpFolderPath())
    }
    
    /// 获取视频缓存文件大小
    @discardableResult
    public static func getVideoCacheFileSize() -> Int {
        return folderSize(atPath: getVideoCacheFolderPath())
    }
    
    /// 获取视频缓存文件地址
    /// - Parameter key: 生成文件的key
    @discardableResult
    public static func getVideoCacheURL(for key: String) -> URL {
        var cachePath = getVideoCacheFolderPath()
        cachePath.append(contentsOf: "/" + key.md5 + ".mp4")
        return URL.init(fileURLWithPath: cachePath)
    }
    
    @discardableResult
    public static func getAudioTmpURL(for key: String) -> URL {
        var cachePath = getAudioTmpFolderPath()
        cachePath.append(contentsOf: "/" + key.md5 + ".mp3")
        return URL.init(fileURLWithPath: cachePath)
    }
    
    /// 视频是否有缓存
    /// - Parameter key: 对应视频的key
    @discardableResult
    public static func isCached(forVideo key: String) -> Bool {
        let fileManager = FileManager.default
        let filePath = getVideoCacheURL(for: key).path
        return fileManager.fileExists(atPath: filePath)
    }
    
    @discardableResult
    public static func isCached(forAudio key: String) -> Bool {
        let fileManager = FileManager.default
        let filePath = getAudioTmpURL(for: key).path
        return fileManager.fileExists(atPath: filePath)
    }
    
    public static func getSystemTempFolderPath() -> String {
        NSTemporaryDirectory()
    }
    
    /// 获取对应后缀的临时路径
    @discardableResult
    public static func getTmpURL(for suffix: String, meidaType: MediaType = .image) -> URL {
        var tempPath: String = getSystemTempFolderPath()
        switch meidaType {
        case .image:
            tempPath = getImageCacheFolderPath()
        case .video:
            tempPath = getVideoCacheFolderPath()
        case .audio:
            tempPath = getAudioTmpFolderPath()
        }
        tempPath.append(contentsOf: "/" + String.fileName(suffix: suffix))
        let tmpURL = URL.init(fileURLWithPath: tempPath)
        return tmpURL
    }
    /// 获取图片临时路径
    @discardableResult
    public static func getImageTmpURL(_ imageContentType: ImageContentType = .jpg) -> URL {
        var suffix: String
        switch imageContentType {
        case .jpg:
            suffix = "jpeg"
        case .png:
            suffix = "png"
        case .gif:
            suffix = "gif"
        default:
            suffix = "jpeg"
        }
        return getTmpURL(for: suffix, meidaType: .image)
    }
    /// 获取视频临时路径
    @discardableResult
    public static func getVideoTmpURL() -> URL {
        return getTmpURL(for: "mp4", meidaType: .video)
    }
    /// 将UIImage转换成Data
    @discardableResult
    public static func getImageData(for image: UIImage?) -> Data? {
        if let pngData = image?.pngData() {
            return pngData
        }else if let jpegData = image?.jpegData(compressionQuality: 1) {
            return jpegData
        }
        return nil
    }
    
    @discardableResult
    public static func write(
        toFile fileURL: URL? = nil,
        image: UIImage?) -> URL? {
            if let imageData = getImageData(for: image) {
                return write(toFile: fileURL, imageData: imageData)
            }
            return nil
        }
    
    @discardableResult
    public static func write(
        toFile fileURL: URL? = nil,
        imageData: Data) -> URL? {
            let imageURL = fileURL == nil ? getImageTmpURL(imageData.isGif ? .gif : .png) : fileURL!
            do {
                if FileManager.default.fileExists(atPath: imageURL.path) {
                    try FileManager.default.removeItem(at: imageURL)
                }
                try imageData.write(to: imageURL)
                return imageURL
            } catch {
                return nil
            }
        }
    
    @discardableResult
    public static func copyFile(at srcURL: URL, to dstURL: URL) -> Bool {
        if srcURL.path == dstURL.path {
            return true
        }
        do {
            if FileManager.default.fileExists(atPath: dstURL.path) {
                try FileManager.default.removeItem(at: dstURL)
            }
            try FileManager.default.copyItem(at: srcURL, to: dstURL)
            return true
        } catch {
            return false
        }
    }
    
    @discardableResult
    public static func removeFile(fileURL: URL) -> Bool {
        removeFile(filePath: fileURL.path)
    }
    
    @discardableResult
    public static func removeFile(filePath: String) -> Bool {
        do {
            if FileManager.default.fileExists(atPath: filePath) {
                try FileManager.default.removeItem(atPath: filePath)
            }
            return true
        } catch {
            return false
        }
    }
    
    public static func folderExists(atPath path: String) {
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: path) {
            try? fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
    }
    
    @discardableResult
    public static func readFileDataFrom(filePath: String) -> Data? {
        do {
            var path: String = filePath
            if filePath.hasPrefix("file://") {
                let index = filePath.index(filePath.startIndex, offsetBy: 7)
                path = String(filePath[index...])
            }
            if FileManager.default.fileExists(atPath: path) {
                if let data = FileManager.default.contents(atPath: path) {
                    return data
                } else {
                    return nil
                }
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    
    public static func exportSessionFileLengthLimit(seconds: Double, exportPreset: ExportPreset, videoQuality: Int) -> Int64 {
        if videoQuality > 0 {
            var ratioParam: Double = 0
            if exportPreset == .ratio_640x480 {
                ratioParam = 0.02
            }else if exportPreset == .ratio_960x540 {
                ratioParam = 0.04
            }else if exportPreset == .ratio_1280x720 {
                ratioParam = 0.08
            }
            let quality = Double(min(videoQuality, 10))
            return Int64(seconds * ratioParam * quality * 1000 * 1000)
        }
        return 0
    }
    
    /// 时间格式化
    public static func transformDuration(for duration: Int) -> String {
        switch duration {
        case 0..<60:
            return String(format: "00:%02d", duration)
        case 60..<3600:
            let m = duration / 60
            let s = duration % 60
            return String(format: "%02d:%02d", m, s)
        case 3600...:
            let h = duration / 3600
            let m = (duration % 3600) / 60
            let s = duration % 60
            return String(format: "%02d:%02d:%02d", h, m, s)
        default:
            return ""
        }
    }
}



// MARK: gif
extension PhotoTools {
    // gif图所有帧
    public static func getGifImages(_ url: URL) -> [UIImage] {
        var images: [UIImage] = []
        if let gifSource = CGImageSourceCreateWithURL(url as! CFURL, nil) {
            let gifCount = CGImageSourceGetCount(gifSource)
            for i in 0..<gifCount {
                if let imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, nil) {
                    let image = UIImage(cgImage: imageRef)
                    images.append(image)
                }
            }
        }
        
        return images
    }
    
    // gif最后一帧
    public static func getLastGifImage(_ url: URL) -> UIImage? {
        let images = getGifImages(url)
        if images.isNotEmpty {
            return images.last!
        } else {
            return nil
        }
    }
    
    
    public static func getGifImagesWithData(_ data: Data) -> [UIImage] {
        var images: [UIImage] = []
        if let gifSource = CGImageSourceCreateWithData(data as CFData, nil) {
            let gifCount = CGImageSourceGetCount(gifSource)
            for i in 0..<gifCount {
                if let imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, nil) {
                    let image = UIImage(cgImage: imageRef)
                    images.append(image)
                }
            }
        }
        return images
    }
    
    public static func getFirstGifImagesWithData(_ data: Data) -> UIImage? {
        let images = getGifImagesWithData(data)
        if images.isNotEmpty {
            return images.last!
        } else {
            return nil
        }
    }
}

// MARK: 格式换转
extension PhotoTools {
    public static func transToJpegData(_ data: Data) -> Data? {
        guard let ciImage = CIImage(data: data),
              let colorSpace = ciImage.colorSpace else {
            return nil
        }
        
        let context = CIContext()
        if let jpegData = context.jpegRepresentation(of: ciImage, colorSpace: colorSpace, options: [:]) {
            return jpegData
        } else {
            return nil
        }
    }
}

// MARK: 图片压缩
extension PhotoTools {
    /*
     压缩策略：
     先压尺寸： 最小尺寸为屏幕宽高
     再进行质量压缩：根据原始不同值设置压缩比例
     ⚠️： 经过压缩的图片 输出统一为jepg格式
     */
    public static func imageCompress(_ data: Data, compressionQuality: CGFloat) -> Data? {
        Logger.log(message: "压缩前原始数据大小： \(paresSize(count: data.count))", level: .info)
        var maxCount: Int = Int(compressionQuality * 1024)
        guard data.count > maxCount else {
            return data
        }
        
        guard var resultImage = UIImage(data: data) else {
            return nil
        }
        
        // 确认转为jpegData
        var jpegData = resultImage.jpegData(compressionQuality: 1)
        Logger.log(message: "转为Jpeg格式后数据大小： \(paresSize(count: jpegData?.count ?? 0))", level: .info)
        
        // 尺寸压缩
        //        let sizeData = imageCompressBySize(jpegData ?? data, targetCount: compressionQuality)
        
        // 颜色转换
        //       let rgbData = imageCompressByRGB(jpegData ?? data, targetCount: compressionQuality)
        
        // 质量压缩
        let qualityData = imageCompressByQuality(jpegData ?? data, targetCount: compressionQuality)
        
        return qualityData
    }
    
    // 尺寸压缩
    public static func imageCompressBySize(_ data: Data, targetCount: CGFloat) -> Data {
        Logger.log(message: "开始尺寸压缩", level: .info)
        Logger.log(message: "压缩前大小： \(paresSize(count: data.count))", level: .info)
        var maxCount: Int = Int(targetCount * 1024)
        guard data.count > maxCount else {
            return data
        }
        
        guard let resultImage = UIImage(data: data) else {
            return data
        }
        
        let ratio = CGFloat(maxCount) / CGFloat(data.count)
        let size = CGSize(width: Int(resultImage.size.width * ratio), height: Int(resultImage.size.height * ratio))
        
        //        guard var resultImage = UIImage(data: data), (resultImage.size.height > UIScreen.screenHeight || resultImage.size.width > UIScreen.screenWidth) else {
        //            Logger.log(message: "尺寸在屏幕范围之内", level: .info)
        //            return data
        //        }
        
        //        let size = UIScreen.main.bounds.size
        UIGraphicsBeginImageContext(size)
        resultImage.draw(in: CGRect(origin: .zero, size: size))
        guard let resImg = UIGraphicsGetImageFromCurrentImageContext(),
              let resImgData = resImg.jpegData(compressionQuality: 1) else {
            UIGraphicsEndImageContext()
            return data
        }
        UIGraphicsEndImageContext()
        return resImgData
    }
    
    /// 质量压缩
    /// - Parameters:
    ///   - data: 原数据
    ///   - targetCount: 目标大小 KB
    ///   - times: 压缩次数 最多压5次
    /// - Returns: 压缩后的数据
    public static func imageCompressByQuality(_ data: Data, targetCount: CGFloat, times: Int = 1) -> Data {
        Logger.log(message: "开始质量压缩>>>>>>>压缩前大小: \(paresSize(count: data.count))", level: .info)
        //        最多压5次
        guard times <= 5 else { return data }
        
        Logger.log(message: "第\(times)次压缩 ", level: .info)
        
        var maxCount: Int = Int(targetCount * 1024)
        
        guard data.count > maxCount else {
            return data
        }
        
        guard var resultImage = UIImage(data: data) else {
            return data
        }
        
        var data = data
        if data.count > 8 * 1024 * 1024 {
            if let jpegData = resultImage.jpegData(compressionQuality: 0.3) {
                data = jpegData
            } else {
                return data
            }
        } else if data.count > 5 * 1024 * 1024  {
            if let jpegData = resultImage.jpegData(compressionQuality: 0.5) {
                data = jpegData
            } else {
                return data
            }
        } else if data.count > 2 * 1024 * 1024 {
            if let jpegData = resultImage.jpegData(compressionQuality: 0.8) {
                data = jpegData
            } else {
                return data
            }
        }
        
        while data.count > maxCount {
            Logger.log(message: "质量压缩次数+1", level: .info)
            return imageCompressByQuality(data, targetCount: targetCount, times: times + 1)
        }
        
        Logger.log(message: "质量压缩后大小： \(paresSize(count: data.count))", level: .info)
        return data
    }
    
    // 色彩模式转换
    public  static func imageCompressByRGB(_ data: Data, targetCount: CGFloat) -> Data {
        Logger.log(message: "开始RGB压缩", level: .info)
        Logger.log(message: "压缩前大小： \(paresSize(count: data.count))", level: .info)
        guard var resultImage = UIImage(data: data) else {
            return data
        }
        
        guard isRGB888(resultImage) else {
            return data
        }
        
        guard let newImg = rgb888ToRgb256(resultImage),
              let newData = newImg.jpegData(compressionQuality: 1) else {
            return data
        }
        
        Logger.log(message: "RGB压缩后大小： \(paresSize(count: newData.count))", level: .info)
        return newData
    }
    
    public static func paresSize(count:Int) -> String {
        let gb = 1024 * 1024 * 1024 //定义GB的计算常量
        let mb = 1024 * 1024 //定义MB的计算常量
        let kb = 1024 //定义KB的计算常量
        if count/gb >= 1 {
            return String(roundf(Float(count)/Float(gb))) + "GB"
        }
        if count/mb >= 1 {
            return String(roundf(Float(count)/Float(mb))) + "MB"
        }
        if count/kb >= 1 {
            return String(roundf(Float(count)/Float(kb))) + "KB"
        }
        return String(count) + "Byte"
    }
    
    public static func isRGB888(_ image: UIImage) -> Bool {
        guard let cgImage = image.cgImage,
              let corlorSpace = cgImage.colorSpace,
              corlorSpace.model == .rgb else {
            return false
        }
        
        if cgImage.bitsPerComponent != 8 {
            return false
        }
        
        return true
    }
    
    public static func rgb888ToRgb256(_ image: UIImage) -> UIImage? {
        guard let cgImage = image.cgImage else {
            return nil
        }
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.noneSkipFirst.rawValue)
        let context = CGContext(data: nil, width: Int(cgImage.width), height: Int(cgImage.height),
                                bitsPerComponent: 8, bytesPerRow: Int(cgImage.width), space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
        context?.draw(cgImage, in: CGRect(x: 0, y: 0, width: cgImage.width, height: cgImage.height))
        
        guard let rgbData = context?.data else {
            return nil
        }
        
        guard  let newCgImage = cgImage.copy(colorSpace: colorSpace) else {
            return nil
        }
        let rgb256Image = UIImage(cgImage: newCgImage)
        return rgb256Image
    }
}


// MARK: 图片编辑信息
extension PhotoTools {
    /// 保存编辑数据到本地
    @discardableResult
    public static func saveEditInfo(_ key: String, info: Data) -> URL? {
        let editPath = getImageEditCacheURL(key)
        do {
            if FileManager.default.fileExists(atPath: editPath.path) {
                try FileManager.default.removeItem(at: editPath)
            }
            try info.write(to: editPath)
            return editPath
        } catch { error
            Logger.log(message: "保存eidtInfo失败\(error)", level: .warning)
            return nil
        }
    }
    
    /// 获取图片编辑信息缓存文件夹路径
    public static func getImageEditCacheURL(_ key: String?) -> URL {
        var tempPath = getImageEditCacheFolderPath()
        if let key = key {
            tempPath.append(contentsOf: "/" + "\(key)")
        }
        let tmpURL = URL.init(fileURLWithPath: tempPath)
        return tmpURL
    }
    
    public static func getImageEditCacheFolderPath() -> String {
        var cachePath = NSTemporaryDirectory()
        cachePath.append(contentsOf: "hkPhotosBrowser/imageEditCache")
        folderExists(atPath: cachePath)
        return cachePath
    }
    
    
    @discardableResult
    public static func removeEditCache() -> Bool {
        return removeFile(filePath: getImageEditCacheFolderPath())
    }
    
}

// MARK: 沙盒路径重定向
public extension PhotoTools {
    @discardableResult
    public static func sandBoxRedirection(_ fileString: String) -> String {
        guard !fileString.isEmpty else { return fileString }
        var fileString = fileString
        if fileString.hasPrefix("file://") {
            let index = fileString.index(fileString.startIndex, offsetBy: 7)
            fileString = String(fileString[index...])
        }
        
        var pathCompoents = fileString.components(separatedBy: "/")
        var systemPathCompoents = NSTemporaryDirectory().components(separatedBy: "/")
        if pathCompoents.count > 7, systemPathCompoents.count > 7 {
            let randomCompoent = pathCompoents[7]
            let systemCompoent = systemPathCompoents[7]
            if randomCompoent == systemCompoent {
                return "file://" + fileString
            } else {
                pathCompoents.remove(at: 7)
                pathCompoents.insert(systemCompoent, at: 7)
                return "file://" + pathCompoents.joined(separator: "/")
            }
        }
        return fileString
    }
    
    //绝对路径 -> 相对路径
    @discardableResult
    public static func getRelativePath(_ path: String?) -> String {
        guard let path = path, !path.isEmpty else { return "" }
        var relativePath = path
        if relativePath.hasPrefix("file://") {
            let index = relativePath.index(relativePath.startIndex, offsetBy: 7)
            relativePath = String(relativePath[index...])
        }
        
        var pathComponets = relativePath.components(separatedBy: "/")
        let newComponets = Array(pathComponets.suffix(3))
        let newPath = newComponets.joined(separator:"/")
        return newPath
    }
    
    //相对路径 -> 绝对路径
    @discardableResult
    public static func getAbsolutePath(_ path: String?) -> String {
        guard let path = path, !path.isEmpty else { return "" }
        var newPath = getSystemTempFolderPath() + path
        if !newPath.hasPrefix("file://") {
            newPath = "file://" + newPath
        }
        return newPath
    }
    
}

// MARK: 内存情况
extension PhotoTools {
    public static func memoryUsage() -> (used: UInt64, total: UInt64) {
        var taskInfo = task_vm_info_data_t()
        var count = mach_msg_type_number_t(MemoryLayout<task_vm_info>.size) / 4
        let result: kern_return_t = withUnsafeMutablePointer(to: &taskInfo) {
            $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
                task_info(mach_task_self_, task_flavor_t(TASK_VM_INFO), $0, &count)
            }
        }
        
        var used: UInt64 = 0
        if result == KERN_SUCCESS {
            used = UInt64(taskInfo.phys_footprint)
        }
        
        let total = ProcessInfo.processInfo.physicalMemory
        return (used, total)
    }
}


// MARK: 水印&Base64
public enum WaterLocation: Int {
    case topLeft = 0
    case topRight
    case bottomLeft
    case bottomRight
}

public extension PhotoTools {
    static func addWaterMark(_ path: String,
                             albumId: String,
                             uniqueId: String? = nil,
                             text: String? = nil,
                             imageLocation: WaterLocation = .bottomRight,
                             textLocation: WaterLocation = .bottomRight,
                             base64: Bool = true,
                             reslutHandler: @escaping ((String, Bool) -> Void))  {
        guard path.isNotEmpty else {
            reslutHandler(path, false)
            return
        }
        
        var mediaAsset: PHAsset?
        if let uniqueId = uniqueId,
           uniqueId.isNotEmpty,
           let uniqueCorrespondId = UserDefaults.standard.value(forKey: uniqueId) as? String,
            uniqueCorrespondId.isNotEmpty {
            mediaAsset = getPhAsset(uniqueCorrespondId)
        } else {
            mediaAsset = getPhAsset(albumId)
        }
        
        
        if let mediaAsset = mediaAsset {
            // 从相册获取data 转位base64
            AssetManager.fetchOriginalImage(for: mediaAsset, synchronous: true, completion: { tempImg, _, _ in
                if let resultImg = tempImg, let resultImgBase64 = resultImg.toBase64() {
                    reslutHandler(resultImgBase64, true)
                }
            })
        } else {
            guard let imageData = readFileDataFrom(filePath: path),
                  let image = UIImage(data: imageData) else {
                reslutHandler(path, false)
                return
            }
            
            var backImg = UIImage(named: "rebuild_stanly")
            let hMargin: CGFloat = 16.0
            var vMargin: CGFloat = 16.0
            if let _ = text {
                backImg = UIImage(named: "water_gray_icon")
                vMargin = 8.0
            }
            
            guard let waterImage = backImg else {
                reslutHandler(path, false)
                return
            }
            
            var waterImg: UIImage?
            if imageData.isGif {
                waterImg = image.waterMarkOnGif(waterImage, text: text, location: imageLocation, hMargin: hMargin, vMarigin: vMargin)
            } else {
                waterImg = image.waterMark(waterImage, text: text, location: imageLocation, hMargin: hMargin, vMarigin: vMargin)
            }
            
            guard let waterImg = waterImg else {
                reslutHandler(path, false)
                return
            }
            
            if base64 {
                if let waterImgBase64 = waterImg.toBase64() {
                    reslutHandler(waterImgBase64, false)
                }
            } else {
                if let waterPath = getWaterImagePath(path),
                   let newPath = write(toFile: URL(string: waterPath), image: waterImg) {
                    var newPathString = newPath.absoluteString
                    if newPathString.hasPrefix("file://") {
                        let index = newPathString.index(newPathString.startIndex, offsetBy: 7)
                        newPathString = String(newPathString[index...])
                    }
                    reslutHandler(newPathString, false)
                }
            }
        }
    }
    
    static func getWaterImagePath(_ path: String) -> String? {
        let waterImgPath = "waterMarkImage.png"
        guard path.isNotEmpty else {
            return nil
        }
        var path = path
        var pathCompoents = path.components(separatedBy: "/")
        pathCompoents.removeLast()
        pathCompoents.append(waterImgPath)
        return "file://" + pathCompoents.joined(separator: "/")
    }
    
    static func imageToBase64(_ path: String,
                             uniqueId: String? = nil,
                             base64: Bool = true,
                             reslutHandler: @escaping ((String, Bool) -> Void))  {
        guard path.isNotEmpty else {
            reslutHandler(path, false)
            return
        }
        
        var mediaAsset: PHAsset?
        if let uniqueId = uniqueId,
           uniqueId.isNotEmpty,
           let uniqueCorrespondId = UserDefaults.standard.value(forKey: uniqueId) as? String,
            uniqueCorrespondId.isNotEmpty {
            mediaAsset = getPhAsset(uniqueCorrespondId)
        }
        
        if let mediaAsset = mediaAsset {
            // 从相册获取data 转位base64
            AssetManager.fetchOriginalImage(for: mediaAsset, synchronous: true, completion: { tempImg, _, _ in
                if let resultImg = tempImg, let resultImgBase64 = resultImg.toBase64() {
                    reslutHandler(resultImgBase64, true)
                }
            })
        } else {
            guard let imageData = readFileDataFrom(filePath: path),
                  let image = UIImage(data: imageData) else {
                reslutHandler(path, false)
                return
            }
            
            if let imageBase64 = image.toBase64() {
                reslutHandler(imageBase64, false)
            }
        }
    }
}


// MARK: Save Album
public extension PhotoTools {
    static func saveMediaToAlbum(_ path: String, albumId: String, type: MediaType, success: ((String) -> Void)?)  {
        guard path.isNotEmpty else {
            success?("")
            return
        }
        
        let isExist = mediaIsExist(albumId)
        
        guard !isExist else {
            success?(albumId)
            return
        }
        
        let status = PHPhotoLibrary.authorizationStatus()
        
        if status == .denied || status == .restricted {
            success?("")
            return
        }
        
        guard let fileURL = URL(string: path) else {
            success?("")
            return
        }
        
        var placeholderAsset: PHObjectPlaceholder?
        var localIdentifier: String?
        PHPhotoLibrary.shared().performChanges {
            if type == .video {
                let newAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: fileURL)
                placeholderAsset = newAssetRequest?.placeholderForCreatedAsset
            } else {
                let newAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: fileURL)
                placeholderAsset = newAssetRequest?.placeholderForCreatedAsset
            }
        } completionHandler: { suc, _ in
            if suc {
                localIdentifier = placeholderAsset?.localIdentifier
                success?(localIdentifier ?? "")
            }
        }
    }
    
    static func mediaIsExist(_ mediaId: String) -> Bool {
        guard mediaId.isNotEmpty else {
            return false
        }
        var tempAsset: PHAsset?
        tempAsset = AssetManager.getAsset(from: mediaId)
        if let tempAsset = tempAsset {
            return true
        }
        return false
    }
    
    static func getPhAsset(_ mediaId: String) -> PHAsset? {
        guard mediaId.isNotEmpty else {
            return nil
        }
        var tempAsset: PHAsset?
        tempAsset = AssetManager.getAsset(from: mediaId)
        return tempAsset
    }
    
}
