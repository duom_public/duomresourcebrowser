//
//  SnapShot.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/7/5.
//

import Foundation
import UIKit

public func getScreenSnapShotFile() -> (String, String)? {
    var sourceurl: String = ""
    var compressURL: String = ""
    if let image = getScreenSnapshotImage() {
        if let url = PhotoTools.write(image: image) {
            sourceurl = "\(url)"
        } else {
            return nil
        }
        
        if let imageData = PhotoTools.getImageData(for: image),
            let compressImg = PhotoTools.imageCompress(imageData, compressionQuality: PhotosConfiguration.default().imageQualityCount),
           let compress_url = PhotoTools.write(imageData: imageData) {
            compressURL = "\(compress_url)"
        }
        return (sourceurl, compressURL)
    }
    return nil
}

public func getScreenSnapshotImage() -> UIImage? {
    guard let windown = UIApplication.getWindow() else {
        return nil
    }
    UIGraphicsBeginImageContextWithOptions((windown.bounds.size), true, 0)
    let context = UIGraphicsGetCurrentContext()
    windown.layer.render(in: context!)
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return img
}


