//
//  PhotosConfiguration.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/30.
//

import UIKit
import Photos
import DuomBase

// 秒
public typealias Second = Int

public class PhotosConfiguration: NSObject {
// 自己账号：   gmSYOyKp4ct6NO5vIbmjHe4zHaFudgnL
// 共用账号：   HfLNZRCei09xPR2WiasHFkqXUiX8wql0
   public let GiphyKey = "HfLNZRCei09xPR2WiasHFkqXUiX8wql0"
    
   public enum SourceSelectType: Int {
        case all = 0
        case imageAndTakePhoto
        case video
        case image
        case takePhoto
    }
    
    private static var single = PhotosConfiguration()
    
    public class func `default`() -> PhotosConfiguration {
        return PhotosConfiguration.single
    }
    
    public class func resetConfiguration() {
        PhotosConfiguration.single = PhotosConfiguration()
    }
    
    // MARK: 整体配置
    // 排序
    public var sortAscending = false
    
    private var _maxSelectCount = 15
    public var maxSelectCount: Int {
        get {
            return _maxSelectCount
        }
        set {
            _maxSelectCount = max(1, newValue)
        }
    }
    
    private var _maxVideoSelectCount = 0
    public var maxVideoSelectCount: Int {
        get {
            if _maxVideoSelectCount <= 0 {
                return maxSelectCount
            } else {
                return max(minVideoSelectCount, min(_maxVideoSelectCount, maxSelectCount))
            }
        }
        set {
            _maxVideoSelectCount = newValue
        }
    }
    
    private var _minVideoSelectCount = 0
    @objc public var minVideoSelectCount: Int {
        get {
            return min(maxSelectCount, max(_minVideoSelectCount, 0))
        }
        set {
            _minVideoSelectCount = newValue
        }
    }
    
    public var cellCornerRadio: CGFloat = 0
    
    public var allowSelectVideo = true
    
    public var allowSelectImage = true
    
    public var allowSelectLivePhoto = true
    
    // 单独选择一张静态图片 UI区别与多选
    public var selectSingleImg = false {
        didSet {
            if selectSingleImg {
                allowSelectVideo = false
            }
        }
    }
    
    private var _selectType: SourceSelectType = .all
    // 外部设置资源选择类型
    public var selectType: SourceSelectType {
        get {
            return _selectType
        }
        set {
            _selectType = newValue
        }
    }
    
    // 内外scrollView是否可以滑动
    public let scrollEable = false

    /// 相机配置
    public var cameraConfiguration = CameraConfiguration()
    
    /// 编辑图片配置
    public var editImageConfiguration = EditImageConfiguration()
    
    /// 视频首帧图size
    public var firstFrameSizeOfVideo = CGSize(width: 150, height: 150)
    
    /// 视频导出模式
    public var videoExportPresent = ExportPreset.highQuality
    
    // MARK: 图片处理
    /// 图片压缩阈值  单位KB 默认：2M
    ///  2 * 1024
    public var imageQualityCount: CGFloat = 2.0 * 1024
    
    // 是否允许编辑
    public var allowEdit: Bool = true
    

    
    
    // MARK: 业务部分
    // 图片最大宽度值
    public let HKMaxImageWidth: CGFloat = 500

    // 主题色
    public let mainBackgroundColor = UIColor(255, 255, 255)
    
    public let mainThemeColor = UIColor(110, 116, 240)
    
    // 主体文字颜色 黑
    public let mainTextColor = UIColor(6, 5, 6)
    
    // nav顶部黄色
    public let navTitleControlColor = UIColor(232, 232, 232)
    
    // nav高度
    public let navHeight: CGFloat = 88

    // 底部工具栏items
    public var segmentItems: [String] {
        get {
            if selectSingleImg {
                return showGif ? ["ALBUM", "CAM", "GIF"].localized : ["ALBUM", "CAM"].localized
            } else {
                return showGif ? ["ALBUM", "VIDEO", "CAM", "GIF"].localized : ["ALBUM", "VIDEO", "CAM"].localized
            }
        }
    }
    
    // 顶部segment 与滑动条间距
    public let segmentTextSpace = 4

    // Segment 按钮选中颜色
    public let segmentSelectedTextColor = UIColor(6, 5, 6)

    // Segment 按钮normal颜色
    public let segmentNormalTextColor = UIColor(153, 153, 153)

    // segment line指示器颜色
    public let segmentLineColor = UIColor(110, 116, 240)

    // main首页底部segmentView高度
    public let segmentViewHight: CGFloat = 106
    
    
    // 选中icon直径
    public let photoSelectIconWidth: CGFloat = 16

    // 相片间距
    public let photoSpace: CGFloat = 8.0

    // 单个line上几个照片
    public let lineCount: Int = 4

    // 单个照片宽高
    public var photoItemWH: CGFloat {
        get {
           return (UIScreen.screenWidth - (photoSpace * CGFloat(lineCount - 1))) / CGFloat(lineCount)
        }
    }

    // MARK: Gif
    public var showGif: Bool = false
    
    // UserDefaults
    public var BubbleKey = "hadShowBubble"
    
    // MARK: 埋点
    public let MobStatisticsNotification = Notification.Name.init("MobStatistics")
}

