//
//  CopyWitterInstance.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/8/28.
//

import Foundation
import DuomBase

public class CopyWitterInstance {
    private static var _sharedInstance: CopyWitterInstance?
    
    private init() {}
    
    public class func `default`() -> CopyWitterInstance {
        guard let instance = _sharedInstance else {
            _sharedInstance = CopyWitterInstance()
            return _sharedInstance!
        }
        return instance
    }
    
    public static func destory() {
        _sharedInstance = nil
    }
    

    public let authorizationToast = "You do not have permission to use this feature.".localized
    // 超选提示
    public var overMaxSelectCount: (Int) -> String = { count in
        return "Select a maximum of \(count) images".localDynamic(.imp_max_images, dynamicCount: count)
    }
    public let noTitleCollectionPlaceholder = "No Title".localized

    // 权限提示文案
    public let authorizationNoneTip: String = "We need permission to access all of your albums.".localized
    public let authorizationPartTip: String = "You only have access to some photos enabled, so go to Settings to authorize.".localized
    public let authorizationPartTitle: String = "We need permission to access all of your albums.".localized
    public let authorizationPartDesc: String = "This allows Stanly to share photos from your library and save photos to your camera roll.".localized
    
    public let authYouUseTitle: String = "How you’ll use this".localized
    public let authWeUseTitle: String = "How we’ll use this".localized
    public let authSettingsTitle: String = "How these settings work".localized
    public let authPhotoAndVideoTitle: String = "Allow Stanly to access your photos and videos".localized
    public let authPhotoAndVideoYouUseTips: String = "To add photos and videos from your camera roll to Stanly and apply effects to your photos and videos.".localized
    public let authPhotoAndVideoWeUseTips: String = "To let you share photos and videos from your camera roll to Stanly, apply effects and suggest photos and videos to share.".localized
    public let authPhotoAndVideoSettingsTips: String = "You can change your choice at any time in your device settings. If you allow access now, you won’t have to allow it again.".localized
    public let authPhotoAndVideoOpenTitle: String = "Enable Library Access".localized
    public let authCameraAndMicrophoneTitle: String = "Allow Stanly to access your camera and microphone".localized
    public let authCameraAndMicrophoneYouUseTips: String = "To take photos, record videos and preview visual and audio effects.".localized
    public let authCameraAndMicrophoneWeUseTips: String = "To show you previews of visual and audio effects.".localized
    public let authCameraAndMicrophoneSettingsTips: String = "You can change your choice at any time in your device settings. If you allow access now, you won’t have to allow it again.".localized
    public let authCameraAndMicrophoneOpenTitle: String = "Open Settings".localized

    // MARK: Gif
    public let gifSearchTexts: [String] = ["Hi", "haha", "love", "cat", "dog", "happy", "sad"].localized
    // recents
    public let gifSearchTextsWithRecents: [String] = ["Recent", "Hi", "haha", "love", "cat", "dog", "happy", "sad"].localized
    public let gifRecentsText: String = "Recent".localized

    public let NextText = "Next".localized
    public let CropText = "Crop".localized
    public let StickcerText = "Stickers".localized
    public let CancelText = "Cancel".localized

    public let AssetNil = "Asset is nil".localized
}


//public let authorizationToast = "You do not have permission to use this feature.".localized
//// 超选提示
//public var overMaxSelectCount: (Int) -> String = { count in
//    return "Select a maximum of \(count) images".localDynamic(.imp_max_images, dynamicCount: count)
//}
//public let noTitleCollectionPlaceholder = "No Title".localized
//
//// 权限提示文案
//public let authorizationNoneTip: String = "We need permission to access all of your albums.".localized
//public let authorizationPartTip: String = "You only have access to some photos enabled, so go to Settings to authorize.".localized
//
//
//// MARK: Gif
//public let gifSearchTexts: [String] = ["Hi", "haha", "love", "cat", "dog", "happy", "sad"].localized
//// recents
//public let gifSearchTextsWithRecents: [String] = ["Recent", "Hi", "haha", "love", "cat", "dog", "happy", "sad"].localized
//public let gifRecentsText: String = "Recent".localized
//
//public let NextText = "Next".localized
//public let CropText = "Crop".localized
//public let StickcerText = "Stickers".localized
//public let CancelText = "Cancel".localized
//
//public let AssetNil = "Asset is nil".localized

