//
//  CameraConfiguration.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/1.
//

import UIKit
import AVFoundation


public class CameraConfiguration: NSObject {
    /// 分辨率
    public enum CaptureSessionPreset: Int {
        var avSessionPresent: AVCaptureSession.Preset {
            switch self {
            case .cif352x288:
                return .cif352x288
            case .vga640x480:
                return .vga640x480
            case .hd1280x720:
                return .hd1280x720
            case .hd1920x1080:
                return .hd1920x1080
            case .hd4K3840x2160:
                return .hd4K3840x2160
            }
        }
        
        case cif352x288
        case vga640x480
        case hd1280x720
        case hd1920x1080
        case hd4K3840x2160
    }
    
    // 对焦模式
    public enum FocusMode: Int {
        var avFocusMode: AVCaptureDevice.FocusMode {
            switch self {
            case .autoFocus:
                return .autoFocus  //自动对焦模式
            case .continuousAutoFocus:
                return .continuousAutoFocus // 连续自动对焦
            }
        }
        
        case autoFocus
        case continuousAutoFocus
    }
    
    // 曝光模式
    public enum ExposureMode: Int {
        case autoExpose
        case continuousAutoExposure
        var avFocusMode: AVCaptureDevice.ExposureMode {
            switch self {
            case .autoExpose:
                return .autoExpose
            case .continuousAutoExposure:
                return .continuousAutoExposure
            }
        }
    }
    
    // 闪光模式
    public enum FlashMode: Int {
        case auto
        case on
        case off
        
        var avFlashMode: AVCaptureDevice.FlashMode {
            switch self {
            case .auto:
                return .auto
            case .on:
                return .on
            case .off:
                return .off
            }
        }
        
        // 转系统相机
        var imagePickerFlashMode: UIImagePickerController.CameraFlashMode {
            switch self {
            case .auto:
                return .auto
            case .on:
                return .on
            case .off:
                return .off
            }
        }
    }
    
    // 导出格式
    public enum VideoExportType: Int {
        case mov
        case mp4
        
        var format: String {
            switch self {
            case .mov:
                return "mov"
            case .mp4:
                return "mp4"
            }
        }
        
        var avFileType: AVFileType {
            switch self {
            case .mov:
                return.mov
            case .mp4:
                return .mp4
            }
        }
    }
    
    public var sessionPreset: CameraConfiguration.CaptureSessionPreset = .hd1920x1080
    
    public var focusMode: CameraConfiguration.FocusMode = .continuousAutoFocus
    
    public var exposureMode: CameraConfiguration.ExposureMode = .continuousAutoExposure
    
    public var flashMode: CameraConfiguration.FlashMode = .off
    
    public var videoExportType: CameraConfiguration.VideoExportType = .mov
}

// MARK: chaining
public extension CameraConfiguration {
    @discardableResult
    func sessionPreset(_ sessionPreset: CameraConfiguration.CaptureSessionPreset) -> CameraConfiguration {
        self.sessionPreset = sessionPreset
        return self
    }
    
    @discardableResult
    func focusMode(_ mode: CameraConfiguration.FocusMode) -> CameraConfiguration {
        focusMode = mode
        return self
    }
    
    @discardableResult
    func exposureMode(_ mode: CameraConfiguration.ExposureMode) -> CameraConfiguration {
        exposureMode = mode
        return self
    }
    
    @discardableResult
    func flashMode(_ mode: CameraConfiguration.FlashMode) -> CameraConfiguration {
        flashMode = mode
        return self
    }
    
    @discardableResult
    func videoExportType(_ type: CameraConfiguration.VideoExportType) -> CameraConfiguration {
        videoExportType = type
        return self
    }
}
