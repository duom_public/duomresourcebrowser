//
//  PhotoWrapper.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/1.
//  命名空间
//

import UIKit
import Photos

public struct DPhotoWrapper<Base> {
    public let base: Base
    
    init(_ base: Base) {
        self.base = base
    }
}

public protocol DPhotoCompatible: AnyObject {}

public protocol DPhotoCompatibleValue {}

extension DPhotoCompatible {
    public var hk: DPhotoWrapper<Self> {
        get {
            DPhotoWrapper(self)
        }
        set {}
    }
    
    public static var hk: DPhotoWrapper<Self>.Type {
        get {
            DPhotoWrapper<Self>.self
        }
        set {}
    }
}

extension DPhotoCompatibleValue {
    public var hk: DPhotoWrapper<Self> {
        get {
            DPhotoWrapper(self)
        }
        set {}
    }
}

extension UIViewController: DPhotoCompatible { }
extension UIColor: DPhotoCompatible { }
extension UIImage: DPhotoCompatible { }
extension CIImage: DPhotoCompatible { }
extension PHAsset: DPhotoCompatible { }
extension AVAsset: DPhotoCompatible { }
extension UIFont: DPhotoCompatible { }
extension UIView: DPhotoCompatible { }

extension Array: DPhotoCompatibleValue { }
extension String: DPhotoCompatibleValue { }
extension CGFloat: DPhotoCompatibleValue { }
extension Bool: DPhotoCompatibleValue { }

