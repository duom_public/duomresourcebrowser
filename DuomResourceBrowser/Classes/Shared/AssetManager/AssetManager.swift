//
//  PhotosManager.swift
//  ResourceBrowser
//
//  Created by kuroky on 2022/11/30.
//

import UIKit
import Photos
import DuomBase

public class AssetManager: NSObject {
    typealias progressHandler = ((CGFloat, Error?, UnsafeMutablePointer<ObjCBool>, [AnyHashable: Any]?) -> Void)?
    typealias imageCompletionHandler = (UIImage?, [AnyHashable: Any]?, Bool) -> Void
    typealias imageDataCompletionHandler = (Data, [AnyHashable: Any]?, Bool) -> Void
    typealias livePhotoCompletionHandler = (PHLivePhoto?, [AnyHashable: Any]?, Bool) -> Void
    typealias videoCompletionHandler = (AVPlayerItem?, [AnyHashable: Any]?, Bool) -> Void
    typealias avAssetCompletionHandler = (AVAsset?, [AnyHashable: Any]?) -> Void
    typealias imageFileURLResultHandler = (Result<URL, AssetError>) -> Void
     
    // MARK: Read
    /// fetch photos  from result
    /// PHFetchResult表示一系列的PHAsset的结果集合
    static func fetchPhoto(in result: PHFetchResult<PHAsset>,
                                 ascending: Bool,
                                 allowSelectImage: Bool,
                                 allowSelectVideo: Bool,
                                 limitCount: Int = .max) -> [PhotoModel] {
        var models: [PhotoModel] = []
        let options: NSEnumerationOptions = ascending ? .init(rawValue: 0) : .reverse
        var count = 1
        
        result.enumerateObjects(options: options) { asset, _, stop in
            let m = PhotoModel(asset: asset)
            
            if m.mediaType == .image, !allowSelectImage {
                return
            }
            
            if m.mediaType == .video, !allowSelectVideo {
                return
            }
            
            if count == limitCount {
                stop.pointee = true
            }
            
            models.append(m)
            count += 1
        }
        return models
    }
    
    /// fetch all album list
    static func fetchPhotoAlbumList(ascending: Bool = true, allowSelectImage: Bool, allowSelectVideo: Bool, completion: ([AlbumListModel]) -> Void) {
        // 获取PHFetchResult时的传入参数，起过滤、排序等作用，可以过滤类型、日期、名称等；
        // 传入nil则使用系统默认值
        let option = PHFetchOptions()
        
        if !allowSelectImage {
            option.predicate = NSPredicate(format: "mediaType == %ld", PHAssetMediaType.video.rawValue)
        }
        
        if !allowSelectVideo {
            option.predicate = NSPredicate(format: "mediaType == %ld", PHAssetMediaType.image.rawValue)
        }
        
        // smartAlbum: Photos app内置的相册（内容动态更新）
        // albumRegular: 用户自己在Photos app中建立的相册
        // PHAssetCollection: 资源集合，表示成组的资源；可以表示照片库中的一个相册、时刻、智能相册； 智能相册：系统默认提供的特定相册，如最近删除、视频列表、收藏等
        // PHCollection: PHAssetCollection和PHCollectionList的父类
        let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil) as! PHFetchResult<PHCollection>
        
        // .album: 用户自己在Photos app中建立的相册、从iTunes同步来的相册
        let albums = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil) as! PHFetchResult<PHCollection>
        
        // albumMyPhotoStream: 用户的iCloud照片流
        let streamAlbums = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumMyPhotoStream, options: nil) as! PHFetchResult<PHCollection>
        
        // albumSyncedAlbum: 从iPhoto同步来的相册
        let syncedAlbums = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumSyncedAlbum, options: nil) as! PHFetchResult<PHCollection>
        
        // albumCloudShared: 用户使用iCloud共享的相册
        let sharedAlbums = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumCloudShared, options: nil) as! PHFetchResult<PHCollection>
        
        let arr = [smartAlbums, albums, streamAlbums, syncedAlbums, sharedAlbums]
        
        var albumList: [AlbumListModel] = []
        arr.forEach {  album in
            album.enumerateObjects { collection, _, _ in
                guard let collection = collection as? PHAssetCollection else { return }
                if collection.assetCollectionSubtype == .smartAlbumAllHidden { // 包含隐藏照片、视频的相册
                    return
                }
                
                if collection.assetCollectionSubtype.rawValue > PHAssetCollectionSubtype.smartAlbumLongExposures.rawValue {
                    // 未知枚举值
                    return
                }
                
                let result = PHAsset.fetchAssets(in: collection, options: option)
                if result.count == 0 {
                    return
                }
                
                let title = self.getCollectionTitle(collection)
                
                if collection.assetCollectionSubtype == .smartAlbumUserLibrary {
                    // 相机相册，包含相机拍摄的所有照片、视频，使用其他应用保存的照片、视频
                    // 区分 isCameraRoll 字段
                    let model = AlbumListModel(title: title, result: result, collection: collection, option: option, isCameraRoll: true)
                    albumList.insert(model, at: 0)
                } else {
                    let model = AlbumListModel(title: title, result: result, collection: collection, option: option, isCameraRoll: false)
                    albumList.append(model)
                }
            }
        }
        completion(albumList)
    }
    
    /// fetch camera roll album
    static func fetchCameraRollAlbum(allowSelectImage: Bool, allowSelectVideo: Bool, completion: @escaping (AlbumListModel) -> Void) {
        let option = PHFetchOptions()
        if !allowSelectImage {
            option.predicate = NSPredicate(format: "mediaType == %ld", PHAssetMediaType.video.rawValue)
        }
        
        if !allowSelectVideo {
            option.predicate = NSPredicate(format: "mediaType == %ld", PHAssetMediaType.image.rawValue)
        }
        
        let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil)
        
        smartAlbums.enumerateObjects { collection, _, stop in
            if collection.assetCollectionSubtype == .smartAlbumUserLibrary {
                let result = PHAsset.fetchAssets(in: collection, options: option)
                let albumModel = AlbumListModel(title: self.getCollectionTitle(collection), result: result, collection: collection, option: option, isCameraRoll: true)
                completion(albumModel)
                stop.pointee = true
            }
        }
    }
    
    @discardableResult
    static func fetchImage(for asset: PHAsset, size: CGSize, progress: progressHandler = nil, completion: @escaping imageCompletionHandler) -> PHImageRequestID {
        /*
         resizeMode: fast. exact ?
         */
        return fetchImage(for: asset, size: size, resizeMode: .fast, progress: progress, completion: completion)
    }
    
    // 获取原图
    @discardableResult
    static func fetchOriginalImage(for asset: PHAsset, synchronous: Bool = false, progress: progressHandler = nil, completion: @escaping imageCompletionHandler) -> PHImageRequestID {
        return fetchImage(for: asset, size: PHImageManagerMaximumSize, resizeMode: .fast, synchronous: synchronous, progress: progress, completion: completion)
    }
    
    // 获取元数据
    @discardableResult
    static func fetchOriginalImageData(for asset: PHAsset, progress: progressHandler = nil, completion: @escaping imageDataCompletionHandler) -> PHImageRequestID {
        let option = PHImageRequestOptions()
        if (asset.value(forKey: "filename") as? String)?.hasPrefix("GIF") == true {
            option.version = .original
        }
        option.isNetworkAccessAllowed = true
        option.resizeMode = .fast
        option.deliveryMode = .highQualityFormat   // 获取一张图片，质量中等的，请求速度比较快
        option.isSynchronous = true
        option.progressHandler = { pro, error, stop, info in
            DispatchQueue.main.async {
                progress?(CGFloat(pro), error, stop, info)
            }
        }
        
        return PHImageManager.default().requestImageData(for: asset, options: option) { data, _, _, info in
            let cancel = info?[PHImageCancelledKey] as? Bool ?? false
            let isDegraded = (info?[PHImageResultIsDegradedKey] as? Bool ?? false)
            if !cancel, let data = data {
                completion(data, info, isDegraded)
            }
        }
    }
    
    // 获取livePhoto
    static func fetchLivePhoto(for asset: PHAsset, progress: progressHandler = nil, completion: @escaping livePhotoCompletionHandler) -> PHImageRequestID {
        let option = PHLivePhotoRequestOptions()
        option.version = .current   // 最新版本（包括所有编辑版本）
        option.deliveryMode = .highQualityFormat  // 异步可能取到好多张图（包括原图），如果你要取原图，用同步方法，返回一张
        option.isNetworkAccessAllowed = true
        option.progressHandler = { pro, error, stop, info in
            DispatchQueue.main.async {
                progress?(CGFloat(pro), error, stop, info)
            }
        }
        
        return PHImageManager.default().requestLivePhoto(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option) { livePhoto, info in
            let isDegraded = info?[PHImageResultIsDegradedKey] as? Bool ?? false
            completion(livePhoto, info, isDegraded)
        }
    }
    
    // 获取video
    static func fetchVideo(for asset: PHAsset, progress: progressHandler = nil, completion: @escaping videoCompletionHandler) -> PHImageRequestID {
        let option = PHVideoRequestOptions()
        option.isNetworkAccessAllowed = true
        option.progressHandler = { pro, error, stop, info in
            DispatchQueue.main.async {
                progress?(CGFloat(pro), error, stop, info)
            }
        }
        
        if !asset.hk.isLocallyAvailable {
            return PHImageManager.default().requestExportSession(forVideo: asset, options: option, exportPreset: AVAssetExportPresetHighestQuality) { session, info in
                DispatchQueue.main.async {
                    let isDegraded = info?[PHImageResultIsDegradedKey] as? Bool ?? false
                    if let avAsset = session?.asset {
                        let item = AVPlayerItem(asset: avAsset)
                        completion(item, info, isDegraded)
                    } else {
                        completion(nil, nil, true)
                    }
                }
            }
        } else {
            return PHImageManager.default().requestPlayerItem(forVideo: asset, options: option) { item, info in
                DispatchQueue.main.async {
                    let isDegraded = info?[PHImageResultIsDegradedKey] as? Bool ?? false
                    completion(item, info, isDegraded)
                }
            }
        }
    }
    
    // fetch error
    static func isFetchImageError(_ error: Error?) -> Bool {
        guard let e = error as NSError? else {
            return false
        }
        
        if e.domain == "CKErrorDomain" || e.domain == "CloudPhotoLibraryErrorDomain" {
            return true
        }
        return false
    }
    
    // 获取AVAsset
    static func fetchAvAsset(forVideo asset: PHAsset, completion: @escaping avAssetCompletionHandler) -> PHImageRequestID {
        let options = PHVideoRequestOptions()
        options.deliveryMode = .automatic
        options.isNetworkAccessAllowed = true
        
        if !asset.hk.isLocallyAvailable {
            return PHImageManager.default().requestExportSession(forVideo: asset, options: options, exportPreset: AVAssetExportPresetHighestQuality) { session, info in
                DispatchQueue.main.async {
                    if let avAsset = session?.asset {
                        completion(avAsset, info)
                    } else {
                        completion(nil, info)
                    }
                }
            }
        } else {
            return PHImageManager.default().requestAVAsset(forVideo: asset, options: options) { asset, _, info in
                DispatchQueue.main.async {
                    completion(asset, info)
                }
            }
        }
    }
    
    // 获取asset本地路径
    static func fetchAsserFilePath(asset: PHAsset, completion: @escaping (String?) -> Void) {
        asset.requestContentEditingInput(with: nil) { input, _ in
            var path = input?.fullSizeImageURL?.absoluteString
            if path == nil,
                let dir = asset.value(forKey: "directory") as? String,
               let name = asset.value(forKey: "filename") as? String {
                path = String(format: "file:///var/mobile/Media/%@/%@", dir, name)
            }
            completion(path)
        }
    }
    
    // 根据ids获取PHAssets
    static func fetchAssetWith(ids: [String]) -> PHFetchResult<PHAsset> {
        let options = PHFetchOptions()
        return PHAsset.fetchAssets(withLocalIdentifiers: ids, options: options)
    }
    
    // MARK: Save
    /// save image to album
    static func saveImageToAlbum(image: UIImage, completion: ((Bool, PHAsset?) -> Void)?) {
        let status = PHPhotoLibrary.authorizationStatus()
        
        if status == .denied || status == .restricted {
            completion?(false, nil)
            return
        }
        
        var placeholderAsset: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges {
            let newAssetRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            placeholderAsset = newAssetRequest.placeholderForCreatedAsset
        } completionHandler: { suc, _ in
            DispatchQueue.main.async { // 主线程回调
                if suc {
                    let asset = self.getAsset(from: placeholderAsset?.localIdentifier)
                    completion?(suc, asset)
                } else {
                    completion?(false, nil)
                }
            }
        }
    }
    
    /// save video to album
    static func saveVideoToAlbum(url: URL, completion: ((Bool, PHAsset?) -> Void)?) {
        let status = PHPhotoLibrary.authorizationStatus()
        
        if status == .denied || status == .restricted {
            completion?(false, nil)
            return
        }
        
        var placeholderAsset: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges {
            let newAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
            placeholderAsset = newAssetRequest?.placeholderForCreatedAsset
        } completionHandler: { suc, _ in
            DispatchQueue.main.async { // 主线程回调
                if suc {
                    let asset = self.getAsset(from: placeholderAsset?.localIdentifier)
                    completion?(suc, asset)
                } else {
                    completion?(false, nil)
                }
            }
        }
    }
    
    /// 保存asset元数据 支持image和video
    static func saveAsset(_ asset: PHAsset, toFile fileURL: URL, complection: @escaping ((Error?) -> Void)) {
        guard let resource = asset.hk.resource else {
            complection(NSError.assetSaveError)
            return
        }
        
        PHAssetResourceManager.default().writeData(for: resource, toFile: fileURL, options: nil) { error in
            DispatchQueue.main.async {
                complection(error)
            }
        }
    }
}

// MARK: Private Method
extension AssetManager {
    // 根据identifier 获取PHAsset
    static func getAsset(from localIdentifier: String?) -> PHAsset?  {
        guard let id = localIdentifier else { return nil }
        let result = PHAsset.fetchAssets(withLocalIdentifiers: [id], options: nil)  // PHFetchResult<PHAsset>
        if result.count > 0 {
            return result[0]
        }
        return nil
    }
    
    // 获取相册集合title
    static func getCollectionTitle(_ collection: PHAssetCollection) -> String {
        // 没做多语言 暂返回系统title
        return collection.localizedTitle ?? CopyWitterInstance.default().noTitleCollectionPlaceholder
    }
    
    //  fetch image from asset
    static func fetchImage(for asset: PHAsset,
                                  size: CGSize,
                                  resizeMode: PHImageRequestOptionsResizeMode,
                                  synchronous: Bool = false,
                                  progress: progressHandler = nil,
                                  completion: @escaping imageCompletionHandler) -> PHImageRequestID {
        // PHImageRequestOptions: 加载资源时的传入参数，控制资源的输出尺寸等规格；
        let option = PHImageRequestOptions()
        option.resizeMode = resizeMode
        option.isNetworkAccessAllowed = true
        if synchronous {
            option.isSynchronous = true
        }
        option.progressHandler = { pro, error, stop, info in
            DispatchQueue.main.async {
                progress?(CGFloat(pro), error, stop, info)
            }
        }
        
        return PHImageManager.default().requestImage(for: asset,
                                                     targetSize: size,
                                                     contentMode: .aspectFill,
                                                     options: option) { image, info in
            var downloadFinished = false
            if let info = info {
                downloadFinished = !(info[PHImageCancelledKey] as? Bool ?? false) && (info[PHImageErrorKey] == nil)
            }
            
            let isDegraded = (info?[PHImageResultIsDegradedKey] as? Bool ?? false)
            if downloadFinished {
                DispatchQueue.main.async {
                    completion(image, info, isDegraded)
                }
            }
        }
    }
}


extension AssetManager {
    static func hasPhotoLibratyAuthority() -> Bool {
        return PHPhotoLibrary.authorizationStatus() == .authorized
    }
    
    static func hasCameraAuthority() -> Bool {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        if status == .restricted || status == .denied {
            return false
        }
        return true
    }
    
    static func hasMicrophoneAuthority() -> Bool {
        let status = AVCaptureDevice.authorizationStatus(for: .audio)
        if status == .restricted || status == .denied {
            return false
        }
        return true
    }
}


// MARK: fileURL
extension AssetManager {
    /// 请求图片地址
    /// - Parameters:
    ///   - asset: asset description
    ///   - fileURL: 指定本地地址
    ///   - resultHandler: resultHandler description
    static func requestImageURL(for asset: PHAsset, toFile fileURL: URL, resultHandler: @escaping imageFileURLResultHandler) {
        asset.checkAdjustmentStatus { isAdjusted in
            if isAdjusted {
                fetchOriginalImageData(for: asset) { data, info, isDegraded in
                    let imageData = data
                    if let jpegData = PhotoTools.transToJpegData(imageData) {
                        DispatchQueue.global().async {
                            if let imageURL = PhotoTools.write(toFile: fileURL, imageData: imageData) {
                                DispatchQueue.main.async {
                                    resultHandler(.success(imageURL))
                                }
                            } else {
                                DispatchQueue.main.async {
                                    resultHandler(.failure(.assetResourceIsEmpty))
                                }
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            resultHandler(.failure(.assetResourceIsEmpty))
                        }
                    }
                }
            } else {
                var imageResource: PHAssetResource?
                for resource in PHAssetResource.assetResources(for: asset) where resource.type == .photo {
                    imageResource = resource
                    break
                }
                
                guard let imageResource = imageResource else {
                    resultHandler(.failure(.assetResourceIsEmpty))
                    return
                }
                
                if !PhotoTools.removeFile(fileURL: fileURL) {
                    resultHandler(.failure(.removeFileFailed))
                    return
                }
                
                let imageURL = fileURL
                let options = PHAssetResourceRequestOptions()
                options.isNetworkAccessAllowed = true
                var before: Int64 = 0
                if asset.hk.assetIsJPG() || asset.hk.assetIsPNG() {
//                    Logger.log(message: "JPG/PNG格式>>>>>>>>>>>>>", level: .info)
                    PHAssetResourceManager.default().writeData(for: imageResource, toFile: imageURL, options: options) { error in
                        DispatchQueue.main.async {
                            if let error = error {
                                resultHandler(.failure(.assetResourceWriteDataFailed(error)))
                            } else {
                                resultHandler(.success(imageURL))
                            }
                        }
                    }
                } else {
//                    Logger.log(message: "其他格式>>>>>>>>>>>>>", level: .info)
                    before = CLongLong(Date().timeIntervalSince1970 * 1000)
                    PHImageManager.default().requestImageDataAndOrientation(for: asset, options: nil) { data, uti, orientation, info in
                        if let data = data,
                           let jpegData = PhotoTools.transToJpegData(data) {
                            PhotoTools.write(toFile: imageURL, imageData: jpegData)
//                            Logger.log(message: "转为所需时间： \(CLongLong(Date().timeIntervalSince1970 * 1000) - before)", level: .info)
                            DispatchQueue.main.async {
                                resultHandler(.success(imageURL))
                            }
                        } else {
                            DispatchQueue.main.async {
                                resultHandler(.failure(.assetResourceIsEmpty))
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    /// 请求livePhoto地址
    /// - Parameters:
    ///   - asset: asset description
    ///   - imageURLHandler: imageURLHandler description
    ///   - videoURLHandler: videoURLHandler description
    ///   - completion: completion description
    static func requestLivePhoto(for asset: PHAsset,
                                       imageURLHandler: @escaping (URL?) -> Void,
                                       videoURLHandler: @escaping (URL?) -> Void,
                                       completion: @escaping (LivePhotoError?) -> Void) {
        fetchLivePhoto(for: asset) { livePhoto, info, isDegraded in
            if livePhoto == nil {
                completion(.allError(PhotoError.error(type: .imageEmpty, message: "livePhoto为nil，获取失败"),
                                     PhotoError.error(type: .videoEmpty, message: "livePhoto为nil，获取失败")))
                return
            }
            
            let assetRources: [PHAssetResource] = PHAssetResource.assetResources(for: livePhoto!)
            if assetRources.isEmpty {
                completion(.allError(PhotoError.error(type: .imageEmpty, message: "assetResources为nil，获取失败"),
                                     PhotoError.error(type: .videoEmpty, message: "assetResources为nil，获取失败")))
                return
            }
            
            let options = PHAssetResourceRequestOptions()
            options.isNetworkAccessAllowed = true
            var imageCompletion = true
            var imageError: Error?
            var videoCompletion = false
            var videoError: Error?
            let imageURL = PhotoTools.getImageTmpURL()
            let videoURL = PhotoTools.getVideoTmpURL()
            let callback = {(imageError: Error?, videoError: Error?) in
                if imageError != nil && videoError != nil {
                    completion(.allError(imageError, videoError))
                }else if imageError != nil {
                    completion(.imageError(imageError))
                }else if videoError != nil {
                    completion(.videoError(videoError))
                }else {
                    completion(nil)
                }
            }
            // 是否编辑过？
            var hasAdjustmentData = false
            for assetResource in assetRources where assetResource.type == .adjustmentData {
                hasAdjustmentData = true
                break
            }
            
            for assetResource in assetRources {
                var photoType: PHAssetResourceType = .photo
                var videoType: PHAssetResourceType = .pairedVideo
                
                if hasAdjustmentData {
                    photoType = .fullSizePhoto
                    videoType = .fullSizePairedVideo
                }
                
                if assetResource.type == photoType {
                    PHAssetResourceManager.default().writeData(for: assetResource,
                                                               toFile: imageURL,
                                                               options: options) { error in
                        DispatchQueue.main.async {
                            if error == nil {
                                imageURLHandler(imageURL)
                            }
                            imageCompletion = true
                            imageError = error
                            if videoCompletion {
                                callback(imageError, videoError)
                            }
                        }
                    }
                } else if assetResource.type == videoType {
                    PHAssetResourceManager.default().writeData(
                        for: assetResource,
                        toFile: videoURL,
                        options: options
                    ) { (error) in
                        DispatchQueue.main.async {
                            if error == nil {
                                videoURLHandler(videoURL)
                            }
                            videoCompletion = true
                            videoError = error
                            if imageCompletion {
                                callback(imageError, videoError)
                            }
                        }
                    }
                }
            }
        }
    }
}


//MARK: iCloud State
extension AssetManager {
    // 下载iCloud资源 可更新image iCloud状态
    @discardableResult
    static func downloadICloudSources(for asset: PHAsset, progress: progressHandler = nil, completion: @escaping imageDataCompletionHandler) -> PHImageRequestID {
        let option = PHImageRequestOptions()
        option.version = .original
        option.isNetworkAccessAllowed = true
        option.resizeMode = .none
        option.deliveryMode = .highQualityFormat
        option.isSynchronous = false
        option.progressHandler = { pro, error, stop, info in
            DispatchQueue.main.async {
                progress?(CGFloat(pro), error, stop, info)
            }
        }
        
        if #available(iOS 13, *) {
            return PHImageManager.default().requestImageDataAndOrientation(for: asset, options: option) { imgData, dataUTI, imgOrientation, info in
                DispatchQueue.main.async {
                    completion(imgData ?? Data(), info, false)
                }
            }
        }
        
        return PHImageManager.default().requestImageData(for: asset, options: option) { data, _, _, info in
            let cancel = info?[PHImageCancelledKey] as? Bool ?? false
            let isDegraded = (info?[PHImageResultIsDegradedKey] as? Bool ?? false)
            if !cancel {
                completion(data ?? Data(), info, isDegraded)
            }
        }
    }
}
