//
//  AssetManager+ExportSession.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/1.
//

import UIKit
import AVFoundation
import Photos
import DuomBase

extension AssetManager {
    
    struct AVAssetError: Error {
        public let info: [AnyHashable: Any]?
        public let error: AssetError
    }
    
    /// video 导出路径
    static func getVideoExportFilePath(format: String? = nil) -> String {
        let format = format ?? PhotosConfiguration.default().cameraConfiguration.videoExportType.format
        return NSTemporaryDirectory().appendingFormat("%@.%@", UUID().uuidString, format)
    }
    
    
    static func exportVideoURL(asset: PHAsset,
                               avAsset: AVAsset?,
                               toFile fileURL: URL,
                               exportPreset: ExportPreset,
                               complectionHandler: ((Result<URL, AVAssetError>) -> Void)?) {
        // 检测是否编辑了视频， 编辑过的视频需要用exportSession导出(采用最高等级清晰度)
        //        asset.checkAdjustmentStatus(completion: {
        //            print("check Status:  >>>>>>>>> \($0)")
        //        })
        
        if let avAsset = avAsset, avAsset.hk.isHEVC() {
            exportVideo(forVideo: asset, avAsset: avAsset, toFile: fileURL, exportPreset: exportPreset, complectionHandler: complectionHandler)
        } else {
            writeVideo(forVideo: asset, toFile: fileURL, exportPreset: exportPreset, complectionHandler: complectionHandler)
        }
    }
    
    @discardableResult
    private static func writeVideo(forVideo asset: PHAsset,
                                   toFile fileURL: URL,
                                   exportPreset: ExportPreset,
                                   complectionHandler: ((Result<URL, AVAssetError>) -> Void)?) {
        Logger.log(message: "进入正常视频流程++++++++++++++++++++++++++++++++++++++++++++++", level: .info)
        var videoResource: PHAssetResource?
        for resource in PHAssetResource.assetResources(for: asset) where
        resource.type == .video {
            videoResource = resource
        }
        guard let videoResource = videoResource else {
            complectionHandler?(.failure(.init(info: nil, error: AssetError.exportFailed(nil))))
            return
        }
        if !PhotoTools.removeFile(fileURL: fileURL) {
            complectionHandler?(.failure(.init(info: nil, error: AssetError.exportFailed(nil))))
            return
        }
        let videoURL = fileURL
        let options = PHAssetResourceRequestOptions()
        options.isNetworkAccessAllowed = true
        PHAssetResourceManager.default().writeData(
            for: videoResource,
            toFile: videoURL,
            options: options
        ) { (error) in
            DispatchQueue.main.async {
                if error == nil {
                    complectionHandler?(.success(videoURL))
                }else {
                    complectionHandler?(.failure(.init(info: nil, error: AssetError.exportFailed(nil))))
                }
            }
        }
    }
    
    @discardableResult
    private static func exportVideo(forVideo asset: PHAsset,
                                    avAsset: AVAsset?,
                                    toFile fileURL: URL,
                                    exportPreset: ExportPreset,
                                    complectionHandler: ((Result<URL, AVAssetError>) -> Void)?) {
        guard let avAsset = avAsset, !avAsset.hk.audioIsEmpty() else {
            toast("video missing parameters---> audio")
            return
        }
    
        Logger.log(message: "进入H265视频流程++++++++++++++++++++++++++++++++++++++++++++++", level: .info)
        exportEditVideo(for: avAsset, toFile: fileURL, range: CMTimeRange(start: .zero, end: avAsset.duration), exportPreset: exportPreset) { result in
            DispatchQueue.main.async {
                switch result {
                case let .success(url):
                    complectionHandler?(.success(url))
                case let .failure(error):
                    complectionHandler?(.failure(.init(info: nil, error: AssetError.exportFailed(nil))))
                }
            }
        }
    }
    
    
    @discardableResult
    static func exportEditVideo(for asset: AVAsset,
                                toFile fileURL: URL,
                                range: CMTimeRange?,
                                exportPreset: ExportPreset = .highQuality,
                                complete: @escaping ((Result<URL, Error>) -> Void)) {
        let composition: AVMutableComposition = AVMutableComposition()
        let videoTrack = composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        let audioTrack = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        guard let videoTrack = videoTrack,
              let audioTrack = audioTrack,
             let video = asset.tracks(withMediaType: .video).first,
              let audio = asset.tracks(withMediaType: .audio).first else {
            toast("video missing parameters")
            return
        }
        
        // fix mov can not export
        let timeRange = range ?? CMTimeRange(start: .zero, duration: asset.duration)
        do {
            try videoTrack.insertTimeRange(timeRange, of: video, at: .zero)
            try audioTrack.insertTimeRange(timeRange, of: audio, at: .zero)
        } catch {
            Logger.log(message: "error: \(error)", level: .info)
            return
        }
        
        var presetName = exportPreset.name
        let presets = AVAssetExportSession.exportPresets(compatibleWith: asset)
        if !presets.contains(presetName) {
            if presets.contains(AVAssetExportPresetHighestQuality) {
                presetName = AVAssetExportPresetHighestQuality
            }else if presets.contains(AVAssetExportPreset1280x720) {
                presetName = AVAssetExportPreset1280x720
            }else {
                presetName = AVAssetExportPresetMediumQuality
            }
        }
        
        guard let exp = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality) else {
            Logger.log(message: "AVAssetExportSession init failure", level: .info)
            return
        }
        
        
        exp.outputURL = fileURL
        exp.outputFileType = .mp4
//        exp.timeRange = range ?? CMTimeRange(start: .zero, duration: asset.duration)
        exp.shouldOptimizeForNetworkUse = true
        let videoComposition = fixedCompositionWithAsset(asset)
        if videoComposition.renderSize.width > 0 {
            exp.videoComposition = videoComposition
        }
        
        exp.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async {
                let suc = exp.status == .completed
                if suc {
                    complete(.success(fileURL))
                } else {
                    complete(.failure(exp.error ?? NSError()))
                }
            }
        })
    }
    
    @discardableResult
    static func exportVideoURL(forVideo avAsset: AVAsset,
                               toFile fileURL: URL,
                               exportPreset: ExportPreset,
                               completionHandler: ((URL?, Error?) -> Void)?) -> AVAssetExportSession? {
        var presetName = exportPreset.name
        let presets = AVAssetExportSession.exportPresets(compatibleWith: avAsset)
        if !presets.contains(presetName) {
            if presets.contains(AVAssetExportPresetHighestQuality) {
                presetName = AVAssetExportPresetHighestQuality
            }else if presets.contains(AVAssetExportPreset1280x720) {
                presetName = AVAssetExportPreset1280x720
            }else {
                presetName = AVAssetExportPresetMediumQuality
            }
        }
        let exportSession = AVAssetExportSession(
            asset: avAsset,
            presetName: presetName
        )
        exportSession?.outputURL = fileURL
        exportSession?.shouldOptimizeForNetworkUse = true
        exportSession?.outputFileType = .mp4
        exportSession?.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async {
                switch exportSession?.status {
                case .completed:
                    completionHandler?(fileURL, nil)
                case .failed, .cancelled:
                    completionHandler?(nil, exportSession?.error)
                default: break
                }
            }
        })
        if exportSession == nil {
            completionHandler?(nil, exportSession?.error)
        }
        return exportSession
    }
    
    
    static func fixedCompositionWithAsset(_ videoAsset: AVAsset) -> AVMutableVideoComposition {
        var videoComposition = AVMutableVideoComposition()
        let degress: Int = videoAsset.hk.degress()
        guard degress != 0 else {
            return videoComposition
        }
        
        guard let videoTrack = videoAsset.tracks(withMediaType: .video).first else {
            return videoComposition
        }
        
        var transToCenter = CGAffineTransform()
        var mixedTransform = CGAffineTransform()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        var roateInstruction = AVMutableVideoCompositionInstruction.init()
        roateInstruction.timeRange = CMTimeRange(start: .zero, end: videoAsset.duration)
        var roateLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
        
        switch degress {
        case 90:
            transToCenter = CGAffineTransformMakeTranslation(videoTrack.naturalSize.height, 0.0)
            mixedTransform = CGAffineTransformRotate(transToCenter, M_PI_2)
            videoComposition.renderSize = CGSize(width: videoTrack.naturalSize.height, height: videoTrack.naturalSize.width)
            roateLayerInstruction.setTransform(mixedTransform, at: .zero)
        case 180:
            transToCenter = CGAffineTransformMakeTranslation(videoTrack.naturalSize.width, videoTrack.naturalSize.height)
            mixedTransform = CGAffineTransformRotate(transToCenter, M_PI)
            videoComposition.renderSize = CGSize(width: videoTrack.naturalSize.width, height: videoTrack.naturalSize.height)
            roateLayerInstruction.setTransform(mixedTransform, at: .zero)
        case 270:
            transToCenter = CGAffineTransformMakeTranslation(0.0, videoTrack.naturalSize.width)
            mixedTransform = CGAffineTransformRotate(transToCenter, M_PI_2*3.0)
            videoComposition.renderSize = CGSize(width: videoTrack.naturalSize.height, height: videoTrack.naturalSize.width)
            roateLayerInstruction.setTransform(mixedTransform, at: .zero)
        default:
            break
        }
        
        roateInstruction.layerInstructions = [roateLayerInstruction]
        videoComposition.instructions = [roateInstruction]
        
        return videoComposition
    }
}

extension PHAsset {
    @discardableResult
    func checkAdjustmentStatus(completion: @escaping (Bool) -> Void) -> PHContentEditingInputRequestID {
        requestContentEditingInput(with: nil) { (input, info) in
            if let isCancel = info[PHContentEditingInputCancelledKey] as? Int, isCancel == 1 {
                return
            }
            let avAsset = input?.audiovisualAsset
            var isAdjusted: Bool = false
            if let path = avAsset != nil ? avAsset?.description : input?.fullSizeImageURL?.path {
                if path.contains("/Mutations/") {
                    isAdjusted = true
                }
            }
            completion(isAdjusted)
        }
    }
}
