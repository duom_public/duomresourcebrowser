//
//  VideoPreView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/6.
//

import Foundation
import DuomBase

class VideoPreView: BaseView {
    lazy var tipLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(6, 5, 6)
        label.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        return label
    }()
    
    override func setupUI() {
        super.setupUI()
        
        backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        addSubview(tipLabel)
        
        tipLabel.snp.makeConstraints { make in
            make.top.equalTo(16)
            make.centerX.equalToSuperview()
            make.height.equalTo(16.DScale)
        }
    }
    
    func setRecordingStatus()  {
        tipLabel.isHidden = true
    }
    
    func setPreingStatus()  {
        tipLabel.isHidden = false
    }
    
    func setvideoTime(_ duration: String)  {
        tipLabel.text = duration
    }
    
}
