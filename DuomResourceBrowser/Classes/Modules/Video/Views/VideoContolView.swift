//
//  VideoContolView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/5.
//

import Foundation
import DuomBase

public class VideoContolView: BaseView {
    static let controlSize = CGSize(width: 73, height: 73)
    
    var startBlock: (() -> Void)?
    var stopBlock: (() -> Void)?
    var timeOutBlock: (() -> Void)?
    
    var microPhontIsAvailable: Bool = true
    
    private lazy var circleControl: UIControl = {
        let control = UIControl()
        control.addTarget(self, action: #selector(self.clickControlAction), for: .touchUpInside)
        control.backgroundColor = .clear
        return control
    }()
    
    private lazy var circleView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private lazy var dotView: UIView = {
        let dotView = UIView()
        dotView.backgroundColor = UIColor(253, 73 , 56)
        dotView.roundCorners(radius: (VideoContolView.controlSize.width - 8 * 2) / 2)
        dotView.isUserInteractionEnabled = false
        return dotView
    }()
    
    private lazy var timedotView: UIView = {
        let dotView = UIView()
        dotView.backgroundColor = UIColor(253, 73 , 56)
        dotView.roundCorners(radius: 5 / 2)
        dotView.isHidden = true
        return dotView
    }()
    
    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .left
        label.font = .appFont(ofSize: 18, fontType: .Inter_Medium)
        label.isHidden = true
        return label
    }()
    
    private lazy var animatedLayer: CAShapeLayer = {
        let animtedLayerRadius: CGFloat = VideoContolView.controlSize.width
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: animtedLayerRadius, height: animtedLayerRadius), cornerRadius: CGFloat(animtedLayerRadius / 2))
        let layer = CAShapeLayer()
        layer.path = path.cgPath
        layer.strokeColor = UIColor.white.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineWidth = 4
        return layer
    }()
    
    private(set) var timer: Timer?
    
    private let duration: CGFloat = 60.0
    
    private(set) var startTime: CGFloat = 0.0
    
    // 是否正在执行动画
    var isCircling: Bool = false
    
    deinit {
        clearTimer()
    }
    
    public override func setupUI() {
        addSubview(timeLabel)
        addSubview(circleControl)
        circleControl.addSubview(circleView)
        circleControl.addSubview(dotView)
        
        circleControl.snp.makeConstraints { make in
            make.bottom.equalTo(-62)
            make.centerX.equalToSuperview()
            make.size.equalTo(VideoContolView.controlSize)
        }
        
        circleView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        dotView.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets.all(8))
        }
        
        timeLabel.snp.makeConstraints { make in
            make.top.equalTo(circleControl.snp.bottom).offset(16)
            make.centerX.equalToSuperview()
            make.height.equalTo(22)
        }
        
        circleView.layer.insertSublayer(animatedLayer, at: 0)
        animatedLayer.frame = CGRect(x: 0, y: 0, width: circleView.bounds.width, height: circleView.bounds.height)
    }
    
}

extension VideoContolView {
    @objc private func clickControlAction()  {
        guard microPhontIsAvailable else {
            toast(CopyWitterInstance.default().authorizationToast)
            return
        }
        if !isCircling {
            startBlock?()
            scaleDotView()
            startTimer()
            isCircling = true
        } else {
            stopBlock?()
            resetSubViewStatus()
            clearTimer()
            isCircling = false
        }
    }
    
    private func startTimer()  {
        clearTimer()
        timer = Timer.schedule(repeatInterval: 0.1, handler: { [weak self] _ in
            guard let self = self else { return }
            self.startTime += 0.1
            self.timeLabel.text = String(format: "%.1f", self.startTime)
            if self.startTime >= self.duration {
                self.timeOutAction()
            }
        })
        timer?.fireDate = .distantPast
        RunLoop.current.add(timer!, forMode: .common)
    }
    
    private func timeOutAction()  {
        timeOutBlock?()
        delay(1.0) { [weak self] in
            self?.resetSubViewStatus()
        }
        self.isCircling = false
        self.startTime = 0.0
        self.clearTimer()
    }
    
    func clearTimer()  {
        timer?.invalidate()
        timer = nil
    }
    
    private func scaleDotView()  {
        dotView.snp.remakeConstraints { make in
            make.size.equalTo(CGSize(width: 25, height: 25).DScale)
            make.center.equalToSuperview()
        }
        dotView.roundCorners(radius: 10)
        dotView.isHidden = false
        timeLabel.isHidden = false
    }
    
    func resetSubViewStatus()  {
        dotView.snp.remakeConstraints { make in
            make.edges.equalTo(UIEdgeInsets.all(8))
        }
        dotView.roundCorners(radius: (VideoContolView.controlSize.width - 8 * 2) / 2)
        dotView.isHidden = false
        timeLabel.isHidden = true
        startTime = 0.0
        isCircling = false
    }
}
