//
//  VideoNavView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/6.
//

import Foundation
import DuomBase

class VideoNavView: BaseView {
    var backBlock: (() -> Void)?
    var nextBlock: (() -> Void)?
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "nav_back"), for: .normal)
        button.setImage(UIImage(named: "nav_back"), for: .highlighted)
        button.addTarget(self, action: #selector(self.clickBackAction(_:)) , for: .touchUpInside)
        button.setEnlargeEdge(.all(15))
        return button
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(CopyWitterInstance.default().NextText, for: .normal)
        button.titleLabel?.font = .appFont(ofSize: 14, fontType: .TT_Bold)
        button.backgroundColor = PhotosConfiguration.default().mainThemeColor
        button.roundCorners(radius: 8)
        button.addTarget(self, action: #selector(self.clickReverAction(_:)) , for: .touchUpInside)
        button.setEnlargeEdge(.all(15))
        return button
    }()

    
    override func setupUI() {
        backgroundColor = .clear
        
        addSubview(backButton)
        addSubview(nextButton)
        
        backButton.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.top.equalTo(10)
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
        
        nextButton.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.top.equalTo(5)
            make.size.equalTo(CGSize(width: 71, height: 34))
        }
    }
    
    @objc private func clickBackAction(_ sender: UIButton)  {
        backBlock?()
    }
    
    @objc private func clickReverAction(_ sender: UIButton)  {
        nextBlock?()
    }
    
    func resetBackButtonHiddenStatus()  {
        backButton.isHidden = false
    }
    
    func setNextButtonStatus(hidden: Bool)  {
        nextButton.isHidden = hidden
    }
}
