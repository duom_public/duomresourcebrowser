//
//  DuomVideoController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/24.
//

import Foundation
import DuomBase
import AVFoundation
import RxCocoa
import RxSwift
import Photos

class VideoController: CameraBaseViewController {
    private var recordVideoPlayerLayer: AVPlayerLayer?
    
    private var videoUrl: URL?
    
    private var isPlaying: Bool = false
    
    // 首页是否正在展示
    var isShowing: Bool = false
    
    private let navView = VideoNavView().then {
        $0.isHidden = true
    }
    
    private lazy var controlView: VideoContolView = {
        let view = VideoContolView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var preTipView: VideoPreView = {
        let preView = VideoPreView()
        preView.isHidden = true
        return preView
    }()

    private lazy var authTipsView: AuthTipsCommonView = {
        let view = AuthTipsCommonView(type: .cameraAndMicrophone)
        view.backgroundColor = .white
        view.isHidden = true
        return view
    }()
    
    override func viewDidLoad() {
        sessionQueue = DispatchQueue(label: "hk.video.sessionQueue")
        setUpSubviews()
        
        AuthorizationHelper.checkAuthorizationWithOutAlert(option: [.recordPermission, .cameraPermission])
            .andThen(Observable.just(true))
            .subscribe(onNext: {  _ in
                self.setupCamera()
                self.sessionQueue.async {
                    self.session.startRunning()
                }
                super.viewDidLoad()
            }, onError: { [weak self] _ in
                self?.microPhontIsAvailable = false
                self?.controlView.microPhontIsAvailable = false
//                toast(CopyWitterInstance.default().authorizationToast)
                if ((self?.authTipsView) != nil) {
                    self!.authTipsView.isHidden = false
                    self!.view.bringSubviewToFront(self!.authTipsView)
                    self!.statusBarStyle = UIStatusBarStyle.darkContent
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    func setUpSubviews() {
        view.backgroundColor = UIColor(6, 5, 6)
        view.addSubview(navView)
        view.addSubview(controlView)
        
        navView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(UIScreen.topOffset)
            make.height.equalTo(UIScreen.navigationBarHeight - UIScreen.topOffset)
        }
        
        controlView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(135)
        }
        
        controlView.startBlock = { [weak self] in
            self?.reverseButton.isHidden = true
            self?.startRecord()
            self?.addVideoPreView()
            self?.preTipView.isHidden = false
            self?.preTipView.tipLabel.isHidden = true
        }
        
        controlView.stopBlock = { [weak self, weak controlView] in
            self?.navView.isHidden = false
            self?.controlView.isHidden = true
            self?.reverseButton.isHidden = true
            self?.preTipView.tipLabel.isHidden = false
            self?.preTipView.tipLabel.text = controlView?.timeLabel.text
            self?.finishRecord()
        }
        
        controlView.timeOutBlock = { [weak self, weak controlView] in
            self?.navView.isHidden = false
            self?.controlView.isHidden = true
            self?.reverseButton.isHidden = true
            self?.preTipView.tipLabel.isHidden = false
            self?.preTipView.tipLabel.text = "60:00"
            self?.finishRecord()
            toast("max 60 second")
        }
        
        navView.nextBlock = { [weak self] in
            MobStatistics.trackAction("ImagePicker_video_next_click")
            guard let self = self else { return }
            if self.isPlaying == true {
                self.resetSubViewStatus()
                self.restartSession()
            }
            self.showEditVC()
        }
        
        navView.backBlock = { [weak self] in
            guard let self = self else { return }
            self.videoUrl = nil
            self.resetSubViewStatus()
            self.restartSession()
        }
        
        // playerLayer
        recordVideoPlayerLayer = AVPlayerLayer()
        recordVideoPlayerLayer?.backgroundColor = UIColor.black.cgColor
        recordVideoPlayerLayer?.videoGravity = .resize
        recordVideoPlayerLayer?.isHidden = true
        recordVideoPlayerLayer?.frame = CGRect(x: 0, y: UIScreen.topOffset, width: view.bounds.width, height: view.bounds.height - UIScreen.topOffset - PhotosConfiguration.default().segmentViewHight)
        view.layer.addSublayer(recordVideoPlayerLayer!)
        NotificationCenter.default.addObserver(self, selector: #selector(videoDidPlayEnd), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer?.videoGravity = .resizeAspectFill
        previewLayer?.opacity = 0
        previewLayer?.frame = CGRect(x: 0, y: UIScreen.topOffset, width: view.bounds.width, height: view.bounds.height - UIScreen.topOffset - PhotosConfiguration.default().segmentViewHight)
        let animation = getFadeAnimation(fromValue: 0, toValue: 1, duration: 0.15)
        previewLayer?.add(animation, forKey: nil)
        view.layer.masksToBounds = true
        view.layer.insertSublayer(previewLayer!, at: 0)
        
        // 放到最前面 防止被遮挡
        view.bringSubviewToFront(navView)
        
        sessionIsReady = true
        
        view.insertSubview(authTipsView, at: 0)
        authTipsView.snp.makeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
    
    override func addNotification() {
        super.addNotification()
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleAudioSessionInterruption), name: AVAudioSession.interruptionNotification, object: nil)
    }
    
    
    func setupCamera()  {
        guard let backCamera = getCamera(position: .back) else { return }
        guard let input = try? AVCaptureDeviceInput(device: backCamera) else { return }
        
        session.beginConfiguration()
        
        // 相机画面输出流
        videoInput = input
        // 照片输出流
        imageOutput = AVCapturePhotoOutput()
        
        let preset = PhotosConfiguration.default().cameraConfiguration.sessionPreset.avSessionPresent
        if session.canSetSessionPreset(preset) {
            session.sessionPreset = preset
        } else {
            session.sessionPreset = .hd1280x720
        }
        
        movieFileOutput = AVCaptureMovieFileOutput()
        movieFileOutput.movieFragmentInterval = .invalid
        
        // 视频输入
        if let videoInput = videoInput, session.canAddInput(videoInput) {
            session.addInput(videoInput)
        }
        
        // 添加音频输入
        addAudioInput()
        
        // 输出流添加到session
        if session.canAddOutput(imageOutput) {
            session.addOutput(imageOutput)
        }
        if session.canAddOutput(movieFileOutput) {
            session.addOutput(movieFileOutput)
        }
        
        session.commitConfiguration()
        
        sessionIsReady = true
    }
    
    func addVideoPreView()  {
        if let parent = UIApplication.topViewController() as? ImagePickerMainController {
            parent.view.addSubview(preTipView)
            preTipView.snp.makeConstraints { make in
                make.bottom.equalTo(parent.view)
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(PhotosConfiguration.default().segmentViewHight)
            }
        }
    }
    
    private func showEditVC()  {
        guard let videoUrl = videoUrl else {
            return
        }
        let avAsset = AVAsset(url: videoUrl)
        let editVideoVC = ClipVideoViewController(avAsset: avAsset)
        editVideoVC.videoURL = videoUrl
        UIApplication.topViewController()?.navigationController?.show(editVideoVC, sender: nil)
    }
    
    deinit {
        preTipView.removeFromSuperview()
        DispatchQueue.main.async {
            UIApplication.shared.isIdleTimerDisabled = true
        }
    }
    
}

// MARK: record
extension VideoController {
    private func startRecord()  {
        guard !movieFileOutput.isRecording else {
            return
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.isIdleTimerDisabled = true
        }
        
        let connection = movieFileOutput.connection(with: .video)
        connection?.videoScaleAndCropFactor = 1
        
        if videoInput?.device.position == .front, connection?.isVideoMirroringSupported == true {
            connection?.isVideoMirrored = true
        }
        
        movieFileOutput.startRecording(to: PhotoTools.getVideoTmpURL(), recordingDelegate: self)
    }
    
    private func finishRecord()  {
        guard movieFileOutput.isRecording else {
            return
        }
        DispatchQueue.main.async {
            UIApplication.shared.isIdleTimerDisabled = false
        }
        try? AVAudioSession.sharedInstance().setCategory(.playback)
        movieFileOutput.stopRecording()
    }
    
    private func playRecordVideo(fileUrl: URL)  {
        isPlaying = true
        preTipView.setPreingStatus()
        recordVideoPlayerLayer?.isHidden = false
        navView.isHidden = false
        navView.resetBackButtonHiddenStatus()
        
        let player = AVPlayer(url: fileUrl)
        player.automaticallyWaitsToMinimizeStalling = false
        recordVideoPlayerLayer?.player = player
        player.play()
    }
    
    @objc private func videoDidPlayEnd()  {
        recordVideoPlayerLayer?.player?.seek(to: CMTime.zero)
        recordVideoPlayerLayer?.player?.play()
    }
    
    private func resetSubViewStatus()  {
        isPlaying = false
        self.reverseButton.isHidden = false
        recordVideoPlayerLayer?.isHidden = true
        recordVideoPlayerLayer?.player?.pause()
        recordVideoPlayerLayer?.player = nil
        preTipView.setRecordingStatus()
        preTipView.isHidden = true
        navView.isHidden = true
        navView.resetBackButtonHiddenStatus()
        controlView.isHidden = false
        controlView.clearTimer()
        controlView.resetSubViewStatus()
    }
    
    private func restartSession()  {
        sessionQueue.async {
            self.session.startRunning()
        }
    }
}

extension VideoController: AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
         Logger.log(message: "record did start", level: .info)
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        self.session.stopRunning()
        self.videoUrl = outputFileURL
        self.playRecordVideo(fileUrl: outputFileURL)
    }
}

// MARK: Notification
extension VideoController {
    override func appWillResignActive() {
        super.appWillResignActive()
        if controlView.isCircling {
            videoUrl = nil
            finishRecord()
            DispatchQueue.main.async {
                self.resetSubViewStatus()
            }
        }
    }
    
    @objc private func appDidBecomeActive() {
        if videoUrl != nil, let player = recordVideoPlayerLayer?.player {
            player.play()
        }
        
        if isShowing {
            sessionQueue.async {
                self.session.startRunning()
            }
        }
    }
    
    @objc private func handleAudioSessionInterruption(_ notify: Notification) {
        guard recordVideoPlayerLayer?.isHidden == false, let player = recordVideoPlayerLayer?.player else {
            return
        }
        guard player.rate == 0 else {
            return
        }
        
        let type = notify.userInfo?[AVAudioSessionInterruptionTypeKey] as? UInt
        let option = notify.userInfo?[AVAudioSessionInterruptionOptionKey] as? UInt
        if type == AVAudioSession.InterruptionType.ended.rawValue, option == AVAudioSession.InterruptionOptions.shouldResume.rawValue {
            player.play()
        }
    }
}
