//
//  DuomCameraController.swift
//  CameraController
//
//  Created by kuroky on 2022/11/24.
//

import UIKit
import DuomBase
import AVFoundation
import RxCocoa
import RxSwift
import Photos

class CameraController: CameraBaseViewController {
    private var takedImage: UIImage?
    // 首页是否正在展示
    var isShowing: Bool = false
    
    // 添加资源
    var isAddScence: Bool = false
    
    var appendItemByCameraBlock: ((PhotoModel)->Void)?
    
    // 是否是作为入口页面(单拍照逻辑)
    var isMain: Bool = false
    
    private lazy var cameraButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "camera_take"), for: .normal)
        button.setImage(UIImage(named: "camera_take"), for: .highlighted)
        button.addTarget(self, action: #selector(takePhotoAction(_:)) , for: .touchUpInside)
        button.setEnlargeEdge(.all(15))
        return button
    }()
    
    private lazy var takedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isHidden = true
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var navView: VideoNavView = {
        let naview = VideoNavView()
        if isMain {
            naview.setNextButtonStatus(hidden: true)
        } else {
            naview.isHidden = true
        }
        return naview
    }()
    
    private lazy var preTipView: VideoPreView = {
        let preView = VideoPreView()
        preView.isHidden = true
        preView.tipLabel.isHidden = true
        return preView
    }()

    private lazy var authTipsView: AuthTipsCommonView = {
        let view = AuthTipsCommonView(type: .cameraAndMicrophone)
        view.backgroundColor = .white
        view.isHidden = true
        return view
    }()
    
    convenience init(isMain: Bool = false) {
        self.init(nibName: nil, bundle: nil)
        self.isMain = isMain
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        sessionQueue = DispatchQueue(label: "hk.video.sessionQueue")
        setUpSubviews()
        AuthorizationHelper.checkAuthorizationWithOutAlert(option: [.cameraPermission, .recordPermission])
            .andThen(Observable.just(true))
            .subscribe(onNext: {  _ in
                self.setupCamera()
                self.sessionQueue.async {
                    self.session.startRunning()
                }
                super.viewDidLoad()
            }, onError: { [weak self] _ in
                self?.microPhontIsAvailable = false
//                toast(CopyWitterInstance.default().authorizationToast)
                if ((self?.authTipsView) != nil) {
                    self!.authTipsView.isHidden = false
                    self!.view.bringSubviewToFront(self!.authTipsView)
                    self!.statusBarStyle = UIStatusBarStyle.darkContent
                }
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        clearViewStates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isMain {
            sessionQueue.async {
                self.session.startRunning()
            }
        }
    }
    
    override func addNotification() {
        super.addNotification()
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    
    func setupCamera()  {
        guard let backCamera = getCamera(position: .back) else { return }
        guard let input = try? AVCaptureDeviceInput(device: backCamera) else { return }
        
        session.beginConfiguration()
        
        // 相机画面输出流
        videoInput = input
        // 照片输出流
        imageOutput = AVCapturePhotoOutput()
        
        let preset = PhotosConfiguration.default().cameraConfiguration.sessionPreset.avSessionPresent
        if session.canSetSessionPreset(preset) {
            session.sessionPreset = preset
        } else {
            session.sessionPreset = .hd1280x720
        }
        
        movieFileOutput = AVCaptureMovieFileOutput()
        movieFileOutput.movieFragmentInterval = .invalid
        
        // 视频输入
        if let videoInput = videoInput, session.canAddInput(videoInput) {
            session.addInput(videoInput)
        }
        
        // 添加音频输入
//        addAudioInput()
        
        // 输出流添加到session
        if session.canAddOutput(imageOutput) {
            session.addOutput(imageOutput)
        }
        if session.canAddOutput(movieFileOutput) {
            session.addOutput(movieFileOutput)
        }
        
        session.commitConfiguration()
        
        sessionIsReady = true
    }
    
    
    func setUpSubviews() {
        view.backgroundColor = .black
        
        view.addSubview(navView)
        view.addSubview(cameraButton)
        view.addSubview(takedImageView)
        view.sendSubviewToBack(takedImageView)
        
        navView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(UIScreen.topOffset)
            make.height.equalTo(UIScreen.navigationBarHeight - UIScreen.topOffset)
        }
        
        cameraButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 73, height: 73))
            make.bottom.equalTo(-62)
            make.centerX.equalToSuperview()
        }
        
        takedImageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(UIScreen.topOffset)
            make.height.equalTo(isMain ? UIScreen.screenHeight - UIScreen.topOffset : UIScreen.screenHeight - PhotosConfiguration.default().segmentViewHight - UIScreen.topOffset)
        }
        view.layoutIfNeeded()
        takedImageView.layoutIfNeeded()
        
        navView.backBlock = { [weak self] in
            guard let self = self else { return }
            if self.takedImage != nil {
                self.clearViewStates()
                self.sessionQueue.async {
                    self.session.startRunning()
                }
            } else {
                self.dismiss()
            }
        }

        navView.nextBlock = { [weak self] in
            MobStatistics.trackAction("ImagePicker_cam_next_click")
            guard let self = self else { return }
            self.nextAction()
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer?.videoGravity = .resizeAspectFill
        previewLayer?.opacity = 0
        previewLayer?.frame = takedImageView.frame
        let animation = getFadeAnimation(fromValue: 0, toValue: 1, duration: 0.15)
        previewLayer?.add(animation, forKey: nil)
        view.layer.masksToBounds = true
        view.layer.insertSublayer(previewLayer!, at: 0)
        
        view.insertSubview(authTipsView, at: 0)
        authTipsView.snp.makeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
    
    func addPreView()  {
        func addViewToParent(_ parent: UIViewController) {
            parent.view.addSubview(preTipView)
            preTipView.snp.makeConstraints { make in
                make.bottom.equalTo(parent.view)
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(PhotosConfiguration.default().segmentViewHight)
            }
            preTipView.isHidden = false
            parent.view.bringSubviewToFront(preTipView)
        }
        if let parent = UIApplication.topViewController() as? ImagePickerMainController {
           addViewToParent(parent)
            return
        }
        
        if let parent = UIApplication.topViewController() as? PresentAllController {
           addViewToParent(parent)
           return
        }
    }
    
    private func nextAction()  {
        guard PhotosConfiguration.default().allowEdit else {
            if let model = self.generatePhotoModel(),
               let imageOriginalData = model.imageOriginalData,
                let url = PhotoTools.write(imageData: imageOriginalData) {
                model.fileURL = url
                model.mediaType = .image
                SourceInstance.shared.selectModels.append(model)
            }
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                SourceInstance.shared.selectedSourceBlock?(SourceInstance.shared.selectModels)
            })
            return
        }
        
        if self.isAddScence {
            if let model = self.generatePhotoModel() {
                self.appendItemByCameraBlock?(model)
                UIApplication.topViewController()?.dismiss(animated: true)
            }
            return
        }
        
        if PhotosConfiguration.default().selectSingleImg {
            self.toClipVC()
        } else {
            self.showEditVC()
        }
    }
    
    private func showEditVC()  {
        if let model = generatePhotoModel() {
            SourceInstance.shared.selectModels.append(model)
            let editVC = EditorController()
            editVC.isPushed = true
            UIApplication.topViewController()?.navigationController?.pushViewController(editVC, animated: true)
        }
    }
    
    private func generatePhotoModel() -> PhotoModel? {
        guard let takedImage = takedImage else {
            return nil
        }
        
        // 初始化一个空model
        let tempModel = PhotoModel(asset: nil)
        tempModel.imageOriginalData = PhotoTools.getImageData(for: takedImage)
        tempModel.isTakedPhoto = true
        return tempModel
    }
    
    private func toClipVC()  {
        guard let img = takedImage else {
            return
        }
        if PhotosConfiguration.default().editImageConfiguration.assignClipRations == .circle {
            let clipVC = ClipOneImageCircleViewController(image: img)
            UIApplication.topViewController()?.navigationController?.pushViewController(clipVC, animated: true)
        } else {
            let clipVC = ClipOneImageViewController(image: img)
            UIApplication.topViewController()?.navigationController?.pushViewController(clipVC, animated: true)
        }
    }
    
}

// MARK: Private Method
extension CameraController {
    @objc private func takePhotoAction(_ sender: UIButton)  {
        guard microPhontIsAvailable else {
            toast(CopyWitterInstance.default().authorizationToast)
            return
        }
        guard imageOutput != nil else { return }
        let connection = imageOutput.connection(with: .video)
        connection?.videoOrientation = orientation
        if videoInput?.device.position == .front, connection?.isVideoMirroringSupported == true {
            connection?.isVideoMirrored = true
        }
        
        let setting = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        imageOutput.capturePhoto(with: setting, delegate: self)
    }
    
    // 拍摄完成后 修改views状态
    private func resetSubviewsStatus()  {
        cameraButton.isHidden = true
        if isMain {
            navView.setNextButtonStatus(hidden: false)
        } else {
            navView.isHidden = false
        }
        self.reverseButton.isHidden = true
        addPreView()
    }
    
    private func dismiss()  {
        if let nav = UIApplication.topViewController()?.navigationController as? PhotoNavgationController {
            SourceInstance.shared.cancelBlock?()
            clearViewStates()
            nav.dismiss(animated: true)
        }
    }
    
    // subViews回初始状态
    private func clearViewStates()  {
        takedImageView.isHidden = true
        takedImage = nil
        takedImageView.image = nil
        if isMain {
            navView.setNextButtonStatus(hidden: true)
        } else {
            navView.isHidden = true
        }
        preTipView.removeFromSuperview()
        reverseButton.isHidden = false
        cameraButton.isHidden = false
    }
    
    @objc private func appDidBecomeActive() {
        if self.takedImage == nil, (isShowing || isMain) {
            sessionQueue.async {
                self.session.startRunning()
            }
        }
    }
}

extension CameraController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if photo.pixelBuffer == nil, error != nil {
            return
        }
        
        if let data = photo.fileDataRepresentation() {
            self.session.stopRunning()
            self.takedImage = UIImage(data: data)?.fixOrientation()
            self.takedImageView.image = self.takedImage
            self.takedImageView.isHidden = false
            self.resetSubviewsStatus()
        } else {
             Logger.log(message: "take photo failure", level: .info)
        }
    }
}


