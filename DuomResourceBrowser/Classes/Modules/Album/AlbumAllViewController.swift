//
//  AlbumAllViewController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/5/9.
//

import Foundation
import DuomBase
import Photos

class AlbumCollectionView: UICollectionView, SyncInnerScroll {}
extension AlbumAllViewController: DuomPagingContainerItem {}
class AlbumAllViewController: BaseViewController {
    /// 指定相册
    var currentAlbum: AlbumListModel? {
        didSet {
            self.loadPhotos()
        }
    }
    
    /// 照片数据源
    private var dataSource: [PhotoModel] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.collectionView.reloadData()
            }
        }
    }
    
    private var assetHasChange: Bool = false
    
    /// 选中的最终数据
    private var selectedModels: [PhotoModel] {
        get {
            return SourceInstance.shared.selectModels
        }
        set {
            SourceInstance.shared.selectModels = newValue
        }
    }
    
    var tipsView: AuthorizationAlbumPlaceholderView?
    
    lazy var authTipsView: AuthTipsCommonView = {
        let view = AuthTipsCommonView(type: .photosAndVideos)
        view.isShowNavView = false
        return view
    }()

    // 无权限回调
    var authNoneStateBlock: (() -> Void)?
    
    // 选择资源回调 是否为空
    var sourceBlock: ((Bool) -> Void)?
    
    lazy var collectionView: AlbumCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: PhotosConfiguration.default().photoItemWH, height: PhotosConfiguration.default().photoItemWH)
        layout.minimumLineSpacing = PhotosConfiguration.default().photoSpace
        layout.minimumInteritemSpacing = PhotosConfiguration.default().photoSpace
        let view = AlbumCollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        view.scrollsToTop = false
        view.dataSource = self
        view.delegate = self
        view.contentInsetAdjustmentBehavior = .never
        view.register(AlbumCollectionCell.self, forCellWithReuseIdentifier: AlbumCollectionCell.identifier)
        if let _ = tipsView {
            layout.footerReferenceSize = CGSize(width: UIScreen.screenWidth, height: 434.DScale)
            view.register(AuthorizationAlbumPlaceholderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: AuthorizationAlbumPlaceholderView.identifier)
        }
        return view
    }()
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
    }
    
    
    override func setupConstraints() {
        super.setupConstraints()
        var isAuthNone = false
        if #available(iOS 14.0, *) {
            if PHPhotoLibrary.authorizationStatus(for: .readWrite) == .limited {
                tipsView = AuthorizationAlbumPlaceholderView(type: .part)
                authTipsView.removeFromSuperview()
                // 注册相册数据变化监听
                PHPhotoLibrary.shared().register(self)
            } else if PHPhotoLibrary.authorizationStatus(for: .readWrite) == .denied {
//                tipsView = AuthorizationAlbumPlaceholderView(type: .none)
                isAuthNone = true
                tipsView = nil
                view.addSubview(authTipsView)
                authTipsView.snp.makeConstraints{ make in
                    make.edges.equalToSuperview()
                }
                authNoneStateBlock?()
            }
        } else {
            if PHPhotoLibrary.authorizationStatus() != .authorized {
//                tipsView = AuthorizationAlbumPlaceholderView(type: .none)
                tipsView = nil
                isAuthNone = true
                view.addSubview(authTipsView)
                authTipsView.snp.makeConstraints{ make in
                    make.edges.equalToSuperview()
                }
                authNoneStateBlock?()
            }
        }
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        if isAuthNone {
            view.bringSubviewToFront(authTipsView)
        }
    }
    
    // 外部主动刷新页面
    func reloadSelctedItems()  {
        selectedModels = SourceInstance.shared.selectModels
        collectionView.reloadData()
    }
    
    deinit {
         Logger.log(message: "AlbumAllViewController deinit", level: .info)
    }
}

extension AlbumAllViewController {
    // 获取指定相册里的asset
    private func loadPhotos() {
        guard let currentAlbum = currentAlbum else {
            return
        }
        
        if self.currentAlbum!.models.isEmpty {
            DispatchQueue.global().async {
                self.currentAlbum?.refetchPhotos()
                
                DispatchQueue.main.async {
                    self.dataSource.removeAll()
                    self.dataSource.append(contentsOf: self.currentAlbum!.models)
                }
            }
        } else {
            dataSource.removeAll()
            dataSource.append(contentsOf: currentAlbum.models)
        }
    }
}

// MARK: collectionDelegate&dataSource
extension AlbumAllViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollectionCell.identifier, for: indexPath) as! AlbumCollectionCell
        let config = PhotosConfiguration.default()
        var currentItem = dataSource[indexPath.row]
        
        if selectedModels.isEmpty {
            currentItem.isSelected = false
        }
        
        cell.selectedBlock = { [weak self, weak cell] (isSelected, selectedItem) in
            guard let self = self, let selectedItem = selectedItem else { return }
            if PhotosConfiguration.default().selectSingleImg {
                if !isSelected {
                    selectedItem.isSelected = true
                    cell?.selectedButton.isSelected = true
                    self.selectedModels.forEach {
                        $0.isSelected = false
                    }
                    self.selectedModels.removeAll()
                    self.selectedModels.append(selectedItem)
                } else {
                    selectedItem.isSelected = false
                    cell?.selectedButton.isSelected = false
                    self.selectedModels.removeAll()
                }
                self.refreshCellIndexAndMaskView()
            } else {
                if !isSelected {
                    if selectedItem.mediaType == .video {
                        selectedItem.isSelected = true
                        cell?.selectedButton.isSelected = true
                        self.selectedModels.forEach {
                            $0.isSelected = false
                        }
                        self.selectedModels.removeAll()
                        self.selectedModels.append(selectedItem)
                    } else {
                        if self.selectedModels.count >= PhotosConfiguration.default().maxSelectCount {
                            toast(CopyWitterInstance.default().overMaxSelectCount(PhotosConfiguration.default().maxSelectCount))
                            return
                        }
                        selectedItem.isSelected = true
                        cell?.selectedButton.isSelected = true
                        self.selectedModels.append(selectedItem)
                    }
                    
                    self.refreshCellIndexAndMaskView()
                } else {
                    selectedItem.isSelected = false
                    cell?.selectedButton.isSelected = false
                    self.selectedModels.removeAll { $0 == selectedItem }
                    self.refreshCellIndexAndMaskView()
                }
            }
            
            self.sourceBlock?(self.selectedModels.isEmpty ? true : false)
        }
        
        cell.indexLabel.isHidden = true
        
        for (index, selM) in selectedModels.enumerated() {
            if currentItem == selM {
                cell.index = index + 1
                cell.indexLabel.isHidden = false
                cell.selectedButton.isSelected = true
                currentItem.isSelected = true
            }
        }
        
        setCellMaskView(cell, isSelected: currentItem.isSelected, model: currentItem)
        
        cell.configCell(&currentItem)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableV = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: AuthorizationAlbumPlaceholderView.identifier, for: indexPath) as! AuthorizationAlbumPlaceholderView
        return reusableV
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? AlbumCollectionCell else {
            return
        }
        
        let model = dataSource[indexPath.row]
        
        // mask
        setCellMaskView(cell, isSelected: model.isSelected, model: model)
    }
    
    func refreshCellIndexAndMaskView()  {
        let visableIndexPaths = collectionView.indexPathsForVisibleItems
        visableIndexPaths.forEach { indexPath in
            guard let cell = self.collectionView.cellForItem(at: indexPath) as? AlbumCollectionCell else {
                return
            }
            var row = indexPath.row
            let currentModel = self.dataSource[row]
            var idx = 0
            var isSelected = false
            var show = false
            for (index, selM) in self.selectedModels.enumerated() {
                if currentModel == selM {
                    show = true
                    idx = index + 1
                    isSelected = true
                    break
                }
            }
            
            cell.index = idx
            cell.indexLabel.isHidden = !isSelected
            
            setCellMaskView(cell, isSelected: isSelected, model: currentModel)
        }
    }
    
    func setCellMaskView(_ cell: AlbumCollectionCell, isSelected: Bool, model: PhotoModel)  {
        cell.coverView.isHidden = true
        cell.enable = true
//        guard selectedModels.isNotEmpty else {
//            return
//        }
        
        if isSelected {
            cell.coverView.isHidden = true
            cell.enable = true
        } else {
            if PhotosConfiguration.default().selectSingleImg {
                cell.enable = true
                cell.coverView.isHidden = true
                cell.selectedButton.isSelected = false
            } else if PhotosConfiguration.default().selectType == .image {
                if model.mediaType == .video {
                    cell.coverView.isHidden = false
                    cell.enable =  false
                }
            } else {
                if selectedModels.isNotEmpty {
                    if self.selectedModels.first!.mediaType == .video {
                        if model.mediaType == .video {
                            cell.coverView.isHidden = true
                            cell.enable =  true
                            cell.selectedButton.isSelected = false
                        } else {
                            cell.coverView.isHidden = false
                            cell.enable =  false
                        }
                    } else {
                        if model.mediaType == .video {
                            cell.coverView.isHidden = false
                            cell.enable =  false
                        } else {
                            cell.coverView.isHidden = true
                            cell.enable =  true
                        }
                    }
                }
            }
        }
    }
}

extension AlbumAllViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        // changeDetails 是否发生变化
        guard let currentAlbum = currentAlbum, let changes = changeInstance.changeDetails(for: currentAlbum.result) else {
            return
        }
        
        // 接收通知可能会在后台线程
        DispatchQueue.main.async {
            self.assetHasChange = true // 再次打开相册列表 要刷新数据
            
            // 更新当前相册数据
            self.currentAlbum?.result = changes.fetchResultAfterChanges
            
            if changes.hasIncrementalChanges {
                // 删除了已经选中的model
                for photoModel in self.selectedModels {
                    if let asset = photoModel.asset {
                        let isDeleted = changeInstance.changeDetails(for: asset)?.objectWasDeleted ?? false
                        if isDeleted {
                            self.selectedModels.removeAll { $0 == photoModel }
                        }
                    }
                }
                
                // 有增删
                if !changes.removedObjects.isEmpty || !changes.insertedObjects.isEmpty {
                    self.currentAlbum?.models.removeAll()
                }
                
                self.loadPhotos()
            } else {
                // hasIncrementalChanges == no  表示可能更改的范围很大 还是应该刷新数据
                for photoModel in self.selectedModels {
                    if let asset = photoModel.asset {
                        let isDeleted = changeInstance.changeDetails(for: asset)?.objectWasDeleted ?? false
                        if isDeleted {
                            self.selectedModels.removeAll { $0 == photoModel }
                        }
                    }
                }
                
                self.currentAlbum?.models.removeAll()
                
                self.loadPhotos()
            }
        }
    }
}
