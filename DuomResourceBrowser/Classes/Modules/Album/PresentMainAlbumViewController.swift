//
//  PresentMainAlbumViewController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/12.
//

import Foundation
import DuomBase
import Photos

class PresentMainAlbumViewController: BaseViewController {
    private let navView: AlbumListNavView = {
        let v = AlbumListNavView.init(title: "")
        return v
    }()
    
    private lazy var filterListView: AlbumFilterListView = {
        let listView = AlbumFilterListView(dataSource: [AlbumListModel]())
        return listView
    }()
    
    /// 相册列表
    private var albumList: [AlbumListModel] = []
    
    /// 指定相册
    var currentAlbum: AlbumListModel? {
        didSet {
            self.loadPhotos()
        }
    }
    
    /// 照片数据源
    private var dataSource: [PhotoModel] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.collectionView.reloadData()
            }
        }
    }
    
    /// 选中的最终数据
    private var selectedModels: [PhotoModel] {
        get {
            return SourceInstance.shared.selectModels
        }
        set {
            SourceInstance.shared.selectModels = newValue
        }
    }
    
    lazy var collectionView: AlbumCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: PhotosConfiguration.default().photoItemWH, height: PhotosConfiguration.default().photoItemWH)
        layout.minimumLineSpacing = PhotosConfiguration.default().photoSpace
        layout.minimumInteritemSpacing = PhotosConfiguration.default().photoSpace
        let view = AlbumCollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        view.scrollsToTop = false
        view.dataSource = self
        view.delegate = self
        view.contentInsetAdjustmentBehavior = .never
        view.register(AlbumCollectionCell.self, forCellWithReuseIdentifier: AlbumCollectionCell.identifier)
        if let _ = tipsView {
            layout.footerReferenceSize = CGSize(width: UIScreen.screenWidth, height: 350.DScale)
            view.register(AuthorizationAlbumPlaceholderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: AuthorizationAlbumPlaceholderView.identifier)
        }
        return view
    }()
    
    var appendItmeBlock: (() -> Void)?
    
    var tipsView: AuthorizationAlbumPlaceholderView?
    
    private var assetHasChange: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        getAlbumList()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        
        if #available(iOS 14.0, *) {
            if PHPhotoLibrary.authorizationStatus(for: .readWrite) == .limited {
                tipsView = AuthorizationAlbumPlaceholderView(type: .part)
                // 注册相册数据变化监听
                PHPhotoLibrary.shared().register(self)
            } else if PHPhotoLibrary.authorizationStatus(for: .readWrite) == .denied {
                tipsView = AuthorizationAlbumPlaceholderView(type: .none)
                self.navView.setNextButtonState(enable: false)
                self.navView.hiddeTitleControl()
            }
        } else {
            if PHPhotoLibrary.authorizationStatus() != .authorized {
                tipsView = AuthorizationAlbumPlaceholderView(type: .none)
                self.navView.setNextButtonState(enable: false)
                self.navView.hiddeTitleControl()
            }
        }
        
        view.addSubview(navView)
        view.addSubview(filterListView)
        
        filterListView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth)
            make.bottom.equalTo(self.view.snp.top).offset(PhotosConfiguration.default().navHeight)
            make.height.equalTo(UIScreen.screenHeight - PhotosConfiguration.default().navHeight)
        }
        
        navView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(PhotosConfiguration.default().navHeight)
        }
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(navView.snp.bottom)
        }
        
        view.bringSubviewToFront(filterListView)
        view.bringSubviewToFront(navView)
        
        navView.selectAlbumBlock = { [weak self] in
            guard let self = self else { return }
            self.filterListView.isHidden ? self.filterListView.show() : self.filterListView.hide()
        }
        
        navView.cancelBlock = { [weak self] in
            guard let self = self else { return }
            self.dismiss(animated: true)
        }
        
        navView.nextBlock = { [weak self] in
            guard let self = self else { return }
            self.showEdit()
        }
        
        filterListView.selectedAlbumBlock = { [weak self] model in
            guard let self = self else { return }
            self.currentAlbum = model
            self.reloadNavView()
        }
        
        if SourceInstance.shared.selectModels.isNotEmpty {
            navView.setNextButtonState(enable: true)
        }
    }
    
    deinit {
         Logger.log(message: "PresentAllAlbumViewController deinit", level: .info)
    }
}

extension PresentMainAlbumViewController {
    // 获取相册列表
    private func getAlbumList(completion: (() -> Void)? = nil)  {
        AssetManager.fetchPhotoAlbumList(ascending: PhotosConfiguration.default().sortAscending,
                                         allowSelectImage: PhotosConfiguration.default().allowSelectImage,
                                         allowSelectVideo: PhotosConfiguration.default().allowSelectVideo) { [weak self] albumList in
            guard let self = self else { return }
            self.albumList.removeAll()
            self.albumList.append(contentsOf: albumList)
            // 默认展示第一个相册
            if let listmodel = self.albumList.first {
                self.currentAlbum = listmodel
            }
            
            DispatchQueue.main.async {
                completion?()
                self.reloadNavView()
                self.reloadFilterListView()
            }
        }
    }
    
    // 刷新导航view
    private func reloadNavView()  {
        navView.title = self.currentAlbum?.title ?? ""
        navView.reset()
    }
    
    // 刷新相册列表
    private func reloadFilterListView()  {
        filterListView.dataSource = self.albumList
        filterListView.snp.updateConstraints { make in
            make.height.equalTo(min(AlbumFilterListView.rowH * CGFloat(self.albumList.count), UIScreen.screenHeight - PhotosConfiguration.default().navHeight))
        }
        filterListView.layoutIfNeeded()
    }
    
    // 获取指定相册里的asset
    private func loadPhotos() {
        guard let currentAlbum = currentAlbum else {
            return
        }
        
        if self.currentAlbum!.models.isEmpty {
            DispatchQueue.global().async {
                self.currentAlbum?.refetchPhotos()
                
                DispatchQueue.main.async {
                    self.dataSource.removeAll()
                    self.dataSource.append(contentsOf: self.currentAlbum!.models)
                }
            }
        } else {
            dataSource.removeAll()
            dataSource.append(contentsOf: currentAlbum.models)
        }
    }
    
    // 进入编辑页
    private func showEdit()  {
        guard PhotosConfiguration.default().allowEdit else {
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                SourceInstance.shared.selectedSourceBlock?(SourceInstance.shared.selectModels)
            })
            return
        }
        
        guard !PhotosConfiguration.default().selectSingleImg else {
            showClipVC()
            return
        }
        
        let editVC = EditorController()
        editVC.isPushed = true
        UIApplication.topViewController()?.navigationController?.pushViewController(editVC, animated: true)
    }
    
    // 单选模式， 直接进入裁剪页
    private func showClipVC()  {
        guard let model = SourceInstance.shared.selectModels.first,
              let imgData = model.imageOriginalData,
              let toImg = UIImage(data: imgData) else {
            return
        }
        
        if PhotosConfiguration.default().editImageConfiguration.assignClipRations == .circle {
            let clipVC = ClipOneImageCircleViewController(image: toImg)
            UIApplication.topViewController()?.navigationController?.pushViewController(clipVC, animated: true)
            return
        }
        
        let clipVC = ClipOneImageViewController(image: toImg)
        UIApplication.topViewController()?.navigationController?.pushViewController(clipVC, animated: true)
    }
}

// MARK: collectionDelegate&dataSource
extension PresentMainAlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableV = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: AuthorizationAlbumPlaceholderView.identifier, for: indexPath) as! AuthorizationAlbumPlaceholderView
        return reusableV
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollectionCell.identifier, for: indexPath) as! AlbumCollectionCell
        let config = PhotosConfiguration.default()
        var currentItem = dataSource[indexPath.row]
        
        cell.selectedBlock = { [weak self, weak cell] (isSelected, selectedItem) in
            guard let self = self, let selectedItem = selectedItem else { return }
            if !isSelected {
                if self.selectedModels.count >= PhotosConfiguration.default().maxSelectCount {
                    toast(CopyWitterInstance.default().overMaxSelectCount(PhotosConfiguration.default().maxSelectCount))
                    return
                }
                selectedItem.isSelected = true
                cell?.selectedButton.isSelected = true
                self.selectedModels.append(selectedItem)
            } else {
                selectedItem.isSelected = false
                cell?.selectedButton.isSelected = false
                self.selectedModels.removeAll { $0 == selectedItem }
            }
            self.navView.setNextButtonState(enable: self.selectedModels.isNotEmpty)
            self.refreshCellIndexAndMaskView()
        }
        
        cell.indexLabel.isHidden = true
        for (index, selM) in selectedModels.enumerated() {
            if currentItem == selM {
                cell.index = index + 1
                cell.indexLabel.isHidden = false
            }
        }
        
        setCellMaskView(cell, isSelected: currentItem.isSelected, model: currentItem)
        
        cell.configCell(&currentItem)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? AlbumCollectionCell else {
            return
        }
        
        let model = dataSource[indexPath.row]
        
        // mask
        setCellMaskView(cell, isSelected: model.isSelected, model: model)
    }
    
    func refreshCellIndexAndMaskView()  {
        let visableIndexPaths = collectionView.indexPathsForVisibleItems
        visableIndexPaths.forEach { indexPath in
            guard let cell = self.collectionView.cellForItem(at: indexPath) as? AlbumCollectionCell else {
                return
            }
            var row = indexPath.row
            let currentModel = self.dataSource[row]
            var idx = 0
            var isSelected = false
            var show = false
            for (index, selM) in self.selectedModels.enumerated() {
                if currentModel == selM {
                    show = true
                    idx = index + 1
                    isSelected = true
                    break
                }
            }
            
            cell.index = idx
            cell.indexLabel.isHidden = !isSelected
            
            setCellMaskView(cell, isSelected: isSelected, model: currentModel)
        }
    }
    
    func setCellMaskView(_ cell: AlbumCollectionCell, isSelected: Bool, model: PhotoModel)  {
        cell.coverView.isHidden = true
        cell.enable = true
        
        var hasSelectModels = SourceInstance.shared.selectModels
        
        if isSelected {
            cell.coverView.isHidden = true
            cell.enable = true
        } else {
//            if hasSelectModels.first!.mediaType == .video {
//                if model.mediaType == .video {
//                    cell.coverView.isHidden = true
//                    cell.enable =  true
//                } else {
//                    cell.coverView.isHidden = false
//                    cell.enable =  false
//                }
//            } else {
//                if model.mediaType == .video {
//                    cell.coverView.isHidden = false
//                    cell.enable =  false
//                } else {
//                    cell.coverView.isHidden = true
//                    cell.enable =  true
//                }
//            }
        }
    }
    
}


extension PresentMainAlbumViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        // changeDetails 是否发生变化
        guard let currentAlbum = currentAlbum, let changes = changeInstance.changeDetails(for: currentAlbum.result) else {
            return
        }
        
        // 接收通知可能会在后台线程
        DispatchQueue.main.async {
            self.assetHasChange = true // 再次打开相册列表 要刷新数据
            
            // 更新当前相册数据
            self.currentAlbum?.result = changes.fetchResultAfterChanges
            
            if changes.hasIncrementalChanges {
                // 删除了已经选中的model
                for photoModel in self.selectedModels {
                    if let asset = photoModel.asset {
                        let isDeleted = changeInstance.changeDetails(for: asset)?.objectWasDeleted ?? false
                        if isDeleted {
                            self.selectedModels.removeAll { $0 == photoModel }
                        }
                    }
                }
                
                // 有增删
                if !changes.removedObjects.isEmpty || !changes.insertedObjects.isEmpty {
                    self.currentAlbum?.models.removeAll()
                }
                
                self.loadPhotos()
            } else {
                // hasIncrementalChanges == no  表示可能更改的范围很大 还是应该刷新数据
                for photoModel in self.selectedModels {
                    if let asset = photoModel.asset {
                        let isDeleted = changeInstance.changeDetails(for: asset)?.objectWasDeleted ?? false
                        if isDeleted {
                            self.selectedModels.removeAll { $0 == photoModel }
                        }
                    }
                }
                
                self.currentAlbum?.models.removeAll()
                
                self.loadPhotos()
            }
        }
    }
}

 
