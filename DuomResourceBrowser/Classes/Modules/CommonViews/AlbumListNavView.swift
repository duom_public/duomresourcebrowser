//
//  AlbumListNavView.swift
//  ResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import UIKit
import DuomBase

class AlbumListNavView: UIView {
    private let titleViewH: CGFloat = 32
    
    private let arrowH: CGFloat = 24
    
    lazy var titleBgControl: UIControl = {
        let control = UIControl()
        control.addTarget(self, action: #selector(titleBgControlClick(_:)), for: .touchUpInside)
        control.backgroundColor = PhotosConfiguration.default().navTitleControlColor
        control.roundCorners(radius: self.titleViewH / 2)
        return control
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .appFont(ofSize: 14, fontType: .TT_Bold)
        label.textAlignment = .center
        label.text = title
        return label
    }()
    
    private lazy var arrow: UIImageView = {
        let view = UIImageView(image: UIImage(named: "title_arrow_down"))
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private lazy var cancelButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "nav_close_icon"), for: .normal)
        btn.setImage(UIImage(named: "nav_close_icon"), for: .selected)
        btn.setImage(UIImage(named: "nav_close_icon"), for: .highlighted)
        btn.setEnlargeEdge(.all(10))
        btn.addTarget(self, action: #selector(cancelButtonClick(_:)), for: .touchUpInside)
        return btn
    }()
    
    private lazy var nextButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = PhotosConfiguration.default().mainThemeColor.withAlphaComponent(0.3)
        btn.isUserInteractionEnabled = false
        btn.setTitle(CopyWitterInstance.default().NextText, for: .normal)
        btn.titleLabel?.textColor = .white
        btn.titleLabel?.font = .appFont(ofSize: 14, fontType: .TT_Bold)
        btn.roundCorners(radius: 8)
        btn.clipsToBounds = false
        btn.setEnlargeEdge(.all(10))
        btn.addTarget(self, action: #selector(nextButtonClick(_:)), for: .touchUpInside)
        return btn
    }()
    
    var title: String {
        didSet {
            titleLabel.text = title
            refreshTitleView()
        }
    }
    
    var selectAlbumBlock: (()->Void)?
    var cancelBlock: (()->Void)?
    var nextBlock: (()->Void)?
    
    var isShowShadow: Bool = false
    
    var isShowNextButton: Bool = true
    
    init(title: String = "") {
        self.title = title
        super.init(frame: .zero)
        addUIViews()
    }

    init(title: String = "", isShowShadow: Bool = false, isShowNextButton: Bool = true) {
        self.title = title
        self.isShowShadow = true
        self.isShowNextButton = false
        super.init(frame: .zero)
        addUIViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        refreshTitleView()
        cancelButton.frame = CGRect(x: 16, y: titleBgControl.center.y - (24 / 2), width: 24, height: 24)
        nextButton.frame = CGRect(x: UIScreen.screenWidth - 16 - 71, y: titleBgControl.center.y - (34 / 2), width: 71, height: 34)
        if isShowShadow {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 0.2
//            layer.shadowOffset = CGSize(width: 0, height: 4)
            layer.shadowRadius = 11
            layer.shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                         y: bounds.height - 4,
                                                         width: bounds.width,
                                                         height: layer.shadowRadius)).cgPath
            layer.masksToBounds = false
        }
    }
    
    private func refreshTitleView()  {
        let insets = safeAreaInsets
        let albumTitleW = min(bounds.width / 2, title.boundingRect(font: .appFont(ofSize: 16, fontType: .Inter_Blod), maxSize: CGSize(width: CGFloat.greatestFiniteMagnitude, height: 44)).width)
        let titleBgControlW = albumTitleW + arrowH + 10 * 2
        
        UIView.animate(withDuration: 0.25) {
            self.titleBgControl.frame = CGRect(x: (self.frame.width - titleBgControlW) / 2, y: self.frame.height - self.titleViewH - 10, width: titleBgControlW, height: self.titleViewH)
            self.titleLabel.frame = CGRect(x: 10, y: 0, width: albumTitleW, height: self.titleViewH)
            self.arrow.frame = CGRect(x: self.titleLabel.frame.maxX, y: (self.titleViewH - self.arrowH) / 2, width: self.arrowH, height: self.arrowH)
        }
    }
    
    private func addUIViews()  {
        backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        nextButton.isHidden = !isShowNextButton
        clipsToBounds = false
        addSubview(cancelButton)
        addSubview(titleBgControl)
        titleBgControl.addSubview(titleLabel)
        titleBgControl.addSubview(arrow)
        addSubview(nextButton)
    }
    
    @objc private func titleBgControlClick(_ sender: UIControl)  {
        selectAlbumBlock?()
        sender.isSelected.toggle()
        if sender.isSelected {
            UIView.animate(withDuration: 0.25) {
                self.arrow.transform = CGAffineTransform(rotationAngle: .pi)
            }
        } else {
            UIView.animate(withDuration: 0.25) {
                self.arrow.transform = .identity
            }
        }
    }
    
    @objc private func cancelButtonClick(_ sender: UIButton)  {
        cancelBlock?()
    }
    
    @objc private func nextButtonClick(_ sender: UIButton)  {
        MobStatistics.trackAction("ImagePicker_image_next_click")
        nextBlock?()
    }
    
    func reset()  {
        titleBgControl.isSelected = false
        self.cancelButton.isHidden = false
        UIView.animate(withDuration: 0.25) {
            self.arrow.transform = .identity
        }
    }
    
    func setNextButtonState(enable: Bool) {
        DispatchQueue.main.async {
            if !enable {
                self.nextButton.isUserInteractionEnabled = false
                self.nextButton.backgroundColor = PhotosConfiguration.default().mainThemeColor.withAlphaComponent(0.3)
            } else {
                self.nextButton.isUserInteractionEnabled = true
                self.nextButton.backgroundColor = PhotosConfiguration.default().mainThemeColor
                self.nextButton.setupShadow(opacity: 1, radius: 11, offset: CGSize(width: 0, height: 4), color: UIColor.black.withAlphaComponent(0.25))
            }
        }
    }
    
    func hiddeTitleControl()  {
        titleBgControl.isHidden = true
    }
}
