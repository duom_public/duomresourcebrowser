//
//  AuthorizationPlaceholderView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/5/9.
//

import Foundation
import DuomBase

class AuthorizationAlbumPlaceholderView: UICollectionReusableView, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(AuthorizationAlbumPlaceholderView.self)
    enum AlbumAuthorizationType {
        case none
        case part
    }
    
    private let placeHolderImage: UIImageView = UIImageView(image: UIImage(named: "auth_part"))
    
    private let placeHolderTitleLabel: UILabel = UILabel().then {
        $0.numberOfLines = 0
        $0.font = .appFont(ofSize: 32, fontType: .CR_Regular)
        $0.textAlignment = .center
        $0.textColor = UIColor(red: 158 / 255, green: 162 / 255, blue: 255 / 255, alpha: 1)
    }
    
    private let placeHolderDescLabel: UILabel = UILabel().then {
        $0.numberOfLines = 0
        $0.font = .appFont(ofSize: 20, fontType: .TT_Bold)
        $0.textAlignment = .center
        $0.textColor = UIColor(red: 158 / 255, green: 162 / 255, blue: 255 / 255, alpha: 1)
    }
    
    private let openButton: UIButton = UIButton().then {
        $0.titleLabel?.textColor = .white
        $0.titleLabel?.font = .appFont(ofSize: 16, fontType: .TT_Bold)
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.setTitle("Enable Library Access".localized, for: .normal)
        $0.roundCorners(radius: 10)
    }
    
    var type: AlbumAuthorizationType = .none
    
    convenience init(type: AlbumAuthorizationType) {
        self.init(frame: .zero)
        self.type = type
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        addSubview(placeHolderImage)
        addSubview(placeHolderTitleLabel)
        addSubview(placeHolderDescLabel)
        addSubview(openButton)
        
        placeHolderImage.snp.makeConstraints { make in
            make.top.equalTo(8.DScale)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: 150, height: 150).DScale)
        }
        
        placeHolderTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(placeHolderImage.snp.bottom)
            make.centerX.equalToSuperview()
            make.leading.equalTo(30)
            make.trailing.equalTo(-30)
        }
        
        placeHolderDescLabel.snp.makeConstraints { make in
            make.top.equalTo(placeHolderTitleLabel.snp.bottom).offset(16.DScale)
            make.centerX.equalToSuperview()
            make.leading.equalTo(30)
            make.trailing.equalTo(-30)
        }
        
        openButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(placeHolderDescLabel.snp.bottom).offset(24.DScale)
            make.size.equalTo(CGSize(width: 226, height: 48).DScale)
        }
        openButton.addTarget(self, action: #selector(openSetting(_:)), for: .touchUpInside)
        
//        switch type {
//        case .none:
//            placeHolderTitleLabel.text = CopyWitterInstance.default().authorizationNoneTip
//        case .part:
//            placeHolderTitleLabel.text = CopyWitterInstance.default().authorizationPartTip
//        }
        placeHolderTitleLabel.text = CopyWitterInstance.default().authorizationPartTitle
        placeHolderDescLabel.text = CopyWitterInstance.default().authorizationPartDesc
    }
    
    @objc func openSetting(_ sender: UIButton) {
        guard let url = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}
