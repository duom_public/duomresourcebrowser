//
//  AuthTipsCommonView.swift
//  DuomResourceBrowser
//
//  Created by Henry on 2023/10/13.
//

import Foundation
import DuomBase

class AuthTipsCommonView: UIView {
    enum AuthorizationType {
        case photosAndVideos
        case cameraAndMicrophone
    }
    
    convenience init(type: AuthorizationType) {
        self.init(frame: .zero)
        self.type = type
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        initUI()
    }

    private let navView: AlbumListNavView = AlbumListNavView(title: "", isShowShadow: true, isShowNextButton: false).then { $0.hiddeTitleControl()
        $0.cancelBlock = {
            let topNav = UIApplication.topViewController()?.navigationController as? PhotoNavgationController
            topNav?.dismiss(animated: true, completion: { [weak topNav] in
                SourceInstance.shared.cancelBlock?()
            })
        }
    }
    
    private let titleLabel: UILabel = UILabel().then {
        $0.numberOfLines = 0
        $0.font = .appFont(ofSize: 26, fontType: .CR_Regular)
        $0.textAlignment = .center
        $0.textColor = .black
    }
    
    let youUseOptionView: OptionView = OptionView()
    
    let weUseOptionView: OptionView = OptionView()
    
    let settingsOptionView: OptionView = OptionView()
    
    private let openButton: UIButton = UIButton().then {
        $0.titleLabel?.textColor = .white
        $0.titleLabel?.font = .appFont(ofSize: 16, fontType: .TT_Bold)
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.roundCorners(radius: 10)
    }
    
    var type: AuthorizationType {
        get {
            return type
        }
        set(type) {
            setupData(type: type)
        }
    }
    
    var isShowNavView: Bool {
        get {
            return isShowNavView
        }
        
        set(isShowNavView) {
            navView.isHidden = !isShowNavView
            navView.snp.updateConstraints({ make in
                make.height.equalTo(isShowNavView ? PhotosConfiguration.default().navHeight - UIScreen.topOffset : 0)
            })
        }
    }

    func initUI() {
        
        addSubview(titleLabel)
        addSubview(youUseOptionView)
        addSubview(weUseOptionView)
        addSubview(settingsOptionView)
        addSubview(openButton)
        addSubview(navView)
        
        navView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(UIScreen.topOffset)
            make.height.equalTo(PhotosConfiguration.default().navHeight - UIScreen.topOffset)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(navView.snp.bottom).offset(64.DScale)
            make.centerX.equalToSuperview()
            make.leading.equalTo(30.DScale)
            make.trailing.equalTo(-30.DScale)
        }
        
        youUseOptionView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(32.DScale)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        weUseOptionView.snp.makeConstraints { make in
            make.top.equalTo(youUseOptionView.snp.bottom).offset(16.DScale)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        settingsOptionView.snp.makeConstraints { make in
            make.top.equalTo(weUseOptionView.snp.bottom).offset(16.DScale)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        openButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(settingsOptionView.snp.bottom).offset(24.DScale)
            make.size.equalTo(CGSize(width: 226, height: 48).DScale)
        }
        openButton.addTarget(self, action: #selector(openSetting(_:)), for: .touchUpInside)
    }
    
    func setupData(type: AuthorizationType) {
        let tipsModel: AuthTipsModel
        switch type {
        case .photosAndVideos:
            tipsModel = AuthTipsModel(title: CopyWitterInstance.default().authPhotoAndVideoTitle, youUseOption: AuthTipsOptionModel(iconName: "auth_media", title: CopyWitterInstance.default().authYouUseTitle, desc: CopyWitterInstance.default().authPhotoAndVideoYouUseTips), weUseOption: AuthTipsOptionModel(iconName: "auth_question", title: CopyWitterInstance.default().authWeUseTitle, desc: CopyWitterInstance.default().authPhotoAndVideoWeUseTips), settingsOption: AuthTipsOptionModel(iconName: "auth_settings", title: CopyWitterInstance.default().authSettingsTitle, desc: CopyWitterInstance.default().authPhotoAndVideoSettingsTips), buttonTitle: CopyWitterInstance.default().authPhotoAndVideoOpenTitle)
            
        case .cameraAndMicrophone:
            tipsModel = AuthTipsModel(title: CopyWitterInstance.default().authCameraAndMicrophoneTitle, youUseOption: AuthTipsOptionModel(iconName: "auth_media", title: CopyWitterInstance.default().authYouUseTitle, desc: CopyWitterInstance.default().authCameraAndMicrophoneYouUseTips), weUseOption: AuthTipsOptionModel(iconName: "auth_question", title: CopyWitterInstance.default().authWeUseTitle, desc: CopyWitterInstance.default().authCameraAndMicrophoneWeUseTips), settingsOption: AuthTipsOptionModel(iconName: "auth_settings", title: CopyWitterInstance.default().authSettingsTitle, desc: CopyWitterInstance.default().authCameraAndMicrophoneSettingsTips), buttonTitle: CopyWitterInstance.default().authCameraAndMicrophoneOpenTitle)
        }
        titleLabel.text = tipsModel.title
        youUseOptionView.setAuthTipsOptionModel(tipsModel.youUseOption)
        weUseOptionView.setAuthTipsOptionModel(tipsModel.weUseOption)
        settingsOptionView.setAuthTipsOptionModel(tipsModel.settingsOption)
        openButton.setTitle(tipsModel.buttonTitle, for: .normal)
    }
    
    @objc func openSetting(_ sender: UIButton) {
        guard let url = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}

class OptionView: UIView {
    let imageView: UIImageView = UIImageView()
    
    let titleLabel: UILabel = UILabel().then {
        $0.textColor = .black
        $0.font = .appFont(ofSize: 20, fontType: .TT_Regular)
        $0.textAlignment = .left
    }
    
    let descLabel: UILabel = UILabel().then {
        $0.numberOfLines = 0
        $0.textColor = UIColor(red: 153 / 255, green: 153 / 255, blue: 153 / 255, alpha: 1)
        $0.font = .appFont(ofSize: 18, fontType: .TT_Regular)
        $0.textAlignment = .left
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        initUI()
    }
    
    func initUI() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descLabel)
        
        imageView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalTo(40.DScale)
            make.size.equalTo(CGSize(width: 24.DScale, height: 24.DScale))
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(imageView.snp.trailing).offset(8.DScale)
            make.trailing.equalTo(-30.DScale)
            make.top.equalTo(imageView.snp.top)
        }
        
        descLabel.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel.snp.leading)
            make.trailing.equalTo(titleLabel.snp.trailing)
            make.top.equalTo(titleLabel.snp.bottom).offset(4.DScale)
            make.bottom.equalToSuperview()
        }
    }
    
    func setAuthTipsOptionModel(_ model: AuthTipsOptionModel)  {
        imageView.image = UIImage(named: model.iconName)
        titleLabel.text = model.title
        descLabel.text = model.desc
    }
}
