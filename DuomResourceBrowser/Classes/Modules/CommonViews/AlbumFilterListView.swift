//
//  AlbumFilterListView.swift
//  ResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import Foundation
import DuomBase
import Photos

class AlbumFilterListView: UIView {
    static let rowH: CGFloat = 58
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        tableView.rowHeight = AlbumFilterListView.rowH
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = .clear
        tableView.register(AlbumFilterCell.self, forCellReuseIdentifier: NSStringFromClass(AlbumFilterCell.self))
        tableView.setupShadow(opacity: 1, radius: 11, offset: CGSize(width: 0, height: 4), color: UIColor.black.withAlphaComponent(0.25))
        tableView.clipsToBounds = false
        return tableView
    }()
    
    var dataSource: [AlbumListModel] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var selectedAlbumBlock: ((AlbumListModel) -> Void)?
    
    init(dataSource: [AlbumListModel]) {
        self.dataSource = dataSource
        super.init(frame: .zero)
        addUIViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addUIViews()  {
        backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        clipsToBounds = false
        setupShadow(opacity: 1, radius: 11, offset: CGSize(width: 0, height: 4), color: UIColor.black.withAlphaComponent(0.25))
        isHidden = true
        addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapAction(_:)))
        tap.delegate = self
        addGestureRecognizer(tap)
    }
    
    @objc private func tapAction(_ sender: UITapGestureRecognizer)  {
        hide()
    }
}

// MARK: Public Method
extension AlbumFilterListView {
    func show()  {
        isHidden = false
        let transY = min(CGFloat(dataSource.count) * AlbumFilterListView.rowH, UIScreen.screenHeight - PhotosConfiguration.default().navHeight)
        UIView.animate(withDuration: 0.25) {
            self.isHidden = false
            self.transform = self.transform.translatedBy(x: 0, y: transY)
        }
    }
    
    func hide() {
        let transY = -(min(CGFloat(dataSource.count) * AlbumFilterListView.rowH, UIScreen.screenHeight - PhotosConfiguration.default().navHeight))
        UIView.animate(withDuration: 0.25) {
            self.transform = self.transform.translatedBy(x: 0, y: transY)
        } completion: { _ in
            self.isHidden = true
            self.transform = .identity
        }
    }
    
}

// MARK: 获取相册列表
extension AlbumFilterListView {
    private func getAlbumList(completion: (() -> Void)? = nil)  {
        DispatchQueue.global().async {
            AssetManager.fetchPhotoAlbumList(allowSelectImage: PhotosConfiguration.default().allowSelectImage,
                                               allowSelectVideo: PhotosConfiguration.default().allowSelectVideo) { [weak self] albumList in
                guard let self = self else { return }
                self.dataSource.removeAll()
                self.dataSource.append(contentsOf: albumList)
                
                DispatchQueue.main.async {
                    completion?()
                    self.tableView.reloadData()
                }
            }
        }
    }
}

// MARK: GestureRecognizerDelegate
extension AlbumFilterListView: UIGestureRecognizerDelegate {
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let point = gestureRecognizer.location(in: self)
        return !tableView.frame.contains(point)
    }
}

// MARK: tableView delegate&datasource
extension AlbumFilterListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AlbumFilterCell.self)) as! AlbumFilterCell
        cell.configCell(model: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AlbumFilterListView.rowH
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = dataSource[indexPath.row]
        selectedAlbumBlock?(model)
        hide()
        if let index = tableView.indexPathsForVisibleRows {
            tableView.reloadRows(at: index, with: .none)
        }
    }
}

class AlbumFilterCell: UITableViewCell {
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        label.textColor = PhotosConfiguration.default().mainTextColor
        return label
    }()
    
    private lazy var countLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        label.textColor = PhotosConfiguration.default().mainTextColor
        return label
    }()
    
    private(set) var model: AlbumListModel?
    
    private(set) var imageIdentifier: String?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addUIViews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addUIViews()  {
        contentView.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        contentView.addSubview(nameLabel)
        contentView.addSubview(countLabel)
        
        nameLabel.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.centerY.equalToSuperview()
            make.height.equalTo(20)
        }
        
        countLabel.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.centerY.equalTo(nameLabel.snp.centerY)
        }
    }
    
    func configCell(model: AlbumListModel) {
        self.model = model
        
        nameLabel.text = model.title
        countLabel.text = "\(model.count)"
    }
}


