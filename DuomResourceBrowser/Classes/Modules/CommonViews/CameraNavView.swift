//
//  CameraNavView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/5.
//

import Foundation
import DuomBase

class CameraNavView: BaseView {
    var backBlock: (() -> Void)?
    var reverseBlock: (() -> Void)?
    var nextActionBlock: (() -> Void)?
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "nav_back"), for: .normal)
        button.setImage(UIImage(named: "nav_back"), for: .highlighted)
        button.addTarget(self, action: #selector(self.clickBackAction(_:)) , for: .touchUpInside)
        button.setEnlargeEdge(.all(15))
        return button
    }()
    
    private lazy var reverseButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "camera_refresh"), for: .normal)
        button.setImage(UIImage(named: "camera_refresh"), for: .highlighted)
        button.addTarget(self, action: #selector(self.clickReverAction(_:)) , for: .touchUpInside)
        button.setEnlargeEdge(.all(15))
        return button
    }()
    
    private lazy var nextButton: DuomThemeButton = {
        let button = DuomThemeButton(style: .highLight, title: CopyWitterInstance.default().NextText)
        button.addTarget(self, action: #selector(self.clickNextAction(_:)) , for: .touchUpInside)
        button.setEnlargeEdge(.all(15))
        button.isHidden = true
        return button
    }()
    
    override func setupUI() {
        backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        
        addSubview(backButton)
        addSubview(reverseButton)
        addSubview(nextButton)
        
        backButton.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.bottom.equalTo(-16)
            make.size.equalTo(CGSize(width: 24, height: 24).DScale)
        }
        
        reverseButton.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.centerY.equalTo(backButton.snp.centerY)
            make.size.equalTo(backButton.snp.size)
        }
        
        nextButton.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.centerY.equalTo(backButton.snp.centerY)
            make.size.equalTo(CGSize(width: 71, height: 34))
        }
    }
    
    @objc private func clickBackAction(_ sender: UIButton)  {
        backBlock?()
    }
    
    @objc private func clickReverAction(_ sender: UIButton)  {
        reverseBlock?()
    }
    
    @objc private func clickNextAction(_ sender: UIButton)  {
        nextActionBlock?()
    }
    
    func hasTakePhotoHandler()  {
        reverseButton.isHidden = true
        nextButton.isHidden = false
    }
}
