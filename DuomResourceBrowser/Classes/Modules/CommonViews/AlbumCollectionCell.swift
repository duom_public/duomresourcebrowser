//
//  AlbumCollectionCell.swift
//  ResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import Foundation
import DuomBase
import Photos

class AlbumCollectionCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(AlbumCollectionCell.self)
    
    private lazy var imageView: UIImageView = {
        let imgview = UIImageView()
        imgview.contentMode = .scaleAspectFill
        imgview.clipsToBounds = true
        imgview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.clickSelectedButton(_:)))
        imgview.addGestureRecognizer(tap)
        return imgview
    }()
    
    lazy var selectedButton: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .clear
        button.setImage(UIImage(named: "photo_unSelected"), for: .normal)
        button.setEnlargeEdge(top: 8, bottom: 100, left: 100, right: 10)
        button.addTarget(self, action: #selector(self.clickSelectedButton(_:)), for: .touchUpInside)
        return button
    }()
    
    private let videoView = VideoTimeView()
    
    let indexLabel: UILabel = UILabel().then {
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.textColor = .white
        $0.font = .appFont(ofSize: 14, fontType: .TT_Bold)
        $0.roundCorners(radius: 23 / 2)
        $0.isHidden = true
        $0.textAlignment = .center
    }
    
    let coverView: UIView = UIView().then {
        $0.isUserInteractionEnabled = false
        $0.isHidden = true
        $0.backgroundColor = .white.withAlphaComponent(0.7)
    }
    
    private lazy var iCloudTag: UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(named: "icloud_icon")
        imgView.isHidden = true
        return imgView
    }()
    
    var selectedBlock: ((Bool, PhotoModel?) -> Void)?
    
    private var imageIdentifier: String = ""
    
    private var smallImageRequestID: PHImageRequestID = PHInvalidImageRequestID
    
    private var bigImageReqeustID: PHImageRequestID = PHInvalidImageRequestID
    
    var model: PhotoModel?
    
    var index: Int = 0 {
        didSet {
            indexLabel.text = String(index)
        }
    }
    
    var enable: Bool = true {
        didSet {
            selectedButton.isHidden = !enable
        }
    }
    
    lazy var shapeLayer: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.shadowColor = UIColor.black.cgColor
        shapeLayer.shadowOffset = CGSize(width: 0, height: 0)
        shapeLayer.shadowRadius = 3
        shapeLayer.shadowOpacity = 0.3
        let path = UIBezierPath.init(arcCenter: CGPoint(x: contentView.center.x, y: contentView.center.y), radius: 22 / 2, startAngle: 0, endAngle: .pi*2, clockwise: true)
        shapeLayer.path = path.cgPath
        return shapeLayer
    }()
    
    lazy var subLayer: CAShapeLayer = {
        let subLayer = CAShapeLayer()
        subLayer.lineWidth = 0
        subLayer.fillColor = UIColor(176, 176, 176).cgColor
        return subLayer
    }()
    
    lazy var downLoadignView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(6, 5, 6).withAlphaComponent(0.4)
        v.isHidden = true
        v.isUserInteractionEnabled = false
        return v
    }()
    
    var progress: CGFloat = 0.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addUIView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func addUIView()  {
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        contentView.addSubview(selectedButton)
        contentView.addSubview(videoView)
        selectedButton.addSubview(indexLabel)
        contentView.addSubview(iCloudTag)
        contentView.addSubview(downLoadignView)
        contentView.addSubview(coverView)
        
        selectedButton.snp.makeConstraints { make in
            make.bottom.equalTo(-4)
            make.trailing.equalTo(-4)
            make.size.equalTo(CGSize(width: 22, height: 22))
        }
        
        videoView.snp.makeConstraints { make in
            make.leading.equalTo(4)
            make.bottom.equalTo(-4)
            make.height.equalTo(24)
        }
        
        indexLabel.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
            make.edges.equalTo(UIEdgeInsets(hAxis: -0.5, vAxis: -0.5))
        }
        
        iCloudTag.snp.makeConstraints { make in
            make.top.equalTo(4)
            make.leading.equalTo(4)
            make.size.equalTo(CGSize(width: 15, height: 15).DScale)
        }
        
        downLoadignView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        coverView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        bringSubviewToFront(coverView)
    }
    
    func configCell(_ model: inout PhotoModel)  {
        self.model = model
        switch model.mediaType {
        case .video:
            videoView.isHidden = false
            videoView.setDuration(model.duration)
        default:
            videoView.isHidden = true
        }
    
        // iCloudTag
        iCloudTag.isHidden = !model.inICloud
        
        selectedButton.isSelected = model.isSelected
        
        if model.isSelected {
            fetchBigImage()
        } else {
            cancelFetchBigImage()
        }
        fetchSmallImage()
    }
    
}

// MARK: 获取大小图
extension AlbumCollectionCell {
    // 获取元数据
    private func fetchBigImage()  {
        guard let model = self.model else { return }
        cancelFetchBigImage()
        
        if let asset = model.asset {
            bigImageReqeustID = AssetManager.fetchOriginalImageData(for: asset, completion: { [weak self] data, _, _ in
                self?.model?.imageOriginalData = data
            })
        }
    }
    
    private func fetchSmallImage()  {
        guard let model = self.model else { return }
        let size: CGSize
        let maxSideLength = bounds.width * 1.2
        if model.whRatio > 1 {
            let w = maxSideLength * model.whRatio
            size = CGSize(width: w, height: maxSideLength)
        } else {
            let h = maxSideLength / model.whRatio
            size = CGSize(width: maxSideLength, height: h)
        }
        
        if smallImageRequestID > PHInvalidImageRequestID {
            PHImageManager.default().cancelImageRequest(smallImageRequestID)
        }
        
        if let ident = model.ident {
            imageIdentifier = ident
        }
        imageView.image = nil
        if let asset = model.asset {
            smallImageRequestID = AssetManager.fetchImage(for: asset, size: size, completion: { [weak self] image, _, isDegraded in
                guard let self = self else { return }
                if self.imageIdentifier == model.ident {
                    self.imageView.image = image
                }
                
                if !isDegraded {
                    self.smallImageRequestID = PHInvalidImageRequestID
                }
            })
        }
    }
    
    private func cancelFetchBigImage()  {
        if bigImageReqeustID > PHInvalidImageRequestID {
            PHImageManager.default().cancelImageRequest(bigImageReqeustID)
        }
    }
    
    @objc private func clickSelectedButton(_ sender: UIButton) {
        guard enable else { return }
       
        if let model = model, model.inICloud, model.downloadStatus != .downloaded {
            downLoadICloudSources()
            return
        }
        
        selectedBlock?(selectedButton.isSelected, self.model)
        if selectedButton.isSelected {
            fetchBigImage()
        } else {
            cancelFetchBigImage()
        }
    }
    
    private func downLoadICloudSources()  {
        guard let model = model, model.inICloud, model.downloadStatus != .downloaded, let asset = model.asset  else { return }
        cancelFetchBigImage()
        if model.mediaType == .video {
            // 提前开始进度UI 因为回调有时间 显示不及时
            self.drawDownLoadProgress(0.01)
            bigImageReqeustID = AssetManager.fetchVideo(for: asset, progress: { [weak self] progress, error, stop, info in
                guard let self = self else { return }
                if error != nil {
                    self.cancelFetchBigImage()
                } else {
                    self.drawDownLoadProgress(progress)
                }
            }, completion: { avPlayerItem, info, isDegraded in
                self.model?.downloadStatus = .downloaded
            })
        } else {
            // 提前开始进度UI 因为回调有时间 显示不及时
            self.drawDownLoadProgress(0.01)
            bigImageReqeustID = AssetManager.downloadICloudSources(for: asset, progress: { [weak self] progress, error, stop, info in
                guard let self = self else { return }
                if error != nil {
                    self.cancelFetchBigImage()
                } else {
                    Logger.log(message: "iCloud image progress \(progress)", level: .info)
                    self.drawDownLoadProgress(progress)
                }
            }, completion: { data, info, _ in
                Logger.log(message: "complection", level: .info)
                self.model?.downloadStatus = .downloaded
                self.model?.imageOriginalData = data
            })
        }
    }
    
    private func drawDownLoadProgress(_ progress: CGFloat)  {
        guard progress < 1.0 else {
            selectedButton.isHidden = false
            downLoadignView.isHidden = true
            iCloudTag.isHidden = true
            self.shapeLayer.removeFromSuperlayer()
            self.subLayer.removeFromSuperlayer()
            return
        }
        
        iCloudTag.isHidden = true
        selectedButton.isHidden = true
        downLoadignView.isHidden = false
        self.layer.addSublayer(shapeLayer)
        self.layer.addSublayer(subLayer)
    
        let subPath = UIBezierPath.init(arcCenter: CGPoint(x: contentView.center.x, y: contentView.center.y), radius: 22 / 2, startAngle: .pi * (-1 / 2), endAngle: CGFloat(.pi * (progress * 2 - 1 / 2)), clockwise: true)
        subPath.addLine(to: CGPoint(x: contentView.center.x, y: contentView.center.y))
        subLayer.path = subPath.cgPath
    }
}


class VideoTimeView: UIView {
    let timeLabel: UILabel = UILabel().then {
        $0.textColor = .white
        $0.font = .appFont(ofSize: 12, fontType: .TT_Regular)
        $0.textAlignment = .left
    }
    
    let imageView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "video_icon")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = UIColor(6, 5, 6).withAlphaComponent(0.6)
        roundCorners(radius: 12)
    }
    
    func setupUI()  {
        addSubview(timeLabel)
        addSubview(imageView)
        
        imageView.snp.makeConstraints { make in
            make.leading.equalTo(5)
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 15, height: 15))
        }
        
        timeLabel.snp.makeConstraints { make in
            make.leading.equalTo(imageView.snp.trailing).offset(2)
            make.centerY.equalToSuperview()
            make.trailing.equalTo(-4)
        }
    }
    
    func setDuration(_ duration: String)  {
        timeLabel.text = duration
    }
}
