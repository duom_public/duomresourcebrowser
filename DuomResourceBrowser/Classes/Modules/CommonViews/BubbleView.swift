//
//  BubbleView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/6/18.
//

import Foundation
import DuomBase

class BubbleView: BaseView {
    let backView: UIView = UIView().then {
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.roundCorners(radius: 10)
    }
    
    let arrowImgView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "bubble_arrow")
        $0.contentMode = .scaleAspectFill
    }
    
    let label: UILabel = UILabel().then {
        $0.textColor = .white
        $0.font = .appFont(ofSize: 18, fontType: .TT_Regular)
        $0.text = "Can‘t find the right photo? Try some GIFs!".localized
        $0.textAlignment = .left
        $0.numberOfLines = 0
    }
    
    var isShowing: Bool = false
    
    var tapBlock:(() -> Void?)?
    
    override func setupUI() {
        addSubview(backView)
        addSubview(arrowImgView)
        backView.addSubview(label)
        
        backView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(-10)
        }
        
        arrowImgView.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.top.equalTo(backView.snp.bottom)
            make.size.equalTo(CGSize(width: 18, height: 10))
        }
        
        label.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets(hAxis: 16, vAxis: 16))
        }
    }
    
    override func addActions() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        addGestureRecognizer(tap)
    }
    
    @objc private func tapAction(_ sender: UITapGestureRecognizer)  {
       dismiss()
       tapBlock?()
    }
    
    func dismiss()  {
        guard isShowing else {
            return
        }
        removeFromSuperview()
        UserDefaults.standard.set(true, forKey: PhotosConfiguration.default().BubbleKey)
        UserDefaults.standard.synchronize()
    }
}
