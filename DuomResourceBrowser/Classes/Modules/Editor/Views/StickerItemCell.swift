//
//  StickerItemCell.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/10.
//

import Foundation
import DuomBase
import Kingfisher

class StickerPanCell: UICollectionViewCell, DuomCellProtocol, UICollectionViewDelegate, UICollectionViewDataSource {
    static var identifier: String = NSStringFromClass(StickerItemCell.self)
    let stckerItemCellSize: CGSize = CGSize(width: (UIScreen.screenWidth - 2 * 16 - 10 * 3) / 4, height: ((254 - 10 * 2 - 10 * 2) / 3).DScale)
    
    private lazy var emptyView = StickerPanEmptyView()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = stckerItemCellSize
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.register(StickerItemCell.self, forCellWithReuseIdentifier: StickerItemCell.identifier)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.dataSource = self
        view.delegate = self
        view.contentInsetAdjustmentBehavior = .never
        view.emptyView = emptyView
        return view
    }()
    
    var dataSource: StickerInfo? {
        didSet {
            if let dataSource = dataSource, let icon = dataSource.icon {
                emptyView.imgView.kf.setImage(with: URL(string: icon))
            }
        }
    }
    
    var selectImageBlock: ((UIImage?, String?) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    private func initUI()  {
        contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func updateCell(_ dataSource: StickerInfo)  {
        self.dataSource = dataSource
        emptyView.stickerInfo = dataSource
        collectionView.reloadData()
    }
    
    // MARK: UICollectionViewDelegate & UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StickerItemCell.identifier, for: indexPath) as? StickerItemCell else {
            return UICollectionViewCell()
        }
        cell.updateCell(dataSource?.decryptImages?[indexPath.row].decryptFirstFrame ?? "")
        return cell
    }
    
   
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource?.downloadStatus == 0 ? 0 : dataSource?.decryptImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let imgString = self.dataSource?.decryptImages?[indexPath.row].decryptOriginal {
            if imgString.isGif() {
                selectImageBlock?(nil, self.dataSource?.decryptImages?[indexPath.row].decryptFirstFrame ?? "")
                return
            }
            
            if let cell = collectionView.cellForItem(at: indexPath) as? StickerItemCell, let img = cell.imageView.image {
                selectImageBlock?(img, imgString)
            }
        }
    }
}

class StickerItemCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(StickerItemCell.self)
    
    let imageView: UIImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    private func initUI()  {
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets.init(top: 20, left: 20, bottom: 20, right: 20))
        }
    }
    
    func updateCell(_ img: String)  {
        if let imgUrl = URL(string: img) {
            imageView.kf.setImage(with: imgUrl)
        }
    }
}

class StickerPanEmptyView: UIView, EmptyAble {
    let imgView: UIImageView = UIImageView()
    
    private let downLoadBtn: UIButton = UIButton().then {
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.roundCorners(radius: 10)
        $0.setTitle("DOWNLOAD".localized, for: .normal)
        $0.setTitle("DOWNLOAD".localized, for: .highlighted)
        $0.titleLabel?.textColor = .white
        $0.titleLabel?.font = .appFont(ofSize: 16, fontType: .TT_Bold)
    }
    
    var customFilter: FilterHandler?
    
    var edgeInsets: UIEdgeInsets = .zero
    
    var stickerInfo: StickerInfo? {
        didSet {
            if let icon = stickerInfo?.icon {
                imgView.kf.setImage(with: URL(string: icon))
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        backgroundColor = .clear
        addSubview(imgView)
        addSubview(downLoadBtn)
        imgView.snp.makeConstraints { make in
            make.top.equalTo(24)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: 117, height: 119).DScale)
        }
        
        downLoadBtn.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(imgView.snp.bottom).offset(24)
            make.size.equalTo(CGSize(width: 226, height: 48).DScale)
        }
        
        downLoadBtn.addTarget(self, action: #selector(downloadAction(_:)), for: .touchUpInside)
    }
    
    @objc func downloadAction(_ sender: UIButton)  {
        let encoder = JSONEncoder()
        if let stickerData = try? encoder.encode(stickerInfo),
            let stickerString = String(data: stickerData, encoding: .utf8) {
//            Logger.log(message: "info>>>>>>>>>>>>>> \(stickerString)", level: .info)
            NotificationCenter.default.post(name: PhotosConfiguration.default().editImageConfiguration.downloadNotificationName,
                                                    object: stickerString)
        }
    }
}
