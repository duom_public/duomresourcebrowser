//
//  EditorItemCell.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/9.
//

import Foundation
import DuomBase
import Kingfisher

class EditorItemCell: UICollectionViewCell, DuomCellProtocol{
    static var identifier: String = NSStringFromClass(EditorItemCell.self)
    
    private let imageView: UIImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
        $0.roundCorners(radius: 4)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addUIView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func addUIView()  {
        contentView.addSubview(imageView)
        roundCorners(radius: 4)
        imageView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.height.equalTo(PhotosConfiguration.default().editImageConfiguration.editItemNormalCellSize.height)
        }
    }
    
    func updateViewState(isSelected: Bool, isAddCell: Bool = false, model: PhotoModel? = nil)  {
        if isSelected {
            imageView.snp.remakeConstraints { make in
                make.bottom.leading.equalToSuperview()
                make.size.equalTo(PhotosConfiguration.default().editImageConfiguration.editItemSelectedCellSize)
            }
            imageView.layer.borderColor = UIColor.white.cgColor
            imageView.layer.borderWidth = 2
            layoutIfNeeded()
        } else {
            imageView.snp.remakeConstraints { make in
                make.bottom.leading.trailing.equalToSuperview()
                make.height.equalTo(PhotosConfiguration.default().editImageConfiguration.editItemNormalCellSize.height)
            }
            imageView.layer.borderColor = UIColor.clear.cgColor
            imageView.layer.borderWidth = 0
            layoutIfNeeded()
        }
        
        if !isAddCell {
            if let model = model {
                let size: CGSize
                let maxSideLength = bounds.width * 1.2
                if model.whRatio > 1 {
                    let w = maxSideLength * model.whRatio
                    size = CGSize(width: w, height: maxSideLength)
                } else {
                    let h = maxSideLength / model.whRatio
                    size = CGSize(width: maxSideLength, height: h)
                }
                
                imageView.image = nil
                
                if let url = model.fileURL {
                    imageView.kf.setImage(with: url)
                } else {
                    if model.mediaType != .giphyGif, let asset = model.asset {
                        AssetManager.fetchImage(for: asset, size: size, completion: { [weak self] image, _, isDegraded in
                            guard let self = self else { return }
                            self.imageView.image = image
                        })
                    } else {
                        if let imageData = model.imageOriginalData {
                            imageView.image = UIImage(data: imageData)
                        }
                    }
                }
            }
        } else {
            imageView.image = UIImage(named: "edit_➕")
        }
        
    }
}

