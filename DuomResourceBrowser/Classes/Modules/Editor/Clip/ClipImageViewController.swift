//
//  ClipImageViewController.swift
//  DuomBase
//
//  Created by kuroky on 2023/4/10.
//

import Foundation
import UIKit
import DuomBase

extension ClipImageViewController {
    enum ClipPanEdge {
        case none
        case top
        case bottom
        case left
        case right
        case topLeft
        case topRight
        case bottomLeft
        case bottomRight
    }
}

class ClipImageViewController: UIViewController {
    private static let bottomToolViewH: CGFloat = 143 + UIScreen.bottomOffset
    
    private static let clipRatioItemSize = CGSize(width: (UIScreen.screenWidth - 16 * 2 - 14 * 4) / 5, height: 32)
    
    /// 取消裁剪时动画frame
    private var cancelClipAnimateFrame: CGRect = .zero
    
    private var viewDidAppearCount = 0
    
    private let originalImage: UIImage
    
    private var editImage: UIImage
    
    private let clipRatios: [ImageClipRatio]
    
    /// 初次进入界面时候，裁剪范围
    private var editRect: CGRect
    
    private lazy var mainScrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.alwaysBounceHorizontal = true
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        if #available(iOS 11.0, *) {
            view.contentInsetAdjustmentBehavior = .never
        }
        view.delegate = self
        return view
    }()
    
    private lazy var containerView = UIView()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.image = editImage
        view.contentMode = .scaleAspectFit
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var shadowView: ClipShadowView = {
        let view = ClipShadowView()
        view.isUserInteractionEnabled = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        return view
    }()
    
    private lazy var overlayView: ClipOverlayView = {
        let view = ClipOverlayView()
        view.isUserInteractionEnabled = false
        view.isCircle = selectedRatio.isCircle
        return view
    }()
    
    
    private lazy var bottomToolView = UIView().then {
        $0.backgroundColor = UIColor(6, 5, 6)
    }
    
    private lazy var cropLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .appFont(ofSize: 16, fontType: .TT_Bold)
        label.textAlignment = .center
        label.text = CopyWitterInstance.default().CropText
        return label
    }()
    
    private lazy var cancelBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "edit_close"), for: .normal)
        btn.adjustsImageWhenHighlighted = false
        btn.setEnlargeEdge(UIEdgeInsets(hAxis: 20, vAxis: 20))
        btn.addTarget(self, action: #selector(cancelBtnClick), for: .touchUpInside)
        return btn
    }()
    
    lazy var doneBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "edit_done"), for: .normal)
        btn.adjustsImageWhenHighlighted = false
        btn.setEnlargeEdge(UIEdgeInsets(hAxis: 20, vAxis: 20))
        btn.addTarget(self, action: #selector(doneBtnClick), for: .touchUpInside)
        return btn
    }()
    
    private lazy var clipRatioColView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = ClipImageViewController.clipRatioItemSize
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = .clear
        view.isHidden = clipRatios.count <= 1
        view.showsHorizontalScrollIndicator = false
        view.register(ImageClipRatioCell.self, forCellWithReuseIdentifier: ImageClipRatioCell.identifier)
        return view
    }()
    
    private var shouldLayout = true
    
    private var panEdge: ClipImageViewController.ClipPanEdge = .none
    
    private var beginPanPoint: CGPoint = .zero
    
    private var clipBoxFrame: CGRect = .zero
    
    private var clipOriginFrame: CGRect = .zero
    
    private var isRotating = false
    
    private var angle: CGFloat = 0
    
    private var selectedRatio: ImageClipRatio {
        didSet {
            if selectedRatio == .circle {
                overlayView.isCircle = true
            } else if selectedRatio == .wh9x16 {
                overlayView.isWH9_16 = true
            } else {
                overlayView.isWH9_16 = false
            }
        }
    }
    
    private var thumbnailImage: UIImage?
    
    private lazy var maxClipFrame = calculateMaxClipFrame()
    
    private var minClipSize = CGSize(width: 45, height: 45)
    
    private var resetTimer: Timer?
    
    var animate = true
    
    /// 用作进入裁剪界面首次动画和取消裁剪时动画的image
    var presentAnimateImage: UIImage?
    
    var dismissAnimateFromRect: CGRect = .zero
    
    var dismissAnimateImage: UIImage?
    
    /// 传回图片编辑区域的rect
    var clipDoneBlock: ((UIImage ,CGRect, ImageClipRatio) -> Void)?
    
    var cancelClipBlock: (() -> Void)?
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    deinit {
        Logger.log(message: "ClipImageViewController deinit", level: .info)
        cleanTimer()
    }
    
    init(image: UIImage, editRect: CGRect?, angle: CGFloat = 0, selectRatio: ImageClipRatio? = PhotosConfiguration.default().editImageConfiguration.defaultClipRatios) {
        originalImage = image
        clipRatios = PhotosConfiguration.default().editImageConfiguration.clipRatios
        self.editRect = editRect ?? .zero
        self.angle = angle
        
        // 修复orientation
        var rotation: UIImage.Orientation?
        if angle == -90 {
            rotation = .left
        } else if self.angle == -180 {
            rotation = .down
        } else if self.angle == -270 {
            rotation = .right
        }
        if let rotation = rotation, let rotationImg = image.rotation(to: rotation) {
            editImage = rotationImg
        } else {
            editImage = image
        }
        
        var firstEnter = false
        if let selectRatio = selectRatio {
            selectedRatio = selectRatio
        } else {
            firstEnter = true
            selectedRatio = PhotosConfiguration.default().editImageConfiguration.defaultClipRatios
        }
        
        super.init(nibName: nil, bundle: nil)
        
//        if firstEnter {
//            calculateClipRect()
//        }
        
        calculateClipRect()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func setupUI() {
        view.backgroundColor = .black
        
        view.addSubview(mainScrollView)
        mainScrollView.addSubview(containerView)
        containerView.addSubview(imageView)
        view.addSubview(shadowView)
        view.addSubview(overlayView)
        
        view.addSubview(bottomToolView)
        bottomToolView.addSubview(cropLabel)
        bottomToolView.addSubview(cancelBtn)
        bottomToolView.addSubview(doneBtn)
        bottomToolView.addSubview(clipRatioColView)
        
//        view.addGestureRecognizer(gridPanGes)
//        mainScrollView.panGestureRecognizer.require(toFail: gridPanGes)
        
        mainScrollView.alpha = 0
        overlayView.alpha = 0
        bottomToolView.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewDidAppearCount += 1
        
        if presentingViewController is PhotoNavgationController,
            (presentingViewController as! PhotoNavgationController).topViewController is EditorController {
            transitioningDelegate = self
        }
        
        guard viewDidAppearCount == 1 else {
            return
        }
        
        bottomToolView.alpha = 1
        mainScrollView.alpha = 1
        overlayView.alpha = 1
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard shouldLayout else {
            return
        }
        shouldLayout = false
        
        mainScrollView.frame = view.bounds
        shadowView.frame = view.bounds
        
        layoutInitialImage()
        
        bottomToolView.frame = CGRect(x: 0, y: view.bounds.height - ClipImageViewController.bottomToolViewH, width: view.bounds.width, height: ClipImageViewController.bottomToolViewH)
        
        let toolBtnH: CGFloat = 25
        let toolBtnY: CGFloat = 16
        cancelBtn.frame = CGRect(x: 16, y: toolBtnY, width: toolBtnH, height: toolBtnH)
        doneBtn.frame = CGRect(x: view.bounds.width - 16 - toolBtnH, y: toolBtnY, width: toolBtnH, height: toolBtnH)
        cropLabel.frame = CGRect(x: (view.bounds.width - 200) / 2, y: 20, width: 200, height: 20)
        
        let ratioColViewY = cropLabel.frame.maxY + 35
        clipRatioColView.frame = CGRect(x: 0, y: ratioColViewY, width: view.bounds.width, height: 32)
        
        if clipRatios.count > 1, let index = clipRatios.firstIndex(where: { $0 == self.selectedRatio }) {
            clipRatioColView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: false)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        shouldLayout = true
        maxClipFrame = calculateMaxClipFrame()
    }
}

// MARK: fun private
extension ClipImageViewController {
    /// 计算editRect
    private func calculateClipRect()  {
        if selectedRatio.whRatio == 0 {
            editRect = CGRect(origin: .zero, size: editImage.size)
        } else {
            let imageSize = editImage.size
            let imageWHRatio = imageSize.width / imageSize.height
            
            var w: CGFloat = 0, h: CGFloat = 0
            if selectedRatio.whRatio >= imageWHRatio {
                w = imageSize.width
                h = w / selectedRatio.whRatio
            } else {
                h = imageSize.height
                w = h * selectedRatio.whRatio
            }
            
            editRect = CGRect(x: (imageSize.width - w) / 2, y: (imageSize.height - h) / 2, width: w, height: h)
        }
    }
    
    /// 计算最大裁剪范围
    private func calculateMaxClipFrame() -> CGRect {
        func deviceSafeAreaInsets() -> UIEdgeInsets {
            var insets: UIEdgeInsets = .zero
            if #available(iOS 11, *) {
                insets = UIApplication.shared.keyWindow?.safeAreaInsets ?? .zero
            }
            return insets
        }
        var insets = deviceSafeAreaInsets()
        insets.top += 20
        var rect = CGRect.zero
        rect.origin.x = 15
        rect.origin.y = insets.top
        rect.size.width = UIScreen.main.bounds.width - 15 * 2
        rect.size.height = UIScreen.main.bounds.height - insets.top - ClipImageViewController.bottomToolViewH - 25
        return rect
    }
    
    private func layoutInitialImage() {
        mainScrollView.minimumZoomScale = 1
        mainScrollView.maximumZoomScale = 1
        mainScrollView.zoomScale = 1
        
        let editSize = editRect.size
        mainScrollView.contentSize = editSize
        let maxClipRect = maxClipFrame
        
        containerView.frame = CGRect(origin: .zero, size: editImage.size)
        imageView.frame = containerView.bounds
        
        // editRect比例，计算editRect所占frame
        let editScale = min(maxClipRect.width / editSize.width, maxClipRect.height / editSize.height)
        let scaledSize = CGSize(width: floor(editSize.width * editScale), height: floor(editSize.height * editScale))
        
        var frame = CGRect.zero
        frame.size = scaledSize
        frame.origin.x = maxClipRect.minX + floor((maxClipRect.width - frame.width) / 2)
        frame.origin.y = maxClipRect.minY + floor((maxClipRect.height - frame.height) / 2)
        
        // 按照edit image进行计算最小缩放比例
        let originalScale = min(maxClipRect.width / editImage.size.width, maxClipRect.height / editImage.size.height)
        // 将 edit rect 相对 originalScale 进行缩放，缩放到图片未放大时候的clip rect
        let scaleEditSize = CGSize(width: editRect.width * originalScale, height: editRect.height * originalScale)
        // 计算缩放后的clip rect相对maxClipRect的比例
        let clipRectZoomScale = min(maxClipRect.width / scaleEditSize.width, maxClipRect.height / scaleEditSize.height)
        
        mainScrollView.minimumZoomScale = originalScale
        mainScrollView.maximumZoomScale = 10
        // 设置当前zoom scale
        let zoomScale = clipRectZoomScale * originalScale
        mainScrollView.zoomScale = zoomScale
        mainScrollView.contentSize = CGSize(width: editImage.size.width * zoomScale, height: editImage.size.height * zoomScale)
        
        changeClipBoxFrame(newFrame: frame)
        
        if (frame.size.width < scaledSize.width - CGFloat.ulpOfOne) || (frame.size.height < scaledSize.height - CGFloat.ulpOfOne) {
            var offset = CGPoint.zero
            offset.x = -floor((mainScrollView.frame.width - scaledSize.width) / 2)
            offset.y = -floor((mainScrollView.frame.height - scaledSize.height) / 2)
            mainScrollView.contentOffset = offset
        }
        
        // edit rect 相对 image size 的 偏移量
        let diffX = editRect.origin.x / editImage.size.width * mainScrollView.contentSize.width
        let diffY = editRect.origin.y / editImage.size.height * mainScrollView.contentSize.height
        mainScrollView.contentOffset = CGPoint(x: -mainScrollView.contentInset.left + diffX, y: -mainScrollView.contentInset.top + diffY)
    }
    
    private func changeClipBoxFrame(newFrame: CGRect) {
        guard clipBoxFrame != newFrame else {
            return
        }
        if newFrame.width < CGFloat.ulpOfOne || newFrame.height < CGFloat.ulpOfOne {
            return
        }
        var frame = newFrame
        let originX = ceil(maxClipFrame.minX)
        let diffX = frame.minX - originX
        frame.origin.x = max(frame.minX, originX)
        if diffX < -CGFloat.ulpOfOne {
            frame.size.width += diffX
        }
        let originY = ceil(maxClipFrame.minY)
        let diffY = frame.minY - originY
        frame.origin.y = max(frame.minY, originY)
        if diffY < -CGFloat.ulpOfOne {
            frame.size.height += diffY
        }
        let maxW = maxClipFrame.width + maxClipFrame.minX - frame.minX
        frame.size.width = max(minClipSize.width, min(frame.width, maxW))
        
        let maxH = maxClipFrame.height + maxClipFrame.minY - frame.minY
        frame.size.height = max(minClipSize.height, min(frame.height, maxH))
        
        clipBoxFrame = frame
        shadowView.clearRect = frame
        overlayView.frame = frame
        
        mainScrollView.contentInset = UIEdgeInsets(top: frame.minY, left: frame.minX, bottom: mainScrollView.frame.maxY - frame.maxY, right: mainScrollView.frame.maxX - frame.maxX)
        
        let scale = max(frame.height / editImage.size.height, frame.width / editImage.size.width)
        mainScrollView.minimumZoomScale = scale
        
        mainScrollView.zoomScale = mainScrollView.zoomScale
    }
    
    @objc private func cancelBtnClick() {
        dismissAnimateFromRect = cancelClipAnimateFrame
        dismissAnimateImage = presentAnimateImage
        cancelClipBlock?()
        dismiss(animated: animate, completion: nil)
    }
    
    @objc private func revertBtnClick() {
        angle = 0
        editImage = originalImage
        calculateClipRect()
        imageView.image = editImage
        layoutInitialImage()
        
        clipRatioColView.reloadData()
    }
    
    @objc private func doneBtnClick() {
        let image = clipImage()
        dismissAnimateFromRect = clipBoxFrame
        dismissAnimateImage = image.clipImage
        clipDoneBlock?(image.clipImage, image.editRect, selectedRatio)
        dismiss(animated: true, completion: nil)
    }
    
    private func clipImage() -> (clipImage: UIImage, editRect: CGRect) {
        let frame = convertClipRectToEditImageRect()
        let clipImage = editImage.clipImage(angle: 0, editRect: frame, isCircle: selectedRatio.isCircle) ?? editImage
        return (clipImage, frame)
    }
    
    private func convertClipRectToEditImageRect() -> CGRect {
        let imageSize = editImage.size
        let contentSize = mainScrollView.contentSize
        let offset = mainScrollView.contentOffset
        let insets = mainScrollView.contentInset
        
        var frame = CGRect.zero
        frame.origin.x = floor((offset.x + insets.left) * (imageSize.width / contentSize.width))
        frame.origin.x = max(0, frame.origin.x)
        
        frame.origin.y = floor((offset.y + insets.top) * (imageSize.height / contentSize.height))
        frame.origin.y = max(0, frame.origin.y)
        
        frame.size.width = ceil(clipBoxFrame.width * (imageSize.width / contentSize.width))
        frame.size.width = min(imageSize.width, frame.width)
        
        frame.size.height = ceil(clipBoxFrame.height * (imageSize.height / contentSize.height))
        frame.size.height = min(imageSize.height, frame.height)
        
        return frame
    }
    
//    @objc private func gridGesPanAction(_ pan: UIPanGestureRecognizer) {
//        let point = pan.location(in: view)
//        if pan.state == .began {
//            startEditing()
//            beginPanPoint = point
//            clipOriginFrame = clipBoxFrame
//            panEdge = calculatePanEdge(at: point)
//        } else if pan.state == .changed {
//            guard panEdge != .none else {
//                return
//            }
//            updateClipBoxFrame(point: point)
//        } else if pan.state == .cancelled || pan.state == .ended {
//            panEdge = .none
//            startTimer()
//        }
//    }
    
//    private func updateClipBoxFrame(point: CGPoint) {
//        var frame = clipBoxFrame
//        let originFrame = clipOriginFrame
//
//        var newPoint = point
//        newPoint.x = max(maxClipFrame.minX, newPoint.x)
//        newPoint.y = max(maxClipFrame.minY, newPoint.y)
//
//        let diffX = ceil(newPoint.x - beginPanPoint.x)
//        let diffY = ceil(newPoint.y - beginPanPoint.y)
//        let ratio = selectedRatio.whRatio
//
//        switch panEdge {
//        case .left:
//            frame.origin.x = originFrame.minX + diffX
//            frame.size.width = originFrame.width - diffX
//            if ratio != 0 {
//                frame.size.height = originFrame.height - diffX / ratio
//            }
//        case .right:
//            frame.size.width = originFrame.width + diffX
//            if ratio != 0 {
//                frame.size.height = originFrame.height + diffX / ratio
//            }
//        case .top:
//            frame.origin.y = originFrame.minY + diffY
//            frame.size.height = originFrame.height - diffY
//            if ratio != 0 {
//                frame.size.width = originFrame.width - diffY * ratio
//            }
//        case .bottom:
//            frame.size.height = originFrame.height + diffY
//            if ratio != 0 {
//                frame.size.width = originFrame.width + diffY * ratio
//            }
//        case .topLeft:
//            if ratio != 0 {
//                frame.origin.x = originFrame.minX + diffX
//                frame.size.width = originFrame.width - diffX
//                frame.origin.y = originFrame.minY + diffX / ratio
//                frame.size.height = originFrame.height - diffX / ratio
//            } else {
//                frame.origin.x = originFrame.minX + diffX
//                frame.size.width = originFrame.width - diffX
//                frame.origin.y = originFrame.minY + diffY
//                frame.size.height = originFrame.height - diffY
//            }
//        case .topRight:
//            if ratio != 0 {
//                frame.size.width = originFrame.width + diffX
//                frame.origin.y = originFrame.minY - diffX / ratio
//                frame.size.height = originFrame.height + diffX / ratio
//            } else {
//                frame.size.width = originFrame.width + diffX
//                frame.origin.y = originFrame.minY + diffY
//                frame.size.height = originFrame.height - diffY
//            }
//        case .bottomLeft:
//            if ratio != 0 {
//                frame.origin.x = originFrame.minX + diffX
//                frame.size.width = originFrame.width - diffX
//                frame.size.height = originFrame.height - diffX / ratio
//            } else {
//                frame.origin.x = originFrame.minX + diffX
//                frame.size.width = originFrame.width - diffX
//                frame.size.height = originFrame.height + diffY
//            }
//        case .bottomRight:
//            if ratio != 0 {
//                frame.size.width = originFrame.width + diffX
//                frame.size.height = originFrame.height + diffX / ratio
//            } else {
//                frame.size.width = originFrame.width + diffX
//                frame.size.height = originFrame.height + diffY
//            }
//        default:
//            break
//        }
//
//        let minSize: CGSize
//        let maxSize: CGSize
//        let maxClipFrame: CGRect
//        if ratio != 0 {
//            if ratio >= 1 {
//                minSize = CGSize(width: minClipSize.height * ratio, height: minClipSize.height)
//            } else {
//                minSize = CGSize(width: minClipSize.width, height: minClipSize.width / ratio)
//            }
//            if ratio > self.maxClipFrame.width / self.maxClipFrame.height {
//                maxSize = CGSize(width: self.maxClipFrame.width, height: self.maxClipFrame.width / ratio)
//            } else {
//                maxSize = CGSize(width: self.maxClipFrame.height * ratio, height: self.maxClipFrame.height)
//            }
//            maxClipFrame = CGRect(origin: CGPoint(x: self.maxClipFrame.minX + (self.maxClipFrame.width - maxSize.width) / 2, y: self.maxClipFrame.minY + (self.maxClipFrame.height - maxSize.height) / 2), size: maxSize)
//        } else {
//            minSize = minClipSize
//            maxSize = self.maxClipFrame.size
//            maxClipFrame = self.maxClipFrame
//        }
//
//        frame.size.width = min(maxSize.width, max(minSize.width, frame.size.width))
//        frame.size.height = min(maxSize.height, max(minSize.height, frame.size.height))
//
//        frame.origin.x = min(maxClipFrame.maxX - minSize.width, max(frame.origin.x, maxClipFrame.minX))
//        frame.origin.y = min(maxClipFrame.maxY - minSize.height, max(frame.origin.y, maxClipFrame.minY))
//
//        if panEdge == .topLeft || panEdge == .bottomLeft || panEdge == .left, frame.size.width <= minSize.width + CGFloat.ulpOfOne {
//            frame.origin.x = originFrame.maxX - minSize.width
//        }
//        if panEdge == .topLeft || panEdge == .topRight || panEdge == .top, frame.size.height <= minSize.height + CGFloat.ulpOfOne {
//            frame.origin.y = originFrame.maxY - minSize.height
//        }
//
//        changeClipBoxFrame(newFrame: frame)
//    }
    
    private func startEditing() {
        cleanTimer()
        shadowView.alpha = 0
        overlayView.isEditing = true
    }
    
    private func cleanTimer() {
        resetTimer?.invalidate()
        resetTimer = nil
    }
    
    private func startTimer() {
        cleanTimer()
        resetTimer = Timer.scheduledTimer(timeInterval: 0.8, target: HKWeakProxy(target: self), selector: #selector(endEditing), userInfo: nil, repeats: false)
        RunLoop.current.add(resetTimer!, forMode: .common)
    }
    
    @objc private func endEditing() {
        overlayView.isEditing = false
        moveClipContentToCenter()
    }
    
    private func moveClipContentToCenter() {
        let maxClipRect = maxClipFrame
        var clipRect = clipBoxFrame
        
        if clipRect.width < CGFloat.ulpOfOne || clipRect.height < CGFloat.ulpOfOne {
            return
        }
        
        let scale = min(maxClipRect.width / clipRect.width, maxClipRect.height / clipRect.height)
        
        let focusPoint = CGPoint(x: clipRect.midX, y: clipRect.midY)
        let midPoint = CGPoint(x: maxClipRect.midX, y: maxClipRect.midY)
        
        clipRect.size.width = ceil(clipRect.width * scale)
        clipRect.size.height = ceil(clipRect.height * scale)
        clipRect.origin.x = maxClipRect.minX + ceil((maxClipRect.width - clipRect.width) / 2)
        clipRect.origin.y = maxClipRect.minY + ceil((maxClipRect.height - clipRect.height) / 2)
        
        var contentTargetPoint = CGPoint.zero
        contentTargetPoint.x = (focusPoint.x + mainScrollView.contentOffset.x) * scale
        contentTargetPoint.y = (focusPoint.y + mainScrollView.contentOffset.y) * scale
        
        var offset = CGPoint(x: contentTargetPoint.x - midPoint.x, y: contentTargetPoint.y - midPoint.y)
        offset.x = max(-clipRect.minX, offset.x)
        offset.y = max(-clipRect.minY, offset.y)
        UIView.animate(withDuration: 0.3) {
            if scale < 1 - CGFloat.ulpOfOne || scale > 1 + CGFloat.ulpOfOne {
                self.mainScrollView.zoomScale *= scale
                self.mainScrollView.zoomScale = min(self.mainScrollView.maximumZoomScale, self.mainScrollView.zoomScale)
            }

            if self.mainScrollView.zoomScale < self.mainScrollView.maximumZoomScale - CGFloat.ulpOfOne {
                offset.x = min(self.mainScrollView.contentSize.width - clipRect.maxX, offset.x)
                offset.y = min(self.mainScrollView.contentSize.height - clipRect.maxY, offset.y)
                self.mainScrollView.contentOffset = offset
            }
            self.shadowView.alpha = 1
            self.changeClipBoxFrame(newFrame: clipRect)
        }
    }
    
//    private func calculatePanEdge(at point: CGPoint) -> ClipImageViewController.ClipPanEdge {
//        let frame = clipBoxFrame.insetBy(dx: -30, dy: -30)
//
//        let cornerSize = CGSize(width: 60, height: 60)
//        let topLeftRect = CGRect(origin: frame.origin, size: cornerSize)
//        if topLeftRect.contains(point) {
//            return .topLeft
//        }
//
//        let topRightRect = CGRect(origin: CGPoint(x: frame.maxX - cornerSize.width, y: frame.minY), size: cornerSize)
//        if topRightRect.contains(point) {
//            return .topRight
//        }
//
//        let bottomLeftRect = CGRect(origin: CGPoint(x: frame.minX, y: frame.maxY - cornerSize.height), size: cornerSize)
//        if bottomLeftRect.contains(point) {
//            return .bottomLeft
//        }
//
//        let bottomRightRect = CGRect(origin: CGPoint(x: frame.maxX - cornerSize.width, y: frame.maxY - cornerSize.height), size: cornerSize)
//        if bottomRightRect.contains(point) {
//            return .bottomRight
//        }
//
//        let topRect = CGRect(origin: frame.origin, size: CGSize(width: frame.width, height: cornerSize.height))
//        if topRect.contains(point) {
//            return .top
//        }
//
//        let bottomRect = CGRect(origin: CGPoint(x: frame.minX, y: frame.maxY - cornerSize.height), size: CGSize(width: frame.width, height: cornerSize.height))
//        if bottomRect.contains(point) {
//            return .bottom
//        }
//
//        let leftRect = CGRect(origin: frame.origin, size: CGSize(width: cornerSize.width, height: frame.height))
//        if leftRect.contains(point) {
//            return .left
//        }
//
//        let rightRect = CGRect(origin: CGPoint(x: frame.maxX - cornerSize.width, y: frame.minY), size: CGSize(width: cornerSize.width, height: frame.height))
//        if rightRect.contains(point) {
//            return .right
//        }
//
//        return .none
//    }
}

//extension ClipImageViewController: UIGestureRecognizerDelegate {
//    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        guard gestureRecognizer == gridPanGes else {
//            return true
//        }
//        let point = gestureRecognizer.location(in: view)
//        let frame = overlayView.frame
//        let innerFrame = frame.insetBy(dx: 22, dy: 22)
//        let outerFrame = frame.insetBy(dx: -22, dy: -22)
//
//        if innerFrame.contains(point) || !outerFrame.contains(point) {
//            return false
//        }
//        return true
//    }
//}

extension ClipImageViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return containerView
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        startEditing()
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        guard scrollView == mainScrollView else {
            return
        }
        if !scrollView.isDragging {
            startTimer()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        guard scrollView == mainScrollView else {
            return
        }
        startEditing()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == mainScrollView else {
            return
        }
        startTimer()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard scrollView == mainScrollView else {
            return
        }
        if !decelerate {
            startTimer()
        }
    }
}

// MARK: UICollectionViewDelegate & UICollectionViewDataSource
extension ClipImageViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clipRatios.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageClipRatioCell.identifier, for: indexPath) as! ImageClipRatioCell
        
        let ratio = clipRatios[indexPath.row]
        cell.configCellRatio(ratio)
        cell.resetCellState(ratio == selectedRatio)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ratio = clipRatios[indexPath.row]
        guard ratio != selectedRatio else {
            return
        }
        selectedRatio = ratio
        clipRatioColView.reloadData()
        clipRatioColView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        calculateClipRect()
        layoutInitialImage()
    }
}

extension ClipImageViewController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ClipImageAnimatedTransition()
    }
}

// MARK: ImageClipRatioCell
class ImageClipRatioCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(ImageClipRatioCell.self)
    
    lazy var titleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: bounds.height - 15, width: bounds.width, height: 12))
        label.font = UIFont.appFont(ofSize: 16, fontType: .TT_Regular)
        label.textColor = .white
        label.textAlignment = .center
        label.roundCorners(radius: 8)
        label.backgroundColor = .clear
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpUI()  {
        backgroundColor = .clear
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func configCellRatio(_ ratio: ImageClipRatio)  {
        titleLabel.text = ratio.title
    }
    
    func resetCellState(_ isSelected: Bool)  {
        titleLabel.backgroundColor = isSelected ? PhotosConfiguration.default().mainThemeColor : .clear
        titleLabel.layer.borderColor = isSelected ? UIColor.clear.cgColor : UIColor.white.cgColor
        titleLabel.layer.borderWidth = isSelected ? 0 : 1
    }
}

// MARK: 裁剪阴影视图
class ClipShadowView: UIView {
    var clearRect: CGRect = .zero {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        isOpaque = false
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        UIColor(white: 0, alpha: 0.7).setFill()
        UIRectFill(rect)
        let cr = clearRect.intersection(rect)
        UIColor.clear.setFill()
        UIRectFill(cr)
    }
}

// MARK: 裁剪网格视图
class ClipOverlayView: UIView {
    // 9: 16 多一行
    var isWH9_16: Bool = false {
        didSet {
            guard oldValue != isWH9_16 else {
                return
            }
            if isWH9_16 {
                hLines = 3
            } else {
                hLines = 2
            }
            setNeedsDisplay()
        }
    }
    
    // ⭕️
    var isCircle = false {
        didSet {
            guard oldValue != isCircle else { return }
            setNeedsDisplay()
        }
    }
    
    var isEditing = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var hLines: Int = 2
    
    var vLines: Int = 2
    
    static let lineWidth: CGFloat = 1
    
    static let circleSize: CGSize = CGSize(width: 280, height: 280)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        backgroundColor = .clear
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        clipsToBounds = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
        layer.borderWidth = isCircle ? 0 : 2
        layer.borderColor = UIColor.white.cgColor
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        context?.setStrokeColor(UIColor.white.withAlphaComponent(0.3).cgColor)
        context?.setLineWidth(ClipOverlayView.lineWidth)
        context?.beginPath()
        
        if isCircle {
            let center = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
            let radius = ClipOverlayView.circleSize.width / 2 - ClipOverlayView.lineWidth
            context?.addArc(center: center, radius: radius, startAngle: 0, endAngle: .pi * 2, clockwise: true)
            context?.addRect(bounds)
            context?.setFillColor(UIColor.black.cgColor)
            context?.fillPath(using:.evenOdd)
//            context?.strokePath()
//            context?.setShadow(offset: CGSize(width: ClipOverlayView.lineWidth, height: ClipOverlayView.lineWidth), blur: 0)
            return
        }
        
        var dw: CGFloat = (rect.size.width - CGFloat(ClipOverlayView.lineWidth * CGFloat(vLines))) / CGFloat(CGFloat(vLines) + ClipOverlayView.lineWidth) + ClipOverlayView.lineWidth
        for i in 0..<vLines {
            context?.move(to: CGPoint(x: rect.origin.x + dw, y: 0))
            context?.addLine(to: CGPoint(x: rect.origin.x + dw, y: rect.height))
            dw += (rect.size.width - CGFloat(ClipOverlayView.lineWidth * CGFloat(vLines))) / CGFloat(CGFloat(vLines) + ClipOverlayView.lineWidth)
        }
        
        var dh: CGFloat = (rect.size.height - CGFloat(ClipOverlayView.lineWidth * CGFloat(hLines))) / CGFloat(CGFloat(hLines) + ClipOverlayView.lineWidth) + ClipOverlayView.lineWidth
        for i in 0..<hLines {
            context?.move(to: CGPoint(x: 0, y: rect.origin.y + dh))
            context?.addLine(to: CGPoint(x: rect.width, y: rect.origin.y + dh))
            dh += (rect.size.height - CGFloat(ClipOverlayView.lineWidth * CGFloat(hLines))) / CGFloat(CGFloat(hLines) + ClipOverlayView.lineWidth)
        }

        context?.strokePath()
        context?.setShadow(offset: CGSize(width: ClipOverlayView.lineWidth, height: ClipOverlayView.lineWidth), blur: 0)
    }
}



