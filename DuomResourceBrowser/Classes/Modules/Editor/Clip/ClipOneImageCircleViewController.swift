//
//  ClipOneImageViewController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/5/23.
//  单选照片 裁剪

import Foundation
import DuomBase
import Photos

let circleDia: CGFloat = 280
let circleYOffset: CGFloat = 70

class ClipOneImageCircleViewController: UIViewController {
    private static let bottomToolViewH: CGFloat = 83 + 48 + UIScreen.bottomOffset

    private let backButton: UIButton = UIButton().then {
        $0.setEnlargeEdge(UIEdgeInsets(hAxis: 15, vAxis: 15))
        $0.setImage(UIImage(named: "nav_back"), for: .normal)
    }
    
    private let saveButton: UIButton = UIButton().then {
        $0.setTitle("Save".localized, for: .normal)
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.roundCorners(radius: 10)
    }
   
    private let originalImage: UIImage
    
    private var editImage: UIImage
    
    /// 裁剪范围
    private var editRect: CGRect = .zero
    
    private lazy var mainScrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.alwaysBounceHorizontal = true
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        if #available(iOS 11.0, *) {
            view.contentInsetAdjustmentBehavior = .never
        }
        view.delegate = self
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.image = editImage
        view.contentMode = .scaleAspectFit
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var shadowView: ClipCircleShadowView = {
        let view = ClipCircleShadowView()
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private var shouldLayout = true
    
    private var clipBoxFrame: CGRect = .zero
    
    private var angle: CGFloat = 0
    
    private lazy var maxClipFrame = calculateMaxClipFrame()
    
    private var initImgFrame: CGRect = .zero
    
    private var initScrollViewContentSize: CGSize = .zero
    
    private var initScrollViewContentOffSet: CGPoint = .zero
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    deinit {
        Logger.log(message: "ClipOneImageViewController deinit", level: .info)
    }
    
    init(image: UIImage, angle: CGFloat = 0) {
        originalImage = image
        self.angle = angle
        
        // 修复orientation
        var rotation: UIImage.Orientation?
        if angle == -90 {
            rotation = .left
        } else if self.angle == -180 {
            rotation = .down
        } else if self.angle == -270 {
            rotation = .right
        }
        if let rotation = rotation, let rotationImg = image.rotation(to: rotation) {
            editImage = rotationImg
        } else {
            editImage = image
        }
        
        super.init(nibName: nil, bundle: nil)
        
        calculateClipRect()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func setupUI() {
        view.backgroundColor = .black
        
        view.addSubview(mainScrollView)
        mainScrollView.addSubview(imageView)
        view.addSubview(shadowView)
        
        view.addSubview(backButton)
        view.addSubview(saveButton)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard shouldLayout else {
            return
        }
        shouldLayout = false
        
        mainScrollView.frame = CGRect(x: 0, y: UIScreen.navigationBarHeight, width: UIScreen.screenWidth, height: UIScreen.screenHeight - ClipOneImageCircleViewController.bottomToolViewH - UIScreen.navigationBarHeight)
        shadowView.frame = view.bounds
        shadowView.layoutIfNeeded()
        
        layoutInitialImage()
        
        backButton.snp.makeConstraints { make in
            make.top.equalTo(54)
            make.leading.equalTo(16)
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
        
        saveButton.snp.makeConstraints { make in
            make.bottom.equalTo(-83)
            make.leading.equalTo(75)
            make.height.equalTo(48)
            make.trailing.equalTo(-75)
        }
        
        backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(saveAction(_:)), for: .touchUpInside)
    }
    
    private func layoutInitialImage() {
        mainScrollView.minimumZoomScale = 1
        mainScrollView.maximumZoomScale = 1
        mainScrollView.zoomScale = 1
        
        let maxClipRect = maxClipFrame
        let editSize = editRect.size
        let scrollViewRect = mainScrollView.bounds
        let whRatio = editRect.width / editRect.height
        
        var contentWidth: CGFloat =  max(editRect.width, maxClipRect.width)
        var contentHeight: CGFloat = max(editRect.height, maxClipRect.height)
        if editSize.height > circleDia {
            contentHeight += (editSize.height - circleDia)
        }
        if editSize.width > circleDia {
            contentWidth += (maxClipRect.width - circleDia)
        }
        
        let editScale = min(contentWidth / editSize.width, contentHeight / editSize.height)
        mainScrollView.minimumZoomScale = editScale
        mainScrollView.maximumZoomScale = 10
        mainScrollView.zoomScale = editScale
        mainScrollView.contentSize = CGSize(width: contentWidth , height: contentHeight)
        initScrollViewContentSize = mainScrollView.contentSize
        
        let offsetY = (editSize.height - circleDia) / 2
        let offsetX = whRatio > 1 ? (maxClipRect.width - circleDia) / 2 : (editSize.width - circleDia) / 2
        var diffX: CGFloat = 0
        var diffY: CGFloat = 0
        if editSize.width > scrollViewRect.width {
            diffX = (editSize.width - scrollViewRect.width) / 2
        }
        
        if editSize.height > scrollViewRect.height {
            diffY = (editSize.height - scrollViewRect.height) / 2
        }
        mainScrollView.contentOffset = CGPoint(x: diffX + offsetX, y: diffY + offsetY)
        initScrollViewContentOffSet = mainScrollView.contentOffset
        
        let circleInWindow = shadowView.convert(shadowView.circleFrameInWindow, to: UIApplication.getWindow())
        let circleInScrolleView = mainScrollView.convert(circleInWindow, from: UIApplication.getWindow())
        if whRatio >= 1 {
            imageView.frame.origin.y = circleInScrolleView.origin.y
            imageView.frame.origin.x = circleInScrolleView.origin.x - (editSize.width - circleDia) / 2
        } else {
            imageView.frame.origin.y = circleInScrolleView.origin.y - (editSize.height - circleDia) / 2
            imageView.frame.origin.x = (scrollViewRect.width - editSize.width) / 2
        }
        imageView.frame.size = CGSize(width: editRect.width, height: editRect.height)
        initImgFrame = imageView.frame
        
        clipBoxFrame = circleInWindow
    }
}

// MARK: fun private
extension ClipOneImageCircleViewController {
    @objc private func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func saveAction(_ sender: UIButton) {
        let image = clipImage()
        let tempModel = PhotoModel(asset: PHAsset())
        tempModel.mediaType = .image
        if let fileURL = PhotoTools.write(image: image.clipImage) {
            tempModel.fileURL = fileURL
        }
        UIApplication.topViewController()?.navigationController?.dismiss(animated: true, completion: {
            SourceInstance.shared.selectedSourceBlock?([tempModel])
        })
    }
    
    /// 计算editRect
    private func calculateClipRect()  {
        let imageSize = editImage.size
        let imageWHRatio = imageSize.width / imageSize.height
        
        var w: CGFloat = 0, h: CGFloat = 0
        if imageWHRatio > 1 {
            h = CGFloat(circleDia)
            w = h * imageWHRatio
        } else {
            w = CGFloat(circleDia)
            h = w / imageWHRatio
        }
        editRect = CGRect(x: (imageSize.width - w) / 2, y: (imageSize.height - h) / 2, width: w, height: h)
    }
    
    /// 计算最大裁剪范围
    private func calculateMaxClipFrame() -> CGRect {
        var rect = CGRect.zero
        rect.origin.x = 0
        rect.origin.y = UIScreen.navigationBarHeight
        rect.size.width = UIScreen.main.bounds.width
        rect.size.height = UIScreen.main.bounds.height - UIScreen.navigationBarHeight - ClipOneImageCircleViewController.bottomToolViewH
        return rect
    }
    
    private func clipImage() -> (clipImage: UIImage, editRect: CGRect) {
        let frame = coverClipRect()
        let clipImage = imageView.image?.clipImage(angle: 0, editRect: frame) ?? editImage
        return (clipImage, frame)
    }
    
    private func coverClipRect() -> CGRect {
        guard let curImg = imageView.image else {
            return clipBoxFrame
        }
        let circleInWindow = shadowView.convert(shadowView.circleFrameInWindow, to: UIApplication.getWindow())
        let circleInImageView = imageView.convert(circleInWindow, from: UIApplication.getWindow())
        let currentScale = min(imageView.frame.width / initImgFrame.width, imageView.frame.height / initImgFrame.height)
        let zoom = curImg.size.width / imageView.frame.width
        
        var frame = CGRect.zero
        
        frame.origin.x = (circleInImageView.origin.x) * zoom * currentScale
        frame.origin.y = (circleInImageView.origin.y) * zoom * currentScale

        frame.size.width = ceil(circleDia * zoom)
        frame.size.width = min(frame.width, curImg.size.width)

        frame.size.height = ceil(circleDia * zoom)
        frame.size.height = min(frame.height, curImg.size.height)
        
        return frame
    }
    
//    func clipImage(editRect: CGRect) -> UIImage? {
//        let render = UIGraphicsImageRenderer(size: editRect.size)
//        let origin = CGPoint(x: -editRect.minX, y: -editRect.minY)
//        let image = render.image { (context) in
//            curImg.draw(at: origin)
//        }
//        return image
//    }
}

extension ClipOneImageCircleViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        guard scrollView == mainScrollView else {
            return
        }
        
        let img_W = initImgFrame.width * scale
        let img_H = initImgFrame.height * scale
        
        UIView.animate(withDuration: 0.3) {
            if scale <= self.mainScrollView.minimumZoomScale {
                self.mainScrollView.contentSize = self.initScrollViewContentSize
                self.imageView.frame = self.initImgFrame
            } else {
                let newContentSize = CGSize(width: img_W + self.mainScrollView.frame.width - circleDia, height: img_H + self.mainScrollView.frame.height - circleDia)
                self.mainScrollView.contentSize = newContentSize
                self.imageView.frame = CGRect(x: self.initImgFrame.origin.x, y: self.initImgFrame.origin.y, width: img_W, height: img_H)
            }
        }
        
//        UIView.animate(withDuration: 0.3) {
//            let contentSize = self.mainScrollView.contentSize
//            let newContentSize = CGSize(width: img_W + self.mainScrollView.frame.width - circleDia, height: img_H + self.mainScrollView.frame.height - circleDia)
//            let addX = newContentSize.width - contentSize.width
//            let addY = newContentSize.height - contentSize.height
//            self.mainScrollView.contentSize = newContentSize
//            self.mainScrollView.contentOffset = CGPoint(x: self.initScrollViewContentOffSet.x + addX, y: self.initScrollViewContentOffSet.y + addY)
//            self.imageView.frame = CGRect(x: self.initImgFrame.origin.x, y: self.initImgFrame.origin.y, width: img_W, height: img_H)
//        }
//
        
//        let offsetX = mainScrollView.bounds.size.width > mainScrollView.contentSize.width ? (scrollView.bounds.size.width - mainScrollView.contentSize.width) / 2 : 0.0
//        let offsetY = mainScrollView.bounds.size.height > mainScrollView.contentSize.height ? (scrollView.bounds.size.height - mainScrollView.contentSize.height) / 2 : 0.0
//        mainScrollView.contentOffset = CGPoint(x: imageView.frame.minX + offsetX + circleDia / 2, y: imageView.frame.minY + offsetY + circleDia / 2)
    }

}

// MARK: 裁剪阴影视图
class ClipCircleShadowView: UIView {
    static let lineWidth: CGFloat = 1
    static let circleSize: CGSize = CGSize(width: circleDia, height: circleDia)
    
    var coverView: UIView = UIView(frame: .zero).then {
        $0.isUserInteractionEnabled = false
        $0.isHidden = true
    }
    
    var circleFrameInWindow: CGRect = .zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        isOpaque = false
        
        addSubview(coverView)
        coverView.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: ClipCircleShadowView.circleSize.width + 2, height: ClipCircleShadowView.circleSize.height + 2))
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-circleYOffset)
        }
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("ClipCircleShadowView init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addCircleMask()
    }
    
    func addCircleMask()  {
        let viewSize = bounds.size
        let maskSize = ClipCircleShadowView.circleSize
        
        let path = UIBezierPath.init(rect: bounds)
        let circlePath = UIBezierPath(ovalIn: CGRect(x: (viewSize.width - maskSize.width) / 2, y: (viewSize.height - maskSize.height) / 2 - circleYOffset, width: maskSize.width, height: maskSize.height))
        path.append(circlePath)
        path.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = UIColor.black.withAlphaComponent(0.3).cgColor
        
        let lineLayer = CAShapeLayer()
        lineLayer.path = UIBezierPath(ovalIn: CGRect(x: (viewSize.width - maskSize.width) / 2, y: (viewSize.height - maskSize.height) / 2 - circleYOffset, width: maskSize.width, height: maskSize.height)).cgPath
        lineLayer.lineWidth = 1
        lineLayer.strokeColor = UIColor.white.cgColor
        lineLayer.fillColor = UIColor.clear.cgColor
        
        layer.addSublayer(fillLayer)
        layer.addSublayer(lineLayer)
        
        circleFrameInWindow = coverView.convert(coverView.bounds, to: self)
    }
}
