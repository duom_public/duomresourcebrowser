//
//  ClipVideoViewController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/5/18.
//

import Foundation
import Photos
import DuomBase
import RxCocoa
import RxSwift

class ClipVideoViewController: UIViewController {
    private static let frameImageSize = CGSize(width: 28.0, height: 28.0)
    
    // 裁剪框宽度
    private static let borderImageWidth: CGFloat = frameImageSize.width * 10
    
    // 左右滑动视图宽度
    private static let leftRightW: CGFloat = 5
    
    private static let borderViewWidth: CGFloat = 3.0
    
    private let avAsset: AVAsset
    
    // 已经录制的视频的URL
    var videoURL: URL?
    
    private var timer: Timer?
    
    private var disposeBag = DisposeBag()
    
    private lazy var playerLayer: AVPlayerLayer = {
        let layer = AVPlayerLayer()
        layer.videoGravity = .resizeAspect
        return layer
    }()
    
    private var measureCount = 0
    
    private lazy var interval: TimeInterval = {
        let assetDuration = round(self.avAsset.duration.seconds)
        if assetDuration < 60 {
            return assetDuration / 10
        } else if assetDuration > 60 * 60 {
            return assetDuration / 60
        } else {
            return 6
        }
    }()
    
    private lazy var avAssetRequestID = PHInvalidImageRequestID
    
    private lazy var videoRequestID = PHInvalidImageRequestID
    
    private var frameImageCache: [Int: UIImage] = [:]
    
    private var shouldLayout = true
    
    private lazy var generator: AVAssetImageGenerator = {
        let g = AVAssetImageGenerator(asset: self.avAsset)
        // 指定生成的图像的最大尺寸
        g.maximumSize = CGSize(width: ClipVideoViewController.frameImageSize.width * 3, height: ClipVideoViewController.frameImageSize.height * 3)
        g.appliesPreferredTrackTransform = true
        g.requestedTimeToleranceBefore = .zero
        g.requestedTimeToleranceAfter = .zero
        g.apertureMode = .productionAperture
        return g
    }()
    
    private lazy var outGenerator: AVAssetImageGenerator = {
        let g = AVAssetImageGenerator(asset: self.avAsset)
        // 指定生成的图像的最大尺寸
        g.maximumSize = CGSize(width: UIScreen.screenWidth, height: UIScreen.screenHeight)
        g.appliesPreferredTrackTransform = true
        g.requestedTimeToleranceBefore = .zero
        g.requestedTimeToleranceAfter = .zero
        g.apertureMode = .productionAperture
        return g
    }()
    
    
    private let navContainerView = UIView().then {
        $0.backgroundColor = .black
    }
    
    private let backButton = UIButton().then {
        $0.setImage(UIImage(named: "edit_close"), for: .normal)
    }
    
    private let nextButton: UIButton = UIButton().then {
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.roundCorners(radius: 8)
        $0.setTitle(CopyWitterInstance.default().NextText, for: .normal)
        $0.titleLabel?.textColor = .white
        $0.titleLabel?.font = .appFont(ofSize: 14, fontType: .TT_Bold)
    }
    
    private let bottomContainerView = UIView().then {
        $0.backgroundColor = .clear
    }
    
    private let bottomImgView = UIImageView().then {
        $0.image = UIImage(named: "edit_bottom_back")
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = ClipVideoViewController.frameImageSize
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = .clear
        view.dataSource = self
        view.delegate = self
        view.contentInsetAdjustmentBehavior = .never
        view.register(ClipVideoFrameImageCell.self, forCellWithReuseIdentifier: ClipVideoFrameImageCell.identifier)
        return view
    }()
    
    private let frameImageBorderView = ClipVideoFrameImageBorderView().then {
        $0.isUserInteractionEnabled = false
    }
    
    private let leftSideView: UIView = UIView().then {
        $0.backgroundColor = .white
        $0.roundCorners(radius: 2)
    }
    
    private let rightSideView: UIView = UIView().then {
        $0.backgroundColor = .white
        $0.roundCorners(radius: 2)
    }
    
    private lazy var leftSidePan: UIPanGestureRecognizer = {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(leftSidePanAction(_:)))
        pan.delegate = self
        return pan
    }()
    
    private lazy var rightSidePan: UIPanGestureRecognizer = {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(rightSidePanAction(_:)))
        pan.delegate = self
        return pan
    }()
    
    private lazy var indicator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.roundCorners(radius: 2)
        return view
    }()
    
    private lazy var clipTimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .appFont(ofSize: 14, fontType: .CL_Regular)
        label.textAlignment = .center
        return label
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    deinit {
        Logger.log(message: "ClipVideoViewController deinit", level: .info)
        cleanTimer()
        if avAssetRequestID > PHInvalidImageRequestID {
            PHImageManager.default().cancelImageRequest(avAssetRequestID)
        }
        if videoRequestID > PHInvalidImageRequestID {
            PHImageManager.default().cancelImageRequest(videoRequestID)
        }
    }
    
    init(avAsset: AVAsset) {
        self.avAsset = avAsset
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        analysisAssetImages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        toggleNextButtonState(true)
        HUD.dismiss()
    }
    
    private func setupUI()  {
        view.backgroundColor = .black
        
        view.layer.addSublayer(playerLayer)
        view.addSubview(navContainerView)
        navContainerView.addSubview(backButton)
        navContainerView.addSubview(nextButton)
        
        
        view.addSubview(bottomContainerView)
        bottomContainerView.addSubview(bottomImgView)
        bottomContainerView.addSubview(collectionView)
        bottomContainerView.addSubview(frameImageBorderView)
        bottomContainerView.addSubview(indicator)
        bottomContainerView.addSubview(leftSideView)
        bottomContainerView.addSubview(rightSideView)
        bottomContainerView.addSubview(clipTimeLabel)
        
        view.addGestureRecognizer(leftSidePan)
        view.addGestureRecognizer(rightSidePan)
        collectionView.panGestureRecognizer.require(toFail: leftSidePan)
        collectionView.panGestureRecognizer.require(toFail: rightSidePan)
        rightSidePan.require(toFail: leftSidePan)
        
        backButton.addTarget(self, action: #selector(clickBackAction(_:)), for: .touchUpInside)
//        nextButton.addTarget(self, action: #selector(clickNextAction(_:)), for: .touchUpInside)
        nextButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.clickNextAction()
            })
            .disposed(by: disposeBag)
    }
    
    private func toggleNextButtonState(_ isEnable: Bool)  {
        nextButton.isUserInteractionEnabled = isEnable
        nextButton.backgroundColor = isEnable ? PhotosConfiguration.default().mainThemeColor : UIColor(110, 116, 240)
        nextButton.setTitleColor(isEnable ? UIColor.white : UIColor.white.withAlphaComponent(0.4), for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard shouldLayout else {
            return
        }
        shouldLayout = false
        
        HUD.showImage(autoDismissDelay: 0)
        toggleNextButtonState(false)
        
        navContainerView.frame = CGRect(x: 0, y: 0, width: UIScreen.screenWidth, height: UIScreen.navigationBarHeight)
        backButton.frame = CGRect(x: 16, y: navContainerView.frame.maxY - 10 - 24, width: 24, height: 24)
        nextButton.frame = CGRect(x: navContainerView.frame.maxX - 16 - 71, y: navContainerView.frame.maxY - 10 - 30, width: 71, height: 34)
        
        playerLayer.frame = CGRect(x: 0, y: navContainerView.frame.maxY, width: UIScreen.screenWidth, height: UIScreen.screenHeight - navContainerView.frame.height)
        
        bottomContainerView.frame = CGRect(x: 0, y: UIScreen.screenHeight - 255, width: UIScreen.screenWidth, height: 255)
        bottomImgView.frame = bottomContainerView.bounds
        collectionView.frame = CGRect(x: 0, y: 145, width: UIScreen.screenWidth, height: ClipVideoViewController.frameImageSize.height)
        
        // 保持60s宽度
        frameImageBorderView.frame = CGRect(x: (bottomContainerView.frame.width - ClipVideoViewController.borderImageWidth) / 2, y: collectionView.frame.minY - ClipVideoViewController.borderViewWidth, width: ClipVideoViewController.borderImageWidth, height: collectionView.frame.height + ClipVideoViewController.borderViewWidth * 2)
        leftSideView.frame = CGRect(x: frameImageBorderView.frame.minX, y: collectionView.frame.minY - 5, width: ClipVideoViewController.leftRightW, height: collectionView.frame.height + 10)
        let rightSideViewX = frameImageBorderView.frame.maxX - ClipVideoViewController.leftRightW
        rightSideView.frame = CGRect(x: rightSideViewX, y: collectionView.frame.minY - 5, width: ClipVideoViewController.leftRightW, height: collectionView.frame.height + 10)
        
        indicator.frame.size = CGSize(width: 4, height: collectionView.frame.height - 4)
        
        frameImageBorderView.validRect = frameImageBorderView.convert(clipRect(), from: view)
        
        clipTimeLabel.frame = CGRect(x: (bottomContainerView.frame.width - 100.0) / 2, y: frameImageBorderView.frame.maxY + 15, width: 100, height: 20)
    }
}

// MARK: app 前后台切换
extension ClipVideoViewController {
    @objc private func appWillResignActive() {
        cleanTimer()
        indicator.layer.removeAllAnimations()
    }
    
    @objc private func appDidBecomeActive() {
        startTimer()
    }
}

// MARK: fun private
extension ClipVideoViewController {
    private func cleanTimer()  {
        timer?.invalidate()
        timer = nil
        indicator.layer.removeAllAnimations()
        indicator.isHidden = true
        playerLayer.player?.pause()
    }
    
    private func startTimer()  {
        cleanTimer()
        // timer所需总时间
        let duration = interval * TimeInterval(clipRect().width / ClipVideoViewController.frameImageSize.width)
        timer = Timer.scheduledTimer(timeInterval: duration, target: HKWeakProxy(target: self), selector: #selector(playPartVideo), userInfo: nil, repeats: true)
        timer?.fire()
        RunLoop.main.add(timer!, forMode: .common)
        
        indicator.isHidden = false
        indicator.frame.origin = CGPoint(x: leftSideView.frame.midX, y: collectionView.frame.minY + 2)
        indicator.layer.removeAllAnimations()
        
        UIView.animate(withDuration: duration, delay: 0, options: [.allowUserInteraction, .curveLinear, .repeat], animations: {
            self.indicator.frame.origin = CGPoint(x: self.rightSideView.frame.maxX - 4, y: self.collectionView.frame.minY + 2)
        })
    }
    
    private func analysisAssetImages() {
        let duration = round(avAsset.duration.seconds)
        guard duration > 0 else {
            toast(CopyWitterInstance.default().AssetNil)
            Logger.log(message: "avAsset error", level: .error)
            return
        }
        let item = AVPlayerItem(asset: avAsset)
        let player = AVPlayer(playerItem: item)
        playerLayer.player = player
        startTimer()
        
        measureCount = Int(duration / interval)
        collectionView.reloadData()
        requestVideoMeasureFrameImage()
        refreshTimeLabel()
    }
    
    // 获取time点帧视图
    private func requestVideoMeasureFrameImage() {
        guard measureCount > 0 else {
            return
        }
        
        var times: [NSValue] = []
        for i in 0..<measureCount {
            let mes = TimeInterval(i) * interval
            let time = CMTimeMakeWithSeconds(Float64(mes), preferredTimescale: avAsset.duration.timescale)
            times.append(NSValue(time: time))
        }
        
        var index: Int = 0
        var hasError = false
        var errorIndex: [Int] = []
        generator.generateCGImagesAsynchronously(forTimes: times) { (time, cgImage, cmTime, result, error) in
            if result != .cancelled {
                if let cgImage = cgImage {
                    self.frameImageCache[index] = UIImage(cgImage: cgImage)
                    if hasError {
                        for idx in errorIndex {
                            self.setCurrentCell(image: UIImage(cgImage: cgImage), index: idx)
                        }
                        errorIndex.removeAll()
                        hasError = false
                    }
                    self.setCurrentCell(image: UIImage(cgImage: cgImage), index: index)
                } else {
                    if self.frameImageCache.count > index - 1, index > 0, let tempImg = self.frameImageCache[index - 1] {
                        self.setCurrentCell(image: tempImg, index: index)
                    } else {
                        errorIndex.append(index)
                        hasError = true
                    }
                }
                
                index += 1
                DispatchQueue.main.async {
                    HUD.dismiss()
                    self.toggleNextButtonState(true)
                }
            }
        }
    }
    
    private func setCurrentCell(image: UIImage, index: Int)  {
        DispatchQueue.main.async {
            guard let cell = self.collectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? ClipVideoFrameImageCell else {
                return
            }
            cell.imageView.image = image
        }
    }
    
    
    private func clipRect() -> CGRect {
        var frame = CGRect.zero
        frame.origin.x = leftSideView.frame.minX
        frame.origin.y = leftSideView.frame.minY
        frame.size.width = rightSideView.frame.maxX - frame.minX
        frame.size.height = leftSideView.frame.height
        return frame
    }
    
    @objc private func playPartVideo() {
        playerLayer.player?.seek(to: getStartTime(), toleranceBefore: .zero, toleranceAfter: .zero)
        if (playerLayer.player?.rate ?? 0) == 0 {
            playerLayer.player?.play()
        }
    }
    
    private func getStartTime() -> CMTime {
        var rect = collectionView.convert(clipRect(), from: view)
        rect.origin.x -= frameImageBorderView.frame.minX
        let second = max(0, CGFloat(interval) * rect.minX / ClipVideoViewController.frameImageSize.width)
        return CMTimeMakeWithSeconds(Float64(second), preferredTimescale: avAsset.duration.timescale)
    }
    
    @objc private func clickBackAction(_ sender: UIButton)  {
        self.navigationController?.popViewController(animated: true)
    }
 
    private func clickNextAction()  {
        // 如果之前有选择的图片 在这里清除
        SourceInstance.shared.selectModels = SourceInstance.shared.selectModels.filter { $0.mediaType == .video }
        
        cleanTimer()
        let d = CGFloat(interval) * clipRect().width / ClipVideoViewController.frameImageSize.width
        // 如果是本地资源，且没有发生编辑动作 那么回调
        if abs(d - round(CGFloat(avAsset.duration.seconds))) <= 0.01 {
            if SourceInstance.shared.selectModels.isNotEmpty {
                sourceVideoBlock()
            } else {
                captureVideoBlock()
            }
            return
        }
        
        let hud = DProgressHUD(style: .darkBlur)
        hud.show()
        AssetManager.exportEditVideo(for: avAsset,
                                     toFile: PhotoTools.getVideoTmpURL(),
                                     range: getTimeRange()) { [weak self] result in
            hud.hide()
            switch result {
            case .success(let url):
                self?.saveEditedVideoToAlbum(url: url)
                break
            case .failure(let error):
                #if DEBUG
                toast("export video error".localized)
                #endif
                break
            }
        }
    }
    
    // 资源库里的视频 回调
    private func sourceVideoBlock()  {
        guard let model = SourceInstance.shared.selectModels.first else {
            return
        }
        model.avAseet = avAsset
        DispatchQueue.main.async {
            SourceInstance.shared.selectedSourceBlock?(SourceInstance.shared.selectModels)
            UIApplication.topViewController()?.navigationController?.dismiss(animated: true)
        }
    }
    
    // 录制的视频 回调
    private func captureVideoBlock()  {
        guard let videoURL = videoURL else {
            return
        }
        
        let hud = DProgressHUD(style: .darkBlur)
        hud.show()
        
        AssetManager.saveVideoToAlbum(url: videoURL) { [weak self] suc, asset in
            guard let self = self else { return }
            hud.hide()
            if suc, let asset = asset {
                let tempModel = PhotoModel(asset: asset)
                tempModel.avAseet = self.avAsset
                print("\(self.avAsset.tracks(withMediaType: .video).first!.naturalSize)")
                tempModel.fileURL = videoURL
                // 首帧视图
                tempModel.getFirstFrameImage(forAsset: asset) {
                    DispatchQueue.main.async {
                        SourceInstance.shared.selectedSourceBlock?([tempModel])
                        UIApplication.topViewController()?.navigationController?.dismiss(animated: true)
                    }
                }
            }
        }
    }
    
    private func saveEditedVideoToAlbum(url: URL)  {
        AssetManager.saveVideoToAlbum(url: url) { suc, asset in
            if suc, let asset = asset {
                let tempModel = PhotoModel(asset: asset)
                tempModel.fileURL = url
                // 首帧视图
                tempModel.getFirstFrameImage(forAsset: asset) {
                    DispatchQueue.main.async {
                        SourceInstance.shared.selectedSourceBlock?([tempModel])
                        UIApplication.topViewController()?.navigationController?.dismiss(animated: true)
                    }
                }
            }
        }
    }
    
    
//    private func resetExportVideoModelProps(url: URL)  {
//        var currentModel: PhotoModel
//        if SourceInstance.shared.selectModels.isEmpty {
//            currentModel = PhotoModel(asset: PHAsset())
//            currentModel.mediaType = .video
//        } else {
//            currentModel = SourceInstance.shared.selectModels.first!
//        }
//
//        var editVideoModel = EditVideoModel()
//        editVideoModel.fileURL = url
//        let rangeTime = getTimeRange()
//        editVideoModel.duration = PhotoTools.transformDuration(for: Int(CMTimeGetSeconds(rangeTime.end) - CMTimeGetSeconds(rangeTime.start)))
//
//        outGenerator.generateCGImagesAsynchronously(forTimes: [NSValue(time: getStartTime())]) { _, cgImg, _, result, _ in
//            if result == .succeeded, let cgImg = cgImg {
//                let image = UIImage(cgImage: cgImg)
//                // 写入
//                if let imgData = PhotoTools.getImageData(for: image),
//                   let compressData = PhotoTools.imageCompress(imgData, compressionQuality: PhotosConfiguration.default().imageQualityCount),
//                   let url = PhotoTools.write(imageData: compressData) {
//                    editVideoModel.firstFrameImgURL = url
//                    currentModel.editVideoModel = editVideoModel
//                    DispatchQueue.main.async {
//                        SourceInstance.shared.selectedSourceBlock?([currentModel])
//                        UIApplication.topViewController()?.navigationController?.dismiss(animated: true)
//                    }
//                } else {
//                     Logger.log(message: "导出首帧视图失败", level: .info)
//                }
//            } else {
//                toast("export video first image failure")
//            }
//        }
//    }
    
    private func getTimeRange() -> CMTimeRange {
        let start = getStartTime()
        let d = CGFloat(interval) * clipRect().width / ClipVideoViewController.frameImageSize.width
        let duration = CMTimeMakeWithSeconds(Float64(d), preferredTimescale: avAsset.duration.timescale)
        return CMTimeRangeMake(start: start, duration: duration)
    }
    
    private func refreshTimeLabel()  {
        let d = CGFloat(interval) * clipRect().width / ClipVideoViewController.frameImageSize.width
        let duration = CMTimeMakeWithSeconds(Float64(d), preferredTimescale: avAsset.duration.timescale)
        let time = CMTimeGetSeconds(duration)
        clipTimeLabel.text = PhotoTools.transformDuration(for: Int(time))
    }
}

// MARK: GestureRecognizer
extension ClipVideoViewController: UIGestureRecognizerDelegate {
    @objc private func leftSidePanAction(_ pan: UIPanGestureRecognizer) {
        let point = pan.location(in: bottomContainerView)

        if pan.state == .began {
            frameImageBorderView.layer.borderColor = UIColor(white: 1, alpha: 0.4).cgColor
            cleanTimer()
        } else if pan.state == .changed {
            let minX = frameImageBorderView.frame.minX
            let maxX = rightSideView.frame.minX - leftSideView.frame.width

            var frame = leftSideView.frame
            frame.origin.x = min(maxX, max(minX, point.x))
            leftSideView.frame = frame
            frameImageBorderView.validRect = frameImageBorderView.convert(clipRect(), from: view)

            playerLayer.player?.seek(to: getStartTime(), toleranceBefore: .zero, toleranceAfter: .zero)
        } else if pan.state == .ended || pan.state == .cancelled {
            frameImageBorderView.layer.borderColor = UIColor.clear.cgColor
            startTimer()
            refreshTimeLabel()
        }
    }
    
    @objc private func rightSidePanAction(_ pan: UIPanGestureRecognizer) {
        let point = pan.location(in: bottomContainerView)

        if pan.state == .began {
            frameImageBorderView.layer.borderColor = UIColor(white: 1, alpha: 0.4).cgColor
            cleanTimer()
        } else if pan.state == .changed {
            let minX = leftSideView.frame.maxX
            let maxX = frameImageBorderView.frame.maxX - rightSideView.frame.width

            var frame = rightSideView.frame
            frame.origin.x = min(maxX, max(minX, point.x))
            rightSideView.frame = frame
            frameImageBorderView.validRect = frameImageBorderView.convert(clipRect(), from: view)

            playerLayer.player?.seek(to: getStartTime(), toleranceBefore: .zero, toleranceAfter: .zero)
        } else if pan.state == .ended || pan.state == .cancelled {
            frameImageBorderView.layer.borderColor = UIColor.clear.cgColor
            startTimer()
            refreshTimeLabel()
        }
    }
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == leftSidePan {
            let point = gestureRecognizer.location(in: bottomContainerView)
            let frame = leftSideView.frame
            let outerFrame = frame.inset(by: UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10))
            return outerFrame.contains(point)
        } else if gestureRecognizer == rightSidePan {
            let point = gestureRecognizer.location(in: bottomContainerView)
            let frame = rightSideView.frame
            let outerFrame = frame.inset(by: UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10))
            return outerFrame.contains(point)
        }
        return true
    }
}

extension ClipVideoViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        cleanTimer()
        playerLayer.player?.seek(to: getStartTime(), toleranceBefore: .zero, toleranceAfter: .zero)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            startTimer()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        startTimer()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let w = ClipVideoViewController.borderImageWidth
        let leftRight = (collectionView.frame.width - w) / 2
        return UIEdgeInsets(top: 0, left: leftRight, bottom: 0, right: leftRight)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return measureCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ClipVideoFrameImageCell.identifier, for: indexPath) as! ClipVideoFrameImageCell
        if let image = frameImageCache[indexPath.row] {
            cell.imageView.image = image
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cellImg = frameImageCache[indexPath.row], let cell = cell as? ClipVideoFrameImageCell {
            cell.imageView.image = cellImg
        }
    }
}

// MARK: FrameImageBorderView
class ClipVideoFrameImageBorderView: UIView {
    var validRect: CGRect = .zero {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 3
        layer.borderColor = UIColor.white.cgColor
        backgroundColor = .clear
        isOpaque = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context?.setStrokeColor(UIColor.white.cgColor)
        context?.setLineWidth(3)
        
        context?.move(to: CGPoint(x: validRect.minX, y: 3 / 2))
        context?.addLine(to: CGPoint(x: validRect.minX + validRect.width, y: 3 / 2))
        
        context?.move(to: CGPoint(x: validRect.minX, y: rect.height - 3 / 2))
        context?.addLine(to: CGPoint(x: validRect.minX + validRect.width, y: rect.height - 3 / 2))
        
        context?.strokePath()
    }
}

class ClipVideoFrameImageCell: UICollectionViewCell, DuomCellProtocol {
    static var identifier: String = NSStringFromClass(ClipVideoFrameImageCell.self)
    
    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(imageView)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = bounds
    }
}
