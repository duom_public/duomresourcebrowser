//
//  PresentAlbumController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/6/18.
//

import Foundation
import DuomBase
import Photos

extension PresentAlbumViewController: DuomPagingContainerItem {}
class PresentAlbumViewController: BaseViewController {
    /// 指定相册
    var currentAlbum: AlbumListModel? {
        didSet {
            self.loadPhotos()
        }
    }
    
    /// 照片数据源
    private var dataSource: [PhotoModel] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.collectionView.reloadData()
            }
        }
    }
    
    /// 总选择区所有的数量
    var hasSelectCount: Int = 0
    
    /// 选中的最终数据
    private var selectedModels: [PhotoModel] = []
    
    lazy var collectionView: AlbumCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: PhotosConfiguration.default().photoItemWH, height: PhotosConfiguration.default().photoItemWH)
        layout.minimumLineSpacing = PhotosConfiguration.default().photoSpace
        layout.minimumInteritemSpacing = PhotosConfiguration.default().photoSpace
        let view = AlbumCollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        view.scrollsToTop = false
        view.dataSource = self
        view.delegate = self
        view.contentInsetAdjustmentBehavior = .never
        view.register(AlbumCollectionCell.self, forCellWithReuseIdentifier: AlbumCollectionCell.identifier)
        return view
    }()
    
    var appendItmeBlock: (([PhotoModel]) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        super.setupConstraints()
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.top.equalToSuperview()
        }
    }
    
    deinit {
         Logger.log(message: "PresentAlbumViewController deinit", level: .info)
    }
}

extension PresentAlbumViewController {
    // 获取指定相册里的asset
    private func loadPhotos() {
        guard let currentAlbum = currentAlbum else {
            return
        }
        
        if self.currentAlbum!.models.isEmpty {
            DispatchQueue.global().async {
                self.currentAlbum?.refetchPhotos()
                
                DispatchQueue.main.async {
                    self.dataSource.removeAll()
                    self.dataSource.append(contentsOf: self.currentAlbum!.models)
                }
            }
        } else {
            dataSource.removeAll()
            dataSource.append(contentsOf: currentAlbum.models)
        }
    }
}

// MARK: collectionDelegate&dataSource
extension PresentAlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollectionCell.identifier, for: indexPath) as! AlbumCollectionCell
        let config = PhotosConfiguration.default()
        var currentItem = dataSource[indexPath.row]
        
        cell.selectedBlock = { [weak self, weak cell] (isSelected, selectedItem) in
            guard let self = self, let selectedItem = selectedItem else { return }
            if !isSelected {
                if self.selectedModels.count >= PhotosConfiguration.default().maxSelectCount - self.hasSelectCount {
                    toast(CopyWitterInstance.default().overMaxSelectCount(PhotosConfiguration.default().maxSelectCount))
                    return
                }
                selectedItem.isSelected = true
                cell?.selectedButton.isSelected = true
                self.selectedModels.append(selectedItem)
            } else {
                selectedItem.isSelected = false
                cell?.selectedButton.isSelected = false
                self.selectedModels.removeAll { $0 == selectedItem }
            }
            self.refreshCellIndexAndMaskView()
            self.appendItmeBlock?(self.selectedModels)
        }
        
        cell.indexLabel.isHidden = true
        for (index, selM) in selectedModels.enumerated() {
            if currentItem == selM {
                cell.index = index + 1
                cell.indexLabel.isHidden = false
            }
        }
        
        setCellMaskView(cell, isSelected: currentItem.isSelected, model: currentItem)
        
        cell.configCell(&currentItem)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? AlbumCollectionCell else {
            return
        }
        
        let model = dataSource[indexPath.row]
        
        // mask
        setCellMaskView(cell, isSelected: model.isSelected, model: model)
    }
    
    func refreshCellIndexAndMaskView()  {
        let visableIndexPaths = collectionView.indexPathsForVisibleItems
        visableIndexPaths.forEach { indexPath in
            guard let cell = self.collectionView.cellForItem(at: indexPath) as? AlbumCollectionCell else {
                return
            }
            var row = indexPath.row
            let currentModel = self.dataSource[row]
            var idx = 0
            var isSelected = false
            var show = false
            for (index, selM) in self.selectedModels.enumerated() {
                if currentModel == selM {
                    show = true
                    idx = index + 1 + hasSelectCount
                    isSelected = true
                    break
                }
            }
            
            cell.index = idx
            cell.indexLabel.isHidden = !isSelected
            
            setCellMaskView(cell, isSelected: isSelected, model: currentModel)
        }
    }
    
    func setCellMaskView(_ cell: AlbumCollectionCell, isSelected: Bool, model: PhotoModel)  {
        cell.coverView.isHidden = true
        cell.enable = true
        
        var hasSelectModels = SourceInstance.shared.selectModels
        
        if isSelected {
            cell.coverView.isHidden = true
            cell.enable = true
        } else {
            if hasSelectModels.first!.mediaType == .video {
                if model.mediaType == .video {
                    cell.coverView.isHidden = true
                    cell.enable =  true
                } else {
                    cell.coverView.isHidden = false
                    cell.enable =  false
                }
            } else {
                if model.mediaType == .video {
                    cell.coverView.isHidden = false
                    cell.enable =  false
                } else {
                    cell.coverView.isHidden = true
                    cell.enable =  true
                }
            }
        }
    }
    
}



 

