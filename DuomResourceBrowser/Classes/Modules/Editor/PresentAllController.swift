//
//  PresentAllController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/6/18.
//

import Foundation
import DuomBase
import Photos

public class PresentAllController: BaseViewController {
    private let navView: AlbumListNavView = {
        let v = AlbumListNavView.init(title: "")
        return v
    }()

    private lazy var filterListView: AlbumFilterListView = {
        let listView = AlbumFilterListView(dataSource: [AlbumListModel]())
        return listView
    }()

    private let segmentView: DuomSegmentView = DuomSegmentView(frame: .zero).then {
        $0.indicatorView = DuomSegmentIndicatorLineView(frame: .zero).then {
            $0.backgroundColor = PhotosConfiguration.default().segmentLineColor
            $0.lineWidth = .fixed(16)
            $0.lineAnimation = .none
            $0.bottom = 4
        }
        $0.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        $0.tapAnimation = .none
        $0.defaultSelectedIndex = 0
        $0.forbidIndex = 1
        $0.itemSpacing = PhotosConfiguration.default().selectSingleImg ? 16 : 0
    }

    private let segmentContainerView: UIView = UIView().then {
        $0.backgroundColor = .white
    }

    private let containerView = DuomPagingContainerView(frame: .zero).then {
        $0.defaultSelectedIndex = 0
        $0.scrollEnable = false
    }

    private lazy var listModel: [DuomSegmentTextModel] = {
        return createListModel()
    }()

    private let videoVC = VideoController()

    private let albumVC = PresentAlbumViewController()

    private let cameraVC = CameraController().then {
        $0.isAddScence = true
    }

    private lazy var gifVC: GifViewController = {
        let gifVC = GifViewController()
        gifVC.isAddScence = true
        return gifVC
    }()

    
    private lazy var pageVCContainer: [DuomPagingContainerItem] = {
        if PhotosConfiguration.default().selectSingleImg {
            return PhotosConfiguration.default().showGif ? [self.albumVC, self.cameraVC, self.gifVC] : [self.albumVC, self.cameraVC]
        } else {
            return PhotosConfiguration.default().showGif ? [self.albumVC, self.videoVC, self.cameraVC, self.gifVC] : [self.albumVC, self.videoVC, self.cameraVC]
        }
    }()

    /// 相册列表
    private var albumList: [AlbumListModel] = []

    private var currentAlbum: AlbumListModel?

    private(set) var segmentIndex: Int = 0
    
    private(set) var selectedModels: [PhotoModel] = []
    
    var appItemsBlock: (([PhotoModel]) -> Void)?
    
    // 所有在editorVC里的资源数
    var hasSelectCount: Int = 0 {
        didSet {
            albumVC.hasSelectCount = hasSelectCount
        }
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        // 获取相册列表数据
        getAlbumList()

        segmentView.delegate = self
        segmentView.dataSource = self

        containerView.delegate = self
        containerView.dataSource = self
    }

    public override var childForStatusBarStyle: UIViewController? {
        return pageVCContainer[segmentIndex] as? UIViewController
    }

    public override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor

        view.addSubview(navView)
        view.addSubview(filterListView)
        view.addSubview(segmentContainerView)
        segmentContainerView.addSubview(segmentView)
        view.addSubview(containerView)

        filterListView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth)
            make.bottom.equalTo(self.view.snp.top).offset(PhotosConfiguration.default().navHeight)
            make.height.equalTo(UIScreen.screenHeight - PhotosConfiguration.default().navHeight)
        }

        navView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(PhotosConfiguration.default().navHeight)
        }

        if PhotosConfiguration.default().selectSingleImg {
            segmentContainerView.snp.makeConstraints { make in
                make.leading.equalTo(85)
                make.trailing.equalTo(-85)
                make.height.equalTo(PhotosConfiguration.default().segmentViewHight)
                make.bottom.equalTo(self.view)
            }
        } else {
            segmentContainerView.snp.makeConstraints { make in
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(PhotosConfiguration.default().segmentViewHight)
                make.bottom.equalTo(self.view)
            }
        }


        segmentView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(52)
        }

        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(navView.snp.bottom)
            make.bottom.equalTo(segmentContainerView.snp.top)
        }

        view.bringSubviewToFront(segmentContainerView)
        view.bringSubviewToFront(filterListView)
        view.bringSubviewToFront(navView)
        segmentView.layoutIfNeeded()
        containerView.layoutIfNeeded()

        navView.selectAlbumBlock = { [weak self] in
            guard let self = self else { return }
            self.filterListView.isHidden ? self.filterListView.show() : self.filterListView.hide()
        }

        navView.cancelBlock = { [weak self] in
            guard let self = self else { return }
            self.selectedModels.removeAll()
            UIApplication.topViewController()?.dismiss(animated: true)
        }

        navView.nextBlock = { [weak self] in
            guard let self = self else { return }
            self.appItemsBlock?(self.selectedModels)
            self.selectedModels.removeAll()
            UIApplication.topViewController()?.dismiss(animated: true)
        }

        filterListView.selectedAlbumBlock = { [weak self] model in
            guard let self = self else { return }
            self.currentAlbum = model
            self.reloadNavView()
            self.albumVC.currentAlbum = self.currentAlbum
        }

        albumVC.appendItmeBlock = { [weak self] models in
            guard let self = self else { return }
            self.selectedModels = models
            self.navView.setNextButtonState(enable: models.isNotEmpty)
            if models.isNotEmpty, models.count + hasSelectCount > 13 {
                self.refreshSegmentItem()
            }
        }
        
        cameraVC.appendItemByCameraBlock = { [weak self] model in
            guard let self = self else { return }
            self.selectedModels.append(model)
            self.appItemsBlock?(self.selectedModels)
        }
        
        gifVC.appendItemByGiphyBlock = { [weak self] model in
            guard let self = self else { return }
            self.selectedModels.append(model)
            self.appItemsBlock?(self.selectedModels)
        }
    }

    private func createListModel() -> [DuomSegmentTextModel] {
        var normalColor: UIColor = PhotosConfiguration.default().segmentNormalTextColor
        if selectedModels.count + hasSelectCount >= PhotosConfiguration.default().maxSelectCount {
            normalColor = UIColor(244, 244, 244)
        }
        
        var textModels: [DuomSegmentTextModel] = []
        for (idx, item) in PhotosConfiguration.default().segmentItems.enumerated() {
            // 此场景video禁止
            let model = DuomSegmentTextModel(text: item, normalColor: idx == 1 ? UIColor(244, 244, 244) : normalColor, selectColor: PhotosConfiguration.default().segmentSelectedTextColor, font: .appFont(ofSize: 16, fontType: .TT_DemiBold))
//            let model = DuomSegmentTextModel(text: item, normalColor: normalColor, selectColor: PhotosConfiguration.default().segmentSelectedTextColor, font: .appFont(ofSize: 16, fontType: .TT_DemiBold))
            model.contentWidth = UIScreen.screenWidth / CGFloat(PhotosConfiguration.default().segmentItems.count)
            textModels.append(model)
        }
        return textModels
    }

    // 获取相册列表
    private func getAlbumList(completion: (() -> Void)? = nil)  {
        AssetManager.fetchPhotoAlbumList(ascending: PhotosConfiguration.default().sortAscending,
                                         allowSelectImage: PhotosConfiguration.default().allowSelectImage,
                                         allowSelectVideo: PhotosConfiguration.default().allowSelectVideo) { [weak self] albumList in
            guard let self = self else { return }
            self.albumList.removeAll()
            self.albumList.append(contentsOf: albumList)
            // 默认展示第一个相册
            if let listmodel = self.albumList.first {
                self.currentAlbum = listmodel
            }

            DispatchQueue.main.async {
                completion?()
                self.reloadNavView()
                self.reloadFilterListView()
                self.albumVC.currentAlbum = self.currentAlbum
            }
        }
    }

    // 刷新导航view
    private func reloadNavView()  {
        navView.title = self.currentAlbum?.title ?? ""
        navView.reset()
    }

    // 刷新相册列表
    private func reloadFilterListView()  {
        filterListView.dataSource = self.albumList
        filterListView.snp.updateConstraints { make in
            make.height.equalTo(min(AlbumFilterListView.rowH * CGFloat(self.albumList.count), UIScreen.screenHeight - PhotosConfiguration.default().navHeight))
        }
        filterListView.layoutIfNeeded()
    }

    deinit {
        Logger.log(message: "PresentAllController deinit", level: .info)
        videoVC.stopSession()
        cameraVC.stopSession()
    }
}

// MARK: DuomSegmentViewDelegate & DuomSegmentViewDataSource
extension PresentAllController: DuomSegmentViewDelegate, DuomSegmentViewDataSource {
    public func segmentView(_ segmentView: DuomSegmentView, didSelectedAtIndex index: Int, isSame: Bool) {
        if selectedModels.count + hasSelectCount >= PhotosConfiguration.default().maxSelectCount, index > 0 {
            return
        }
        
        if !isSame {
            containerView.scroll(toIndex: index)
            segmentIndex = index
            setNeedsStatusBarAppearanceUpdate()
        }
    }

    public func registerCellClass(in segmentView: DuomSegmentView) {
        segmentView.register(cellWithClass: DuomSegmentTextCell.self)
    }

    public func segmentItemModels(for segmentView: DuomSegmentView) -> [DuomSegmentBaseItemModel] {
        listModel
    }

    public func segmentView(_ segmentView: DuomSegmentView, itemViewAtIndex index: Int) -> DuomSegmentBaseCell {
        let cell = segmentView.dequeueReusableCell(withClass: DuomSegmentTextCell.self, at: index) as! DuomSegmentTextCell
        return cell
    }

    func refreshSegmentItem()  {
        segmentView.forbidEnable = selectedModels.count + hasSelectCount >= PhotosConfiguration.default().maxSelectCount
        let visableIndexPaths = segmentView.collectionView.indexPathsForVisibleItems
        var normalColor = PhotosConfiguration.default().segmentNormalTextColor
        if selectedModels.count + hasSelectCount >= PhotosConfiguration.default().maxSelectCount {
            normalColor = UIColor(244, 244, 244)
        }

        visableIndexPaths.forEach { indexPath in
            guard let cell = segmentView.collectionView.cellForItem(at: indexPath) as? DuomSegmentTextCell, indexPath.row > 0, indexPath.row != 1 else {
                return
            }
            cell.textLabel.textColor = normalColor
        }
    }
}

// MARK: DuomPagingContainerViewDelegate & DuomPagingContainerViewDataSource
extension PresentAllController: DuomPagingContainerViewDelegate, DuomPagingContainerViewDataSource {
    public func containerView(_ containerView: DuomPagingContainerView, itemAt index: Int) -> DuomPagingContainerItem? {
        var item: DuomPagingContainerItem?
        if index < pageVCContainer.count {
            item = pageVCContainer[index]
        }
        return item
    }

    public func numberOfItems(in containerView: DuomPagingContainerView) -> Int {
        return listModel.count
    }

    public func containerViewDidScroll(containerView: DuomPagingContainerView) {
        segmentView.scroll(by: containerView.scrollView)
    }

    public func containerView(_ containerView: DuomPagingContainerView, from fromIndex: Int, to toIndex: Int, percent: CGFloat) {
        if toIndex == 2 {
            scrollToVideoOrCam()
            videoVC.isShowing = false
            videoVC.stopSession()
            cameraVC.isShowing = true
            cameraVC.startSession()
        } else if toIndex == 0 {
            scrollToalbum()
            videoVC.isShowing = false
            videoVC.stopSession()
            cameraVC.isShowing = false
            cameraVC.stopSession()
        } else if toIndex == 3 {
            scrollToGif()
            videoVC.isShowing = false
            videoVC.stopSession()
            cameraVC.isShowing = false
            cameraVC.stopSession()
        }
        containerView.addSubView(at: toIndex)
    }


    func scrollToVideoOrCam()  {
        navView.isHidden = true
        containerView.snp.remakeConstraints { make in
            make.top.equalTo(self.view)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(segmentContainerView.snp.top)
        }
        self.view.layoutIfNeeded()
    }

    func scrollToalbum() {
        navView.isHidden = false
        containerView.snp.remakeConstraints { make in
            make.top.equalTo(navView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(segmentContainerView.snp.top)
        }
        self.view.layoutIfNeeded()
    }

    func scrollToGif() {
        navView.isHidden = true
        containerView.snp.remakeConstraints { make in
            make.top.equalTo(self.view)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(segmentContainerView.snp.top)
        }
        self.view.layoutIfNeeded()
    }
}
