//
//  EditorController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/9.
//  SourceInstance.shared.selectModels只负责输入和输出 不参与本页面内部业务
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import Photos
import Kingfisher

class EditorController: UIViewController, StickerViewDelegate  {
    private let scrollViewContentHeight = UIScreen.screenHeight - UIScreen.navigationBarHeight
    
    private let navContainerView = UIView().then {
        $0.backgroundColor = .black
    }
    
    private let backButton = UIButton().then {
        $0.setImage(UIImage(named: "edit_close"), for: .normal)
    }
    
    private let nextButton: UIButton = UIButton().then {
        $0.backgroundColor = PhotosConfiguration.default().mainThemeColor
        $0.roundCorners(radius: 8)
        $0.setTitle(CopyWitterInstance.default().NextText, for: .normal)
        $0.titleLabel?.textColor = .white
        $0.titleLabel?.font = .appFont(ofSize: 14, fontType: .TT_Bold)
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .appFont(ofSize: 20, fontType: .CR_Regular)
        return label
    }()
    
    private var scrollView = UIScrollView().then {
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.isPagingEnabled = true
        $0.isScrollEnabled = true
        $0.bounces = false
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 0
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = .clear
        view.scrollsToTop = false
        view.dataSource = self
        view.delegate = self
        view.contentInsetAdjustmentBehavior = .never
        view.register(EditorItemCell.self, forCellWithReuseIdentifier: EditorItemCell.identifier)
        return view
    }()
    
    private let bottomContainerView: UIView = UIView().then {
        $0.backgroundColor = .clear
    }
    
    private let bottomImageView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "edit_bottom_back")
    }
    
    private let stickerButton: DuomButton = DuomButton().then {
        $0.duomAlignment = .normal
        $0.titleLabel?.textColor = .white
        $0.titleLabel?.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        $0.setTitle(CopyWitterInstance.default().StickcerText, for: .normal)
        $0.setTitle(CopyWitterInstance.default().StickcerText, for: .highlighted)
        $0.setImage(UIImage(named: "add_sticker"), for: .normal)
        $0.setImage(UIImage(named: "add_sticker"), for: .highlighted)
        $0.setEnlargeEdge(UIEdgeInsets(hAxis: 10, vAxis: 10))
    }
    
    private let cropButton: DuomButton = DuomButton().then {
        $0.duomAlignment = .normal
        $0.titleLabel?.textColor = .white
        $0.titleLabel?.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        $0.setTitle(CopyWitterInstance.default().CropText, for: .normal)
        $0.setTitle(CopyWitterInstance.default().CropText, for: .highlighted)
        $0.setImage(UIImage(named: "add_clipIcon"), for: .normal)
        $0.setImage(UIImage(named: "add_clipIcon"), for: .highlighted)
        $0.setEnlargeEdge(UIEdgeInsets(hAxis: 10, vAxis: 10))
    }
    
    private lazy var editingStickerView: UIView = {
        let view = UIView()
        view.isHidden = true
        return view
    }()
    
    private lazy var editingCloseButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "edit_close"), for: .normal)
        btn.setEnlargeEdge(UIEdgeInsets(hAxis: 10, vAxis: 10))
        return btn
    }()
    
    private lazy var editingDoneButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "edit_done"), for: .normal)
        btn.setEnlargeEdge(UIEdgeInsets(hAxis: 10, vAxis: 10))
        return btn
    }()
    
    private lazy var editingStickerButton: DuomButton = {
        let btn = DuomButton()
        btn.duomAlignment = .normal
        btn.titleLabel?.textColor = .white
        btn.titleLabel?.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        btn.setTitle(CopyWitterInstance.default().StickcerText, for: .normal)
        btn.setTitle(CopyWitterInstance.default().StickcerText, for: .highlighted)
        btn.setImage(UIImage(named: "add_sticker"), for: .normal)
        btn.setImage(UIImage(named: "add_sticker"), for: .highlighted)
        btn.setEnlargeEdge(UIEdgeInsets(hAxis: 10, vAxis: 10))
        return btn
    }()
    
    private lazy var panView: StickerPanView = {
        let panView = StickerPanView()
        return panView
    }()
    
    private var deleteButton: UIButton = UIButton().then {
        $0.setImage(UIImage(named: "edit_delete_icon"), for: .normal)
        $0.setEnlargeEdge(UIEdgeInsets(hAxis: 15, vAxis: 15))
    }

    lazy var datas: [PhotoModel] = SourceInstance.shared.selectModels
    
    // 默认选中第一个
    private var currentIndex: Int = 0 {
        didSet {
            titleLabel.text = "\(currentIndex + 1)/\(datas.count)"
        }
    }
    
    private var stickerContainerViews: [EditMainScrollViewItemView] = []

    // 是否正在编辑sticker
    private(set) var isEditSticker: Bool = false {
        didSet {
            scrollView.isScrollEnabled = !isEditing
        }
    }
    
    // 记录frame,裁剪dismiss使用
    var originalFrame: CGRect = .zero
    
    // 是否发生了顺序重排
    private(set) var hasResort = false
    
    // 重排回调 （有弹窗拦截，暂不需要了）
//    var resortItemBlock: (() -> Void)?
    
    // 删除全部资源回调
    var deleteAllItemBlock: (() -> Void)?
    
    // 是否是push过来的
    var isPushed: Bool = false
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
        self.setSubViews()
        self.configEditModels()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("EditorController init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentIndex = 0
        if datas.isNotEmpty {
            self.isCropOnGiphyGif(self.datas[self.currentIndex].mediaType == .giphyGif)
        }
    }
    
    // 初始设置每个model.editModel 为空
    private func configEditModels()  {
        self.datas.map {
            if $0.editImageModel == nil {
                $0.editImageModel = EditImageModel()
            }
        }
    }
    
    private func setSubViews()  {
        view.backgroundColor = .black
        view.addSubview(navContainerView)
        navContainerView.addSubview(backButton)
        navContainerView.addSubview(nextButton)
        navContainerView.addSubview(titleLabel)
        
        view.addSubview(scrollView)
        scrollView.delegate = self
        view.addSubview(bottomImageView)
        view.addSubview(bottomContainerView)
        view.addSubview(deleteButton)
        
        bottomContainerView.addSubview(collectionView)
        bottomContainerView.addSubview(stickerButton)
        bottomContainerView.addSubview(cropButton)
        
        view.addSubview(editingStickerView)
        editingStickerView.addSubview(editingCloseButton)
        editingStickerView.addSubview(editingDoneButton)
        editingStickerView.addSubview(editingStickerButton)
        
        navContainerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(UIScreen.statusBarHeight)
            make.height.equalTo(44)
        }
        
        nextButton.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.size.equalTo(CGSize(width: 71, height: 34))
            make.bottom.equalTo(-7)
        }
        
        backButton.snp.makeConstraints { make in
            make.centerY.equalTo(nextButton.snp.centerY)
            make.size.equalTo(CGSize(width: 30, height: 30))
            make.leading.equalTo(15)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(nextButton.snp.centerY)
            make.height.equalTo(24)
        }
        
        scrollView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(navContainerView.snp.bottom)
            make.bottom.equalTo(self.view)
        }
        
        bottomContainerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(145.DScale)
            make.bottom.equalTo(self.view)
        }
        
        bottomImageView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(255.DScale)
            make.bottom.equalTo(self.view)
        }

        stickerButton.snp.makeConstraints { make in
            make.bottom.equalTo(-28)
            make.size.equalTo(CGSize(width: 60, height: 50).DScale)
            make.trailing.equalTo(bottomContainerView.snp.centerX).offset(-20)
        }
        
        cropButton.snp.makeConstraints { make in
            make.centerY.equalTo(stickerButton.snp.centerY)
            make.size.equalTo(stickerButton.snp.size)
            make.leading.equalTo(bottomContainerView.snp.centerX).offset(20)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(stickerButton.snp.top).offset(-25)
            make.height.equalTo(40)
        }
        
        editingStickerView.snp.makeConstraints { make in
            make.bottom.equalTo(-65)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: UIScreen.screenWidth, height: 50))
        }
        
        editingStickerButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 60, height: 50))
        }
        
        editingCloseButton.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.top.equalTo(editingStickerButton.snp.top)
            make.size.equalTo(CGSize(width: 24, height: 24).DScale)
        }

        editingDoneButton.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.top.equalTo(editingStickerButton.snp.top)
            make.size.equalTo(CGSize(width: 24, height: 24).DScale)
        }
        
        deleteButton.snp.makeConstraints { make in
            make.top.equalTo(navContainerView.snp.bottom).offset(16)
            make.trailing.equalTo(-17)
            make.size.equalTo(CGSize(width: 40, height: 40))
        }
        
        backButton.addTarget(self, action: #selector(clickBackAction(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(clickNextAction(_:)), for: .touchUpInside)
        stickerButton.addTarget(self, action: #selector(addStickerAction(_:)), for: .touchUpInside)
        cropButton.addTarget(self, action: #selector(toClipAction(_:)), for: .touchUpInside)
        deleteButton.addTarget(self, action: #selector(deleteAction(_:)), for: .touchUpInside)
        
        editingCloseButton.addTarget(self, action: #selector(clickEditingCloseAction(_:)), for: .touchUpInside)
        editingStickerButton.addTarget(self, action: #selector(addStickerAction(_:)), for: .touchUpInside)
        editingDoneButton.addTarget(self, action: #selector(clickEditingDoneAction(_:)), for: .touchUpInside)
          
        let longGes = UILongPressGestureRecognizer(target: self, action: #selector(longGestureAction(_:)))
        collectionView.addGestureRecognizer(longGes)
        
        reloadScrollView()
        collectionView.reloadData()
    }
    
    func reloadScrollView()  {
        stickerContainerViews.forEach { $0.removeFromSuperview() }
        stickerContainerViews.removeAll()
        
        scrollView.contentSize = CGSize(width: UIScreen.screenWidth * CGFloat(datas.count), height: scrollViewContentHeight)
        for (index, item) in datas.enumerated() {
            var imgData: Data = Data()
            var frame = CGRect(x: 0 + CGFloat(index) * UIScreen.screenWidth, y: 0, width: UIScreen.screenWidth, height: scrollViewContentHeight)
            if item.mediaType == .giphyGif {
                addViewToScrollView(model: item, frame: frame, url: item.fileURL)
            } else {
                imgData = getImageDataOnScrollView(item)
                addViewToScrollView(model: item, frame: frame, imageData: imgData)
            }
        }
        scrollView.setContentOffset(CGPoint(x: 0 + currentIndex * Int(UIScreen.screenWidth), y: 0), animated: false)
        originalFrame = scrollView.frame
    }
    
    func appendItemsOnScrollView(_ addModels: [PhotoModel])  {
        guard addModels.isNotEmpty else { return }
        scrollView.contentSize = CGSize(width: UIScreen.screenWidth * CGFloat(datas.count + addModels.count), height: scrollViewContentHeight)
       
        for (index, item) in addModels.enumerated() {
            var imgData: Data = Data()
            var frame = CGRect(x: 0 + CGFloat(index + datas.count) * UIScreen.screenWidth, y: 0, width: UIScreen.screenWidth, height: scrollViewContentHeight)
            if let imageOrinalData = item.imageOriginalData {
                imgData = imageOrinalData
                addViewToScrollView(model: item, frame: frame, imageData: imgData)
            } else if item.mediaType == .giphyGif {
                addViewToScrollView(model: item, frame: frame, url: item.fileURL)
            }
        }
        scrollView.setContentOffset(CGPoint(x: 0 + currentIndex * Int(UIScreen.screenWidth), y: 0), animated: false)
    }
    
    func addViewToScrollView(model: PhotoModel, frame: CGRect, imageData: Data? = nil, url: URL? = nil) {
        var itemView: EditMainScrollViewItemView?
        if let imageData = imageData {
            itemView = EditMainScrollViewItemView(imageData: imageData, frame: calculateImageStickerFrame(frame, imageData: imageData))
        } else if let url = url {
            itemView = EditMainScrollViewItemView(url: url, frame: frame)
        }
        
        guard let itemView = itemView else {
            Logger.log(message: "have a error itemView on scrollView", level: .error)
            return
        }
        scrollView.addSubview(itemView)
        itemView.isUserInteractionEnabled = true
        stickerContainerViews.append(itemView)
        
        applyStickersIfExist(model, itemView: itemView)
    }
    
    // 根据图片宽高比 计算frame
    private func calculateImageStickerFrame(_ parentFrame: CGRect, imageData: Data?) -> CGRect {
        guard let imageData = imageData, let image = UIImage(data: imageData) else {
            return parentFrame
        }
        
        let imgWidth = image.size.width
        let imgHeight = image.size.height
        let imgWHRatio = image.size.width / image.size.height
        
        var frame: CGRect = parentFrame
        let screenRatio = UIScreen.screenWidth / UIScreen.screenHeight
        if imgWHRatio > screenRatio {
            frame.size.width = UIScreen.screenWidth
            frame.size.height = UIScreen.screenWidth / imgWHRatio
        } else {
            frame.size.height = UIScreen.screenHeight
            frame.size.width = UIScreen.screenHeight * imgWHRatio
        }
        frame.origin.x = max(0, (UIScreen.screenWidth - frame.size.width) / 2 + frame.origin.x)
        frame.origin.y = max(0, (UIScreen.screenHeight - frame.size.height) / 2 - UIScreen.navigationBarHeight)
        return frame
    }
    
    func setIndex(_ index: Int) {
        guard self.datas.isNotEmpty else { return }
        DispatchQueue.main.async {
            self.currentIndex = index
            self.scrollView.setContentOffset(CGPoint(x: 0 + self.currentIndex * Int(UIScreen.screenWidth), y: 0), animated: false)
            self.collectionView.setContentOffset(CGPoint(x:self.currentIndex * 36, y: 0), animated: false)
            self.refreshItemCellHeight()
            self.isCropOnGiphyGif(self.datas[self.currentIndex].mediaType == .giphyGif)
        }
    }
}


// MARK: private func
extension EditorController {
    @objc private func longGestureAction(_ ges: UILongPressGestureRecognizer)  {
        let point = ges.location(in: collectionView)
        var maxIndex = 0
        if datas.count == PhotosConfiguration.default().maxSelectCount {
            maxIndex = datas.count - 1
        } else {
            maxIndex = datas.count - 1
        }
        
        switch ges.state {
        case .began:
            if let index: IndexPath = collectionView.indexPathForItem(at: point),
               index.row <= maxIndex,
                let cell = collectionView.cellForItem(at: index) as? EditorItemCell {
                UIView.animate(withDuration: 0.2) {
                    cell.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                }
                collectionView.beginInteractiveMovementForItem(at: index)
            }
            break
        case .changed:
            if let index: IndexPath = collectionView.indexPathForItem(at: point),
               index.row <= maxIndex {
                collectionView.updateInteractiveMovementTargetPosition(point)
            }
            break
        case .ended:
            collectionView.endInteractiveMovement()
            break
        default:
            collectionView.cancelInteractiveMovement()
            break
        }
    }
    
    @objc private func clickBackAction(_ sender: UIButton)  {
        let alert = DuomAlertCardViewController(title: "Discard".localized,
                                                content: "Your photo has not been saved yet. Are you sure you want to discard?".localized,
                                                insideItem: DuomAlertCardItemConfig(title: "Cancel".localized, dismissOnTap: true, font: .appFont(ofSize: 20, fontType: .TT_Regular), titleColor: UIColor(6, 5, 6), backgroundColor: .clear, highlightedColor: .clear),
                                                outsideItem: DuomAlertCardItemConfig.init(title: "Confirm".localized, backgroundColor: UIColor(83, 89, 214), highlightedColor: UIColor(110, 116, 240)))
        alert.outsideAction = { [weak self] in
            guard let self = self else { return }
            if SourceInstance.shared.backupModels.isEmpty {
                SourceInstance.shared.selectModels.removeAll()
            }
            if self.isPushed {
                UIApplication.topViewController()?.navigationController?.popToRootViewController(animated: true)
            } else {
                // 这里改动 注意刷新首页数据
                UIApplication.topViewController()?.dismiss(animated: true)
            }
        }
        UIApplication.topViewController()?.present(alert, animated: true)
    }
    
    // MARK: 上传
    @objc private func clickNextAction(_ sender: UIButton)  {
        var selectedModels = [(PhotoModel, Bool, UIImage?)]()
        for (idx, item) in stickerContainerViews.enumerated() {
            var needSaveAblum: Bool = false
            var editedImage = item.imageView.image
            // 添加拍摄的图片
            if datas[idx].isTakedPhoto {
                needSaveAblum = true
                editedImage = item.imageView.image
            }
            
            // 是否裁剪
            if item.hasClip {
                needSaveAblum = true
                editedImage = item.imageView.image
            }
            
            // 是否有sticker
            var hasSticker: Bool = false
            for (stickerIdx, stickerItem) in item.subviews.enumerated() {
                if stickerItem.isKind(of: ImageStickerView.self) {
                    (stickerItem as! ImageStickerView).currentItemView?.hideBorder()
                    hasSticker = true
                }
            }
            if hasSticker {
                needSaveAblum = true
                editedImage = rebuildImage(item)
            }
            
            selectedModels.append((datas[idx], needSaveAblum, editedImage))
        }

        var allModels = [PhotoModel]()
        selectedModels.forEach { (model, isEdited, resImage) in
            if model.isTakedPhoto { // 拍摄图片需要补全信息
                if let resImage = resImage {
                    model.width = Int(resImage.width)
                    model.height = Int(resImage.height)
                }
                model.mediaType = .image
            }
            
            if isEdited, let resImage = resImage {
                saveEditInfoToLocal(model, rebuildImg: resImage)
                model.fileURL = PhotoTools.write(image: resImage)
            }
            
            allModels.append(model)
        }
        
        UIApplication.topViewController()?.navigationController?.dismiss(animated: true, completion: {
            SourceInstance.shared.selectModels = allModels
            SourceInstance.shared.selectedSourceBlock?(allModels)
        })
    }
    
    @objc private func addStickerAction(_ sender: UIButton)  {
        MobStatistics.trackAction("ImagePicker_edit_sticker_click")
        panView.show(in: view)
        
        panView.hideBlock = { [weak self] in
            guard let self = self else { return }
            self.resetEditViewState(false)
        }
        
        panView.closeBlock = { [weak self] in
            guard let self = self else { return }
            self.resetNavViewState(false)
            self.resetBottomContainerState(false)
        }
        
        panView.tapGesBlock = { [weak self] in
            guard let self = self else { return }
            self.resetNavViewState(false)
            self.resetBottomContainerState(false)
        }
        
        panView.selectImageBlock = { [weak self] (img, imgString) in
            if let imgString = imgString {
                self?.addImageStickerView(img, URL(string: imgString))
            }
        }
        
        resetNavViewState(true)
        resetBottomContainerState(true)
        resetEditViewState(true)
    }
    
    @objc private func toClipAction(_ sender: UIButton) {
        MobStatistics.trackAction("ImagePicker_edit_clip_click")
        let clipInfo = getImageDataToClip(datas[currentIndex])
        
        guard let currentImg = UIImage(data: clipInfo.0) else {
            return
        }
        let clipVc = ClipImageViewController(image: currentImg, editRect: .zero, selectRatio: clipInfo.1)
        clipVc.presentAnimateImage = currentImg.clipImage(angle: 0, editRect: CGRect(x: 0, y: 0, width: (currentImg.size.width), height: (currentImg.size.height)))
        clipVc.modalPresentationStyle = .fullScreen
        clipVc.clipDoneBlock = { [weak self] editedImg, editFrame, selectRadio in
            guard let self = self else { return }
            self.datas[currentIndex].width = Int(editedImg.size.width)
            self.datas[currentIndex].height = Int(editedImg.size.height)
            self.updateEditModelForClip(editImage: editedImg, editFrame: editFrame, selectRadio: selectRadio)
            self.resetCurrentImageFrame(editedImg: editedImg, editFrame: editFrame)
        }
        present(clipVc, animated: false)
    }
    
    @objc private func deleteAction(_ sender: UIButton) {
        guard currentIndex < stickerContainerViews.count, currentIndex < datas.count else {
            return
        }
        var scrollToRight: Bool = true
        scrollToRight = currentIndex == datas.count - 1 ?  false : true
        
        datas.remove(at: currentIndex)
        if datas.isEmpty {
            SourceInstance.shared.selectModels.removeAll()
            if isPushed {
                deleteAllItemBlock?()
                UIApplication.topViewController()?.navigationController?.popToRootViewController(animated: true)
            } else {
                SourceInstance.shared.selectedSourceBlock?([])
                UIApplication.topViewController()?.navigationController?.dismiss(animated: true)
            }
            return
        }
        
        let currentStickerView = stickerContainerViews[currentIndex]
        currentStickerView.removeFromSuperview()
        stickerContainerViews.remove(at: currentIndex)
        if !scrollToRight, currentIndex > 0 {
            currentIndex -= 1
        }
        
        titleLabel.text = "\(self.currentIndex + 1)/\(self.datas.count)"
        collectionView.reloadData()
        collectionView.setContentOffset(CGPoint(x:currentIndex * 36, y: 0), animated: true)
        reloadScrollViewWhenDelete()
    }
    
    private func reloadScrollViewWhenDelete() {
        for (idx, item) in stickerContainerViews.enumerated() {
            item.frame.origin.x = CGFloat(idx) * UIScreen.screenWidth
        }
        scrollView.contentSize = CGSize(width: UIScreen.screenWidth * CGFloat(datas.count), height: scrollViewContentHeight)
        scrollView.setContentOffset(CGPoint(x: 0 + currentIndex * Int(UIScreen.screenWidth), y: 0), animated: false)
    }
    
    // 裁剪后重新设置当前item image
    private func resetCurrentImageFrame(editedImg: UIImage, editFrame: CGRect)  {
        guard let currentItemView = stickerContainerViews[currentIndex] as? EditMainScrollViewItemView  else {
            return
        }
        let oldFrame = currentItemView.frame
        currentItemView.frame = reCalculateImageStickerFrame(image: editedImg)
        currentItemView.resetImageFrame(editImage: editedImg, editFrame: currentItemView.bounds)
        updateStickersFrame(currentStickerView: currentItemView, oldFrame: oldFrame)
    }
    
    private func reCalculateImageStickerFrame(image: UIImage) -> CGRect {
        let imgWidth = image.size.width
        let imgHeight = image.size.height
        let imgWHRatio = image.size.width / image.size.height
        let parentFrame = CGRect(x: 0 + CGFloat(currentIndex) * UIScreen.screenWidth, y: 0, width: UIScreen.screenWidth, height: scrollViewContentHeight)
        
        var frame: CGRect = parentFrame
        let screenRatio = UIScreen.screenWidth / UIScreen.screenHeight
        if imgWHRatio > screenRatio {
            frame.size.width = UIScreen.screenWidth
            frame.size.height = UIScreen.screenWidth / imgWHRatio
        } else {
            frame.size.height = UIScreen.screenHeight
            frame.size.width = UIScreen.screenHeight * imgWHRatio
        }
        frame.origin.x = max(0, (UIScreen.screenWidth - frame.size.width) / 2 + frame.origin.x)
        frame.origin.y = max(0, (UIScreen.screenHeight - frame.size.height) / 2 - UIScreen.navigationBarHeight)
        return frame
    }
    
    private func updateStickersFrame(currentStickerView: EditMainScrollViewItemView, oldFrame: CGRect) {
        guard currentStickerView.subviews.isNotEmpty else {
            return
        }
        
        for stickItemView in currentStickerView.subviews {
            stickItemView.frame = currentStickerView.bounds
            
            for (index, stick) in stickItemView.subviews.enumerated() {
                let stickCenterX = stick.frame.minX + stick.frame.width / 2
                let stickCenterY = stick.frame.origin.y + stick.frame.height / 2
                let width = stick.frame.width
                let height = stick.frame.height
                let centerDiffY: CGFloat = stickCenterY - (oldFrame.size.height / 2)
                let centerDiffX: CGFloat = stickCenterX - (oldFrame.size.width / 2)
                let heighDiff: CGFloat = oldFrame.height - stick.frame.maxY
                
                var newCenterY: CGFloat = 0.0
                if height >= stickItemView.frame.height {
                    newCenterY = stickItemView.center.y
                } else {
                    if centerDiffY < 0 {
                        newCenterY = max(height / 2, stickItemView.center.y + centerDiffY)
                    } else {
                        newCenterY = min(stickItemView.frame.maxY - height / 2, stickItemView.center.y + centerDiffY)
                    }
                }
                stick.center = CGPoint(x: stickItemView.center.x + centerDiffX, y: newCenterY)
                stick.frame.size = CGSize(width: width, height: height)
                
                // 更新对应editModel数据
                updateStickerInfoAfterClip(index, newCenter: stick.center)
            }
        }
    }
    
    // 重置导航栏元素隐藏状态
    private func resetNavViewState(_ hidden: Bool)  {
        backButton.isHidden = hidden
        nextButton.isHidden = hidden
        deleteButton.isHidden = hidden
    }
    
    // 底部container隐藏状态
    private func resetBottomContainerState(_ hidden: Bool)  {
        bottomContainerView.isHidden = hidden
    }
    
    // 正在编辑的view
    private func resetEditViewState(_ hidden: Bool) {
        editingStickerView.isHidden = hidden
    }
    
    private func rebuildImage(_ renderView: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(renderView.frame.size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        renderView.layer.render(in: context)
        let newImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let newImg = newImg else {
            return nil
        }
        return newImg
    }
    
    private func addImageStickerView(_ image: UIImage?, _ imgURL: URL?)  {
        isEditSticker = true
        var stickerContentView: ImageStickerView?
        if let currentView = stickerContainerViews[currentIndex] as? UIView {
            if let contentView = currentView.subviews.first(where: { $0 is ImageStickerView }) as? ImageStickerView {
                stickerContentView = contentView
            } else {
                stickerContentView = ImageStickerView(frame: currentView.bounds, parent: self)
                stickerContentView?.delegate = self
            }
            
            if let stickerContentView = stickerContentView {
                if let currentModel = datas[currentIndex] as? PhotoModel, let currentEditModle = currentModel.editImageModel {
                    // 添加数据
                    var tempEditInfoModel = EditImageStickerModel()
                    tempEditInfoModel.stickURL = imgURL
                    currentModel.editImageModel?.stickerInfo.append(tempEditInfoModel)
                }
                // 添加stickerItem到stickerContentView上
                stickerContentView.applyStickerItemOnView(image, imageURL: imgURL)
                currentView.addSubview(stickerContentView)
            }
        }
    }
    
    @objc private func clickEditingCloseAction(_ sender: UIButton)  {
        let currentEditView = stickerContainerViews[currentIndex]
        for (index, stickerItem) in currentEditView.subviews.enumerated() {
            if stickerItem.isKind(of: ImageStickerView.self) {
                (stickerItem as! ImageStickerView).currentItemView?.removeFromSuperview()
                (stickerItem as! ImageStickerView).removeCurrentItem()
                (stickerItem as! ImageStickerView).currentItemView = nil
                
                // 删除编辑数据
                if let currentModel = datas[currentIndex] as? PhotoModel,
                   let editModle = currentModel.editImageModel,
                    let stickerIndex = (stickerItem as! ImageStickerView).currentIndex {
                    if editModle.stickerInfo.count > stickerIndex {
                        currentModel.editImageModel?.stickerInfo.remove(at: stickerIndex)
                    }
                }
            }
        }
        
        resetNavViewState(false)
        resetBottomContainerState(false)
        resetEditViewState(true)
        isEditSticker = false
    }
    
    @objc private func clickEditingDoneAction(_ sender: UIButton)  {
        let currentEditView = stickerContainerViews[currentIndex]
        for (index, stickerItem) in currentEditView.subviews.enumerated() {
            if stickerItem.isKind(of: ImageStickerView.self) {
                (stickerItem as! ImageStickerView).panGes.isEnabled = false
                (stickerItem as! ImageStickerView).currentItemView?.hideBorder()
            }
        }
        
        resetNavViewState(false)
        resetBottomContainerState(false)
        resetEditViewState(true)
        isEditSticker = false
    }
    
    // 如果当前是giphy gif 那么隐藏编辑操作
    private func isCropOnGiphyGif(_ isGiphyGif: Bool)  {
        stickerButton.isHidden = isGiphyGif
        cropButton.isHidden = isGiphyGif
    }
}

// MARK: ImageStickerViewDelegate
extension EditorController {
    func stickerBeginOperation() {
        scrollView.isScrollEnabled = false
        resetBottomContainerState(true)
        resetEditViewState(false)
    }
    
    func stickerEndOperation() {
        scrollView.isScrollEnabled = true
        resetBottomContainerState(false)
        resetEditViewState(true)
        self.resetNavViewState(false)
    }
    
    func deleteItemOperation(index: Int?, stickerView: ImageStickerView) {
        self.resetNavViewState(false)
        self.resetBottomContainerState(false)
        self.resetEditViewState(true)
        
        if var currentModel = datas[currentIndex] as? PhotoModel,
           var editModle = currentModel.editImageModel,
            let stickerIndex = stickerView.currentIndex {
            if editModle.stickerInfo.count > stickerIndex {
                editModle.stickerInfo.remove(at: stickerIndex)
                datas[currentIndex].editImageModel?.stickerInfo = editModle.stickerInfo
            }
        }
    }
    
    func updateStickerItemInfo(index: Int?, stickerView: ImageStickerView, center: CGPoint?, agle: CGFloat?, scale: CGFloat?) {
        if let currentModel = datas[currentIndex] as? PhotoModel,
           let editModle = currentModel.editImageModel,
            let stickerIndex = stickerView.currentIndex {
            if editModle.stickerInfo.count > stickerIndex {
                var stickerModel = editModle.stickerInfo[stickerIndex]
                stickerModel.angle = agle
                stickerModel.scale = scale
                stickerModel.center = center
                datas[currentIndex].editImageModel?.stickerInfo[stickerIndex] = stickerModel
            }
        }
    }
}

// MARK: UIScrollViewDelegate
extension EditorController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView != collectionView else { return }
        currentIndex = Int(scrollView.contentOffset.x / UIScreen.screenWidth)
        collectionView.setContentOffset(CGPoint(x:currentIndex * 36, y: 0), animated: true)
        refreshItemCellHeight()
        self.isCropOnGiphyGif(datas[self.currentIndex].mediaType == .giphyGif)
    }
}

// MARK: UICollectionViewDelegate
extension EditorController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EditorItemCell.identifier, for: indexPath) as? EditorItemCell else {
            return UICollectionViewCell()
        }
        if indexPath.row < datas.count {
            cell.updateViewState(isSelected: indexPath.row == currentIndex, model: datas[indexPath.row])
        } else {
            cell.updateViewState(isSelected: false, isAddCell: true)
        }
        return  cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if PhotosConfiguration.default().selectSingleImg {
            return datas.count
        }
        return  datas.count >= 15 ? datas.count : datas.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < datas.count {
            currentIndex = indexPath.row
            scrollView.setContentOffset(CGPoint(x: 0 + currentIndex * Int(UIScreen.screenWidth), y: 0), animated: false)
            collectionView.setContentOffset(CGPoint(x: currentIndex * 36, y: 0), animated: true)
            refreshItemCellHeight()
            self.isCropOnGiphyGif(datas[self.currentIndex].mediaType == .giphyGif)
        } else {
            if datas.count >= PhotosConfiguration.default().maxSelectCount {
                toast(CopyWitterInstance.default().overMaxSelectCount(PhotosConfiguration.default().maxSelectCount))
                return
            }
            
            let albumVC = PresentAllController()
            albumVC.appItemsBlock = { [weak self] addModels in
                guard let self = self else { return }
                addModels.forEach {
                    $0.editImageModel = EditImageModel()
                }
                self.appendItemsOnScrollView(addModels) // 注意这里的入参
                self.datas += addModels
                self.collectionView.reloadData()
                self.titleLabel.text = "\(self.currentIndex + 1)/\(self.datas.count)"
            }
            albumVC.hasSelectCount = datas.count
            let nav = PhotoNavgationController.init(rootViewController: albumVC)
            nav.modalPresentationStyle = .fullScreen
            self.showDetailViewController(nav, sender: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EditorItemCell.identifier, for: indexPath) as? EditorItemCell else {
            return PhotosConfiguration.default().editImageConfiguration.editItemSelectedCellSize
        }
        return PhotosConfiguration.default().editImageConfiguration.editItemSelectedCellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: (UIScreen.screenWidth - PhotosConfiguration.default().editImageConfiguration.editItemSelectedCellSize.width) / 2, bottom: 0, right: (UIScreen.screenWidth - PhotosConfiguration.default().editImageConfiguration.editItemNormalCellSize.width) / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        if datas.count == PhotosConfiguration.default().maxSelectCount {
            return true
        } else {
            return (indexPath.row < datas.count) ? true : false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let tempModel = datas[sourceIndexPath.row]
        if datas.count == PhotosConfiguration.default().maxSelectCount {
            datas.remove(at: sourceIndexPath.row)
            datas.insert(tempModel, at: destinationIndexPath.row)
        } else {
            guard sourceIndexPath.row < datas.count, destinationIndexPath.row < datas.count else {
                return
            }
            datas.remove(at: sourceIndexPath.row)
            datas.insert(tempModel, at: destinationIndexPath.row)
        }
        
        resortStickerContainerViews(moveItemAt: sourceIndexPath.row, to: destinationIndexPath.row)
        
        hasResort = true
        
        // 更新currentIndex  落点就是currentIndex
        currentIndex = destinationIndexPath.row
        refreshItemCellHeight()
        scrollView.setContentOffset(CGPoint(x: 0 + currentIndex * Int(UIScreen.screenWidth), y: 0), animated: false)
        collectionView.setContentOffset(CGPoint(x: currentIndex * 36, y: 0), animated: true)
  
        self.isCropOnGiphyGif(self.datas[self.currentIndex].mediaType == .giphyGif)
    }
    
    func resortStickerContainerViews(moveItemAt sourceIndex: Int, to destinationIndex: Int)  {
        var stickerItemViews = stickerContainerViews
        var stickerItemFrames: [CGRect] = []
        var originStickerItemFrams: [CGRect] = []
        stickerItemViews.map {
            originStickerItemFrams.append($0.frame)
            stickerItemFrames.append($0.frame)
        }
        let moveView = stickerItemViews[sourceIndex]
        let moveViewFrame = stickerItemFrames[sourceIndex]
        if stickerItemViews.count == PhotosConfiguration.default().maxSelectCount {
            stickerItemViews.remove(at: sourceIndex)
            stickerItemViews.insert(moveView, at: destinationIndex)
            stickerItemFrames.remove(at: sourceIndex)
            stickerItemFrames.insert(moveViewFrame, at: destinationIndex)
        } else {
            guard sourceIndex < stickerItemViews.count, destinationIndex < datas.count else {
                return
            }
            stickerItemViews.remove(at: sourceIndex)
            stickerItemViews.insert(moveView, at: destinationIndex)
            stickerItemFrames.remove(at: sourceIndex)
            stickerItemFrames.insert(moveViewFrame, at: destinationIndex)
        }
        stickerContainerViews = stickerItemViews
        
        stickerContainerViews.map {
            $0.removeFromSuperview()
        }
        
        for(index, itemView) in stickerItemViews.enumerated() {
            let tempFrame = stickerItemFrames[index]
            let div = (tempFrame.origin.x / UIScreen.screenWidth).rounded()
            itemView.frame.origin = CGPoint(x: 0 + CGFloat(index) * UIScreen.screenWidth + (tempFrame.origin.x - div * (UIScreen.screenWidth)) , y: tempFrame.origin.y)
            itemView.frame.size = tempFrame.size
            scrollView.addSubview(itemView)
        }
    }
    
    func refreshItemCellHeight()  {
        let visableIndexPaths = collectionView.indexPathsForVisibleItems
        visableIndexPaths.forEach { indexPath in
            guard let cell = collectionView.cellForItem(at: indexPath) as? EditorItemCell else {
                return
            }
            if indexPath.row == currentIndex {
                cell.updateViewState(isSelected: true)
            } else {
                cell.updateViewState(isSelected: false)
            }
        }
    }
}

// MARK: 编辑信息
extension EditorController {
    // 裁剪后更新editModel
    private func updateEditModelForClip(editImage: UIImage, editFrame: CGRect, selectRadio: ImageClipRatio)  {
        let currentModel = self.datas[self.currentIndex]
        currentModel.editImageModel?.clipImageData = editImage.pngData()
        currentModel.editImageModel?.editImage = editImage.pngData()
        currentModel.editImageModel?.editRect = editFrame
        switch selectRadio {
        case .circle:
            currentModel.editImageModel?.clipRatio  = 6
        case .wh1x1:
            currentModel.editImageModel?.clipRatio  = 3
        case .wh4x5:
            currentModel.editImageModel?.clipRatio  = 1
        case .wh5x4:
            currentModel.editImageModel?.clipRatio  = 2
        case .wh9x16:
            currentModel.editImageModel?.clipRatio  = 4
        case .wh16x9:
            currentModel.editImageModel?.clipRatio  = 5
        default:
            currentModel.editImageModel?.clipRatio  = 3
        }
    }
    
    // next 保存编辑信息
    private func saveEditInfoToLocal(_ model: PhotoModel, rebuildImg: UIImage)  {
        guard var editModel = model.editImageModel else { return }
        if editModel.originImageData == nil {
            editModel.originImageData = model.imageOriginalData
        }
        if let editData = try? JSONEncoder().encode(editModel) {
            model.editId = "\(CLongLong(Date().timeIntervalSince1970 * 1000))"
            PhotoTools.saveEditInfo("\(model.editId!)", info: editData)
        }
    }
    
    
    // 获取渲染image data
    private func getImageDataOnScrollView(_ model: PhotoModel)  -> Data {
        var imageData: Data = Data()
        if let _ = model.editId {
            if let editModel = model.editImageModel {
                if let _ = editModel.clipRatio, let clipImageData = editModel.clipImageData {
                    imageData = clipImageData
                    return imageData
                }
                
                if let imageOrinalData = editModel.originImageData {
                    imageData = imageOrinalData
                    return imageData
                }
            }
        } else {
            if let imageOrinalData = model.imageOriginalData {
                imageData = imageOrinalData
                return imageData
            }
        }
        return imageData
    }
    
    // 跳转裁剪前 获取原图
    private func getImageDataToClip(_ model: PhotoModel) -> (Data, ImageClipRatio) {
        var imageData: Data = Data()
        var clipRatio: ImageClipRatio = .wh4x5
        
        if let imageOrinalData = model.imageOriginalData {
            imageData = imageOrinalData
        }
        
        if let _ = model.editId {
            if let editModel = model.editImageModel {
                if let imageOrinalData = editModel.originImageData {
                    imageData = imageOrinalData
                }
                if let clipInt = editModel.clipRatio {
                    switch clipInt {
                    case 1:
                        clipRatio = .wh4x5
                    case 2:
                        clipRatio = .wh5x4
                    case 3:
                        clipRatio = .wh1x1
                    case 4:
                        clipRatio = .wh9x16
                    case 5:
                        clipRatio = .wh16x9
                    case 6:
                        clipRatio = .wh1x1
                    default:
                        clipRatio = .wh4x5
                    }
                }
            }
        }
        
        return (imageData, clipRatio)
    }
    
    // 添加Sticker 如果有
    private func applyStickersIfExist(_ model: PhotoModel, itemView: EditMainScrollViewItemView)  {
        guard let _ = model.editId, let editModel = model.editImageModel, editModel.stickerInfo.isNotEmpty else { return }
        let stickerContentView = ImageStickerView(frame: itemView.bounds, parent: self)
        stickerContentView.delegate = self
        itemView.addSubview(stickerContentView)
        
        editModel.stickerInfo.forEach {
            if let stickerURL = $0.stickURL, stickerURL.absoluteString.isNotEmpty {
                stickerContentView.applyStickerInfoIfExist(nil, imageURL: stickerURL, photoModel: model, stickerInfo: $0)
            }
        }
    }
    
    // 已添加贴纸 裁剪后更新位置信息
    private func updateStickerInfoAfterClip(_ stickerIndex: Int, newCenter: CGPoint) {
        let currentModel = datas[currentIndex]
        guard let editModle = currentModel.editImageModel, editModle.stickerInfo.count > stickerIndex else {
            return
        }
        
        var stickerInfo = editModle.stickerInfo[stickerIndex]
        stickerInfo.center = newCenter
        datas[currentIndex].editImageModel?.stickerInfo[stickerIndex] = stickerInfo
    }
}


// MARK: ScrollView上单个itemView
class EditMainScrollViewItemView: UIView {
    var imageView: UIImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }
    
    private(set) var hasClip: Bool = false
    
    convenience init(imageData: Data, frame: CGRect) {
        self.init(frame: frame)
        guard let img = UIImage(data: imageData) else {
            return
        }
        imageView.image = img
    }
    
    convenience init(url: URL, frame: CGRect) {
        self.init(frame: frame)
        imageView.kf.setImage(with: url)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
        addSubview(imageView)
        imageView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // 裁剪后重设image frame
    func resetImageFrame(editImage: UIImage, editFrame: CGRect)  {
        hasClip = true
        imageView.image = editImage
        imageView.frame = editFrame
//        imageView.frame = CGRect(x: (bounds.width - editFrame.width) / 2, y: (bounds.height - editFrame.height) / 2, width: editFrame.width, height: editFrame.height)
    }

}
