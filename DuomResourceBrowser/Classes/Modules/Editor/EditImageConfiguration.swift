//
//  EditImageConfiguration.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/9.
//

import Foundation
import UIKit
 
public protocol ImageStickerContainerDelegate {
    var selectImageBlock: ((UIImage?, String?) -> Void)? { get set }
    var hideBlock: (() -> Void)? { get set }
    func show(in view: UIView)
}

public class EditImageConfiguration: NSObject {
    public var stickerSectionItmes: Int = 12
    
    // MARK: 编辑
    public var editItemSelectedCellSize = CGSize(width: 28, height: 35)
    
    public var editItemNormalCellSize = CGSize(width: 28, height: 28)
    
    public let downloadNotificationName = Notification.Name.init("key_download_sticker")
    
    public let refreshStickerNotification = Notification.Name.init("refreshStickerNotification")
    
    private var _clipRatios: [ImageClipRatio] = [.wh4x5, .wh5x4, .wh1x1, .wh9x16, .wh16x9]
    
    public var clipRatios: [ImageClipRatio] {
        get {
            if _clipRatios.isEmpty {
                return [.custom]
            } else {
                return _clipRatios
            }
        }
        set {
            _clipRatios = newValue
        }
    }
    
    public var defaultClipRatios = ImageClipRatio.wh4x5
    
    // 外部指定裁剪比例
    public var assignClipRations = ImageClipRatio.wh1x1
    
    // MARK: 编辑视频 秒
    public var maxEditVideoTime: Second = 60
    
    public var minEditVideoTime: Second = 5
}

// MARK: 裁剪
public class ImageClipRatio: NSObject {
    public var title: String
    
    public let whRatio: CGFloat
    
    public let isCircle: Bool
    
    public init(title: String, whRatio: CGFloat, isCircle: Bool = false) {
        self.title = title
        self.whRatio = isCircle ? 1 : whRatio
        self.isCircle = isCircle
        super.init()
    }
}

public extension ImageClipRatio {
    static func ==(lhs: ImageClipRatio, rhs: ImageClipRatio) -> Bool {
        return lhs.whRatio == rhs.whRatio && lhs.title == rhs.title
    }
}

public extension ImageClipRatio {
    static let custom = ImageClipRatio(title: "custom", whRatio: 0)
    
    static let circle = ImageClipRatio(title: "circle", whRatio: 1, isCircle: true)
    
    static let wh1x1 = ImageClipRatio(title: "1:1", whRatio: 1)
    
    static let wh4x5 = ImageClipRatio(title: "4:5", whRatio: 4.0 / 5.0)
    
    static let wh5x4 = ImageClipRatio(title: "5:4", whRatio: 5.0 / 4.0)
    
    static let wh9x16 = ImageClipRatio(title: "9:16", whRatio: 9.0 / 16.0)
    
    static let wh16x9 = ImageClipRatio(title: "16:9", whRatio: 16.0 / 9.0)
}
