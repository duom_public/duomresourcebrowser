//
//  ImageSticerView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/10.
//

import Foundation
import DuomBase
import Kingfisher

protocol StickerViewDelegate: NSObject {
    func stickerBeginOperation()
    
    func stickerEndOperation()
    
    func deleteItemOperation(index: Int?, stickerView: ImageStickerView)
    
    func updateStickerItemInfo(index: Int?, stickerView: ImageStickerView, center: CGPoint?, agle: CGFloat?, scale: CGFloat?)
}

class ImageStickerView: UIView  {
    private static let minScale: CGFloat = 60 / 140
    
    private static let maxScale: CGFloat = 375 / 140
    
    private lazy var tapGes: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapAction(_:)))
        return tap
    }()
    
    private lazy var pinchGes: UIPinchGestureRecognizer = {
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchAction(_:)))
        pinch.delegate = self
        return pinch
    }()
    
    lazy var panGes: UIPanGestureRecognizer = {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.panAction(_:)))
        pan.delegate = self
        return pan
    }()
    
    private lazy var rotationGes: UIRotationGestureRecognizer = {
        let rotationGes = UIRotationGestureRecognizer(target: self, action: #selector(self.rotationAction(_:)))
        rotationGes.delegate = self
        return rotationGes
    }()
    
    weak var delegate: StickerViewDelegate?
    
    /// 当前view上贴纸数据
    var stickerItems: [ImageStickerEditItemView] = []
    
    /// 正在编辑的贴纸
    var currentItemView: ImageStickerEditItemView?
    
    var currentIndex: Int?
    
    private(set) var onEditing: Bool = false {
        didSet {
            onEditing ? delegate?.stickerBeginOperation() : delegate?.stickerEndOperation()
        }
    }
        
    convenience init(frame: CGRect, parent: StickerViewDelegate?) {
        self.init(frame: frame)
        self.delegate = parent
        
        isUserInteractionEnabled = true
        addGestureRecognizer(tapGes)
        addGestureRecognizer(pinchGes)
        addGestureRecognizer(panGes)
        addGestureRecognizer(rotationGes)

        tapGes.require(toFail: panGes)
        
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
         Logger.log(message: "ImageStickerView deinit", level: .info)
    }
    
    private func setupUI()  {}
    
    func applyStickerItemOnView(_ image: UIImage?, imageURL: URL?)  {
        func applyStickerAfterGetImageSize(_ imgSize: CGSize) {
            let stickerItem = ImageStickerEditItemView(frame: CGRect(x: bounds.size.width - imgSize.width - 28, y: (bounds.height * 0.28), width: imgSize.width + 24, height: imgSize.height + 24), image: image, imageURL: imageURL)
            stickerItem.delegate = self
            stickerItems.forEach {
                $0.isSelected = false
            }
            stickerItem.isSelected = true
            currentItemView = stickerItem
            onEditing = true
            stickerItems.append(stickerItem)
            panGes.isEnabled = true
            addSubview(stickerItem)
            
            setCurrentIndex()
            
            delegate?.updateStickerItemInfo(index: self.currentIndex, stickerView: self, center: currentItemView?.center, agle: currentItemView?.gesRotation, scale: currentItemView?.gesScale)
        }
        
        getImageSize(image, imageURL: imageURL) { imgSize in
            applyStickerAfterGetImageSize(imgSize)
        }
       
    }
    
    func applyStickerInfoIfExist(_ image: UIImage?, imageURL: URL?, photoModel: PhotoModel? = nil, stickerInfo: EditImageStickerModel? = nil) {
        guard let photoModel = photoModel, let _ = photoModel.editId, let editModel = photoModel.editImageModel else {
            return
        }
        
        func applyStickerInfoIfExistAfterImageSize(_ imgSize: CGSize) {
            let stickerItem = ImageStickerEditItemView(frame: CGRect(x: 0, y: 0, width: imgSize.width + 24, height: imgSize.height + 24), image: image, imageURL: imageURL)
            stickerItem.delegate = self
            stickerItems.forEach {
                $0.isSelected = false
            }
            currentItemView = stickerItem
            stickerItems.append(stickerItem)
            panGes.isEnabled = false
            addSubview(stickerItem)
            
            setCurrentIndex()
            
            if let stickerInfo = stickerInfo {
                if let center = stickerInfo.center {
                    currentItemView?.center = center
                }
                
                if let angle = stickerInfo.angle {
                    currentItemView?.gesRotation = angle
                }
                
                if let scale = stickerInfo.scale {
                    currentItemView?.gesScale = scale
                }
                
                updateTransform()
            }
            
            currentItemView?.isSelected = false
        }
        
        getImageSize(image, imageURL: imageURL) { imgSize in
            applyStickerInfoIfExistAfterImageSize(imgSize)
        }
    }
    
    
    func removeCurrentItem()  {
        if let idx = stickerItems.firstIndex(where: { $0 === currentItemView }) {
            stickerItems.remove(at: idx)
        }
    }
    
    func setCurrentIndex()  {
        guard let currentItemView = currentItemView else {
            currentIndex = nil
            return
        }
        if let idx = stickerItems.firstIndex(where: { $0 === currentItemView }) {
            currentIndex = idx
        }
    }

    func getImageSize(_ image: UIImage?, imageURL: URL?, completion: @escaping ((CGSize) -> Void)) {
        var buildImg: UIImage?
        var buildSize: CGSize = .zero
        func getSize(_ image: UIImage?) -> CGSize {
            guard let image = image else {
                return CGSize(width: 140, height: 140)
            }
            let W = image.width
            let H = image.height
            if W / H > 1 {
                return CGSize(width: 140, height: 140 * H / W)
            } else {
                return CGSize(width: 140 * W / H , height: 140)
            }
        }

        buildImg = image
        
        if let imageURL = imageURL {
            let tempImgView = UIImageView()
            tempImgView.kf.setImage(with: imageURL) { res in
                completion(getSize(tempImgView.image))
            }
        } else {
            completion(getSize(buildImg))
        }
    }
}

// MARK: private fun
extension ImageStickerView {
    @objc private func tapAction(_ ges: UITapGestureRecognizer)  {
        var isFoucsOn: Bool = false
        stickerItems.forEach {
            if $0.layer.contains(ges.location(in: $0)) {
                deselectedAllStickerItems()
                currentItemView = $0
                currentItemView?.isSelected = true
                onEditing = true
                isFoucsOn = true
                return
            }
        }
        
        panGes.isEnabled = isFoucsOn
        if !isFoucsOn {
            deselectedAllStickerItems()
            onEditing = false
        }
        
        setCurrentIndex()
    }
    
    func deselectedAllStickerItems()  {
        stickerItems.forEach {
            $0.isSelected = false
        }
    }

    @objc private func pinchAction(_ ges: UIPinchGestureRecognizer) {
        guard onEditing, let currentItemView = currentItemView else { return }
        currentItemView.gesScale *= ges.scale
        currentItemView.gesScale = min(max(currentItemView.gesScale, ImageStickerView.minScale), ImageStickerView.maxScale)
        delegate?.updateStickerItemInfo(index: self.currentIndex, stickerView: self, center: currentItemView.center, agle: currentItemView.gesRotation, scale: currentItemView.gesScale)
        ges.scale = 1

        switch ges.state {
        case .began, .changed:
            updateTransform()
        default:
            break
        }
    }

    @objc private func panAction(_ ges: UIPanGestureRecognizer) {
        guard onEditing, let currentItemView = currentItemView else { return }
        let point = ges.translation(in: self)
        
        let boundedTranslation = translateWithinBounds(point, bounds: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        
        currentItemView.center = CGPoint(x: currentItemView.center.x + boundedTranslation.x, y: currentItemView.center.y + boundedTranslation.y)
        delegate?.updateStickerItemInfo(index: self.currentIndex, stickerView: self, center: currentItemView.center, agle: currentItemView.gesRotation, scale: currentItemView.gesScale)
        ges.setTranslation(.zero, in: self)
    }

    @objc private func rotationAction(_ ges: UIRotationGestureRecognizer) {
        guard onEditing, let currentItemView = currentItemView else { return }
        currentItemView.gesRotation += ges.rotation
        delegate?.updateStickerItemInfo(index: self.currentIndex, stickerView: self, center: currentItemView.center, agle: currentItemView.gesRotation, scale: currentItemView.gesScale)
        ges.rotation = 0

        switch ges.state {
        case .began, .changed:
            updateTransform()
        default:
            break
        }
    }
    
    private func updateTransform()  {
        guard let currentItemView = currentItemView else { return }
        var transform = currentItemView.originTransform
        transform = transform.scaledBy(x: currentItemView.gesScale, y: currentItemView.gesScale)
        transform = transform.rotated(by: currentItemView.gesRotation)
        currentItemView.transform = transform
        currentItemView.deleteButton.transform = CGAffineTransform(scaleX: 1 / currentItemView.gesScale, y: 1 / currentItemView.gesScale)
//        Logger.log(message: "currentItemView Frame: \(currentItemView.frame)", level: .info)
    }
    
    private func translateWithinBounds(_ translation: CGPoint, bounds: CGRect) -> CGPoint {
      guard let currentItemView = currentItemView else { return  translation }
      var boundedX = currentItemView.center.x + translation.x
      boundedX = min(max(boundedX, bounds.minX), bounds.maxX)
      
      var boundedY = currentItemView.center.y + translation.y
      boundedY = min(max(boundedY, bounds.minY), bounds.maxY)
      return CGPoint(x: boundedX - currentItemView.center.x, y: boundedY - currentItemView.center.y)
    }
}


// MARK: ImageStickerEditItemViewDelegate
extension ImageStickerView: ImageStickerEditItemViewDelegate {
    func stickerItemBeginEdit(itemView: ImageStickerEditItemView) {
        onEditing = true
    }
    
    func stickerItemEndEdit(itemView: ImageStickerEditItemView) {
        onEditing = false
    }
    
    func delete(itemView: ImageStickerEditItemView) {
        itemView.removeFromSuperview()
        if let idx = self.stickerItems.firstIndex(where: { $0 === itemView }) {
            self.stickerItems.remove(at: idx)
        }
        delegate?.deleteItemOperation(index: currentIndex, stickerView: self)

        onEditing = false
        currentItemView = nil
        currentIndex = nil
    }
}

// MARK: UIGestureRecognizerDelegate
extension ImageStickerView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

