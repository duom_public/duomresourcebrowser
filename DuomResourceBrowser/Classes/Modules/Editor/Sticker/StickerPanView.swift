//
//  StickerPanView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/9.
//

import Foundation
import DuomBase
import RxSwift
import RxCocoa
import Kingfisher

class StickerPanView: UIView, ImageStickerContainerDelegate {
    static let containerViewH: CGFloat = 299.DScale
    
    private var containerView: UIView!
    
    private let bag = DisposeBag()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.screenWidth, height: 254.DScale)
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .clear
        view.register(StickerPanCell.self, forCellWithReuseIdentifier: StickerPanCell.identifier)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.scrollsToTop = false
        view.dataSource = self
        view.delegate = self
        view.isPagingEnabled = true
        view.contentInsetAdjustmentBehavior = .never
        return view
    }()
    
    private let segmentContainerView = UIView()
    
    private let segmentScrollView = UIScrollView().then {
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
    }
    
    private let closeButton: UIButton = UIButton().then {
        $0.setEnlargeEdge(top: 10, bottom: 10, left: 20, right: 20)
        $0.setImage(UIImage(named: "edit_close"), for: .normal)
    }
    
    private let lineView = UIView().then {
        $0.backgroundColor = UIColor.white.withAlphaComponent(0.3)
    }
    
    var dataSource: [StickerInfo] = AccountManager.shared.getStickersInfo()
    
    private var segmentButtons: [StickerSegmentButton] = []
    
    private let selectedSegment = BehaviorRelay(value: -1)
    
    var selectImageBlock: ((UIImage?, String?) -> Void)?
    
    // 点击关闭按钮回调
    var closeBlock: (() -> Void)?
    // 选择好图片后的回调
    var hideBlock: (() -> Void)?
    
    // 啥也没选，手势回调
    var tapGesBlock: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUIView()
        setupSegmentButtons()
        
        selectedSegment.subscribe(onNext: { [unowned self] section in
            if section >= 0, section < self.segmentButtons.count {
                self.segmentButtons.forEach { $0.isSelected = false }
                self.segmentButtons[section].isSelected = true
            }
        })
        .disposed(by: bag)
        
        NotificationCenter.default.rx
            .notification(PhotosConfiguration.default().editImageConfiguration.refreshStickerNotification)
            .take(until: self.rx.deallocated)
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.dataSource = AccountManager.shared.getStickersInfo()
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            })
            .disposed(by: bag)
        
        let tapGes = UITapGestureRecognizer.init(target: self, action: #selector(tapGesture(_:)))
        tapGes.delegate = self
        addGestureRecognizer(tapGes)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUIView()
    }
    
    private func setupUIView()  {
        backgroundColor = UIColor.clear
        containerView = UIView()
        containerView.backgroundColor = UIColor(6, 5, 6).withAlphaComponent(0.6)
        addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(self.snp.bottom).offset(StickerPanView.containerViewH)
            make.height.equalTo(StickerPanView.containerViewH)
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        containerView.addSubview(collectionView)
        containerView.addSubview(segmentContainerView)
        segmentContainerView.addSubview(segmentScrollView)
        segmentContainerView.addSubview(closeButton)
        segmentContainerView.addSubview(lineView)
        
        segmentContainerView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(45)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(segmentContainerView.snp.bottom)
        }
        
        closeButton.snp.makeConstraints { make in
            make.trailing.equalTo(-16)
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
        
        lineView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.size.equalTo(CGSize(width: UIScreen.screenWidth, height: 0.5))
        }
        
        segmentScrollView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(16)
            make.trailing.equalTo(closeButton.snp.leading).offset(-15)
            make.height.equalTo(36.DScale)
        }
        
        closeButton.addTarget(self, action: #selector(self.tapAction), for: .touchUpInside)
    }
    
    @objc func tapGesture(_ sender: UITapGestureRecognizer)  {
        tapGesBlock?()
        hide()
    }
    
    @objc private func tapAction()  {
        closeBlock?()
        hide()
    }
    
    private func selectedDone()  {
        hideBlock?()
        hide()
    }
    
    func hide()  {
        let transY = StickerPanView.containerViewH
        UIView.animate(withDuration: 0.25) {
            self.containerView.transform = self.containerView.transform.translatedBy(x: 0, y: transY)
        } completion: { _ in
            self.isHidden = true
            self.containerView.transform = .identity
        }
    }
    
    func show(in view: UIView) {
        if superview !== view {
            removeFromSuperview()
            view.addSubview(self)
        }
        
        self.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(UIScreen.screenHeight)
        }
        view.layoutIfNeeded()
        
        self.isHidden = false
        let transY = -(StickerPanView.containerViewH)
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25) {
                self.containerView.transform = self.containerView.transform.translatedBy(x: 0, y: transY)
            }
        }
    }
    
    private func setupSegmentButtons()  {
        segmentScrollView.subviews.forEach { $0.removeFromSuperview() }
        segmentButtons.forEach { $0.removeFromSuperview() }
        segmentButtons = []
        
        let w: CGFloat = 36.DScale
        let h: CGFloat = 36.DScale
        
        segmentScrollView.contentSize = CGSize(width: w * CGFloat(dataSource.count) + CGFloat(16 * dataSource.count - 1), height: h)
        var tempBtn: StickerSegmentButton!
        for (idx, item) in dataSource.enumerated() {
            tempBtn = StickerSegmentButton(frame: CGRect(x: w * CGFloat(idx) + CGFloat(16 * idx), y: 0, width: w, height: h))
            if let imgStr = self.dataSource[idx].icon, let imgURL = URL(string: imgStr) {
                tempBtn.kf.setImage(with: imgURL, for: .normal)
                tempBtn.kf.setImage(with: imgURL, for: .highlighted)
            }
            segmentScrollView.addSubview(tempBtn)
            segmentButtons.append(tempBtn)
            tempBtn.rx
                .tap
                .subscribe(onNext: { [weak self] in
                    guard let self = self else { return }
                    self.collectionView.setContentOffset(CGPoint(x: idx * Int(self.collectionView.bounds.width), y: 0), animated: false)
                    self.selectedSegment.accept(idx)
                })
                .disposed(by: bag)
        }
        
        selectedSegment.accept(0)
    }
    
    private func pageIndexOfSection(_ section: Int) -> Int {
        let pageIndex = (0 ..< section).map {
            let count = CGFloat(self.dataSource[$0].image.count) / CGFloat(PhotosConfiguration.default().editImageConfiguration.stickerSectionItmes)
            let pageCount = Int(ceil(count))
            return max(1, pageCount)
        }.reduce(0, +)
        return Int(pageIndex)
    }
}

extension StickerPanView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StickerPanCell.identifier, for: indexPath) as? StickerPanCell else {
            return UICollectionViewCell()
        }
        
        cell.updateCell(dataSource[indexPath.section])
        cell.selectImageBlock = { [weak self] (img, imgStr) in
            guard let self = self else { return }
            self.selectedDone()
            self.selectImageBlock?(img, imgStr)
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
}

extension StickerPanView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.width)
        selectedSegment.accept(page)
        
        if let button = segmentButtons.first(where: { $0.isSelected }) {
            let minX = button.frame.minX + 2
            let maxX = button.frame.maxX - 2
            
            if minX < segmentScrollView.contentOffset.x {
                segmentScrollView.setContentOffset(CGPoint(x: minX, y: 0), animated: true)
            } else if maxX > segmentScrollView.contentOffset.x + segmentScrollView.frame.width {
                segmentScrollView.setContentOffset(CGPoint(x: maxX - segmentScrollView.frame.width, y: 0), animated: true)
            }
        }
    }
}

extension StickerPanView: UIGestureRecognizerDelegate {
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let point = gestureRecognizer.location(in: self)
        return !containerView.frame.contains(point)
    }
}


// MARK: StickerSegmentButton
class StickerSegmentButton: UIButton {
    private let imgView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setup()  {
        addSubview(imgView)
        imgView.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets(hAxis: 4, vAxis: 4))
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = isSelected ? UIColor(43, 43, 43) : .clear
        roundCorners(radius: 5)
    }
}
