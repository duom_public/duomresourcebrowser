//
//  ImageSticerItemView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/2/27.
//

import Foundation
import UIKit
import DuomBase

protocol ImageStickerEditItemViewDelegate: AnyObject {
    func stickerItemBeginEdit(itemView: ImageStickerEditItemView)
    
    func stickerItemEndEdit(itemView: ImageStickerEditItemView)
    
    func delete(itemView: ImageStickerEditItemView)
}

class ImageStickerEditItemView: UIView {
    private static let edgeInsetMargin: CGFloat = 2
    
    private static let borderWidth = 1
    
    private var image: UIImage?
    
    private var imageURL: URL?
    
    weak var delegate: ImageStickerEditItemViewDelegate?
    
    private let imageContainerView = UIView()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        if let image = self.image {
            view.image = image
        }
        if let imageURL = self.imageURL {
          view.kf.setImage(with: imageURL)
        }
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.isUserInteractionEnabled = false
        return view
    }()
    
    let deleteButton: UIButton = UIButton().then {
        $0.setEnlargeEdge(UIEdgeInsets(hAxis: 30, vAxis: 30))
        $0.setImage(UIImage(named: "edit_delete"), for: .normal)
    }
    
    var originTransform: CGAffineTransform = .identity
    
    var gesTranslationPoint: CGPoint = .zero
    
    var gesRotation: CGFloat = 0
    
    var gesScale: CGFloat = 1
    
    var transXY: (CGFloat, CGFloat) = (0.0, 0.0)
    
    var isSelected: Bool = false {
        didSet {
            isSelected ? showBorder() : hideBorder()
        }
    }
    
    convenience init(frame: CGRect, image: UIImage?, imageURL: URL?) {
        self.init(frame: frame)
        self.image = image
        self.imageURL = imageURL
        
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI()  {
        addSubview(imageContainerView)
        imageContainerView.addSubview(imageView)
        
        imageContainerView.frame = bounds.insetBy(dx: 12, dy: 12)
        imageView.frame = imageContainerView.bounds.insetBy(dx: ImageStickerEditItemView.edgeInsetMargin, dy: ImageStickerEditItemView.edgeInsetMargin)
        
        addSubview(deleteButton)
        deleteButton.snp.makeConstraints { make in
            make.leading.equalTo(imageContainerView.snp.leading).offset(-12)
            make.top.equalTo(imageContainerView.snp.top).offset(-12)
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
        
        deleteButton.addTarget(self, action: #selector(self.deleteAction(_:)), for: .touchUpInside)
    }
    
}

// MARK: fun private
extension ImageStickerEditItemView {
    @objc private func deleteAction(_ sender: UIButton) {
        delegate?.delete(itemView: self)
    }
    
    private func beginEdit()  {
        showBorder()
        delegate?.stickerItemBeginEdit(itemView: self)
    }
    
    private func endEdit()  {
        hideBorder()
        delegate?.stickerItemEndEdit(itemView: self)
    }
}

// MARK: fun public
extension ImageStickerEditItemView {
    public func hideBorder()  {
        imageContainerView.layer.borderColor = UIColor.clear.cgColor
        deleteButton.isHidden = true
    }
    
    public func showBorder()  {
        imageContainerView.layer.borderColor = UIColor.white.cgColor
        imageContainerView.layer.borderWidth = 1
        deleteButton.isHidden = false
    }
}
