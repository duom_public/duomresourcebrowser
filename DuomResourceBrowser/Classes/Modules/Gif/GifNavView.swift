//
//  GifNavView.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/6/14.
//

import Foundation
import DuomBase

class GifNavView: BaseView {
    var backBlock: (() -> Void)?
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "nav_close_icon"), for: .normal)
        button.setImage(UIImage(named: "nav_close_icon"), for: .highlighted)
        button.addTarget(self, action: #selector(self.clickBackAction(_:)) , for: .touchUpInside)
        button.setEnlargeEdge(.all(15))
        return button
    }()
    
    override func setupUI() {
        backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        clipsToBounds = false
        setupShadow(opacity: 1, radius: 11, offset: CGSize(width: 0, height: 4), color: UIColor.black.withAlphaComponent(0.25))
        
        addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.leading.equalTo(16)
            make.bottom.equalTo(-10)
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
    }
    
    @objc private func clickBackAction(_ sender: UIButton)  {
        backBlock?()
        UIApplication.topViewController()?.dismiss(animated: true)
    }
}

