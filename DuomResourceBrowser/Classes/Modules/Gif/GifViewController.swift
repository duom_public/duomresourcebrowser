//
//  GifViewController.swift
//  DuomBase
//
//  Created by kuroky on 2023/6/13.
//

import Foundation
import DuomBase
import UIKit
import GiphyUISDK
import RxSwift
import RxCocoa
import Photos
import MBProgressHUD

extension GifViewController: DuomPagingContainerItem {}
class GifViewController: BaseViewController {
    enum TextFieldState {
        case normal  // 初始状态
        case willEdit  // 即将开始编辑
        case editing   // 正在编辑
    }
    
    lazy var giphy: GiphyGridController = {
        let giphy = GiphyGridController()
        giphy.view.backgroundColor = UIColor(241, 241, 241)
        giphy.cellPadding = 5.0
        giphy.direction = .vertical
        giphy.numberOfTracks = 3
        giphy.delegate = self
        giphy.fixedSizeCells = true
        let trendingGifs = GPHContent.trending(mediaType: .gif)
//        let recents = GPHContent.recents
//        var contents = [trendingGifs]
//        if GPHRecents.count > 0 {
//            contents.append(recents)
//        }
//        giphy.content =
        giphy.content = trendingGifs
        giphy.update()
        GPHCache.shared.cache.diskCapacity = 500 * 1000 * 1000
        GPHCache.shared.cache.memoryCapacity = 500 * 1000 * 1000
        return giphy
    }()
    
    private lazy var navView: GifNavView = {
        let nav = GifNavView()
        return nav
    }()
    
    private let containerView = UIView().then {
        $0.backgroundColor = UIColor(241, 241, 241)
    }
    
    private let textContainerView: UIView = UIView().then {
        $0.backgroundColor = .white
        $0.roundCorners(radius: 10)
    }
    
    private let gifTagView: UIImageView = UIImageView().then {
        $0.image = UIImage(named: "gif_tag")
    }
    
    private lazy var textField: UITextField = {
        let txf = UITextField()
        txf.backgroundColor = .white
        txf.placeholder = " " + "Search".localized
        txf.textColor = UIColor(6, 5, 6)
        txf.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        txf.textAlignment = .left
        txf.returnKeyType = .search
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 48))
        leftView.isUserInteractionEnabled = false
        let leftImgView = UIImageView(image: UIImage(named: "gif_search_icon"))
        leftImgView.frame = CGRect(x: 8, y: 12, width: 24, height: 24)
        leftView.addSubview(leftImgView)
        txf.leftView = leftView
        txf.leftViewMode = .unlessEditing
        return txf
    }()
    
    private lazy var clearButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "gif_close"), for: .normal)
        btn.setImage(UIImage(named: "gif_close"), for: .highlighted)
        btn.setEnlargeEdge(UIEdgeInsets(hAxis: 12, vAxis: 12))
        btn.isHidden = true
        return btn
    }()
    
    private lazy var cancelButton: UIButton = {
        let btn = UIButton()
        btn.setTitle(CopyWitterInstance.default().CancelText, for: .normal)
        btn.setTitle(CopyWitterInstance.default().CancelText, for: .highlighted)
        btn.titleLabel?.font = .appFont(ofSize: 16, fontType: .TT_Regular)
        btn.setTitleColor(UIColor(6, 5, 6), for: .normal)
        btn.setTitleColor(UIColor(6, 5, 6), for: .highlighted)
        btn.setEnlargeEdge(UIEdgeInsets(hAxis: 12, vAxis: 12))
        btn.isHidden = true
        return btn
    }()
    
    private lazy var scrollView: UIScrollView = UIScrollView().then {
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.contentInsetAdjustmentBehavior = .never
    }
    
    private var editState: TextFieldState = .normal
    
    private var searchTexts: [String] {
        if GPHRecents.count > 0 {
            return CopyWitterInstance.default().gifSearchTextsWithRecents
        } else {
            return CopyWitterInstance.default().gifSearchTexts
        }
    }
    
    // 每个item起点位置
    private(set) var textItemOrigins: [Double] = []
    
    var isShowing: Bool = true {
        didSet {
            if !isShowing {
                DispatchQueue.main.async {
                    HUD.dismiss(immediately: true)
                }
            }
        }
    }
    
    // 添加资源
    var isAddScence: Bool = false
    
    var appendItemByGiphyBlock: ((PhotoModel)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.showImage()
        Giphy.configure(apiKey: PhotosConfiguration.default().GiphyKey)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    deinit {
        HUD.dismiss(immediately: true)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        view.addSubview(navView)
        view.addSubview(containerView)
        containerView.addSubview(textContainerView)
        containerView.addSubview(gifTagView)
        textContainerView.addSubview(textField)
        textContainerView.addSubview(clearButton)
        containerView.addSubview(cancelButton)
        containerView.addSubview(scrollView)
        
        addChild(giphy)
        view.addSubview(giphy.view)
        giphy.didMove(toParent: self)
        view.bringSubviewToFront(navView)
        
        navView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(PhotosConfiguration.default().navHeight)
        }
        
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(100)
            make.top.equalTo(navView.snp.bottom)
        }
        
        textContainerView.snp.makeConstraints { make in
            make.leading.equalTo(8)
            make.top.equalTo(16)
            make.bottom.equalTo(-36)
            make.width.equalTo(108)
        }
        
        textField.snp.makeConstraints { make in
            make.leading.equalTo(8)
            make.top.bottom.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        
        clearButton.snp.makeConstraints { make in
            make.trailing.equalTo(-15)
            make.size.equalTo(CGSize(width: 18, height: 18))
            make.centerY.equalToSuperview()
        }
        
        cancelButton.snp.makeConstraints { make in
            make.leading.equalTo(textField.snp.trailing).offset(10)
            make.size.equalTo(CGSize(width: 55, height: 19).DScale)
            make.centerY.equalTo(textField.snp.centerY)
        }
        
        gifTagView.snp.makeConstraints { make in
            make.top.equalTo(textContainerView.snp.bottom).offset(16)
            make.bottom.equalTo(-8)
            make.leading.equalTo(16)
            make.width.equalTo(116)
        }
        
        giphy.view.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom)
            make.leading.equalTo(5)
            make.trailing.equalTo(-5)
            make.bottom.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints { make in
            make.leading.equalTo(textContainerView.snp.trailing)
            make.centerY.equalTo(textContainerView.snp.centerY)
            make.trailing.equalToSuperview()
            make.height.equalTo(textContainerView.snp.height)
        }
        cancluteScrollViewWidth()
        
        navView.backBlock = { [weak self] in
            self?.textField.resignFirstResponder()
        }
        
        textField.rx.text.orEmpty
            .skip(1)
            .subscribe(onNext: { [weak self] text in
                guard let self = self else { return }
                self.clearButton.isHidden = !text.isNotEmpty
                if text.isNotEmpty {
                    self.editState = .editing
                    self.updateViewsState()
                } else {
                    self.editState = .willEdit
                    self.updateViewsState()
                }
            })
            .disposed(by: disposeBag)
        
        textField.rx.controlEvent(.editingDidBegin)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.editState = .willEdit
                self.updateViewsState()
            })
            .disposed(by: disposeBag)
        
        textField.rx.controlEvent(.editingDidEndOnExit)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                if let text = self.textField.text, text.isNotEmpty {
                    self.searchContent(text)
                }
            })
            .disposed(by: disposeBag)
        
        cancelButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.textField.text = ""
                self.textField.resignFirstResponder()
                self.editState = .normal
                self.updateViewsState()
            })
            .disposed(by: disposeBag)
        
        clearButton.rx.tap
            .throttle(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.textField.text = ""
                self.editState = .willEdit
                self.updateViewsState()
            })
            .disposed(by: disposeBag)
    }
    
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        textField.resignFirstResponder()
    //    }
    
    private func cancluteScrollViewWidth() {
        var contentWidth: CGFloat = 8
        for (idx, item) in searchTexts.enumerated() {
            textItemOrigins.append(contentWidth)
            let textWidth = item.boundingRect(font: .appFont(ofSize: 18, fontType: .TT_Regular), maxSize: CGSize(width: CGFloat.infinity, height: 22)).width
            let itemWidth = textWidth + 34
            contentWidth += itemWidth + 8
            createButtonsOnScrollView(item, width: itemWidth, index: idx)
        }
        scrollView.contentSize = CGSize(width: contentWidth, height: 48)
    }
    
    private func createButtonsOnScrollView(_ text: String, width: Double, index: Int)  {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.roundCorners(radius: 10)
        button.setTitle(text, for: .normal)
        button.setTitle(text, for: .selected)
        button.setTitle(text, for: .highlighted)
        button.titleLabel?.font = .appFont(ofSize: 18, fontType: .TT_Regular)
        button.setTitleColor(UIColor.black, for: .normal)
        button.roundCorners(radius: 10)
        button.tag = index
        scrollView.addSubview(button)
        button.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.width.equalTo(width)
            make.height.equalTo(48)
            make.leading.equalTo(textItemOrigins[index])
        }
        button.addTarget(self, action: #selector(selectTextItemAction(_:)), for: .touchUpInside)
    }
    
    private func updateViewsState()  {
        switch editState {
        case .normal:
            self.textContainerView.snp.updateConstraints { make in
                make.width.equalTo(108)
            }
            self.textContainerView.layoutIfNeeded()
            self.clearButton.isHidden = true
            self.cancelButton.isHidden = true
            self.scrollView.isHidden = false
        case .willEdit:
            self.textContainerView.snp.updateConstraints { make in
                make.width.equalTo(284.DScale)
            }
            self.textContainerView.layoutIfNeeded()
            self.cancelButton.isHidden = false
            self.clearButton.isHidden = true
            self.scrollView.isHidden = true
        case .editing:
            self.textContainerView.snp.updateConstraints { make in
                make.width.equalTo(343.DScale)
            }
            self.textContainerView.layoutIfNeeded()
            self.clearButton.isHidden = false
            self.cancelButton.isHidden = true
            self.scrollView.isHidden = true
        }
    }
    
    @objc private func selectTextItemAction(_ sender: UIButton)  {
        guard sender.tag < searchTexts.count else {
            return
        }
        let currentText = searchTexts[sender.tag]
        
        if currentText == CopyWitterInstance.default().gifRecentsText {
            giphy.content = GPHContent.recents
            giphy.update()
        } else {
            textField.becomeFirstResponder()
            textField.text = currentText
            editState = .editing
            updateViewsState()
        }
    }
    
    private func searchContent(_ content: String)  {
        guard content != "" else {
            return
        }
        let search = GPHContent.search(withQuery: content, mediaType: .gif, language: .english)
        giphy.content = search
        giphy.update()
    }
}

extension GifViewController: GPHGridDelegate {
    func contentDidUpdate(resultCount: Int, error: Error?) {
        HUD.dismiss()
    }
    
    func didSelectMedia(media: GiphyUISDK.GPHMedia, cell: UICollectionViewCell) {
        MobStatistics.trackAction("ImagePicker_gif_next_click")
        let tempModel = PhotoModel.init(asset: nil)
        tempModel.mediaType = .giphyGif
        tempModel.ident = media.id
        if let contentURL = media.url(rendition: .original, fileType: .gif) {
            tempModel.fileURL = URL(string: contentURL)
        }
        
        if let fixed_Json = media.images?.fixedWidth {
            var gifModel = OutputModel()
            if let gifURL = fixed_Json.gifUrl {
                gifModel.imagePath = gifURL
            }
            gifModel.id = fixed_Json.mediaId
            gifModel.height = fixed_Json.height
            gifModel.width = fixed_Json.width
            tempModel.gifThumb = gifModel
        }
        
        if let origin = media.images?.fixedWidth {
            tempModel.width = origin.width
            tempModel.height = origin.height
        }
        
        if isAddScence {
            self.appendItemByGiphyBlock?(tempModel)
            UIApplication.topViewController()?.dismiss(animated: true)
        } else {
            SourceInstance.shared.selectModels.append(tempModel)
            UIApplication.topViewController()?.dismiss(animated: true, completion: {
                SourceInstance.shared.selectedSourceBlock?(SourceInstance.shared.selectModels)
            })
        }
    }
    
    func didSelectMoreByYou(query: String) {
        print("didSelectMoreByYou delegate: \(query)")
    }
    
    func didScroll(offset: CGFloat) {}
    
    func errorDidOccur(_ error: Error) {
        Logger.log(message: "GiphyUISDK error: \(error)", level: .info)
    }
}

