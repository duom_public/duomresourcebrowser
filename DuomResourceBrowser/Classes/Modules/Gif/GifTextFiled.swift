//
//  GifTextFiled.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2023/6/14.
//

import Foundation

class GifTextFiled: UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.minX + 48, y: bounds.minY, width: bounds.width, height: bounds.height)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.minX + 48, y: bounds.minY, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.minX + 48, y: bounds.minY, width: bounds.width, height: bounds.height)
    }
}
