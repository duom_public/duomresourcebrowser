//
//  CameraBaseViewController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/12/6.
//

import UIKit
import DuomBase
import AVFoundation
import RxCocoa
import RxSwift

open class CameraBaseViewController: UIViewController {
    let session = AVCaptureSession()
    
    var videoInput: AVCaptureDeviceInput?
    
    var imageOutput: AVCapturePhotoOutput!
    
    var movieFileOutput: AVCaptureMovieFileOutput!
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var orientation: AVCaptureVideoOrientation = .portrait
    
    var sessionQueue = DispatchQueue(label: "hk.cameraBase.sessionQueue")
    
    var microPhontIsAvailable = true
    
    var sessionIsReady: Bool = false
    
    let disposeBag = DisposeBag()
    
    let reverseButton: UIButton = UIButton().then {
        $0.setImage(UIImage(named: "camera_reverse"), for: .normal)
        $0.setImage(UIImage(named: "camera_reverse"), for: .selected)
        $0.setEnlargeEdge(UIEdgeInsets(hAxis: 10.0, vAxis: 10.0))
    }
    
    var statusBarStyle = UIStatusBarStyle.lightContent {
      didSet {
        setNeedsStatusBarAppearanceUpdate()
      }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            return
        }

//        setupCamera()
//        sessionQueue.async {
//            self.session.startRunning()
//        }

        setUpUI()

        addNotification()
    }
    
    func setUpUI() {
        view.addSubview(reverseButton)
        reverseButton.snp.makeConstraints { make in
            make.bottom.equalTo(-74)
            make.trailing.equalTo(-16)
            make.size.equalTo(CGSize(width: 48, height: 48))
        }
        reverseButton.addTarget(self, action: #selector(switchCameraAction(_:)), for: .touchUpInside)
    }
    
    
    // 仅支持竖屏
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func addNotification()  {
        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    deinit {
        Logger.log(message: "camera VC deinit", level: .info)
        session.stopRunning()
    }
}

// MARK: Private Method
extension CameraBaseViewController {
//    func setupCamera()  {
//        guard let backCamera = getCamera(position: .back) else { return }
//        guard let input = try? AVCaptureDeviceInput(device: backCamera) else { return }
//        
//        session.beginConfiguration()
//        
//        //        let captureConnection = movieFileOutput.connection(with: .video)
//        //        if backCamera.activeFormat.isVideoStabilizationModeSupported(.cinematic) {
//        //            captureConnection?.preferredVideoStabilizationMode = .cinematic
//        //        }
//                
////        do {
////            try backCamera.lockForConfiguration()
////            if backCamera.isFocusPointOfInterestSupported {
////                backCamera.focusPointOfInterest = view.center
////            }
////
////            if backCamera.isFocusModeSupported(.autoFocus) {
////                backCamera.focusMode = .continuousAutoFocus
////            }
////
////            if backCamera.isExposurePointOfInterestSupported {
////                backCamera.exposurePointOfInterest = view.center
////            }
////
////            if backCamera.isExposureModeSupported(.autoExpose) {
////                backCamera.exposureMode = .continuousAutoExposure
////            }
////
////            backCamera.unlockForConfiguration()
////        } catch {
////             Logger.log(message: "camera 设置失败", level: .info)
////        }
//        
//        // 相机画面输出流
//        videoInput = input
//        // 照片输出流
//        imageOutput = AVCapturePhotoOutput()
//        
//        let preset = PhotosConfiguration.default().cameraConfiguration.sessionPreset.avSessionPresent
//        if session.canSetSessionPreset(preset) {
//            session.sessionPreset = preset
//        } else {
//            session.sessionPreset = .hd1280x720
//        }
//        
//        movieFileOutput = AVCaptureMovieFileOutput()
//        movieFileOutput.movieFragmentInterval = .invalid
//        
//        // 视频输入
//        if let videoInput = videoInput, session.canAddInput(videoInput) {
//            session.addInput(videoInput)
//        }
//        
//        // 添加音频输入
//        addAudioInput()
//        
//        // 输出流添加到session
//        if session.canAddOutput(imageOutput) {
//            session.addOutput(imageOutput)
//        }
//        if session.canAddOutput(movieFileOutput) {
//            session.addOutput(movieFileOutput)
//        }
//        
//        session.commitConfiguration()
//        
//        sessionIsReady = true
//    }
    
    func getCamera(position: AVCaptureDevice.Position) -> AVCaptureDevice?  {
        let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: position).devices
        for device in devices {
            if device.position == position {
                return device
            }
        }
        return nil
    }
    
    func addAudioInput() {
        // 音频输入流
        var audioInput: AVCaptureDeviceInput?
        if let microphone = getMicrophone() {
            audioInput = try? AVCaptureDeviceInput(device: microphone)
        }
        guard microPhontIsAvailable, let ai = audioInput else { return }
        removeAudioInput()
        
        if session.isRunning {
            session.beginConfiguration()
        }
        if session.canAddInput(ai) {
            session.addInput(ai)
        }
        if session.isRunning {
            session.commitConfiguration()
        }
    }
    
    private func removeAudioInput() {
        var audioInput: AVCaptureInput?
        for input in session.inputs {
            if (input as? AVCaptureDeviceInput)?.device.deviceType == .builtInMicrophone {
                audioInput = input
            }
        }
        guard let audioInput = audioInput else { return }
        
        if session.isRunning {
            session.beginConfiguration()
        }
        session.removeInput(audioInput)
        if session.isRunning {
            session.commitConfiguration()
        }
    }
    
    private func getMicrophone() -> AVCaptureDevice? {
        return AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInMicrophone], mediaType: .audio, position: .unspecified).devices.first
    }
    
    func getFadeAnimation(fromValue: CGFloat, toValue: CGFloat, duration: TimeInterval) -> CAAnimation {
        let animation = CABasicAnimation(keyPath: "opacity")
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = duration
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        return animation
    }
    
    
    @objc func switchCameraAction(_ sender: UIButton)  {
        do {
            guard let currInput = videoInput else {
                return
            }
            
            var newVideoInput: AVCaptureDeviceInput?
            if currInput.device.position == .back, let front = getCamera(position: .front) {
                newVideoInput = try AVCaptureDeviceInput(device: front)
            } else if currInput.device.position == .front, let back = getCamera(position: .back) {
                newVideoInput = try AVCaptureDeviceInput(device: back)
            } else {
                return
            }
            
            if let newVideoInput = newVideoInput {
                session.beginConfiguration()
                session.removeInput(currInput)
                if session.canAddInput(newVideoInput) {
                    session.addInput(newVideoInput)
                    videoInput = newVideoInput
                } else {
                    session.addInput(currInput)
                }
                session.commitConfiguration()
            }
        } catch {
            Logger.log(message: "切换摄像头失败～～～", level: .warning)
        }
    }
    
    @objc func appWillResignActive()  {
        if session.isRunning {
            session.stopRunning()
        }
    }
}

// MARK: Public Method
extension CameraBaseViewController {
    func stopSession()  {
        if session.isRunning {
            self.session.stopRunning()
        }
    }
    
    func startSession()  {
        if sessionIsReady {
            sessionQueue.async {
                self.session.startRunning()
            }
        }
    }
}


extension CameraBaseViewController: DuomPagingContainerItem {}


