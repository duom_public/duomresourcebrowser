//
//  BrowserMainController.swift
//  DuomResourceBrowser
//
//  Created by kuroky on 2022/11/29.
//

import Foundation
import DuomBase
import Photos

public class ImagePickerMainController: BaseViewController {
    private let navView: AlbumListNavView = {
        let v = AlbumListNavView.init(title: "")
        v.isShowShadow = true
        return v
    }()
    
    private lazy var filterListView: AlbumFilterListView = {
        let listView = AlbumFilterListView(dataSource: [AlbumListModel]())
        return listView
    }()
    
    private let segmentView: DuomSegmentView = DuomSegmentView(frame: .zero).then {
        $0.indicatorView = DuomSegmentIndicatorLineView(frame: .zero).then {
            $0.backgroundColor = PhotosConfiguration.default().segmentLineColor
            $0.lineWidth = .fixed(16)
            $0.lineAnimation = .none
            $0.bottom = 4
        }
        $0.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        $0.tapAnimation = .none
        $0.defaultSelectedIndex = 0
        $0.itemSpacing = PhotosConfiguration.default().selectSingleImg ? 16 : 0
    }
    
    private let segmentContainerView: UIView = UIView().then {
        $0.backgroundColor = .white
    }
    
    private let containerView = DuomPagingContainerView(frame: .zero).then {
        $0.defaultSelectedIndex = 0
        $0.scrollEnable = false
    }
    
    private lazy var listModel: [DuomSegmentTextModel] = {
        return createListModel()
    }()
    
    private let videoVC = VideoController()
    
    private let albumVC = AlbumAllViewController()
    
    private let cameraVC = CameraController()
    
    private lazy var pageVCContainer: [DuomPagingContainerItem] = {
        if PhotosConfiguration.default().selectSingleImg {
            return PhotosConfiguration.default().showGif ? [self.albumVC, self.cameraVC, self.gifVC] : [self.albumVC, self.cameraVC]
        } else {
            return PhotosConfiguration.default().showGif ? [self.albumVC, self.videoVC, self.cameraVC, self.gifVC] : [self.albumVC, self.videoVC, self.cameraVC]
        }
    }()
        
    private lazy var gifVC: GifViewController = {
        let gifVC = GifViewController()
        return gifVC
    }()
    
    private var bubbleView: BubbleView?
    
    /// 相册列表
    private var albumList: [AlbumListModel] = []
    
    private var currentAlbum: AlbumListModel?
    
    private(set) var segmentIndex: Int = 0
    
    private(set) var isFirstEnter: Bool = true
    
    // 是否满足展示bubble(是否有选择的图片)
    var shouldShowBubble: Bool = true
    
    // 是否有删除操作
    private(set) var hasDeletedAction: Bool = false
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // 获取相册列表数据
        getAlbumList()
        
        segmentView.delegate = self
        segmentView.dataSource = self
        
        containerView.delegate = self
        containerView.dataSource = self
        
        showBubble()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if segmentIndex == 1 {
            videoVC.startSession()
        } else if segmentIndex == 2 {
            cameraVC.startSession()
        } else if segmentIndex == 0, !isFirstEnter {
            // 二次选择后，刷新当前已选择item
            albumVC.reloadSelctedItems()
            if SourceInstance.shared.selectModels.isEmpty {
                navView.setNextButtonState(enable: false)
            }
        }
        
        isFirstEnter = false
    }
    
    public override var childForStatusBarStyle: UIViewController? {
        return pageVCContainer[segmentIndex] as? UIViewController
    }
    
    public override func setupConstraints() {
        super.setupConstraints()
        view.backgroundColor = PhotosConfiguration.default().mainBackgroundColor
        
        view.addSubview(navView)
        view.addSubview(filterListView)
        view.addSubview(segmentContainerView)
        segmentContainerView.addSubview(segmentView)
        view.addSubview(containerView)
        
        filterListView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.width.equalTo(UIScreen.screenWidth)
            make.bottom.equalTo(self.view.snp.top).offset(PhotosConfiguration.default().navHeight)
            make.height.equalTo(UIScreen.screenHeight - PhotosConfiguration.default().navHeight)
        }
        
        navView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(PhotosConfiguration.default().navHeight)
        }
        
        if PhotosConfiguration.default().selectSingleImg {
            segmentContainerView.snp.makeConstraints { make in
                make.leading.equalTo(85)
                make.trailing.equalTo(-85)
                make.height.equalTo(PhotosConfiguration.default().segmentViewHight)
                make.bottom.equalTo(self.view)
            }
        } else {
            segmentContainerView.snp.makeConstraints { make in
                make.leading.trailing.equalToSuperview()
                make.height.equalTo(PhotosConfiguration.default().segmentViewHight)
                make.bottom.equalTo(self.view)
            }
        }
        
        
        segmentView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(52)
        }
        
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(navView.snp.bottom)
            make.bottom.equalTo(segmentContainerView.snp.top)
        }
        
        view.bringSubviewToFront(segmentContainerView)
        view.bringSubviewToFront(filterListView)
        view.bringSubviewToFront(navView)
        segmentView.layoutIfNeeded()
        containerView.layoutIfNeeded()
        
        navView.selectAlbumBlock = { [weak self] in
            guard let self = self else { return }
            self.filterListView.isHidden ? self.filterListView.show() : self.filterListView.hide()
        }
        
        navView.cancelBlock = { [weak self] in
            guard let self = self else { return }
            let topNav = UIApplication.topViewController()?.navigationController as? PhotoNavgationController
            if self.hasDeletedAction {
                topNav?.dismiss(animated: true, completion: {
                    SourceInstance.shared.selectedSourceBlock?(SourceInstance.shared.selectModels)
                })
            } else {
                topNav?.dismiss(animated: true, completion: { [weak topNav] in
                    SourceInstance.shared.cancelBlock?()
                })
            }
        }
        
        navView.nextBlock = { [weak self] in
            guard let self = self else { return }
            self.showEdit()
        }
        
        filterListView.selectedAlbumBlock = { [weak self] model in
            guard let self = self else { return }
            self.currentAlbum = model
            self.reloadNavView()
            self.albumVC.currentAlbum = self.currentAlbum
        }
        
        albumVC.authNoneStateBlock = { [weak self] in
            guard let self = self else { return }
            self.navView.setNextButtonState(enable: false)
            self.navView.hiddeTitleControl()
            // TODO: 没有权限就不展示bubble吧？ 需和产品确认
            self.shouldShowBubble = false
        }
        
        albumVC.sourceBlock = { [weak self] isEmpty in
            guard let self = self else { return }
            self.navView.setNextButtonState(enable: !isEmpty)
            self.shouldShowBubble = isEmpty
            if !isEmpty, SourceInstance.shared.selectModels.count > 13 {
                self.refreshSegmentItem()
            }
        }
        
        if SourceInstance.shared.outSelectedModels.isNotEmpty {
            navView.setNextButtonState(enable: true)
        }
    }
    
    private func createListModel() -> [DuomSegmentTextModel] {
        var normalColor: UIColor = PhotosConfiguration.default().segmentNormalTextColor
        if SourceInstance.shared.selectModels.count >= PhotosConfiguration.default().maxSelectCount {
            normalColor = UIColor(244, 244, 244)
        }
        return PhotosConfiguration.default().segmentItems.map { text in
            let model = DuomSegmentTextModel(text: text, normalColor: normalColor, selectColor: PhotosConfiguration.default().segmentSelectedTextColor, font: .appFont(ofSize: 16, fontType: .TT_DemiBold))
            if PhotosConfiguration.default().selectSingleImg {
                // margin:85 itemSpace:16
                model.contentWidth = (UIScreen.screenWidth - 85 * 2 - 16) / 2
            } else {
                model.contentWidth = UIScreen.screenWidth / CGFloat(PhotosConfiguration.default().segmentItems.count)
            }
            return model
        }
    }
    
    // 获取相册列表
    private func getAlbumList(completion: (() -> Void)? = nil)  {
        AssetManager.fetchPhotoAlbumList(ascending: PhotosConfiguration.default().sortAscending,
                                         allowSelectImage: PhotosConfiguration.default().allowSelectImage,
                                         allowSelectVideo: PhotosConfiguration.default().allowSelectVideo) { [weak self] albumList in
            guard let self = self else { return }
            self.albumList.removeAll()
            self.albumList.append(contentsOf: albumList)
            // 默认展示第一个相册
            if let listmodel = self.albumList.first {
                self.currentAlbum = listmodel
            }
            
            DispatchQueue.main.async {
                completion?()
                self.reloadNavView()
                self.reloadFilterListView()
                self.albumVC.currentAlbum = self.currentAlbum
            }
        }
    }
    
    // 刷新导航view
    private func reloadNavView()  {
        navView.title = self.currentAlbum?.title ?? ""
        navView.reset()
    }
    
    // 刷新相册列表
    private func reloadFilterListView()  {
        filterListView.dataSource = self.albumList
        filterListView.snp.updateConstraints { make in
            make.height.equalTo(min(AlbumFilterListView.rowH * CGFloat(self.albumList.count), UIScreen.screenHeight - PhotosConfiguration.default().navHeight))
        }
        filterListView.layoutIfNeeded()
    }

    // 进入编辑页
    private func showEdit()  {
        guard !PhotosConfiguration.default().selectSingleImg else {
            showClipVC()
            return
        }
        if SourceInstance.shared.selectModels.first?.mediaType == .video {
            guard let model = SourceInstance.shared.selectModels.first, let asset = model.asset else {
                toast(CopyWitterInstance.default().AssetNil)
                return
            }
            _ = AssetManager.fetchAvAsset(forVideo: asset, completion: { [weak self] avAsset, _ in
                guard let self = self else { return }
                if let avAsset = avAsset {
                    let editVideoVC = ClipVideoViewController(avAsset: avAsset)
                    UIApplication.topViewController()?.navigationController?.show(editVideoVC, sender: nil)
                } else {
                    toast(CopyWitterInstance.default().AssetNil)
                }
            })
        } else {
            let editVC = EditorController()
            editVC.isPushed = true
            editVC.deleteAllItemBlock = { [weak self] in
                SourceInstance.shared.selectModels.removeAll()
                self?.hasDeletedAction = true
                self?.albumVC.refreshCellIndexAndMaskView()
                self?.navView.setNextButtonState(enable: false)
            }
            UIApplication.topViewController()?.navigationController?.pushViewController(editVC, animated: true)
        }
    }
    
    // 单选模式， 直接进入裁剪页
    private func showClipVC()  {
        guard let model = SourceInstance.shared.selectModels.first,
              let imgData = model.imageOriginalData,
              let toImg = UIImage(data: imgData) else {
            return
        }
        let clipVC = ClipOneImageViewController(image: toImg)
        UIApplication.topViewController()?.navigationController?.pushViewController(clipVC, animated: true)
    }
    
    deinit {
        videoVC.stopSession()
        cameraVC.stopSession()
    }
}

// MARK: DuomSegmentViewDelegate & DuomSegmentViewDataSource
extension ImagePickerMainController: DuomSegmentViewDelegate, DuomSegmentViewDataSource {
    public func segmentView(_ segmentView: DuomSegmentView, didSelectedAtIndex index: Int, isSame: Bool) {
        if SourceInstance.shared.selectModels.count >= PhotosConfiguration.default().maxSelectCount, index > 0 {
            return
        }
        if !isSame {
            containerView.scroll(toIndex: index)
            segmentIndex = index
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    public func registerCellClass(in segmentView: DuomSegmentView) {
        segmentView.register(cellWithClass: DuomSegmentTextCell.self)
    }
    
    public func segmentItemModels(for segmentView: DuomSegmentView) -> [DuomSegmentBaseItemModel] {
        listModel
    }
    
    public func segmentView(_ segmentView: DuomSegmentView, itemViewAtIndex index: Int) -> DuomSegmentBaseCell {
        let cell = segmentView.dequeueReusableCell(withClass: DuomSegmentTextCell.self, at: index) as! DuomSegmentTextCell
        return cell
    }
    
    func refreshSegmentItem()  {
        segmentView.forbidEnable = SourceInstance.shared.selectModels.count >= PhotosConfiguration.default().maxSelectCount
        let visableIndexPaths = segmentView.collectionView.indexPathsForVisibleItems
        var normalColor = PhotosConfiguration.default().segmentNormalTextColor
        if SourceInstance.shared.selectModels.count >= PhotosConfiguration.default().maxSelectCount {
            normalColor = UIColor(244, 244, 244)
        }
        
        visableIndexPaths.forEach { indexPath in
            guard let cell = segmentView.collectionView.cellForItem(at: indexPath) as? DuomSegmentTextCell, indexPath.row > 0 else {
                return
            }
            cell.textLabel.textColor = normalColor
        }
        
    }
}

// MARK: DuomPagingContainerViewDelegate & DuomPagingContainerViewDataSource
extension ImagePickerMainController: DuomPagingContainerViewDelegate, DuomPagingContainerViewDataSource {
    public func containerView(_ containerView: DuomPagingContainerView, itemAt index: Int) -> DuomPagingContainerItem? {
        var item: DuomPagingContainerItem?
        if index < pageVCContainer.count {
            item = pageVCContainer[index]
        }
        return item
    }
    
    public func numberOfItems(in containerView: DuomPagingContainerView) -> Int {
        return listModel.count
    }
    
    public func containerViewDidScroll(containerView: DuomPagingContainerView) {
        segmentView.scroll(by: containerView.scrollView)
    }
    
    public func containerView(_ containerView: DuomPagingContainerView, from fromIndex: Int, to toIndex: Int, percent: CGFloat) {
        if PhotosConfiguration.default().selectSingleImg {
            if toIndex == 0 {
                scrollToalbum()
                cameraVC.isShowing = false
                cameraVC.stopSession()
            } else if toIndex == 1 {
                cameraVC.isShowing = true
                cameraVC.startSession()
                scrollToVideoOrCam()
                filterListView.hide()
                reloadNavView()
            }
        } else {
            if PhotosConfiguration.default().selectType == .image {
                if toIndex == 0 {
                    scrollToalbum()
                    videoVC.isShowing = false
                    videoVC.stopSession()
                    cameraVC.isShowing = false
                    cameraVC.stopSession()
                } else {
                    scrollToVideoOrCam()
                    videoVC.isShowing = false
                    videoVC.stopSession()
                    cameraVC.isShowing = true
                    cameraVC.startSession()
                    filterListView.hide()
                    reloadNavView()
                }
            } else {
                if toIndex == 1 {
                    scrollToVideoOrCam()
                    videoVC.isShowing = true
                    videoVC.startSession()
                    cameraVC.isShowing = false
                    cameraVC.stopSession()
                    gifVC.isShowing = false
                    filterListView.hide()
                    reloadNavView()
                } else if toIndex == 2 {
                    scrollToVideoOrCam()
                    videoVC.isShowing = false
                    videoVC.stopSession()
                    cameraVC.isShowing = true
                    cameraVC.startSession()
                    gifVC.isShowing = false
                    filterListView.hide()
                    reloadNavView()
                } else if toIndex == 0 {
                    scrollToalbum()
                    videoVC.isShowing = false
                    videoVC.stopSession()
                    cameraVC.isShowing = false
                    cameraVC.stopSession()
                    gifVC.isShowing = false
                } else if toIndex == 3 {
                    scrollToGif()
                    videoVC.isShowing = false
                    videoVC.stopSession()
                    cameraVC.isShowing = false
                    cameraVC.stopSession()
                    gifVC.isShowing = true
                    filterListView.hide()
                    reloadNavView()
                }
            }
            containerView.addSubView(at: toIndex)
        }
    }
    
    
    func scrollToVideoOrCam()  {
        hideBubble(true)
        navView.isHidden = true
        containerView.snp.remakeConstraints { make in
            make.top.equalTo(self.view)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(segmentContainerView.snp.top)
        }
        self.view.layoutIfNeeded()
    }
    
    func scrollToalbum() {
        navView.isHidden = false
        containerView.snp.remakeConstraints { make in
            make.top.equalTo(navView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(segmentContainerView.snp.top)
        }
        self.view.layoutIfNeeded()
        hideBubble(false)
    }
    
    
    func scrollToGif() {
        dismissBubble()
        navView.isHidden = true
        containerView.snp.remakeConstraints { make in
            make.top.equalTo(self.view)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(segmentContainerView.snp.top)
        }
        self.view.layoutIfNeeded()
    }
}

// MARK: Bubble
extension ImagePickerMainController {
    func showBubble()  {
        guard PhotosConfiguration.default().showGif else {
            return
        }
        guard !UserDefaults.standard.bool(forKey: PhotosConfiguration.default().BubbleKey) else {
            return
        }
        guard !PhotosConfiguration.default().selectSingleImg else {
            return
        }

        delay(2) {
            self.bubbleView = BubbleView()
            guard self.shouldShowBubble else { return }
            self.view.addSubview(self.bubbleView!)
            self.bubbleView?.snp.makeConstraints { make in
                make.bottom.equalTo(self.segmentContainerView.snp.top).offset(-10)
                make.trailing.equalTo(-16)
                make.width.lessThanOrEqualTo(343.DScale)
            }
            self.view.bringSubviewToFront(self.bubbleView!)
            self.bubbleView?.isShowing = true
            self.bubbleView?.tapBlock = { [weak self] in
                guard let self = self else { return }
                let gifIndex = self.pageVCContainer.count - 1
                self.containerView.scroll(toIndex: gifIndex)
                self.segmentIndex = gifIndex
                self.segmentView.scroll(by: self.containerView.scrollView)
            }
            self.bubbleView?.isHidden = self.segmentIndex != 0
            
            self.view.bringSubviewToFront(self.filterListView)
            self.view.bringSubviewToFront(self.navView)
        }
    }
    
     func dismissBubble()  {
        guard let _ = bubbleView else {
            return
        }
        bubbleView?.dismiss()
        bubbleView = nil
        shouldShowBubble = false
    }
    
     func hideBubble(_ hidden: Bool)  {
        guard let _ = bubbleView else {
            return
        }
        bubbleView?.isHidden = hidden
    }
}
