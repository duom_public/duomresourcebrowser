//
//  PhotoNavgationController.swift
//  ResourceBrowser
//
//  Created by kuroky on 2022/12/2.
//

import Foundation
import Photos
import DuomBase

public class PhotoNavgationController: UINavigationController {
    
    deinit {
        Logger.log(message: "PhotoNavgationController deinit", level: .info)
    }
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        navigationBar.barStyle = .black
        navigationBar.isTranslucent = true
        isNavigationBarHidden = true
        modalPresentationStyle = .fullScreen
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
    }
}

